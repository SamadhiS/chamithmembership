﻿ using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models.Comman.User;
 using Inx.Data.Base.Utility;
 using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Attributes;
namespace Inx.Module.Contacts.Data.Entity.User
{
    [Table("WorkingInfo", Schema = Constraints.Schema)]
    public class WorkingInfo : AuditableEntity, IWorkInfo
    {
        [NumberNotZero] public int PersonId { get; set; }
        [Required, StringLength(DbConstraints.NameLength)] public string Designation { get; set; }
        public int? Country { get; set; } = 0;
        [Required, StringLength(DbConstraints.NameLength)] public string Company { get; set; }
        [StringLength(DbConstraints.AddressLength)] public string Address { get; set; }
        [StringLength(DbConstraints.NameLength)] public string Phone { get; set; }
        [StringLength(DbConstraints.PhoneLength)]
        public string Telephone { get; set; }
        [NotMapped, Obsolete] public new string Name { get; set; }
        [ForeignKey("PersonId")] public PersonInfo PersonInfo { get; set; }
    }
}
