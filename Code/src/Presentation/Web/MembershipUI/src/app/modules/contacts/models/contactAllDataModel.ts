import { PersonalModel } from './personalModel';
import { ContactModel } from './contactModel';
import { WorkModel } from './workModel';

export class ContactAllDataModel {
    personInfo: PersonalModel;
    contactInfo: ContactModel;
    workingInfo: WorkModel;
    id:number; 
}