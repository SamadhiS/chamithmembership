import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable()
export class LookupService extends BaseService {

  private apiEndPoint = `${this.baseApiEndPoint}contacts/v1/lookups/`;
  private categoryEndPoint = `${this.baseApiEndPoint}contacts/v1/contactcategories/keyvalue`
  private memberGroupEndPoint = `${this.baseApiEndPoint}membership/v1/groups/keyvalue`
  private templatePoint = `${this.baseApiEndPoint}messenger/v1/emailtemplate/keyvalue`

  constructor(private http: HttpClient) {
    super();
  }

  getTenantInfo(){
    return this.http.get(`${this.baseApiEndPoint}identity/v1/tenantinfo`,this.httpOptions)
    .map((response) => response)
    .catch(this.errorHandler)
  }
  
  getContactTypes() {
    return this.http.get(`${this.categoryEndPoint}`).map(res => res).catch(this.errorHandler)
  }

  getMartialStatus() {
    return this.http.get(`${this.apiEndPoint}martialstatus`).map(res => res).catch(this.errorHandler)
  }

  getCountries() {
    return this.http.get(`${this.apiEndPoint}countries`).map(res => res).catch(this.errorHandler)
  }

  getTemplates(){
    return this.http.get(`${this.templatePoint}`).map(res => res).catch(this.errorHandler)
  }

  getMembershipGroups(){
    return this.http.get(`${this.memberGroupEndPoint}`).map(res => res).catch(this.errorHandler)
  }

  getLanguages(){
    return this.http.get(`${this.apiEndPoint}languages`).map(res => res).catch(this.errorHandler)
  }

  getCurrency(){
    return this.http.get(`${this.apiEndPoint}currency`).map(res => res).catch(this.errorHandler)
  }

}
