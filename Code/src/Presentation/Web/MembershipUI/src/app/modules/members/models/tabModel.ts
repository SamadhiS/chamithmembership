import { FormGroup } from "@angular/forms";

export class TabModel {
    displayName: string;
    elements: any;
    form: FormGroup;
    tabCount: number;
    active: boolean;
    disabled: boolean;
    isFormSubmitted: boolean;
}