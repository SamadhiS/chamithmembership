import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'membership-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {

  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @Output() delete: EventEmitter<any> = new EventEmitter();
  @Input() message: string;
  deleteItem: any;
  constructor() { }

  ngOnInit() {
    // set default message
    if (!this.message)
      this.message = 'Are you sure want to delete?';
  }

  public showDeleteModal(item): void {
    this.deleteItem = item;
    this.deleteModal.show();
  }

  public hideDeleteModal(): void {
    this.deleteModal.hide();
  }

  confirm(): void {
    this.delete.emit(this.deleteItem);
    this.deleteModal.hide();
  }

  reject() {
    this.deleteModal.hide();
  }

}
