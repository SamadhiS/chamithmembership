﻿using Abp.AutoMapper;
using Abp.Configuration.Startup;
using Inx.Module.Membership.Core.Bo.Dashboard;
using Inx.Module.Membership.Core.Bo.Member;
using Inx.Module.Membership.Core.Bo.Member.Master;
using Inx.Module.Membership.Core.Bo.MemberGroup;
using Inx.Module.Membership.Core.Bo.Settings;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Data.Entity.MemberGroup;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Module.Membership.Data.Entity.Subscription;
using Inx.Module.Membership.Service.Models.Groups;
using Inx.Module.Membership.Service.Models.Subscription;
using Inx.Module.Membership.Service.Models.Members;
using Inx.Module.Membership.Service.Models.Utility;
using Inx.Utility.Extentions;
using Inx.Module.Membership.Service.Models.Members.Master;
using Inx.Module.Membership.Service.Models.CustomFieldDefinition;
using Inx.Module.Membership.Data.Entity.Settings;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;

namespace Inx.Module.Membership.Service.Utility.Config
{
    public class Automapper
    {
        public static void Register(IAbpStartupConfiguration Configuration)
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {

                cfg.CreateMap<Models.Members.MemberInfoViewModel, PersonalInfoBo>().ReverseMap();



                cfg.CreateMap<MembershipGroupViewModel, GroupBo>().ReverseMap();
                cfg.CreateMap<GroupBo, Group>().ReverseMap();

                cfg.CreateMap<Subscription, SubscriptionBo>().ReverseMap();
                cfg.CreateMap<SubscriptionViewModel, SubscriptionBo>().ReverseMap();

                cfg.CreateMap<Subscription, SubscriptionRenewalBo>().ReverseMap();
                cfg.CreateMap<SubscriptionRenewalViewModel, SubscriptionRenewalBo>().ReverseMap();


                cfg.CreateMap<SubscriptionType, SubscriptionTypeBo>().ReverseMap();
                cfg.CreateMap<SubscriptionTypeViewModel, SubscriptionTypeBo>().ReverseMap();

                cfg.CreateMap<MemberFile, MemberFileBo>().ReverseMap();
                cfg.CreateMap<MemberFileViewModel, MemberFileBo>().ReverseMap();

                cfg.CreateMap<TableTemplateViewModel, TableTemplateBo>()
                .ForMember(des => des.Keys, (AutoMapper.IMemberConfigurationExpression<TableTemplateViewModel, TableTemplateBo, string> opt) => opt.MapFrom(src => src.Keys.ToJsonString()))
                .ReverseMap();

                cfg.CreateMap<MemberGroupBo, MemberGroup>().ReverseMap();
                cfg.CreateMap<MemberGroupViewModel, MemberGroupBo>().ReverseMap();

                cfg.CreateMap<DashboardGraphBo, DashboardGraphsViewModel>().ReverseMap();
                cfg.CreateMap<DashboardGraphsViewModel, DashboardGraphBo>().ReverseMap();

                cfg.CreateMap<DashboardGraphBo, MemberGroup>().ReverseMap();
                cfg.CreateMap<MemberGroupViewModel, MemberGroupBo>().ReverseMap();

                cfg.CreateMap<MemberSubscriptionBo, MemberSubscription>().ReverseMap();
                cfg.CreateMap<MemberSubscriptionViewModel, MemberSubscriptionBo>().ReverseMap();

                cfg.CreateMap<Models.Members.Master.MemberInfoViewModel, OrganizationInfoBo>().ReverseMap();
                cfg.CreateMap<OrganizationInfoBo, OrganizationInfo>().ReverseMap();

                cfg.CreateMap<MemberTabDefinitionViewModel, MemberTabDefinitionBo>().ReverseMap();
                cfg.CreateMap<MemberTabDefinitionBo, MemberTabDefinition>().ReverseMap();

                cfg.CreateMap<MemberTabFieldDefinitionViewModel, MemberFieldDefinitionBo>().ReverseMap();
                cfg.CreateMap<MemberFieldDefinitionBo, MemberFieldDefinition>().ReverseMap();

                cfg.CreateMap<MemberTypeTabViewModel, MemberTypeTabBo>().ReverseMap();
                cfg.CreateMap<MemberTypeTabBo, MemberTypeTab>().ReverseMap();

                cfg.CreateMap<MemberTabDefinitionBo, MemberTabDefinition>().ReverseMap();
                cfg.CreateMap<MemberFieldDefinitionBo, MemberFieldDefinition>().ReverseMap();
                cfg.CreateMap<TabsFieldsViewResult, MemberFieldDefinition>().ReverseMap();
                cfg.CreateMap<MemberTabFieldBo, MemberTabField>().ReverseMap();
                cfg.CreateMap<MemberTypeTabBo, MemberTypeTab>().ReverseMap();
                cfg.CreateMap<MemberFieldKeyValueViewModel, MemberFieldKeyValueBo>().ReverseMap();
                cfg.CreateMap<MemberToGroupsViewModel, MemberToGroupsBo>().ReverseMap();
            });
        }
    }
}
