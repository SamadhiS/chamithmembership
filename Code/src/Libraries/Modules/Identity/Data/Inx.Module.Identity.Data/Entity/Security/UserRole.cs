﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Attributes;

namespace Inx.Module.Identity.Data.Entity.Security
{
    [Table("UserRoles", Schema = Constraints.Schema)]
    public class UserRole : AuditableEntity
    {
        [NumberNotZero]
        public int UserId { get; set; }
        [NumberNotZero]
        public int RoleId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
        [NotMapped,Obsolete]
        public new string Name { get; set; }

    }
}
