﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;

namespace Membership.StepDefinition.Category
{
    [Binding]
    public sealed class AddCategorySteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();

        [Given(@"Load Membership Login page")]
        public void GivenLoadMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"Enter username & password ""(.*)"" and ""(.*)""")]
        public void GivenEnterUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"Click on Sign in button")]
        public void GivenClickOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"Navigated to Dashboard")]
        public void ThenNavigatedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"Click on Settings")]
        public void ThenClickOnSettings()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(6000);
        }

        [Then(@"i have entered (.*) as Category")]
        public void ThenIHaveEnteredManageryyyAsCategory(string p0)
        {
            categoryPO.Category(ngWebDriver).SendKeys(p0);
        }

             

        [Then(@"Click on Add button")]
        public void ThenClickOnAddButton()
        {
            categoryPO.Btn_AddCategory_Click(ngWebDriver);
            
        }

       
        [Then(@"Click on Signout button")]
        public void ThenClickOnSignoutButton()
        {
           
            Thread.Sleep(2000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //Negative

        [Given(@"Load Membership")]
        public void GivenLoadMembership()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I Enter username & password ""(.*)"" and ""(.*)""")]
        public void GivenIEnterUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"Clicked on Sign in button")]
        public void GivenClickedOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Navigat to Dashboard")]
        public void ThenNavigatToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"Clicked on Settings")]
        public void ThenClickedOnSettings()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I Enter (.*) as Category")]
        public void ThenIEnterAsCategory(string p0)
        {
            categoryPO.Category(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Clicked on Add button")]
        public void ThenClickedOnAddButton()
        {
            categoryPO.Btn_AddCategory_Click(ngWebDriver);
        }
        [Then(@"Clicked on Cancel button")]
        public void ThenClickedOnCancelButton()
        {
            categoryPO.Btn_CancelCategory_Click(ngWebDriver);
        }

        [Then(@"Clicked on Signout button")]
        public void ThenClickedOnSignoutButton()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
