﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberFieldDefinitionBo : BaseBo
    {
        public Guid Key { get; set; }
        public int DataType { get; set; }
        public string DisplayName { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public int Order { get; set; }
    }
}
