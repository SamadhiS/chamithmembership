import { RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LevelService extends BaseService {

  private apiEndPointMemberLevel = `${this.baseApiEndPoint}membership/v1/subscription`;
  private apiEndPointMemberLevelRenewal = `${this.baseApiEndPoint}membership/v1/subscriptionRenewal`;
  private apiEndPointMemberLevelType = `${this.baseApiEndPoint}membership/v1/subscriptiontype`;
  private apiEndPointMemberSubscription = `${this.baseApiEndPoint}membership/v1`;

  constructor(private http: HttpClient) {
    super();
  }

  archiveMemberLevel(memberLevelId) {
    return this.http.delete(`${this.apiEndPointMemberLevel}/${memberLevelId}`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  addMemberLevel(memberLevel) {
    return this.http.post(`${this.apiEndPointMemberLevel}`, memberLevel, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  updateMemberLevel(memberLevel) {
    return this.http.put(`${this.apiEndPointMemberLevelType}`, memberLevel, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  updateMemberLevelRenewal(memberLevelRenewal) {
    return this.http.put(`${this.apiEndPointMemberLevelRenewal}`, memberLevelRenewal, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  updateMemberLevelCapacity(memberLevel) {
    return this.http.put(`${this.apiEndPointMemberLevel}`, memberLevel, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  getMemberLevelById(id) {
    return this.http.get(`${this.apiEndPointMemberLevel}/${id}`).map(res => res).catch(this.errorHandler)
  }

  getAllMemberLevel(skip, take) {
    return this.http.get(`${this.apiEndPointMemberLevel}?skip=` + skip + '&take=' + take,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getMemberLevelKeyValues() {
    return this.http.get(`${this.apiEndPointMemberLevel}/keyvalue`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  changeMemberStatus(payment) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/members/subscriptions/${payment.id}/changestate`,
      payment, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

}
