import { BlockUiService } from '../../core/services/block-ui.service';
import { BlockUiComponent } from '../../shared/block-ui/block-ui.component';
import { Subject } from 'rxjs/Subject';
import { MemberSearchModel } from '../models/memberSearchModel';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { MembersService } from '../../core/services/members.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'membership-member-restore',
  templateUrl: './member-restore.component.html',
  styleUrls: ['./member-restore.component.scss']
})
export class MemberRestoreComponent implements OnInit {

  archivedList = [];
  noRecords: boolean;
  searchTerm$ = new Subject<string>();
  memberSearch = new MemberSearchModel();
  skip = 0;
  take = 0;
  memberId = 0;
  MemberCount = 0;
  orderby = "";
  isBlocked = false;

  constructor(private fb: FormBuilder, private memberService: MembersService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastsManager, vcr: ViewContainerRef,
    private blockUiService: BlockUiService) {
      this.toastr.setRootViewContainerRef(vcr);
      
          this.searchTerm$.debounceTime(300)
            .distinctUntilChanged().subscribe(data => {
              if (data != "" && data) {
                this.memberSearch.search = data;
              }
              else {
                this.memberSearch.search = '';
              }
              this.getArchiveMember();
            })
        }

  ngOnInit() {
    this.noRecords = false;
    this.getArchiveMember();
  }

  getArchiveMember(){
    this.isBlocked = true;
    this.memberService.getArchiveMembers(this.skip, this.take,JSON.stringify(this.memberSearch),this.orderby)
      .subscribe(allArchivemember => {
        this.archivedList = allArchivemember.result.items
        this.MemberCount = allArchivemember.result.totalRecodeCount
        if (allArchivemember.result.totalRecodeCount == 0) {
          this.noRecords = true;
          this.isBlocked = false;
        }
        this.isBlocked = false;
      },
      error => {
        this.toastr.error('Error in Loading Member Level', 'Oops');
        this.isBlocked = false;
      });
      
  }

  restoreMemberId(Id){
    this.memberId = Id;
  }

  restoreMember(){
    this.isBlocked = true;
    this.memberService.restoreMember(this.memberId)
    .subscribe(allCategories => {
      this.toastr.success('Restored Successfully!', 'Success');
      this.getArchiveMember();
      this.isBlocked = false;
    },
      error => {
        this.toastr.error('Error in Restoring Member', 'Oops');
        this.isBlocked = false;
      });
     
  }

}
