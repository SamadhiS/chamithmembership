﻿using Inx.Module.Identity.Service.Models.Security.ChangePassword;
using Inx.Module.Identity.Service.Models.Security.Login;
using Inx.Module.Identity.Service.Models.Security.Register;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inx.Module.Identity.Test.ModelValidation
{
    public class SecurityModelValidation
    {
        [DataTestMethod]
        [DataRow("123","1234")] // pass 
        [DataRow("", "")] // fail
        public void ChangePasswordViewModel(string currentPassword,string newPassword)
        {
            var model = new ChangePasswordViewModel()
            {
                CurrentPassword = currentPassword,
                NewPassword = newPassword
            };
        }

        [DataTestMethod]
        [DataRow("123", "1234")]
        [DataRow("", "")]
        public void AuthenticateViewModel(string email, string password)
        {
            var model = new AuthenticateViewModel()
            {
                Email = email,
                Password = password
            };
        }
        [DataTestMethod]
        [DataRow("123", "1234")]
        [DataRow("", "")]
        public void RegisterViewModel(string email, string password)
        {
            var model = new RegisterViewModel()
            {
                Email = email,
                Password = password
            };
        }
    }
}
