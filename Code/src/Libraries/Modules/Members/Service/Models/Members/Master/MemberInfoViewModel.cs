﻿using System.Collections.Generic;
using Inx.Service.Base.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Service.Models.Members.Master
{
    public class MemberInfoViewModel : BaseViewModel
    {
        public int MemberId { get; set; }
        public Enums.MemberType MemberType { get; set; }
        public List<MemberFieldKeyValueViewModel> MemberFieldKeyValue { get; set; }
    }
}
