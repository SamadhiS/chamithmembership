﻿using Inx.Utility.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inx.Module.Contacts.Core.Bo.User
{
    public class ContactsViewResult
    {
        //person
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProfileImage { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime dob { get; set; }
        public string Identity { get; set; }
        public string MartialStatus { get; set; }
        public int Age { get; set; }
        //public int MemberSince { get; set; }

        //contact
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Telephone { get; set; }
        public string Zip { get; set; }

        //working info
        public string Company { get; set; }
        public string Designation { get; set; }
        public int? Country { get; set; }
        public string WorkAddress { get; set; }
        public string WorkPhone { get; set; }
        public string WorkTelephone { get; set; }

        public string ContactCategory { get; set; }

        public List<string> GetProperties(List<string> exclude)
        {
            var foo = this;
            var lst = foo.GetType().GetProperties().Select(prop => prop.Name).ToList();
            var result = new List<string>();
            foreach (var item in lst)
            {
                if (!exclude.Contains(item.ToLower()))
                {
                    result.Add(item.ChangeFirstLetterCase());
                }
            }
            return result;
        }
    }
}
