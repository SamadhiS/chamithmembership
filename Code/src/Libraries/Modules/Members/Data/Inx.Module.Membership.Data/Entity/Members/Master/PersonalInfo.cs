﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Membership.Data.Entity.Members.Master
{
    [Table("PersonalInfo", Schema = Constraints.Schema)]
    public class PersonalInfo : AuditableEntity
    {
        [Key]
        public new int Id { get; set; }
        [ForeignKey("Id")]
        public Member Member { get; set; }
        #region not map
        [NotMapped, Obsolete]
        public new string Name { get; set; }
        #endregion

    }
}
