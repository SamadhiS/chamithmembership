﻿
Feature: AddContactFeature
	As a Admin, I should be able to add contact information

@mytag
Scenario Outline: Add valid Contact Information
	Given The Application Page Loads
	Then User enters "membership.inexis@gmail.com" and "Admin@123"
	And Clicks on Submit 
	And is navigated to Homepage 
	And User navigates to Contacts Section
	And Clicks on Add New Contact
	And user enters First name as <_FN>
	And user enters Last name as <_LN>
	And user selects profile Image
	And user enters DOB  as <_DOB>
	And user selects martial Status
	And user enters NIC as <_NIC>
	And Selects Category 
	And Clicks on the NEXT button 
	And Clicks on Contact Information 
	And user enters Country as <_Country>
	And user enters City as <_City>
	And user enters Postal as <_Postal>
	And user enters Address as <_Address>
	And user enters Mobile as <_Mob>
	And user enters Telephone as <_Tele>
	And user enters Email as <_Email>
	And Clicks on NEXT button 
	And Clicks on Contact Information 
	And user enters Designation as <_desig>
	And user enters Company as <_Cmpy>
	And user enters Company Address as <_CmpAdd>
	And user select Company country
	And user enters Office Telephone as <_TeleAdd>
	And user enters Office Mobile as <_TeleMob>
	And Click on Save
	And Log Out 
Examples: 
| _FN   | _LN		| _DOB      | _NIC			| _Country	| _City			| _Postal	| _Address        | _Mob			| _Tele     | _Email            | _desig         | _Cmpy				| _CmpAdd                      | _TeleAdd  | _TeleMob   |
| Onela | Racheal	| 1990-2-4	| 923314329V	| Kuwait     | Ras-Salmiya	| 2020   | 45/2  Mubarak street | 423311211		| 444356434 | perrri@hotmail.com | Finance Intern | High Octane Fitness | 68th Postman Suite, New York | 082773812 | 026673829	|

#
#Scenario Outline: Enter Empty Contact Information
#	Given The Application Page Loads
#	Then User enters "membership.inexis@gmail.com" and "Admin@123"
#	And Clicks on Submit 
#	And is navigated to Homepage 
#	And User navigates to Contacts Section
#	And Clicks on Add New Contact
#	And user enters First name as <_FN>
#	And user enters Last name as <_LN>
#	And user selects profile Image
#	And user enters DOB  as <_DOB>
#	And user selects martial Status
#	And user enters NIC as <_NIC>
#	And Selects Category 
#	And Clicks on the NEXT button 
#	Then Error messages are visible on relavant fields 
#	And Log Out
#	Examples: 
#| _FN  | _LN     | _DOB      | _NIC       |
#| Mary |		 | 1998-4-13 | 926723229V |
#
#Scenario Outline: Enter Invalid Contact Information
#	Given The Application Page Loads
#	Then User enters "membership.inexis@gmail.com" and "Admin@123"
#	And Clicks on Submit 
#	And is navigated to Homepage 
#	And User navigates to Contacts Section
#	And Clicks on Add New Contact
#	And user enters First name as <_FN>
#	And user enters Last name as <_LN>
#	And user selects profile Image
#	And user enters DOB  as <_DOB>
#	And user selects their martial Status
#	And user enters NIC as <_NIC>
#	And Selects a Category 
#	And Clicks on the NEXT button 
#	And Clicks on Contact Information 
#	And user enters Country as <_Country>
#	And user enters City as <_City>
#	And user enters Postal as <_Postal>
#	And user enters Address as <_Address>
#	And user enters Mobile as <_Mob>
#	And user enters Telephone as <_Tele>
#	And user enters Email as <_Email>
#	And Clicks on NEXT button
#	Then Error messages are visible on relavant fields 
#	And Log Out
#	Examples: 
#| _FN      | _LN      | _DOB      | _NIC       | _Country | _City   | _Postal | _Address          | _Mob     | _Tele     | _Email                |
#| Liallian | Salvator | 1998-4-13 | 926723229V | Kuwait   | Jabriya | 9090    | 22, Bayan Streeet | 996312230 | 2572432130 | salvatory@hotmail.com | 