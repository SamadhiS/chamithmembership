import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ContactCategoryService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }

  addCategory(categorieName){
    return this.http.post(`${this.baseApiEndPoint}contacts/v1/contactcategories`,
    (categorieName), this.httpOptions)
    .map(res => res).catch(this.errorHandler)
  }

  getAllCategories(skip, take): any
  {
    return this.http.get(`${this.baseApiEndPoint}contacts/v1/contactcategories?skip=`+ skip + '&take=' + take,
    this.httpOptions)
    .map((response) => response)
    .catch(this.errorHandler)
  }

  deleteCategory(categoryId){
    return this.http.delete(`${this.baseApiEndPoint}contacts/v1/contactcategories/` + categoryId,
    this.httpOptions)
    .map((response) => response)
    .catch(this.errorHandler)
  }
  updateCategory(categorieName){
    return this.http.put(`${this.baseApiEndPoint}contacts/v1/contactcategories`,
      (categorieName),this.httpOptions)
      .map(res=>res).catch(this.errorHandler)
  
  }

}
