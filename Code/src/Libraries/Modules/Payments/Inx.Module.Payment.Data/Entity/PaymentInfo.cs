﻿using Inx.Module.Identity.Data.Entity.Application;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Payment.Data.Entity
{
    [Table("PaymentInfoes", Schema = Constraints.Schema)]
    public class PaymentInfo : AuditableEntity
    {
        public DateTimeOffset PaymentDate { get; set; }
        public PaymentTypes PaymentType { get; set; }
        public decimal Amount { get; set; }
        public int PaymentMasterId { get; set; }
        [ForeignKey("PaymentMasterId")]
        public PaymentMaster PaymentMaster { get; set; }

        public PaymentInfo()
        {
            PaymentDate = DateTime.UtcNow;
        }
    }
}
