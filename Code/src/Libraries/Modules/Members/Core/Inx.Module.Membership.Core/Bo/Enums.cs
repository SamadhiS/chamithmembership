﻿
namespace Inx.Module.Membership.Core.Bo
{
    public class Enums : Inx.Utility.Utility.Enums
    {
        //public enum MembershipStatus : int
        //{
        //    Active = 1,
        //    Inactive = 2,
        //    Pending = 3,
        //    Terminated = 4
        //}

        public enum CacheKey : int
        {
            SubscriptionType = 1,
            Subscription = 2,
            MemberViewTableTemplate = 3,
            Members = 4
        }

        public enum TableTemplate
        {
            None = 0,
            MemberView = 1
        }

        public enum DateRangeFilter
        {
            ThreeMonths =1,
            SixMonths = 2,
            OneYear = 3,
            Week = 4
        }

        public enum TimelineTemplateType
        {
            None = 0,
            MembershipRenewalEmailReminder = 1,
            MembershipExpire = 2,
            MembershipPayment = 3,
            ChangeMembership = 4,
            MembershipCreated = 5
        }
    }
}
