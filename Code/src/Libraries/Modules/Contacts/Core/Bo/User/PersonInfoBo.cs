﻿using System;
using Inx.Utility.Models;
namespace Inx.Module.Contacts.Core.Bo.User
{
    public class PersonInfoBo : BaseBo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DOB { get; set; }
        public Utility.Utility.Enums.MartialStatus MartialStatus { get; set; }
        public string Identity { get; set; }
        public string ProfileImage { get; set; }
        public int ContactCategoryId { get; set; }
        [Obsolete]
        public new string Name { get; set; }
    }
}
