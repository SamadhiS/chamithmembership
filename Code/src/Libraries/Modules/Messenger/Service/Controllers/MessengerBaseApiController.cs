﻿using Inx.Service.Base.Controllers;

namespace Inx.Module.Messenger.Service.Controllers
{
    public class MessengerBaseApiController: BaseApiController
    {
        public MessengerBaseApiController(IBaseInjector baseinject) : base(baseinject)
        {
        }
    }
}
