﻿using Inx.Data.Base.Utility;
namespace Inx.Module.Contacts.Data
{
    public class Constraints: DbConstraints
    {
        public new const string Schema = "Inexis.Contacts";
        public const int IdentityLength = 50;
    }
}
