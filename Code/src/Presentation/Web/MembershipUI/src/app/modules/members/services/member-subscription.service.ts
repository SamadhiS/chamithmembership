import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MemberSubscriptionService extends BaseService {

  private apiEndPointMemberSubscription = `${this.baseApiEndPoint}membership/v1`;

  constructor(private http: HttpClient) {
    super();
  }

  createSubscription(memberSubscription) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/members/subscriptions`, memberSubscription, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

  getAllMemberSubscription(subscriptionTypeId) {
    return this.http.get(`${this.apiEndPointMemberSubscription}/subscriptions/${subscriptionTypeId}/members`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  updateMemberSubscription(memberSubscription) {
    return this.http.put(`${this.apiEndPointMemberSubscription}/members/${memberSubscription.MemberId}/subscription`,
      (memberSubscription), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  terminateSubscription(terminationModel) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/member/subscription/terminate`,
      (terminationModel), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  getSubscribedMembershipData(memberId) {
    return this.http.get(`${this.apiEndPointMemberSubscription}/member/${memberId}/subscription`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  activateMembership(memberId) {
    return this.http.put(`${this.apiEndPointMemberSubscription}/member/${memberId}/subscription/activate`,
      (memberId), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  renewMembership(memberSubscription) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/member/${memberSubscription.MemberId}/subscription/renew`,
      (memberSubscription), this.httpOptions)
      .map(res => res).catch(this.errorHandler)
  }

  sendRenewalMail(memberId) {
    return this.http.post(`${this.apiEndPointMemberSubscription}/member/${memberId}/subscription/sendrenewalemail`, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

  updatePayment(updatePayment) {
    return this.http.put(`${this.apiEndPointMemberSubscription}/payments`,
      updatePayment, this.httpOptions)
      .map(res => res)
      .catch(this.errorHandler)
  }

}
