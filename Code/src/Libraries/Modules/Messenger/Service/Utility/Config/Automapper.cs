﻿using Abp.AutoMapper;
using Abp.Configuration.Startup;
using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Module.Messenger.Data.Entity;
using Inx.Module.Messenger.Service.Models;

namespace Inx.Module.Messenger.Service.Utility.Config
{
    public class Automapper
    {
        public static void Register(IAbpStartupConfiguration Configuration)
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                cfg.CreateMap<EmailTemplateViewModel, EmailTemplateBo>().ReverseMap();
                cfg.CreateMap<EmailTemplateBo, EmailTemplate>().ReverseMap();
                cfg.CreateMap<EmailSendingInfoViewModel, EmailSendingInfoModel>().ReverseMap();
            });
        }
    }
}
