﻿using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Subscription;
namespace Inx.Module.Membership.Core.Service.Subscription.Interfaces
{
    public interface ISubscriptionTypeService : IBaseService<SubscriptionTypeBo>
    {
    }
}
