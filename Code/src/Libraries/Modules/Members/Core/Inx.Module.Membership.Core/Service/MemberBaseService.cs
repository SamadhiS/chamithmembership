﻿using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo;
using Inx.Utility.Cache.Runtime;
namespace Inx.Module.Membership.Core.Service
{
    public class MemberBaseService:BaseService
    {
        protected IMemoryCacheManager cache { get; set; } = null;
        public MemberBaseService()
        {
        }
        public MemberBaseService(ICoreInjector inject)
        {
            cache = inject.Cache;
        }
        protected async Task RemoveContactCacheAsync(int tenantid)
        {
            await cache.RemoveByStartwith(CacheKeyGen(new[] {tenantid.ToString(), Enums.CacheKey.Members.ToString()}
                .ToList()));
        }
    }
}
