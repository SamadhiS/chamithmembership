import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';

@Injectable()
export class CutomMemberService extends BaseService {

  private memberEndPoint = `${this.baseApiEndPoint}membership/v1/`;
  private memberGroupsEndPoint = `${this.baseApiEndPoint}membership/v1/membergroups/`;
  
  constructor(private http: HttpClient) {
    super();
  }

  createMember(memberData) {
    return this.http.post(`${this.memberEndPoint}members`, memberData, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getById(memberId) {
    return this.http.get(`${this.memberEndPoint}members/${memberId}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  updateMember(memberData) {
    return this.http.put(`${this.memberEndPoint}members`, memberData, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  assignMemberToGroups(assignedGroups) {
    return this.http.post(`${this.memberGroupsEndPoint}assigntogroups`, assignedGroups, this.httpOptions)
    .map((response) => response)
    .catch(this.errorHandler)
  }
  

}
