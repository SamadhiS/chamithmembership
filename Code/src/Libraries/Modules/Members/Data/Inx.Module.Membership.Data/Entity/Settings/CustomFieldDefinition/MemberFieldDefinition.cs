﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition
{
    [Table("MemberFieldDefinition", Schema = Constraints.Schema)]
    public class MemberFieldDefinition : AuditableEntity
    {
        [Required]
        public Guid Key { get; set; }
        public Enums.FieldType DataType { get; set; }
        [StringLength(DbConstraints.NameLength),Required]
        public string DisplayName { get; set; }
        [StringLength(DbConstraints.MaxLength)]
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public int Order { get; set; }
        [NotMapped,Obsolete]
        public new string Name { get; set; }
    }
}
