﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Member;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.MemberSubscription.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Subscription;
using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Data.DbContext;
using Inx.Module.Payment.Data.Entity;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.MemberSubscription
{
    public class MemberSubscriptionService : MemberBaseService, IMemberSubscriptionService
    {
        private readonly IUnitOfWokMembership uowMembership;
        private readonly IUnitOfWorkPayment uowPayment;
        public MemberSubscriptionService(IUnitOfWokMembership _uowMembership, IUnitOfWorkPayment _uowPayment, ICoreInjector inject) : base(inject)
        {
            uowMembership = _uowMembership;
            uowPayment = _uowPayment;
        }

        public async Task<MemberSubscriptionBo> Create(Request<MemberSubscriptionBo> req)
        {
            var subscriptionInfo = await uowMembership.SubscriptionRepository.Read(subs => subs.Id == req.Item.SubscriptionTypeId, req.TenantId);
            var ReNewDate = DateTime.UtcNow;
            ReNewDate = ChangeRenewalDate(subscriptionInfo.MembershipExpired);
            
            var memberSubscription = await uowMembership.MemberSubscriptionRepository.Create(new Request<Data.Entity.Subscription.MemberSubscription>
            {
                Item = new Data.Entity.Subscription.MemberSubscription
                {
                    MemberId = req.Item.MemberId,
                    SubscriptinId = req.Item.SubscriptionTypeId,
                    MembershipStatus = Enums.MembershipStatus.Pending,
                    JoinDate = DateTime.UtcNow,
                    RenewDate = ReNewDate
                },
                TenantId = req.TenantId,
                UserId = req.UserId
            });

            await uowMembership.SaveAsync();
            await RemoveContactCacheAsync(req.TenantId);
            return memberSubscription.MapObject<Data.Entity.Subscription.MemberSubscription, MemberSubscriptionBo>();
        }

        #region memberSubscription
        public async Task ChangeMemberState(Request<MemberState> item)
        {
            try
            {
                var result = await uowMembership.MemberRepository.Read(p => p.Id == item.Item.MemberId, item.TenantId);
                result.RecordState = item.Item.State;
                result.EditedOn = DateTime.UtcNow;
                result.EditedById = item.UserId;
                await uowMembership.SaveAsync();
                await RemoveContactCacheAsync(item.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberSubscriptionBo> ChangeMemberSubscription(Request<MemberSubscriptionBo> item)
        {
            using (var transaction = await uowMembership.Context.Database.BeginTransactionAsync())
            {
                try
                {
                    var subscription =
                        await uowMembership.SubscriptionRepository.Read(subs => subs.Id == item.Item.SubscriptionId,
                            item.TenantId);
                    var member =
                        await uowMembership.MemberRepository.Read(mem => mem.Id == item.Item.MemberId, item.TenantId);

                    var memberSubscription = await uowMembership.MemberSubscriptionRepository.Read(
                        memberSubs => memberSubs.MemberId == item.Item.MemberId && memberSubs.IsRenewed == false,
                        item.TenantId);
                    member.SubscriptionId = item.Item.SubscriptionId;
                    await uowMembership.SaveAsync();

                    memberSubscription.SubscriptinId = item.Item.SubscriptionId;
                    memberSubscription.JoinDate = DateTime.UtcNow;
                    memberSubscription.EditedOn = DateTime.UtcNow;
                    memberSubscription.EditedById = item.UserId;
                    memberSubscription.RenewDate = ChangeRenewalDate(subscription.MembershipExpired);
                    await uowMembership.SaveAsync();
                    transaction.Commit();
                    return memberSubscription
                        .MapObject<Data.Entity.Subscription.MemberSubscription, MemberSubscriptionBo>();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e.HandleException();
                }
            }
        }

        public async Task<PaymentBo> UpdatePayment(Request<PaymentBo> req)
        {
            //TODO : payment master not undastand
            try
            {
                var paymentMaster = await (from payMaster in uowPayment.PaymentMasterRepository.Table
                                           where payMaster.MemberSubscriptionId == req.Item.MemberSubscriptionId
                                           select payMaster).FirstOrDefaultAsync();

                paymentMaster.Name = req.Item.Name;
                paymentMaster.Amount = req.Item.Amount;
                paymentMaster.PaymentState = req.Item.PaymentState;
                paymentMaster.Description = req.Item.Description;
                paymentMaster.TransActionType = req.Item.TransActionType;
                paymentMaster.EditedOn = DateTime.UtcNow;
                paymentMaster.EditedById = req.UserId;
                await uowPayment.SaveAsync();
                if (req.Item.PaymentState == Enums.PaymentState.Paid)
                {
                    req.Item.PaymentMasterId = paymentMaster.Id;
                    (await uowPayment.PaymentInfoRepository.CreateAndSave(req.MapRequestObject<PaymentBo, PaymentInfo>())).MapObject<PaymentInfo, PaymentBo>();
                    var paymentmaster = await uowPayment.PaymentMasterRepository.ReadAsTracking(new Request<int>() { TenantId = req.TenantId, UserId = req.UserId, Item = req.Item.PaymentMasterId });
                    paymentmaster.PaymentState = Enums.PaymentState.Paid;
                    paymentMaster.EditedOn = DateTime.UtcNow;
                    await uowPayment.SaveAsync();
                }
                await ChangeSubscriptionState(req);
                return new PaymentBo
                {
                    PaymentMasterId = paymentMaster.Id,
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task TerminateSubscription(Request<TerminationNoteBo> item)
        {
            try
            {
                var memberSubscription = await uowMembership.MemberSubscriptionRepository.Read(memberSubs => memberSubs.MemberId == item.Item.MemberId && memberSubs.MembershipStatus != Enums.MembershipStatus.Inactive && memberSubs.IsRenewed == false, item.TenantId);
                memberSubscription.MembershipStatus = Enums.MembershipStatus.Terminated;
                memberSubscription.EditedById = item.UserId;
                memberSubscription.EditedOn = DateTime.UtcNow;
                await uowMembership.SaveAsync();

                if (item.Item.Notes == null)
                {
                    await RemoveContactCacheAsync(item.TenantId);
                    return;
                }
                await uowMembership.TerminationNoteRepository.Create(new Request<TerminationNote>
                {
                    Item = new TerminationNote
                    {
                        MemberSubscriptionId = memberSubscription.Id,
                        Notes = item.Item.Notes
                    },
                    TenantId = item.TenantId,
                    UserId = item.UserId
                });
                await uowMembership.SaveAsync();
                await RemoveContactCacheAsync(item.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberSubscriptionBo> ReadMemberSubscription(Request<int> item)
        {
            try
            {
                var payments = await (from payment in uowPayment.PaymentMasterRepository.TableAsNoTracking
                                      select payment).ToListAsync();

                return await (from memberSubscription in uowMembership.MemberSubscriptionRepository.TableAsNoTracking
                              join terminationNote in uowMembership.TerminationNoteRepository.TableAsNoTracking on memberSubscription.Id equals terminationNote.MemberSubscriptionId into tn
                              from terminationNote in tn.DefaultIfEmpty()
                              join payment in payments on memberSubscription.MemberId equals payment.MemberId
                              join subscription in uowMembership.SubscriptionRepository.TableAsNoTracking on memberSubscription.SubscriptinId equals subscription.Id
                              join subscriptionType in uowMembership.SubscriptionTypeRepository.TableAsNoTracking on subscription.Id equals subscriptionType.Id
                              where memberSubscription.TenantId == item.TenantId && memberSubscription.MemberId == item.Item && memberSubscription.IsRenewed == false
                              select new
                              {
                                  memberSubscription.MemberId,
                                  memberSubscription.SubscriptinId,
                                  subscriptionType.Id,
                                  memberSubscription.MembershipStatus,
                                  memberSubscription.RenewDate,
                                  //Enums.payment.PaymentState,
                                  memberSubscription.JoinDate,
                                  subscriptionType.Name,
                                  subscriptionType.Description,
                                  subscription.Price,
                                  subscription.MembershipExpired,
                                  terminationNote.Notes
                              }).AsQueryable().Select(p => new MemberSubscriptionBo
                              {
                                  MemberId = p.MemberId,
                                  SubscriptionId = p.SubscriptinId,
                                  SubscriptionTypeId = p.Id,
                                  RenewDate = p.RenewDate,
                                  MembershipStatus = p.MembershipStatus,
                                  MembershipExpired = p.MembershipExpired,
                                  // PaymentState = p.PaymentState,
                                  JoinDate = p.JoinDate,
                                  Name = p.Name,
                                  Description = p.Description,
                                  Price = p.Price,
                                  TerminationNote = p.Notes

                              }).FirstOrDefaultAsync();
            }

            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task ActivateSubscription(Request<int> item)
        {
            try
            {
                //var payments = await (from payment in uowPayment.PaymentMasterRepository.TableAsNoTracking
                //                      select payment).ToListAsync();

                var memberSubscription = await uowMembership.MemberSubscriptionRepository.Read(memberSubs => memberSubs.MemberId == item.Item && memberSubs.MembershipStatus == Enums.MembershipStatus.Terminated && memberSubs.IsRenewed == false, item.TenantId);
                var subscriptionPayment = await uowPayment.PaymentMasterRepository.Read(subPayment => subPayment.MemberSubscriptionId == memberSubscription.Id, item.TenantId);

                if (subscriptionPayment.PaymentState == Enums.PaymentState.Paid)
                {
                    memberSubscription.MembershipStatus = Enums.MembershipStatus.Active;
                }
                else
                {
                    memberSubscription.MembershipStatus = Enums.MembershipStatus.Pending;
                }

                memberSubscription.EditedById = item.UserId;
                memberSubscription.EditedOn = DateTime.UtcNow;
                await uowMembership.SaveAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberSubscriptionBo> RenewSubscription(Request<MemberSubscriptionBo> item)
        {
            using (var transaction = await uowMembership.Context.Database.BeginTransactionAsync())
            {

                try
                {
                    var memberInfo =
                        await uowMembership.MemberRepository.Read(mem => mem.Id == item.Item.MemberId, item.TenantId);
                    memberInfo.SubscriptionId = item.Item.SubscriptionId;
                    await uowMembership.SaveAsync();

                    var membersubscription = await uowMembership.MemberSubscriptionRepository.Read(
                        memSubs => memSubs.MemberId == item.Item.MemberId &&
                                   memSubs.MembershipStatus == Enums.MembershipStatus.Inactive &&
                                   memSubs.IsRenewed == false, item.TenantId);
                    membersubscription.IsRenewed = true;
                    await uowMembership.SaveAsync();

                    var subscriptionInfo =
                        await uowMembership.SubscriptionRepository.Read(subs => subs.Id == item.Item.SubscriptionId,
                            item.TenantId);

                    var ReNewDate = DateTime.UtcNow;
                    ReNewDate = ChangeRenewalDate(subscriptionInfo.MembershipExpired);

                    var memberSubscription = await uowMembership.MemberSubscriptionRepository.Create(
                        new Request<Data.Entity.Subscription.MemberSubscription>
                        {
                            Item = new Data.Entity.Subscription.MemberSubscription
                            {
                                MemberId = item.Item.MemberId,
                                SubscriptinId = item.Item.SubscriptionId,
                                RenewDate = ReNewDate,
                                JoinDate = DateTime.UtcNow,
                                MembershipStatus = Enums.MembershipStatus.Pending
                            },
                            TenantId = item.TenantId,
                            UserId = item.UserId
                        });
                    await uowMembership.SaveAsync();
                    transaction.Commit();
                    await RemoveContactCacheAsync(item.TenantId);
                    return memberSubscription
                        .MapObject<Data.Entity.Subscription.MemberSubscription, MemberSubscriptionBo>();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e.HandleException();
                }
            }
        }

        public async Task ChangeSubscriptionState(Request<PaymentBo> req)
        {
            var memberSubscription = await (from memberSub in uowMembership.MemberSubscriptionRepository.Table
                                            where memberSub.Id == req.Item.MemberSubscriptionId
                                            select memberSub).FirstOrDefaultAsync();
            var subscription = await uowMembership.SubscriptionRepository.Read(subs => subs.Id == memberSubscription.SubscriptinId, req.TenantId);

            if (req.Item.PaymentState == Enums.PaymentState.Paid)
            {
                memberSubscription.MembershipStatus = Enums.MembershipStatus.Active;
            }
            else
            {
                memberSubscription.MembershipStatus = Enums.MembershipStatus.Pending;
            }
            memberSubscription.JoinDate = DateTime.UtcNow;
            memberSubscription.RenewDate = ChangeRenewalDate(subscription.MembershipExpired);
            memberSubscription.EditedById = req.UserId;
            memberSubscription.EditedOn = DateTime.UtcNow;
            await uowMembership.SaveAsync();
        }

        DateTime ChangeRenewalDate(Enums.MembershipExpired membershipExpired)
        {
            if (membershipExpired == Enums.MembershipExpired.Annualy)
            {
                return DateTime.UtcNow.AddYears(1);
            }
            else if (membershipExpired == Enums.MembershipExpired.Monthly)
            {
                return DateTime.UtcNow.AddMonths(1);
            }
            else if (membershipExpired == Enums.MembershipExpired.Weekly)
            {
                return DateTime.UtcNow.AddDays(7);
            }
            else if (membershipExpired == Enums.MembershipExpired.Daily)
            {
                return DateTime.UtcNow.AddDays(1);
            }
            else if (membershipExpired == Enums.MembershipExpired.LifeTime)
            {
                return DateTime.UtcNow.AddYears(100);
            }
            return DateTime.UtcNow;
        }

        #endregion

        #region NotImplemented
        public Task<PageList<MemberSubscriptionBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }

        public Task<MemberSubscriptionBo> Read(Request<int> request)
        {
            throw new NotImplementedException();
        }

        public Task<List<KeyValueListItem<int>>> ReadKeyValue(Search request)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Request<int> request)
        {
            throw new NotImplementedException();
        }
        public Task Update(Request<MemberSubscriptionBo> request)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
