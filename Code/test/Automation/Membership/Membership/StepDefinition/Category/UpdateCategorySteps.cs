﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;

namespace Membership.StepDefinition.Category
{
    [Binding]
    public sealed class UpdateCategorySteps
    {

        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();

        [Given(@"Load the Membership Login page")]
        public void GivenLoadTheMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I enter username & password ""(.*)"" and ""(.*)""")]
        public void GivenIEnterUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I click on Sign in button")]
        public void GivenIClickOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigated to Dashboard")]
        public void ThenINavigatedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I Click on Settings")]
        public void ThenIClickOnSettings()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"Click on edit button")]
        public void ThenClickOnEditButton()
        {
            categoryPO.Btn_EditCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
        [Then(@"I enter (.*)")]
        public void ThenIEnterManager(string p0)
        {
            categoryPO.Category(ngWebDriver).Clear();
            Thread.Sleep(2000);
            categoryPO.Category(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }

        [Then(@"I click on Update button")]
        public void ThenIClickOnUpdateButton()
        {
            categoryPO.Btn_UpdateCategory_Click(ngWebDriver);
            Thread.Sleep(30000);
        }


        [Then(@"I click on Signout button")]
        public void ThenIClickOnSignoutButton()
        {
           
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //cancel update 
        [Given(@"Load the Membership Login")]
        public void GivenLoadTheMembershipLogin()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I enter un & pw ""(.*)"" and ""(.*)""")]
        public void GivenIEnterUnPwAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I click on Signin")]
        public void GivenIClickOnSignin()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigate Dashboard")]
        public void ThenINavigateDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I Click on Settings button")]
        public void ThenIClickOnSettingsButton()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"Click on edit")]
        public void ThenClickOnEdit()
        {
            categoryPO.Btn_EditCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Enter (.*) as Cat")]
        public void ThenEnterAssistantManagerAsCat(string p0)
        {
            categoryPO.Category(ngWebDriver).Clear();
            Thread.Sleep(2000);
            categoryPO.Category(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);

        }

        [Then(@"I click on Cancel")]
        public void ThenIClickOnCancel()
        {
            categoryPO.Btn_CancelCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }


        [Then(@"I click on Signout")]
        public void ThenIClickOnSignout()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //Negative

        [Given(@"Load the Membership Login pg")]
        public void GivenLoadTheMembershipLoginPg()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I enter un and pw ""(.*)"" and ""(.*)""")]
        public void GivenIEnterUnAndPwAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I click on Signin btn")]
        public void GivenIClickOnSigninBtn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigate Dashboard pg")]
        public void ThenINavigateDashboardPg()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I Click on Settings btn")]
        public void ThenIClickOnSettingsBtn()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"Click on edit btn")]
        public void ThenClickOnEditBtn()
        {
            categoryPO.Btn_EditCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Enter (.*) as Categories")]
        public void ThenEnterAsCategories(string p0)
        {
            categoryPO.Category(ngWebDriver).Clear();
            Thread.Sleep(2000);
            categoryPO.Category(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }

        [Then(@"I click on Update btn")]
        public void ThenIClickOnUpdateBtn()
        {
            categoryPO.Btn_UpdateCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I click on Signout btn")]
        public void ThenIClickOnSignoutBtn()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
