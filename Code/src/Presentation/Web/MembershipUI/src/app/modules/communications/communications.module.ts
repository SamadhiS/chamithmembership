
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunicationsRoutingModule } from './communications-routing.module';
import { SharedModule } from '../shared/shared.module';
import { EditorModule } from 'primeng/editor';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { EmailInboxComponent } from './email-inbox/email-inbox.component';
import { EmailTemplateTypeComponent } from './email-template-type/email-template-type.component';
import { EmailPreviewComponent } from './email-preview/email-preview.component';
import { EmailDetailsComponent } from './email-details/email-details.component';
import { EmailTemplateCreateComponent } from './email-template-create/email-template-create.component';
import { EmailTemplatDetailsComponent } from './email-templat-details/email-templat-details.component';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  imports: [
    SharedModule,
    EditorModule,
    TranslateModule,
    CommunicationsRoutingModule,
    AccordionModule.forRoot(),  
  ],
  declarations: [EmailPreviewComponent,
     EmailInboxComponent, EmailTemplateTypeComponent, EmailPreviewComponent,
      EmailDetailsComponent, EmailTemplateCreateComponent, EmailTemplatDetailsComponent],
  providers:[]
})
export class CommunicationsModule { }
