export class User {
    Host: string;
    Name: string;
    TenantTypeId: number;
    OrganizationLogo: string;
}
