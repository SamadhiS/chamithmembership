﻿using System;
namespace Inx.Module.Membership.Core.Bo.Member.SearchViewResult.ViewResult
{
    public class MembersViewResult
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public Enums.MemberType MemberType { get; set; }
        public string Name { get; set; }
        public string SubscriptionType { get; set; }
        public Enums.MembershipStatus MembershipStatus { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime RenewDate { get; set; }
        public string PaymentState { get; set; }
        public string Phone { get; set; }
        public DateTime CreateOn { get; set; }
    }
}
