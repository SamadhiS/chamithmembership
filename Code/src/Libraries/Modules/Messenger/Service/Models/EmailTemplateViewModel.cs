﻿using Inx.Service.Base.Models;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Service.Models
{
    public class EmailTemplateViewModel: BaseViewModel
    {
        public string EmailBody { get; set; }
        public EmailTemplateTypes EmailTemplateTypes { get; set; }
    }
}
