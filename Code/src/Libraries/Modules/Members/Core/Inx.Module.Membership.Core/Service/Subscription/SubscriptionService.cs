﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using data = Inx.Module.Membership.Data.Entity.Subscription;
using Inx.Module.Membership.Core.Bo.Subscription.SearchViewResult;

namespace Inx.Module.Membership.Core.Service.Subscription
{
    public class SubscriptionService : MemberBaseService, ISubscriptionService
    {
        private readonly IUnitOfWokMembership uow;
        public SubscriptionService(IUnitOfWokMembership _uow, ICoreInjector inject) : base(inject)
        {
            uow = (IUnitOfWokMembership)_uow;
        }
        [Description("add subscription type and subscription")]
        public async Task<SubscriptionBo> Create(Request<SubscriptionBo> req)
        {
            IDbContextTransaction tra = null;
            try
            {
                tra = await uow.Context.Database.BeginTransactionAsync();
                var request = req.Item;
                var type = await uow.SubscriptionTypeRepository.CreateAndSave(new Request<data.SubscriptionType>
                {
                    Item = new data.SubscriptionType
                    {
                        Name = request.Name,
                        Description = request.Description,
                    },
                    TenantId = req.TenantId,
                    UserId = req.UserId
                });
                var subscriptionrequest = new data.Subscription
                {
                    Id = type.Id,
                    Capacity = request.Capacity,
                    IsLimited = request.IsLimited,
                    Price = request.Price,
                    SubscriptionFrom = request.SubscriptionFrom,
                    SubscriptionTo = request.SubscriptionTo,
                    MembershipExpired = request.MembershipExpired,
                };
                var subscription = await uow.SubscriptionRepository.Create(new Request<data.Subscription>()
                {
                    Item = subscriptionrequest,
                    TenantId = req.TenantId,
                    UserId = req.UserId
                });

                request.Id = subscription.Id;
                request.SubscriptionTypeId = type.Id;
                await uow.SaveAsync();
                tra.Commit();
                return request;
            }
            catch (Exception e)
            {
                if (!tra.IsNull())
                {
                    tra.Rollback();
                }
                throw e.HandleException();
            }
        }
        public async Task<PageList<SubscriptionSearchResult>> Read(Search search)
        {
            try
            {
                var items = (await (from type in uow.SubscriptionTypeRepository.TableAsNoTracking
                                    join subscription in uow.SubscriptionRepository.TableAsNoTracking on type.Id equals subscription
                                        .Id
                                    where subscription.TenantId == search.TenantId && subscription.RecordState == Enums.RecordStatus.Active
                                    select new
                                    {
                                        type.Id,
                                        type.Name,
                                        type.Description,
                                        subscription.Capacity,
                                        subscription.IsLimited,
                                        subscription.MembershipExpired,
                                        subscription.Price,
                                        subscription.SubscriptionFrom,
                                        subscription.SubscriptionTo,
                                        subscriptionId = subscription.Id,
                                        subscription.CreatedOn,
                                        subscription.EditedOn,
                                    }).AsQueryable().Select(p => new SubscriptionSearchResult
                                    {
                                        Name = p.Name,
                                        Description = p.Description,
                                        Capacity = p.Capacity,
                                        IsLimited = p.IsLimited,
                                        MembershipExpired = p.MembershipExpired,
                                        Price = p.Price,
                                        SubscriptionFrom = p.SubscriptionFrom,
                                        SubscriptionTo = p.SubscriptionTo,
                                        Id = p.subscriptionId,
                                        SubscriptionTypeId = p.Id,
                                        CreatedOn = p.CreatedOn,
                                        EditedOn = p.EditedOn
                                    }).ToListAsync());
                return new PageList<SubscriptionSearchResult>
                {
                    Items = items,
                    TotalRecodeCount = items.Count()
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<SubscriptionBo> Read(Request<int> req)
        {
            try
            {
                return await (from type in uow.SubscriptionTypeRepository.TableAsNoTracking
                              join subscription in uow.SubscriptionRepository.TableAsNoTracking on type.Id equals subscription
                                  .Id
                              where subscription.Id == req.Item && subscription.TenantId == req.TenantId && subscription.RecordState == Enums.RecordStatus.Active
                              select new
                              {
                                  type.Id,
                                  type.Name,
                                  type.Description,
                                  subscription.Capacity,
                                  subscription.IsLimited,
                                  subscription.MembershipExpired,
                                  subscription.Price,
                                  subscription.SubscriptionFrom,
                                  subscription.SubscriptionTo,
                                  subscription.IsRenewable,
                                  subscription.IsSendEmail,
                                  subscription.EmaillTemplateId,
                                  subscription.RenewalDays,
                                  subscriptionId = subscription.Id
                              }).AsQueryable().Select(p => new SubscriptionBo
                              {
                                  Name = p.Name,
                                  Description = p.Description,
                                  Capacity = p.Capacity,
                                  IsLimited = p.IsLimited,
                                  MembershipExpired = p.MembershipExpired,
                                  Price = p.Price,
                                  SubscriptionFrom = p.SubscriptionFrom,
                                  SubscriptionTo = p.SubscriptionTo,
                                  Id = p.subscriptionId,
                                  EmaillTemplateId = p.EmaillTemplateId,
                                  IsRenewable = p.IsRenewable,
                                  IsSendEmail = p.IsSendEmail,
                                  RenewalDays = p.RenewalDays,
                                  SubscriptionTypeId = p.Id
                              }).FirstAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberSubscriptionViewResult> GetSubscriptions(Request<int> item)
        {
            try
            {
                var query = (from memberSubscription in uow.MemberSubscriptionRepository.TableAsNoTracking
                             where memberSubscription.TenantId == item.TenantId
                             && memberSubscription.SubscriptinId == item.Item
                             select new
                             {
                                 memberSubscription.MemberId,
                                 memberSubscription.MembershipStatus,
                             }).AsQueryable().Select(p => new MemberSubscriptionViewResult
                             {
                                 MemberId = p.MemberId,
                                 MembershipStatus = p.MembershipStatus,
                             });
                var activeCount = query.Where(s => s.MembershipStatus == Inx.Utility.Utility.Enums.MembershipStatus.Active).Count();
                var inActiveCount = query.Where(s => s.MembershipStatus == Inx.Utility.Utility.Enums.MembershipStatus.Inactive).Count();
                var terminatedCount = query.Where(s => s.MembershipStatus == Inx.Utility.Utility.Enums.MembershipStatus.Terminated).Count();
                var pendingCount = query.Where(s => s.MembershipStatus == Inx.Utility.Utility.Enums.MembershipStatus.Pending).Count();
                var itemcount = await query.CountAsync();

                return new MemberSubscriptionViewResult
                {
                    TotalRecordCount = itemcount,
                    ActiveCount = activeCount,
                    InActiveCount = inActiveCount,
                    PendingCount = pendingCount,
                    TerminatedCount = terminatedCount
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.SubscriptionTypeRepository.ReadKeyValue<data.SubscriptionType>(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        private async Task RemoveCache(int tenantid)
        {
            await cache.Remove(CacheKeyGen(new List<string> { Enums.CacheKey.Subscription.ToString(), tenantid.ToString() }));
            await RemoveContactCacheAsync(tenantid);
        }


        #region Obsolete
        [Obsolete]
        public async Task Update(Request<SubscriptionBo> req)
        {
            try
            {
                await uow.SubscriptionRepository.UpdateAndSave(req.MapRequestObject<SubscriptionBo, data.Subscription>(req.Item.Id));
                await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        [Description("update info")]
        public async Task UpdateRenewal(Request<SubscriptionRenewalBo> item)
        {
            try
            {
                var result = await uow.SubscriptionRepository.Read(p => p.Id == item.Item.Id, item.TenantId);
                result.IsRenewable = item.Item.IsRenewable;
                result.IsSendEmail = item.Item.IsSendEmail;
                result.RenewalDays = item.Item.RenewalDays;
                result.EmaillTemplateId = item.Item.EmaillTemplateId;
                result.EditedOn = DateTime.UtcNow;
                result.EditedById = item.UserId;
                await uow.SaveAsync();
                await RemoveContactCacheAsync(item.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        //[Obsolete]
        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.SubscriptionRepository.DeleteSafe(req);
                await uow.SaveAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        #endregion


        #region NotImplementedException
        Task<PageList<SubscriptionBo>> IBaseService<SubscriptionBo>.Read(Search search)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
