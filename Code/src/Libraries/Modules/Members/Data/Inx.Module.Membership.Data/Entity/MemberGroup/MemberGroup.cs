﻿using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Data.Entity.Members.Master;

namespace Inx.Module.Membership.Data.Entity.MemberGroup
{
    [Table("MemberGroups", Schema = Constraints.Schema)]
    public class MemberGroup : AuditableEntity
    {
        public int MemberId { get; set; }
        public int GroupId { get; set; }
        [ForeignKey("MemberId")]
        public Member MemberInfo { get; set; }
        [ForeignKey("GroupId")]
        public Group Group { get; set; }
        [NotMapped]
        public new string Name { get; set; }
    }
}
