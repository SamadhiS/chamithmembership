﻿using Inx.Utility.Models;

namespace Inx.Module.Identity.Core.Bo.Security
{
    public class ChangePasswordBo: BaseBo
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus RecordStatus { get; set; }
      
    }
}