﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
namespace Inx.Module.Membership.Data.Entity.Subscription
{
    [Table("SubscriptionType", Schema = Constraints.Schema)]
    public class SubscriptionType : AuditableEntity
    {
        [StringLength(500)]
        public string Description { get; set; }
    }
}
