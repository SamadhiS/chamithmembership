import { RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ContactsService extends BaseService {

  private apiEndPoint = `${this.baseApiEndPoint}contacts/v1/contacts`;
  private contactsArchiveEndPoint = `${this.baseApiEndPoint}contacts/v1/contactsArchive`;
  private apiEndPointContactFile = `${this.baseApiEndPoint}contacts/v1/contactfiles`;
  private utilityEndPoint = `${this.baseApiEndPoint}contacts/v1/utility/tabletemplate`;


  constructor(private http: HttpClient) {
    super();
  }

  getContacts(skip, take, search, orderby) {
    return this.http.get(`${this.apiEndPoint}?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  getArchiveContacts(skip, take, search, orderby) {
    return this.http.get(`${this.contactsArchiveEndPoint}?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  getContactById(id) {
    return this.http.get(`${this.apiEndPoint}/${id}`).map(res => res).catch(this.errorHandler)
  }

  saveContact(files, contactData) {

    let formData: FormData = new FormData();

    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('contactViewData', JSON.stringify(contactData));
    return this.http.post(this.apiEndPoint, formData).map(res => res).catch(this.errorHandler)
  }

  saveDocument(files, docObj) {
    let formData: FormData = new FormData();
    
        if (files != null && files != undefined && files.length > 0) {
          for (let i = 0; i < files.length; i++) {
            formData.append('uploadFile' + i, files[i]);
          }
        }
        formData.append('contactFileViewData', JSON.stringify(docObj));
        return this.http.post(`${this.apiEndPointContactFile}`, formData, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  archiveDocument(documentId) {
    return this.http.delete(`${this.apiEndPointContactFile}/${documentId}`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getDocumentsById(skip, take, docObj) {
    return this.http.get(`${this.apiEndPointContactFile}?skip=${skip}&take=${take}`, docObj).map(res => res).catch(this.errorHandler)
  }

  updateContact(contact) {
    return this.http.put(`${this.apiEndPoint}/contacts`, contact, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  updateWork(work) {
    return this.http.put(`${this.apiEndPoint}/working`, work, this.httpOptions).map(res => res).catch(this.errorHandler)
  }

  updatePerson(files, personData) {

    let formData: FormData = new FormData();

    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('personViewData', JSON.stringify(personData));

    return this.http.put(`${this.apiEndPoint}/person`, formData).map(res => res).catch(this.errorHandler)
  }

  archivecontact(contactId) {
    return this.http.delete(`${this.apiEndPoint}/${contactId}`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  restoreContact(contactId) {
    return this.http.put(`${this.apiEndPoint}/${contactId}/restore`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getData() {
    var contacts = {
      "firstName": "isuru",
      "lastName": "mahesh",
      "dateOfBirth": "1989/01/21",
      "identity": "890213235v",
      "categoryId": 1
    }
    return Observable.of({ contacts })
  }
}
