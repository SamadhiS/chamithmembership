import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberGroupDetailsComponent } from './member-group-details.component';

describe('MemberGroupDetailsComponent', () => {
  let component: MemberGroupDetailsComponent;
  let fixture: ComponentFixture<MemberGroupDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberGroupDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberGroupDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
