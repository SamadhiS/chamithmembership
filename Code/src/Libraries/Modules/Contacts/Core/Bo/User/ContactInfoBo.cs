﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Contacts.Core.Bo.User
{
    public class ContactInfoBo : BaseBo
    {
        public int PersonId { get; set; }
        public int Country { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Telephone { get; set; }
        [Obsolete]
        public new string Name { get; set; }
    }
}
