﻿Feature: As an Admin, I must be able to update categories

@mytag
Scenario Outline:As an Admin, I should be able to update Categories 
Given Load the Membership Login page
And I enter username & password "membership.inexis@gmail.com" and "Admin@123" 
And I click on Sign in button 
Then I navigated to Dashboard
And I Click on Settings
And Click on edit button
And I enter <Category>
And I click on Update button
And I click on Signout button
 
Examples: 
| Category |
| Assistant Manager     |
| System Engineer   |

Scenario Outline:As an Admin, I should be able to cancel update Categories 
Given Load the Membership Login
And I enter un & pw "membership.inexis@gmail.com" and "Admin@123" 
And I click on Signin 
Then I navigate Dashboard
And I Click on Settings button
And Click on edit
And Enter <Category> as Cat
And I click on Cancel
And I click on Signout
 
Examples: 
| Category          |
| Managing Director |
| Architect         |

Scenario Outline:As an Admin, I should not be able to update Categories 
Given Load the Membership Login pg
And I enter un and pw "membership.inexis@gmail.com" and "Admin@123" 
And I click on Signin btn
Then I navigate Dashboard pg
And I Click on Settings btn
And Click on edit btn
And Enter <Category> as Categories
And I click on Update btn
And I click on Signout btn
 
Examples: 
| Category |
|          |