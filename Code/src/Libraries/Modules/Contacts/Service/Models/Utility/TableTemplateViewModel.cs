﻿using System;
using System.Collections.Generic;
using Inx.Module.Contacts.Core.Bo;
using Inx.Service.Base.Models;
namespace Inx.Module.Contacts.Service.Models.Utility
{
    public class TableTemplateViewModel:BaseViewModel
    {
        public Enums.TableTemplate TemplateType { get; set; }
        public List<string> Keys { get; set; } = new List<string>();
        [Obsolete]
        public new string Name { get; set; }
    }
}
