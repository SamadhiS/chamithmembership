﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;

namespace Inx.Module.Identity.Service.Models.Security.Login
{
    public class AuthenticateViewModel
    {
        [Required,EmailAddress,StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [Required, StringLength(DbConstraints.EmailLength)]
        public string Password { get; set; } 
    }
}
