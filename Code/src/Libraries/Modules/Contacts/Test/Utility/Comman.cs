﻿using Inx.Module.Contacts.Data.DbContext;
using Inx.Module.Identity.Data.DbContext;
using Inx.Utility;
using UnitOfWork = Inx.Module.Contacts.Data.DbContext.UnitOfWork;

namespace Inx.Module.Contacts.Test.Utility
{
    public class Comman
    {
        public IdentityDbContext IdentityContext { get; set; } = new IdentityDbContext(GlobleConfig.ConnectionStringTest);

        public ContactsDbContext Context { get; set; } = new ContactsDbContext(GlobleConfig.ConnectionStringTest);
        public IUnitOfWokContacts IUnitOfWokContacts { get; set; } = new UnitOfWork(new ContactsDbContext(GlobleConfig.ConnectionStringTest));
    }
}
