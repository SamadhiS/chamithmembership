﻿using System.Threading.Tasks;

namespace Inx.Module.Membership.Core.Service.Subscription.Interfaces
{
    public interface IBackgroundProcess
    {
        Task ChangeSubscriptionStatus();
    }
}
