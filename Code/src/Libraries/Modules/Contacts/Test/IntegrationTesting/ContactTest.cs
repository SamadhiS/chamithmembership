﻿using System;
using System.Threading.Tasks;
using Inx.Module.Contacts.Core.Bo.User;
using Inx.Module.Contacts.Core.Service.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Utility;

namespace Inx.Module.Contacts.Test.IntegrationTesting
{
    public class ContactTest : CoreTestBase, ICoreTestBase
    {
        private IContactService service;
        //[TestInitialize]
        public void Initialize()
        {
            service = new ContactService(new Comman().IUnitOfWokContacts,null);
        }
        //[DataTestMethod]
        //[DataRow(1, "Chamith", "Saranga", "880240684v", Enums.MartialStatus.Married)] //true
        public async Task Create()
        {
            object[] data = null;
            var bo = new ContactBo
            {
                PersonInfo = new PersonInfoBo
                {
                    ContactCategoryId = Convert.ToInt32(data[0]),
                    DOB = new DateTime(1988, 1, 24),
                    FirstName = Convert.ToString(data[1]),
                    LastName = Convert.ToString(data[2]),
                    Identity = Convert.ToString(data[3]),
                    MartialStatus = (Enums.MartialStatus) (data[4]),
                },
                ContactInfo = new ContactInfoBo()
                {
                    Address = Convert.ToString(data[5]),
                    City = Convert.ToString(data[6]),
                    Email = Convert.ToString(data[6]),
                    Phone = "",
                    Telephone = "",
                    Zip = "",
                    Country = 1
                },
                WorkingInfo = new WorkingInfoBo()
                {
                    Phone = "",
                    Telephone = "",
                    Company = "",
                    Designation = ""
                }
            };
            try
            {
                await base.Create(bo, service);
                True("ContactTest=>Create"); 
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
        //[DataTestMethod]
        //[DataRow(1)] //true
        //[DataRow(999)] //false
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<ContactBo,IContactService>(id, service);
                True("ContactTest=>Delete");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
        //[DataTestMethod]
        //[DataRow(0,25, "searchTerm")] //true
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<ContactBo, IContactService>(
                    base.TestSearchRequest(skip:skip,take: take
                        , searchTerm: search, fk:0,orderByTerm:string.Empty), service);
                True("ContactTest=>Read");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
        //[DataTestMethod]
        //[DataRow(1)] //true
        public async Task ReadById(int id)
        {
            try
            {
                await service.Read(TestRequest(id));
                True("ContactTest=>ReadById");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
        //[DataTestMethod]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<ContactBo,IContactService>(service);
                True("ContactTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
        //[DataTestMethod]
        //[DataRow(0,25, "searchTerm")] //true
        public async Task Search(int skip, int take, string search)
        {
            try
            {
                await service.Search(TestSearchRequest(skip: skip,
                    take: take, searchTerm: search));
                True("ContactTest=>Search");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
        //[DataTestMethod]
        //[DataRow(1)] //true
        public async Task Update()
        {
            try
            {
                await base.Update(new ContactBo(),service);
                True("ContactTest=>Update");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
    }
}
