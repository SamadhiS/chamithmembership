﻿
using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Utility.Models;
using System;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using Inx.Module.Messenger.Data.DbContext;


namespace Inx.Module.Messenger.Core.Service
{
    public class EmailSendService : IEmailSendService
    {
        private readonly IUnitOfWorkMassengers uow;


        public EmailSendService(IUnitOfWorkMassengers _uow)
        {
            uow = (IUnitOfWorkMassengers)_uow;

        }

        /// <summary>
        /// Send multiple emails base on member level and member status
        /// </summary>

        public async Task SendEmails(Request<EmailSendingInfoModel> emailInfoModel)
        {
            await SendEmail(emailInfoModel.Item);

        }

        public async Task SendEmail(EmailSendingInfoModel model)
        {
            var apiKey = Environment.GetEnvironmentVariable("membership_email_api_key");
            var client = new SendGridClient("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");
            var from = new EmailAddress("infoassocifyportal@gmail.com","Assocify");
            var subject = model.Subject;
            var to = new EmailAddress(model.ToAddress, "Example User");

            var htmlContent = model.EmailBody;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
