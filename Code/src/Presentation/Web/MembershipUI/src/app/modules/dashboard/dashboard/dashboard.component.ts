import { BlockUiService } from '../../core/services/block-ui.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import * as Chart from 'chart.js'
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { HttpClient } from '@angular/common/http';

import { ToastsManager } from 'ng2-toastr';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';
import { DashboardService } from '../service/dashboard.service';
import { DateRange } from '../../core/enums/dateRange';
import { element } from 'protractor';
import { type } from 'os';

import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MonthRange } from '../../core/enums/monthRange';
import { LookupService } from '../../core/services/lookup.service';
import { TenantInfoModel } from '../../settings/models/tenantInfoModel';

@Component({
  selector: 'membership-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  week = 4;
  threemonths = 1;
  sixmonths = 2;
  oneyear = 3;
  term: number;
  day: any;
  todaydate: any;
  monthName:any;

  dashboardlevelCollection = [];
  dashboardActiveCollection = [];
  dashboardPaymentDueCollection = [];
  MemberLevels = [];
  ActiveMembers = [];
  InActiveMembers = [];
  MemberStatusActive = [];
  MemberSubscriptionDate = [];
  DateArray = [];
  MonthsArray=[];
  chartc: any;
  coOrdinates=[];
  Activemem= [];
  memberActivecount = 0;
  totalNetRevenue = 0;
  totalPaymentDue = 0;
  dateRange = DateRange;
  xAxis :string;
  currency= "";
  monthRange = MonthRange;

  isBlocked: boolean = false;
  isNew: boolean = false;

  @ViewChild('chartTarget') chartTarget: ElementRef;
  @ViewChild('chartTarget2') chartTarget2: ElementRef;

  chart: Highcharts.ChartObject;

  constructor(private http: HttpClient,
    private _dashboardService: DashboardService,
    private datePipe: DatePipe,
    private lookupService :LookupService,
    private blockUiService: BlockUiService,
    private activatedRoute: ActivatedRoute,
    public _toastr: ToastsManager, vcr: ViewContainerRef,
    private router: Router,
    private translate: TranslateService
  ) { this._toastr.setRootViewContainerRef(vcr); }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.isNew = params['bool'];
    });
    if(!this.isNew){
      this.term = this.week;
      this.getDashBoardDetails(this.term);
      this.MonthsArray =[];
      this.DateArray =[];
    }
    else{
      this.term = this.threemonths;
      this.totalNetRevenue = 10000;
      this.memberActivecount = 58;
      this.todaydate = Date.now();
      this.day = Date.now();
      this.totalPaymentDue = 2500;
      this.MemberLevels= ["Gold Package","Platinum Package","Silver Package","Browns Package"]
      this.InActiveMembers = [2,1,4,1]
      this.ActiveMembers =[0,1,0,1,]

      this.coOrdinates =  [["2018-05-18", 8],["2018-05-18", 5],["2018-05-18", 1],["2018-05-19", 9],["2018-05-19", 2],["2018-05-20", 5],]

      this.getMemberlevelchart();
      this.getMemberActivation();
    }
  }
  ngAfterViewInit() { }

  filterbyTerms(term) {
    if (term == this.dateRange.week) {
      this.term = this.week;
      this.getDashBoardDetails(this.term);
    }
    else if (term == this.dateRange.threemonths) {
      this.term = this.threemonths;
      this.getDashBoardDetails(this.term);
    }
    else if (term == this.dateRange.sixmonths) {
      this.term = this.sixmonths;
      this.getDashBoardDetails(this.term);
    }
    else if (term == this.dateRange.oneyear) {
      this.term = this.oneyear;
      this.getDashBoardDetails(this.term);
    }
  }

  filterbyMonths(month){
    if(month == "01")
    {
      this.monthName = this.monthRange.one;
      this.MonthsArray.push(this.monthName);
    }
    else if(month == "02")
    {
        this.monthName = this.monthRange.two;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "03")
    {
        this.monthName = this.monthRange.three;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "04")
    {
        this.monthName = this.monthRange.four;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "05")
    {
        this.monthName = this.monthRange.five;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "06")
    {
        this.monthName = this.monthRange.six;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "07")
    {
        this.monthName = this.monthRange.seven;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "08")
    {
        this.monthName = this.monthRange.eight;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "09")
    {
        this.monthName = this.monthRange.nine;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "10")
    {
        this.monthName = this.monthRange.ten;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "11")
    {
        this.monthName = this.monthRange.eleven;
        this.MonthsArray.push(this.monthName);
    }
    else if(month == "12")
    {
        this.monthName = this.monthRange.twelve;
        this.MonthsArray.push(this.monthName);
    }
  }

  getDashBoardDetails(term) {
    this.isBlocked = true;
    this.Activemem = [];
    this.MonthsArray =[];
    this.setEmptyforchart();
    this.currency  = localStorage.getItem('Currency');
    this._dashboardService.getAllDashBoardDetails(term)
      .subscribe(dashboard => {

        this.dashboardlevelCollection = dashboard.result.memberLevelGraph;
        this.dashboardActiveCollection = dashboard.result.memberActiveGraph;
        this.dashboardPaymentDueCollection = dashboard.result.dashboardCounts;

        this.MemberLevels = [];
        this.ActiveMembers = [];
        this.InActiveMembers = [];

        this.MemberLevels = dashboard.result.memberLevelGraph.memberLevels;
        this.ActiveMembers = dashboard.result.memberLevelGraph.activeMembers;
        this.InActiveMembers = dashboard.result.memberLevelGraph.inActiveMembers;
        this.totalPaymentDue = dashboard.result.dashboardCounts.paymentDue[0];
        this.totalNetRevenue = dashboard.result.dashboardCounts.netRevenue[0];
        this.day = dashboard.result.dashboardCounts.fromDate;
        this.todaydate = Date.now();
        this.MemberStatusActive = [];
        this.MemberSubscriptionDate = [];

        this.MemberStatusActive = dashboard.result.memberActiveGraph.activeMembers
        if (this.MemberStatusActive.length != 0) {
          this.MemberStatusActive.forEach(data => {
            if (data == 1) {
              this.Activemem.push(data);
            }
          })
          this.memberActivecount = this.Activemem.length;
        }
        else {
          this.memberActivecount = 0;
        }
        this.MemberSubscriptionDate = dashboard.result.memberActiveGraph.memberSubscriptionDate
        if (this.MemberSubscriptionDate.length != 0) {
          this.MemberSubscriptionDate.forEach(data => {
           var res =  this.datePipe.transform(data, 'yyyy-MM-dd');
            this.DateArray.push(res);
           var month = this.datePipe.transform(res,'MM');  //"01"
           this.filterbyMonths(month);
          })
        }
        this.coOrdinates = this.DateArray
        .map((x, i) => [x, dashboard.result.memberActiveGraph.activeMembers[i]]);

        if (this.dashboardlevelCollection.length != 0) {
          this.getMemberlevelchart();
        }
        if (this.dashboardActiveCollection.length != 0) {
          this.getMemberActivation();
        }
        else {
          this.isBlocked = true;
          this.setEmptyforchart();
          this.getMemberlevelchart();
          this.getMemberActivation();
          this.isBlocked = false;
        }
        this.isBlocked = false;
      }
      );
  }

  getMemberActivation() {

    var x = this.coOrdinates;
    const options: Highcharts.Options = {
      chart: {
        zoomType: 'x'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: document.ontouchstart == null ?
         'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      xAxis: {
      categories : this.MonthsArray 
      },
      yAxis: {
        allowDecimals: false,
        title: {
          type: 'double',
          min: 0,
          text: 'Active Status'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1
            },
            stops: [
              [0, 'rgba(84, 178, 253, 0.24)'],
              [1, 'rgba(84, 178, 253, 0.24)']

           ]
          },
          marker: {
            radius: 4
          },
          lineWidth: 2,
          states: {
            hover: {
              lineWidth: 1
            }
          },
          threshold: null
        }
      },
      series: [{
        type: 'area',
        name: 'Active',
        data: this.coOrdinates,


      }]
    };
    this.chart = chart(this.chartTarget2.nativeElement, options);
  }

  getMemberlevelchart() {

    const options: Highcharts.Options = {
      chart: {
        type: 'bar'
      },
      title: {
        text: ''
      },
      xAxis: {
        allowDecimals: false,
        categories: this.MemberLevels
      },
      yAxis: {
        allowDecimals: false,        
        min: 0,
        title: {
          text: 'Count'
        }
      },
      legend: {
        reversed: true
      },
      plotOptions: {


        series: {
          stacking: 'normal'
        },

         bar: {
      dataLabels: {
        enabled: false
      }

    }


      },
      series: [{
        name: 'Active',
        data: this.ActiveMembers,

        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, 'rgba(0, 199, 182, 0.4)'],
            [1, 'rgba(0, 199, 182, 0.4)']
          ]
        }

      }, {
        name: 'Inactive',
        data: this.InActiveMembers,

        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, 'rgba(255, 235, 59, 0.54)'],
            [1, 'rgba(255, 235, 59, 0.54)']
          ]
        }
      }]
    };
    this.chart = chart(this.chartTarget.nativeElement, options);
  }

 
  setEmptyforchart() {
    this.MemberLevels = [];
    this.ActiveMembers = [];
    this.InActiveMembers = [];
    this.MemberStatusActive = [];
    this.MemberSubscriptionDate = [];
    this.Activemem = [];
    this.totalPaymentDue = 0;
    this.totalNetRevenue = 0;
    this.MonthsArray =[];
    this.DateArray =[];
  }

}


