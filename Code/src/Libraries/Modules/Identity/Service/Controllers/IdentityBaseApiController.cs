﻿using Inx.Service.Base.Controllers;

namespace Inx.Module.Identity.Service.Controllers
{
    public class IdentityBaseApiController : BaseApiController
    {
        public IdentityBaseApiController(IBaseInjector baseinject) : base(baseinject)
        {
        }
    }
}
