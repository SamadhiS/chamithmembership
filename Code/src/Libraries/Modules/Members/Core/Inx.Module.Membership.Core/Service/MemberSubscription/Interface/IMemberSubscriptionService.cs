﻿using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Member;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Payment.Core.Bo;
using Inx.Utility.Models;
namespace Inx.Module.Membership.Core.Service.MemberSubscription.Interface
{
    public interface IMemberSubscriptionService : IBaseService<MemberSubscriptionBo>
    {
        Task ChangeMemberState(Request<MemberState> item);
        Task<MemberSubscriptionBo> ChangeMemberSubscription(Request<MemberSubscriptionBo> item);
        Task<PaymentBo> UpdatePayment(Request<PaymentBo> req);
        Task TerminateSubscription(Request<TerminationNoteBo> item);
        Task<MemberSubscriptionBo> ReadMemberSubscription(Request<int> item);
        Task ActivateSubscription(Request<int> item);
        Task<MemberSubscriptionBo> RenewSubscription(Request<MemberSubscriptionBo> item);
        Task ChangeSubscriptionState(Request<PaymentBo> req);
    }
}
