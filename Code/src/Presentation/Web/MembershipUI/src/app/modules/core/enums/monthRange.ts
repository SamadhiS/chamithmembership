export enum MonthRange {
    "one" = "Jan" ,
    "two" = "Feb",
    "three" = "Mar",
    "four" = "Apr",
    "five" = "May",
    "six" = "Jun",
    "seven" = "Jul",
    "eight" = "Aug",
    "nine" = "Sep",
    "ten" = "Oct",
    "eleven" = "Nov",
    "twelve" = "Dec"
}