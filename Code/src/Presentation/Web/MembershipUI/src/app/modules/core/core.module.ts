import { GroupService } from './services/group.service';
import { LevelService } from './services/level.service';
import { EmailTemplateService } from './services/email-template.service';
import { BlockUiService } from './services/block-ui.service';
import { TableTemplateService } from './services/table-template.service';
import { ToastOptions } from 'ng2-toastr';
import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LookupService } from './services/lookup.service';
import { SortBaseService } from './services/sort-base.service';
import { TenantInfoService } from './services/tenant-info.service';
import { ContactsService } from './services/contacts.service';
import { MembersService } from './services/members.service';
import { PaymentService } from './services/payment.service';
import { ElementControlService } from './custom-field-base/element-control.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ElementInitialisationService } from './custom-field-base/element-initialisation.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [LookupService, SortBaseService, TenantInfoService, TableTemplateService, EmailTemplateService, ElementControlService,
        BlockUiService, ContactsService, LevelService, GroupService, MembersService, PaymentService, ElementInitialisationService]
    };
  }

}
