export class WorkModel {
    id: number;
    country: string;
    tenantId: number;
    designation: string;
    company: string;
    address: string;
    telephone: string;
    phone: string;

    constructor() {
        this.id = 0;
        this.tenantId = 0;
    }
}