﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.MemberPO;
using System.Windows.Forms;
using System;
using System.Diagnostics;

namespace Membership.FeatureFile.Member.MemberGroup
{
    [Binding]
    public sealed class AddMemberGroup
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private MemberGroupPO groupPO = new MemberGroupPO();

        [Given(@"The Application Page opens")]
        public void GivenTheApplicationPageOpens()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"Users enters ""(.*)"" and ""(.*)""")]
        public void ThenUsersEntersAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"User Click on SignIn")]
        public void ThenUserClickOnSignIn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User navigated to Homepage")]
        public void ThenUserNavigatedToHomepage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(5000);
        }

        [Then(@"User directed to Members Section")]
        public void ThenUserDirectedToMembersSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"User Navigate to Groups")]
        public void ThenUserNavigateToGroups()
        {
            groupPO.Btn_Group_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Click on New group")]
        public void ThenUserClickOnNewGroup()
        {
            groupPO.Btn_NewGroup_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Enter (.*)")]
        public void ThenEnter(string p0)
        {
            groupPO.GrpName(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);        
        }

        [Then(@"Enters (.*)")]
        public void ThenEnters(string p0)
        {
            groupPO.GrpDescription(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }


        [Then(@"User Click on Save Group")]
        public void ThenUserClickOnSaveGroup()
        {
            groupPO.Btn_GrpSave_Click(ngWebDriver);
        }

        [Then(@"a Succes message is displayed")]
        public void ThenASuccesMessageIsDisplayed()
        {
            Thread.Sleep(3000);
        }

        [Then(@"User Click on members")]
        public void ThenUserClickOnMembers()
        {
            groupPO.Btn_GrpMember_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Click on Drop Down")]
        public void ThenUserClickOnDropDown()
        {
            groupPO.Btn_MemberDD_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Select a Member")]
        public void ThenUserSelectAMember()
        {
            groupPO.Btn_SelectMember_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"UserClick to Close Drop Down List")]
        public void ThenUserClickToCloseDropDownList()
        {
            groupPO.Btn_MemberDD_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Click on Add Member To Group")]
        public void ThenUserClickOnAddMemberToGroup()
        {
            groupPO.Btn_SaveMemToGrp_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"a Sucess message appears")]
        public void ThenASucessMessageAppears()
        {
            Thread.Sleep(3000);
        }

        [Then(@"The added Members are shown")]
        public void ThenTheAddedMembersAreShown()
        {
            Thread.Sleep(3000);
        }

        [Then(@"User logout")]
        public void ThenUserLogout()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        [Then(@"does not Enter for ""(.*)""")]
        public void ThenDoesNotEnterFor(string p0)
        {
            groupPO.GrpName(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }


        [Then(@"User Error  message is displayed")]
        public void ThenUserErrorMessageIsDisplayed()
        {
            Thread.Sleep(2000);
        }





    }
}
