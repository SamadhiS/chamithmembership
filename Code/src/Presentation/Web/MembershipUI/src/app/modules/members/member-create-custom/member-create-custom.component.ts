import { Component, OnInit, Input, ViewContainerRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { ToastsManager } from 'ng2-toastr';

import { ElementControlService } from '../../core/custom-field-base/element-control.service';
import { ElementBase } from '../../core/custom-field-base/element-base';
import { TabModel } from '../models/tabModel';
import { ElementInitialisationService } from '../../core/custom-field-base/element-initialisation.service';
import { HTMLElements } from '../../core/enums/htmlElement';
import { RenderViewService } from '../services/render-view.service';
import { CustomMemberModel } from '../models/customMemberModel';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { CutomMemberService } from '../services/member.service';
import { MemberType } from '../../core/enums/memberType';
import { FormBuilder } from '../../../../../node_modules/@angular/forms';
import { LevelService } from '../../core/services/level.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { LevelInfoModel } from '../models/levelInfoModel';

@Component({
  selector: 'membership-member-create-custom',
  templateUrl: './member-create-custom.component.html',
  styleUrls: ['./member-create-custom.component.scss']
})
export class MemberCreateCustomComponent implements OnInit {

  tab = new TabModel();
  tabs = new Array<TabModel>();
  customMemberModel = new CustomMemberModel();
  fields: ElementBase<any>[] = new Array();
  levelInfo = new LevelInfoModel();

  renderData = [];
  memberFieldKeyValue = [];

  memberType: MemberType;
  memberId: number = 0;
  tabCount: number = 0;
  subscriptionChanged: string = null;
  levelPrice: string="";

  isActive: boolean = true;
  isDisabled: boolean = false;
  isDynamicTabLoaded: boolean = false;
  isEdit: boolean = false;
  isMemberLevelActive: boolean = false;
  isMemberLevelDisabled: boolean = true;
  isMemberPaymentActive: boolean = false;
  isMemberPaymentDisabled: boolean = true;

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @Input() elements: ElementBase<any>[] = [];

  constructor(private fb: FormBuilder, private elementControlService: ElementControlService, private initalizeElement: ElementInitialisationService,
    private renderViewService: RenderViewService, private memberService: CutomMemberService, private toastr: ToastsManager,
    vcr: ViewContainerRef, private activatedRoute: ActivatedRoute, private levelService: LevelService) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberType = params['type'];
      this.memberId = params['id'];
      this.subscriptionChanged = params['state'];

      if (this.memberId != 0 && this.memberId != undefined) {
        this.isEdit = true;
        this.renderUpdateView();
      }
      else {
        this.renderView();
      }
    });
  }

  renderView() {
    this.renderViewService.getRenderFieldData(this.memberType)
      .subscribe(renderElements => {
        this.renderData = renderElements.result.memberTypeTabs;

        if (this.isEdit) {
          for (let memberField of this.memberFieldKeyValue) {
            for (let fields of this.renderData) {
              for (let element of fields.tabsFields) {
                if (element.key == memberField.key) {
                  element.value = memberField.value;
                  break;
                }
              }
            }
          }
        }

        for (let t of renderElements.result.memberTypeTabs) {
          this.tab.displayName = t.displayName;

          for (let fields of t.tabsFields) {
            this.addFieldsToTabs(fields);
          }

          this.tab.elements = this.fields.sort((a, b) => a.order - b.order);
          this.tab.form = this.elementControlService.toFormGroup(this.tab.elements);

          this.tab.active = this.isActive;
          this.tab.disabled = this.isDisabled;
          this.tab.isFormSubmitted = false;

          this.tabs.push(this.tab);
          this.tab.tabCount = this.tabs.length;

          this.isActive = false;
          this.isDisabled = this.isEdit == true ? false : true;
          this.fields = [];
          this.tab = new TabModel();
        }

        this.tabCount = this.tabs.length;
        this.isDynamicTabLoaded = true;
      }, error => {
        this.toastr.error('Error in rendering the view', 'Oops');
      });
  }

  renderUpdateView() {
    this.memberService.getById(this.memberId)
      .subscribe(res => {
        this.memberFieldKeyValue = res.result.memberFieldKeyValue;
        this.renderView();
      }, error => {
        this.toastr.error('Error in rendering the view', 'Oops');
      });
  }

  addFieldsToTabs(fields) {
    switch (fields.dataType) {
      case HTMLElements.text:
        this.fields.push(this.initalizeElement.textElement(fields))
        break;
      case HTMLElements.checkbox:
        this.fields.push(this.initalizeElement.checkboxElement(fields))
        break;
      case HTMLElements.dropdown:
      fields.value = [{'key': '1', 'value': 'Married' }, { 'key': '2', 'value': 'Single' }];
        this.fields.push(this.initalizeElement.dropdownElement(fields))
        break;
      case HTMLElements.textarea:
        this.fields.push(this.initalizeElement.textareaElement(fields))
        break;
      case HTMLElements.calendar:
        this.fields.push(this.initalizeElement.calendarElement(fields))
        break;
      case HTMLElements.file:
        this.fields.push(this.initalizeElement.fileElement(fields))
        break;
    }
  }

  goToNextTab(nextTab) {
    this.tabs[nextTab - 1].isFormSubmitted = true;
    if (this.tabs[nextTab - 1].form.valid == false) return;
    this.tabs[nextTab].disabled = false;
    this.tabs[nextTab].active = true;
  }

  saveMember(tab) {
    this.tabs[tab - 1].isFormSubmitted = true;
    if (this.tabs[tab - 1].form.valid == false) return;

    if (this.memberId != 0 && this.memberId != undefined) {
      this.tabs[tab - 1].active = false;
      this.isMemberLevelDisabled = false;
      this.isMemberLevelActive = true;
      return;
    }

    this.customMemberModel.memberType = this.memberType;

    for (let tabs of this.tabs) {
      let formValues = tabs.form.value;
      for (let formField of tabs.elements) {
        this.customMemberModel.memberFieldKeyValue.push({ 'key': formField.key, 'value': formValues[formField.key] });
      }
    }
    this.memberService.createMember(this.customMemberModel)
      .subscribe(res => {
        this.tabs[tab - 1].active = false;
        this.memberId = res.result;
        this.isMemberLevelDisabled = false;
        this.isMemberLevelActive = true;
      }, error => {
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      });
  }

  updateMember(tab) {
    this.tabs[tab - 1].isFormSubmitted = true;
    if (this.tabs[tab - 1].form.valid == false) return;

    this.customMemberModel.memberId = this.memberId;
    this.customMemberModel.memberType = this.memberType;

    for (let tabs of this.tabs) {
      let formValues = tabs.form.value;
      for (let formField of tabs.elements) {
        this.customMemberModel.memberFieldKeyValue.push({ 'key': formField.key, 'value': formValues[formField.key] });
      }
    }
    this.memberService.updateMember(this.customMemberModel)
      .subscribe(res => {
        this.toastr.success(ToasterMessages.update('Member'), toasterHeadings.Success);
      }, error => {
        this.toastr.error(ToasterMessages.updateError('Member'), toasterHeadings.Error);
      });
  }

  goToPaymentTab($event) {
    this.levelPrice = $event.levelPrice;

    if ($event.levelCreated == true) {
      this.isMemberLevelActive = false;
      this.isMemberPaymentDisabled = false;
      this.isMemberPaymentActive = true;
    }
  }
}
