﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.MemberPO;
using System.Windows.Forms;
using System;
using System.Diagnostics;

namespace Membership.FeatureFile.Member.MemberGroup
{
    [Binding]
    public sealed class EditMemberGroup
    {

        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private MemberGroupPO groupPO = new MemberGroupPO();

        [Given(@"The application Page ppens")]
        public void GivenTheApplicationPagePpens()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"Users enters thE ""(.*)"" and ""(.*)""")]
        public void ThenUsersEntersThEAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"User click SignIn")]
        public void ThenUserClickSignIn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Navigated to homepage")]
        public void ThenUserNavigatedToHomepage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(5000);
        }

        [Then(@"User Directed To Members Section")]
        public void ThenUserDirectedToMembersSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"User Navigate To groups")]
        public void ThenUserNavigateToGroups()
        {
            groupPO.Btn_Group_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Select A Member Group")]
        public void ThenSelectAMemberGroup()
        {
            groupPO.Btn_viewMembGrp(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"Update the (.*)")]
        public void ThenUpdateThe(string p0)
        {
            groupPO.GrpDescription(ngWebDriver).Clear();
            groupPO.GrpDescription(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

        [Then(@"I click on Update")]
        public void ThenIClickOnUpdate()
        {
            groupPO.Btn_update_MemGrp(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"a Sucess Message appears")]
        public void ThenASucessMessageAppears()
        {
            Thread.Sleep(2000);
        }

        [Then(@"LOg Out")]
        public void ThenLOgOut()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
