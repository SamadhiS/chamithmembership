﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Inx.Module.Contacts.Core.Bo.User;
using Inx.Utility.Models;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Data.DbContext;
using Inx.Utility.Extentions;
using Inx.Module.Contacts.Data.Entity.User;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Inx.Core.Base;
using Inx.Utility.Utility;

namespace Inx.Module.Contacts.Core.Service.Contacts
{
    public class ContactService : ContactBaseService, IContactService
    {
        private readonly IUnitOfWokContacts uow;
        public ContactService(IUnitOfWokContacts _uow, ICoreInjector inject) : base(inject)
        {
            uow = _uow;
        }
        public async Task<ContactBo> Create(Request<ContactBo> req)
        {
            try
            {
                var workings = req.Item.WorkingInfo.MapObject<WorkingInfoBo, WorkingInfo>();
                var contacts = req.Item.ContactInfo.MapObject<ContactInfoBo, ContactInfo>();
                var personalresult = await uow.PersonInfoRepository.Create(new Request<PersonInfo>()
                {
                    Item = req.Item.PersonInfo.MapObject<PersonInfoBo, PersonInfo>(),
                    TenantId = req.TenantId,
                    UserId = req.UserId
                });
                workings.PersonId = contacts.PersonId = personalresult.Id;
                var workingInfos = await uow.WorkingInfoRepository.Create(new Request<WorkingInfo>()
                {
                    Item = workings,
                    TenantId = req.TenantId,
                    UserId = req.UserId
                });
                var contactInfo = await uow.ContactInfoRepository.Create(new Request<ContactInfo>()
                {
                    Item = contacts,
                    TenantId = req.TenantId,
                    UserId = req.UserId
                });
                await uow.SaveAsync();
                await base.RemoveContactCacheAsync(req.TenantId);
                return new ContactBo
                {
                     WorkingInfo = workingInfos.MapObject<WorkingInfo, WorkingInfoBo>(),
                     ContactInfo = contactInfo.MapObject<ContactInfo, ContactInfoBo>(),
                     PersonInfo = personalresult.MapObject<PersonInfo,PersonInfoBo>()
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.PersonInfoRepository.Delete(req);
                await uow.SaveAsync();
                await base.RemoveContactCacheAsync(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Restore(Request<int> req)
        {
            try
            {
                var result = await uow.PersonInfoRepository.Read(p => p.Id == req.Item && p.TenantId == req.TenantId);
                result.RecordState = Enums.RecordStatus.Active;
                result.EditedOn = DateTime.UtcNow;
                result.EditedById = req.UserId;
                await uow.SaveAsync();
                await RemoveContactCacheAsync(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<ContactBo> Read(Request<int> req)
        {
            try
            {
                var res = (await uow.PersonInfoRepository.Read(req)).MapObject<PersonInfo, PersonInfoBo>();
                res.ProfileImage = ImagePath(Enums.FileType.ProfileImageDefault, res.ProfileImage);
                return new ContactBo
                {

                    PersonInfo = res,
                    ContactInfo = (await uow.ContactInfoRepository.Read(p => p.PersonId == req.Item, req.TenantId))
                        .MapObject<ContactInfo, ContactInfoBo>(),
                    WorkingInfo = (await uow.WorkingInfoRepository.Read(p => p.PersonId == req.Item, req.TenantId))
                        .MapObject<WorkingInfo, WorkingInfoBo>()
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<ContactsViewResult>> Search(Search request)
        {
            try
            {
                var search = request.SearchTerm.ToJsonObject<ContactSearch>();
                Func<string> getCacheKey = () =>
                {
                    search.ContactCategoryTypes.Sort();
                    var arrString = Array.ConvertAll(search.ContactCategoryTypes.ToArray(),
                        element => element.ToString());
                    var prefix = new List<string>
                    {
                        request.TenantId.ToString(),
                        Bo.Enums.CacheKey.Contact.ToString(),
                        request.Skip.ToString(),
                        request.Take.ToString()
                    };
                    prefix.AddRange(arrString.ToList());
                    var key = CacheKeyGen(prefix);
                    return key;
                };
                var cachekey = getCacheKey();
                if (search.IsDefault)
                {
                    Func<PageList<ContactsViewResult>> getCache = () => cache.Get<PageList<ContactsViewResult>>(cachekey);
                    var cacheresult = getCache();
                    if (!cacheresult.IsNull())
                    {
                        return cacheresult;
                    }
                }
                if (search.IsNull())
                {
                    search = new ContactSearch();
                }
                var query = (from person in uow.PersonInfoRepository.TableAsNoTracking
                    join contact in uow.ContactInfoRepository.TableAsNoTracking on person.Id equals contact.PersonId
                    join works in uow.WorkingInfoRepository.TableAsNoTracking on person.Id equals works.PersonId
                    join contactcategory in uow.ContactCategoryRepository.TableAsNoTracking on person.ContactCategoryId
                        equals contactcategory.Id
                    where (person.FirstName.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                           person.LastName.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                           contact.Email.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                           contact.Phone.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                           works.Designation.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                           works.Company.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase))
                          && person.TenantId == request.TenantId
                          && search.ContactCategoryTypes.Contains(person.ContactCategoryId)
                          && person.RecordState == Enums.RecordStatus.Active
                             select new
                    {
                        person.Id,
                        Name = string.Join(" ", person.FirstName, person.LastName),
                        ProfileImage = ImagePath(Enums.FileType.ProfileImageThumbnail, person.ProfileImage),
                        //---
                        person.CreatedOn,
                        person.DOB,
                        person.Identity,
                        person.MartialStatus,
                        age = person.DOB.GetAge(),
                        membersince = person.CreatedOn.GetDeferent(DateTime.Today),
                        //===
                        contact.Email,
                        contact.Phone,
                         dob = person.DOB,
                        //-----
                        contact.Address,
                        contact.City,
                        Telephone = contact.Telephone ?? string.Empty,
                        Zip = contact.Zip ?? string.Empty,
                        //=====
                        works.Designation,
                        works.Company,
                        workaddress = works.Address ?? string.Empty,
                        workphone = works.Phone ?? string.Empty,
                        worktelephone = works.Telephone ?? string.Empty,
                        contactcategory = contactcategory.Name,
                    }).AsNoTracking().AsQueryable();

                if (string.IsNullOrEmpty(request.OrderByTerm))
                {
                    query = query.OrderByDescending(p => p.Id);
                }
                else
                {
                    var term = request.OrderByTerm.Split(':')[0].TrimAndToLower();
                    var orderby = request.OrderByTerm.Split(':')[1];

                    switch (term)
                    {
                        case "name":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Name)
                                : query.OrderByDescending(p => p.Name);
                            break;
                        case "company":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Company)
                                : query.OrderByDescending(p => p.Company);
                            break;
                        case "designation":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Designation)
                                : query.OrderByDescending(p => p.Designation);
                            break;
                        case "email":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Email)
                                : query.OrderByDescending(p => p.Email);
                            break;
                        case "telephone":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Telephone)
                                : query.OrderByDescending(p => p.Telephone);
                            break;
                        case "category":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.contactcategory)
                                : query.OrderByDescending(p => p.contactcategory);
                            break;
                        case "address":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Address)
                                : query.OrderByDescending(p => p.Address);
                            break;
                        case "mobile":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Phone)
                                : query.OrderByDescending(p => p.Phone);
                            break;
                        case "identity":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Identity)
                                : query.OrderByDescending(p => p.Identity);
                            break;
                        case "age":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.age)
                                : query.OrderByDescending(p => p.age);
                            break;
                        case "dob":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.dob)
                                : query.OrderByDescending(p => p.dob);
                            break;
                        case "martialstatus":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.MartialStatus)
                                : query.OrderByDescending(p => p.MartialStatus);
                            break;
                        case "createdOn":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.CreatedOn)
                                : query.OrderByDescending(p => p.CreatedOn);
                            break;
                        case "city":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.City)
                                : query.OrderByDescending(p => p.City);
                            break;
                        case "zip":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Zip)
                                : query.OrderByDescending(p => p.Zip);
                            break;
                        case "workaddress":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.workaddress)
                                : query.OrderByDescending(p => p.workaddress);
                            break;
                        case "workphone":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.workphone)
                                : query.OrderByDescending(p => p.workphone);
                            break;
                        case "worktelephone":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.worktelephone)
                                : query.OrderByDescending(p => p.worktelephone);
                            break;
                        default:
                            throw new ArgumentException();
                    }
                }
                var itemcount = await query.CountAsync();
                query = (request.Take != 0) ? query.Skip(request.Skip).Take(request.Take) : query;
                var result = await query.ToListAsync();
                var fresut = result.Select(item => new ContactsViewResult
                {

                    Id = item.Id,
                    ProfileImage = item.ProfileImage,

                    Identity = item.Identity,
                    MartialStatus = item.MartialStatus.GetDescription(),
                    Name = item.Name,
                    createdOn = item.CreatedOn,
                    dob = item.dob,
                   // MemberSince = item.membersince,
                    Age = item.age,
                    // contacts
                    Email = item.Email,
                    Phone = item.Phone,
                    Address = item.Address,
                    City = item.City,
                    Zip = item.Zip,
                    Telephone = item.Telephone,
                    //working
                    Designation = item.Designation,
                    Company = item.Company,
                    WorkAddress = item.workaddress,
                    WorkPhone = item.workphone,
                    WorkTelephone = item.worktelephone,

                    ContactCategory = item.contactcategory,

                }).ToList();
                var pageresult = new PageList<ContactsViewResult>
                {
                    Skip = request.Skip,
                    Take = request.Take,
                    Items = fresut,
                    TotalRecodeCount = itemcount
                };
                if (search.IsDefault)
                {
                    await cache.SetAsync(cachekey, pageresult);
                }
                return pageresult;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<ContactsViewResult>> SearchArchive(Search request)
        {
            try
            {
                var search = request.SearchTerm.ToJsonObject<ContactSearch>();
                if (search.IsNull())
                {
                    search = new ContactSearch();
                }
                var query = (from person in uow.PersonInfoRepository.TableAsNoTracking
                             join contact in uow.ContactInfoRepository.TableAsNoTracking on person.Id equals contact.PersonId
                             join works in uow.WorkingInfoRepository.TableAsNoTracking on person.Id equals works.PersonId
                             join contactcategory in uow.ContactCategoryRepository.TableAsNoTracking on person.ContactCategoryId
                                 equals contactcategory.Id
                             where (person.FirstName.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                                    person.LastName.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                                    contact.Email.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                                    contact.Phone.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                                    works.Designation.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase) ||
                                    works.Company.StartsWith(search.Search, StringComparison.OrdinalIgnoreCase))
                                   && person.TenantId == request.TenantId
                                   && person.RecordState == Enums.RecordStatus.Delete
                             select new
                             {
                                 person.Id,
                                 Name = string.Join(" ", person.FirstName, person.LastName),
                                 ProfileImage = ImagePath(Enums.FileType.ProfileImageThumbnail, person.ProfileImage),
                                 //---
                                 createdOn =person.CreatedOn,
                                 dob =person.DOB,
                                 person.Identity,
                                 person.MartialStatus,
                                 age = person.DOB.GetAge(),
                                 membersince = person.CreatedOn.GetDeferent(DateTime.Today),
                                 //===
                                 contact.Email,
                                 contact.Phone,
                                 //-----
                                 contact.Address,
                                 contact.City,
                                 Telephone = contact.Telephone ?? string.Empty,
                                 Zip = contact.Zip ?? string.Empty,
                                 //=====
                                 works.Designation,
                                 works.Company,
                                 works.Country,
                                 workaddress = works.Address ?? string.Empty,
                                 workphone = works.Phone ?? string.Empty,
                                 worktelephone = works.Telephone ?? string.Empty,
                                 contactcategory = contactcategory.Name,
                             }).AsNoTracking().AsQueryable();

                if (string.IsNullOrEmpty(request.OrderByTerm))
                {
                    query = query.OrderByDescending(p => p.Id);
                }
                else
                {
                    var term = request.OrderByTerm.Split(':')[0].TrimAndToLower();
                    var orderby = request.OrderByTerm.Split(':')[1];
                    switch (term)
                    {
                        case "name":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Name)
                                : query.OrderByDescending(p => p.Name);
                            break;
                        case "company":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Company)
                                : query.OrderByDescending(p => p.Company);
                            break;
                        case "designation":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Designation)
                                : query.OrderByDescending(p => p.Designation);
                            break;
                        case "email":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Email)
                                : query.OrderByDescending(p => p.Email);
                            break;
                        case "phone":
                            query = @orderby.IsAsc()
                                ? query.OrderBy(p => p.Phone)
                                : query.OrderByDescending(p => p.Phone);
                            break;
                        default:
                            throw new ArgumentException();
                    }
                }
                var itemcount = await query.CountAsync();
                query = (request.Take != 0) ? query.Skip(request.Skip).Take(request.Take) : query;
                var result = await query.ToListAsync();
                var fresut = result.Select(item => new ContactsViewResult
                {

                    Id = item.Id,
                    ProfileImage = item.ProfileImage,

                    Identity = item.Identity,
                    MartialStatus = item.MartialStatus.GetDescription(),
                    Name = item.Name,
                    createdOn = item.createdOn,
                    dob = item.dob,
                   // MemberSince = item.membersince,
                    Age = item.age,
                    // contacts
                    Email = item.Email,
                    Phone = item.Phone,
                    Address = item.Address,
                    City = item.City,
                    Zip = item.Zip,
                    Telephone = item.Telephone,
                    //working
                    Designation = item.Designation,
                    Company = item.Company,
                    Country = item.Country,
                    WorkAddress = item.workaddress,
                    WorkPhone = item.workphone,
                    WorkTelephone = item.worktelephone,

                    ContactCategory = item.contactcategory,

                }).ToList();
                var pageresult = new PageList<ContactsViewResult>
                {
                    Skip = request.Skip,
                    Take = request.Take,
                    Items = fresut,
                    TotalRecodeCount = itemcount
                };
                return pageresult;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        #region update
        [Description("update contact/person info")]
        public async Task Update(Request<PersonInfoBo> item)
        {
            try
            {
                await uow.PersonInfoRepository.UpdateAndSave(item.MapRequestObject<PersonInfoBo, PersonInfo>(item.Item.Id));
                await RemoveContactCacheAsync(item.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        [Description("update contact/contact  info")]
        public async Task Update(Request<ContactInfoBo> item)
        {
            try
            {
                await uow.ContactInfoRepository.UpdateAndSave(item.MapRequestObject<ContactInfoBo, ContactInfo>(item.Item.Id));
                await base.RemoveContactCacheAsync(item.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            } 
        }
        [Description("update contact/working info  info")]
        public async Task Update(Request<WorkingInfoBo> item)
        {
            try
            {
                if (item.Item.Id.IsZero())
                {
                    await uow.WorkingInfoRepository.CreateAndSave(item.MapRequestObject<WorkingInfoBo, WorkingInfo>());
                }
                else
                {
                    await uow.WorkingInfoRepository.UpdateAndSave(item.MapRequestObject<WorkingInfoBo, WorkingInfo>(item.Item.Id));
                }
                await base.RemoveContactCacheAsync(item.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        #endregion

        #region not implemented 

        public Task<PageList<ContactBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }

        public Task Update(Request<ContactBo> req)
        {
            throw new NotImplementedException();
        }

        public Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
