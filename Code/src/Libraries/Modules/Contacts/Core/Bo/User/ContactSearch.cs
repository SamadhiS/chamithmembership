﻿using System.Collections.Generic;

namespace Inx.Module.Contacts.Core.Bo.User
{
    public class ContactSearch
    {
        public List<int> ContactCategoryTypes { get; set; }
        public string Search { get; set; }
        public bool IsDefault { get; set; } = false;
        public ContactSearch()
        {
            Search = string.Empty;
            ContactCategoryTypes = new List<int>();
        }
    }
}
