﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo;
using Inx.Module.Contacts.Core.Bo.User;
using Inx.Module.Contacts.Core.Bo.Utlity;
using Inx.Module.Contacts.Core.Service.Utility.Interface;
using Inx.Module.Contacts.Data.DbContext;
using Inx.Module.Contacts.Data.Entity.Utility;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Contacts.Core.Service.Utility
{
    public class TableTemplateService : ContactBaseService, ITableTemplateService
    {
        private readonly IUnitOfWokContacts uow;
        public TableTemplateService(IUnitOfWokContacts _uow, ICoreInjector inject) :base(inject)
        {
            uow = (IUnitOfWokContacts) _uow;
        }

        public async Task<TableTemplateBo> Create(Request<TableTemplateBo> req)
        {
            try
            {
                var item = await uow.TableTemplateRepository.CreateAndSave(
                    req.MapRequestObject<TableTemplateBo,TableTemplate>());
                await RemoveCache(req.TenantId,(int)req.Item.TemplateType);
                return item.MapObject<TableTemplate, TableTemplateBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public List<string> GetColomHeaders(TableTemplateTypes request)
        {
            switch (request)
            {
                case TableTemplateTypes.ContactView:
                    return new ContactsViewResult().GetProperties(new List<string>() { "id", "name", "email" });
                default:
                    throw new ArgumentException();
            }
        }

        public async Task<List<string>> Read(Request<int> req)
        {
            var cachekey = CacheKeyGen(new[]
                {Enums.CacheKey.ContactTableTemplate.ToString(), req.TenantId.ToString(), req.Item.ToString()}.ToList());
            var result = await cache.GetAsync<List<string>>(cachekey);
            if (!result.IsNull())
            {
                return result;
            }
            try
            {
                result = (await uow.TableTemplateRepository.Read(p => p.TemplateType == req.Item, req.TenantId)).Keys
                    .ToJsonObject<List<string>>();
                await cache.SetAsync(cachekey, result);
                return result;
            }
            catch (RecordNotFoundException)
            {
                result = new List<string>();
                await cache.SetAsync(cachekey, result);
                return result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Update(Request<TableTemplateBo> req)
        {
            try
            {
                var result = await uow.TableTemplateRepository.Read(p => p.TemplateType == (int) req.Item.TemplateType,
                    req.TenantId);
                result.Keys = req.Item.Keys;
                result.EditedOn = DateTime.UtcNow;
                result.EditedById = req.UserId;
                await uow.SaveAsync();
                await RemoveCache(req.TenantId,(int)req.Item.TemplateType);
            }
            catch (RecordNotFoundException)
            {
                await Create(req);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        private async Task RemoveCache(int tenantid,int tabletype)
        {
            await cache.Remove(CacheKeyGen(new List<string> { Enums.CacheKey.ContactTableTemplate.ToString(), tenantid.ToString(), tabletype.ToString() }));
        }

        #region ignore

        [Obsolete]
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            throw new NotImplementedException();
        }

        [Obsolete]
        public async Task Delete(Request<int> req)
        {
            throw new NotImplementedException();
        }

        public Task<PageList<TableTemplateBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }

        Task<TableTemplateBo> IBaseService<TableTemplateBo>.Read(Request<int> req)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
