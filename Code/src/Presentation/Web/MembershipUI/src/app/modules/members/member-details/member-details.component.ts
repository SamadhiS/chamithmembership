import { MemberStatusSin, MemberStatusTam } from '../../core/enums/memberStatus';
import { LookupService } from '../../core/services/lookup.service';
import { ToastsManager } from 'ng2-toastr';
import { SortBaseService } from '../../core/services/sort-base.service';
import { Router } from '@angular/router';
import { PaginationModel } from '../../core/models/paginationModel';
import { MemberSearchModel } from '../models/memberSearchModel';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { TableTemplateService } from '../../core/services/table-template.service';
import { TableTemplate } from '../../core/enums/tableTemplate';
import { MemberStatusEng } from '../../core/enums/memberStatus';
import { MembersService } from '../../core/services/members.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { MembershipState } from '../../core/enums/membershipState';
import { TranslateService } from '@ngx-translate/core';
import { Z_MEM_ERROR } from 'zlib';
import { MemberType } from '../../core/enums/memberType';

@Component({
  selector: 'membership-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.scss']
})
export class MemberDetailsComponent implements OnInit {

  searchTerm$ = new Subject<string>();
  memberSearch = new MemberSearchModel();
  paginationModel = new PaginationModel(20, '', false);

  memberCount = 0;
  language = "";
  filter = "All"
  isBlocked = false;
  show = false;
  memberDetails = [];
  filters = [];
  categories = []
  memberCategoryIds = [];
  selectedMemberColumns = [];
  memberStatusE: string = MemberStatusEng.all;
  memberStatusS: string = MemberStatusSin.allS;
  memberStatusT: string = MemberStatusTam.allT;
  tableTemplateType = TableTemplate.MemberTableTemplate;

  membershipState = MembershipState;

  reportFormats = ["PDF", "CSV"];
  maxSize = 10;

  isMemberLevelZero = false;

  constructor(private router: Router, private memberService: MembersService, private sortBaseService: SortBaseService,
    private tableTemplateService: TableTemplateService, private blockUiService: BlockUiService, translate: TranslateService,
    public toastr: ToastsManager, vcr: ViewContainerRef, private lookUpService: LookupService) {

    this.toastr.setRootViewContainerRef(vcr);
    this.searchTerm$.debounceTime(300)
      .distinctUntilChanged().subscribe(data => {
        if (data != "" && data) {
          this.isBlocked = true;
          this.memberSearch.contactCategoryTypes = this.memberCategoryIds;
          this.memberSearch.search = data;
          this.isBlocked = false;
        }
        else {
          this.memberSearch.search = '';
          this.isBlocked = false;
        }
        this.isBlocked = true;
        this.getMembers(MembershipState.All);
        this.isBlocked = false;
      })
  }

  ngOnInit() {
    this.getMembers(MembershipState.All);
    this.getFilterOption();
  }

  getFilterOption() {
    this.language = localStorage.getItem('Language');
    if (this.language == "en") {
      this.filter = "All"
      this.filters = ["All", "Active", "Inactive", "Pending", "Terminated"];
    }
    if (this.language == "sh") {
      this.filter = "සියලු"
      this.filters = ["සියලු", "සක්රීයයි", "අක්රියයි", "විභාග වෙමින්", "අවලංගුයි"];
    }
    if (this.language == "tm") {
      this.filter = "அனைத்து"
      this.filters = ["அனைத்து", "செயலில்", "செயல்படா", "நிலுவையில்", "நிறுத்தப்பட்டது"];
    }
  }

  getMembers(status) {
    this.isBlocked = true;
    this.memberSearch.status = status;
    this.tableTemplateService.init(TableTemplate.MemberTableTemplate);
    this.memberService.getMembers(this.paginationModel.skip, this.paginationModel.take, JSON.stringify(this.memberSearch), this.paginationModel.orderByTerm)
      .subscribe(allmembers => {
        this.memberDetails = allmembers.result.items;
        this.paginationModel.totalRecords = allmembers.result.totalRecodeCount;
        this.memberCount = allmembers.result.totalRecodeCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toastr.error('Error in Loading member', 'Oops');
        });
  }

  archiveMember(memberId) {
    this.memberService.archiveMember(memberId)
      .subscribe(allCategories => {
        this.toastr.success('Deleted Successfully!', 'Success');
      },
        error => {
          this.toastr.error('Error in Deleting Member', 'Oops');
        });
  }

  goToNewMember() {
    this.router.navigate(["/members/createMember"])
  }

  viewMember(id) {
    this.router.navigate(["/members/memberprofile", id])
  }

  filterByCategory(id) {
    this.memberSearch.contactCategoryTypes = [];
    this.memberSearch.contactCategoryTypes.push(id)
    this.getMembers(MembershipState.All);
  }

  public pageChanged(event: any): void {
    this.paginationModel.skip = (event.page - 1) * this.paginationModel.take;
    this.paginationModel.currentPage = event.page;
    this.getMembers(MembershipState.All);
  }

  changeSorting(columnName): void {
    this.sortBaseService.changeSorting(columnName, this.paginationModel)
    this.getMembers(MembershipState.All);
  }

  filterMembers(filter) {
    this.filter = filter
    if (filter.toLowerCase() == MemberStatusEng.inactive || filter == MemberStatusSin.inactiveS || filter == MemberStatusTam.inactiveT) {
      this.getMembers(MembershipState.Inactive);
    }
    if (filter.toLowerCase() == MemberStatusEng.active || filter == MemberStatusSin.activeS || filter == MemberStatusTam.activeT) {
      this.getMembers(MembershipState.Active);
    }
    if (filter.toLowerCase() == MemberStatusEng.all || filter == MemberStatusSin.allS || filter == MemberStatusTam.allT) {
      this.getMembers(MembershipState.All);
    }
    if (filter.toLowerCase() == MemberStatusEng.pending || filter == MemberStatusSin.pendingS || filter == MemberStatusTam.pendingT) {
      this.getMembers(MembershipState.Pending);
    }
    if (filter.toLowerCase() == MemberStatusEng.terminated || filter == MemberStatusSin.terminatedS || filter == MemberStatusTam.terminatedT) {
      this.getMembers(MembershipState.Terminated);
    }
  }




  checkMemberLevels(name) {
    if (name == 'member') {
      this.router.navigate(["/members/createMember/", MemberType.member]);
    } else if (name == 'organization') {
      this.router.navigate(["/members/createMember/", MemberType.organization]);
    }

    // this.memberService.getAllMemberLevel(0, 0)
    //   .subscribe(memberLevels => {
    //     this.isMemberLevelZero = memberLevels.result.items.totalRecodeCount == 0 ? true : false;
    //     if (!this.isMemberLevelZero && name == 'member')
    //       this.router.navigate(["/members/createMember"]);
    //     else if (!this.isMemberLevelZero && name == 'organization')
    //       this.router.navigate(["/organization/createOrganization"]);
    //     else
    //       console.log('MEMBER LEVEL ZERO... SO CANT ROUTE');
    //   });
  }
}
