﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models.Comman.User;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Utility;

namespace Inx.Module.Contacts.Data.Entity.User
{
    [Table("PersonInfo", Schema = Constraints.Schema)]
    public class PersonInfo : AuditableEntity, IPersonInfo
    {
        [Required,StringLength(DbConstraints.NameLength)]
        public string FirstName { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string LastName { get; set; }
        [Required, Column(TypeName = "Date")]
        public DateTime DOB { get; set; }
        public Enums.MartialStatus MartialStatus { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string Identity { get; set; }
        public int ContactCategoryId { get; set; }
        [ForeignKey("ContactCategoryId")]
        public ContactCategory ContactCategory { get; set; }
        [NotMapped,Obsolete]
        public new string Name { get; set; }
        [Required,StringLength(100)]
        public string ProfileImage { get; set; } = Constants.ProfileNoImage;
    }
}
