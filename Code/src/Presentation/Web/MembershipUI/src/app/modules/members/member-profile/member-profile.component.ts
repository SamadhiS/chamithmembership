import { LevelService } from './../../core/services/level.service';
import { TimeLine } from '../models/memberTimelineModel';
import { ContactModel } from '../models/contactModel';
import { WorkModel } from '../models/workModel';
import { MemberModel } from '../models/memberModel';
import { PaymentViewModel } from '../../payments/models/paymentViewModel';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { PaginationModel } from '../../core/models/paginationModel';
import { SubscribedLevelInfo } from '../models/subscribedLevelInfo';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MembersService } from '../../core/services/members.service';
import { MemberSubscriptionService } from '../services/member-subscription.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BlockUiService } from '../../core/services/block-ui.service';
import { PaymentService } from '../../core/services/payment.service';
import { ToastsManager } from 'ng2-toastr';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { MembershipState } from '../../core/enums/membershipState';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TerminationModel } from '../models/terminationModel';
import { MemberSubscriptionModel } from '../models/memberSubscriptionModel';
import { PaymentStates } from '../../core/enums/paymentState';
import { LevelInfoModel } from '../models/levelInfoModel';
import { MembershipLevelStates } from '../../core/enums/membershipLevelState';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-member-profile',
  templateUrl: './member-profile.component.html',
  styleUrls: ['./member-profile.component.scss']
})

export class MemberProfileComponent implements OnInit {

  paginationModel = new PaginationModel(20, '', false);
  IsRequred = true;
  isBlocked = false;
  isDocumentFormSubmitted = false;
  Imageurl: any;
  fileDocList: FileList;
  documentForm: FormGroup;
  timeline= new  TimeLine();
  paymentViewModel = new PaymentViewModel();
  documentName = "Choose Documents to Upload...";
  memberId = 0;
  subscriptionId = 0;
  memberInformation = new MemberModel();
  documentId = 0;
  DocumentCount = 0;
  DocumentList = [];
  contactInformation = new ContactModel();
  workInformation = new WorkModel();
  subscriptionInformation = [];
  membershipID :string;

  //Membership 
  subscription: number = 0;
  selectedSubscription: number = 0;
  membershipState = MembershipState;
  paymentState = PaymentStates;
  isMembershipFormSubmitted = false;
  isDisplayLevels = false;
  isLevelDropDownClicked = false;
  isPackageUtilized = false;
  memberLevels = [];
  levelInfo = new LevelInfoModel();
  terminationModel = new TerminationModel();
  subscribedLevelInfo = new SubscribedLevelInfo();
  memberSubscription = new MemberSubscriptionModel();
  IsSubscriptionChange = '';
  orgName = '';
  currency ='';

  @ViewChild('terminateNoteModal') public terminateNoteModal: ModalDirective;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(private fb: FormBuilder, private memberService: MembersService,
    private memberSubscriptionService: MemberSubscriptionService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private blockUiService: BlockUiService,
    private paymentService: PaymentService,
    private levelService: LevelService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.documentForm = this.getDocumentForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberId = params['id'];
      if (this.memberId && this.memberId != 0) {
        this.getTimeLine();
        this.getMember();
        this.getDocuments();
        this.getMemberPayment();
        this.getmemberLevelData();
      }
      else {
        this.memberId = 0;
        this.Imageurl = 'assets/img/user.svg';
      }
    });
  }

  //Member Information
  EditMember() {
    if(this.memberInformation.isaMember){
      this.router.navigate(["/members/updateMember/", this.memberId])      
    }
    else{
      this.router.navigate(["/organization/updateOrganization/", this.memberId])            
    }
  }
  getMember() {
    this.isBlocked = true;
    this.memberService.getMemberById(this.memberId).subscribe(res => {
      this.memberInformation = res.result.item.memberInfo;
      this.contactInformation = res.result.item.contactInfo;
      this.workInformation = res.result.item.workingInfo;
      this.subscription = (res.result.item.subscription);
      var prefix  = localStorage.getItem('Prefix');
      var ID = this.memberId;
       this.membershipID = prefix + ID;
      if (this.subscription != 0) {
        this.getSubscribedMembershipData(this.memberId);
      }
      this.isBlocked = false;
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.loadError('Member'), toasterHeadings.Error);
      })
  }

  //TimeLine
  getTimeLine(){
    let tenantObj = localStorage.getItem('tenantObj');
    let tenantJson = JSON.parse(tenantObj);
    this.orgName = tenantJson.name
    this.memberService.getTimeLineById(this.memberId) 
    .subscribe(res => {
      this.timeline = (res.result);  
    },
      error => {
        this.toastr.error(ToasterMessages.loadError('TimeLine'), toasterHeadings.Error);
      })
  }


  viewInvoice(paymentId){
    
    this.router.navigate(["/payments/invoice",paymentId,this.memberId])
  }

  viewSubscription(subscriptionId){
  
    this.router.navigate(["/members/memberlevelInformation",subscriptionId])
  }

  //Subscription
  getSubscribedMembershipData(memberId) {
    this.currency  = localStorage.getItem('Currency');

    this.memberSubscriptionService.getSubscribedMembershipData(memberId)
      .subscribe(res => {
        this.subscribedLevelInfo = (res.result);
      },
        error => {
          this.toastr.error(ToasterMessages.loadError('Subscription'), toasterHeadings.Error);
        })
  }

  //Payment
  getMemberPayment() {
    this.paymentService.getMemberPayment(this.memberId).subscribe(res => {
      this.paymentViewModel = res.result;
    }, error => {

    })
  }

  //Document
  fileDocUpload(event) {
    this.fileDocList = event.target.files;
    this.documentName = this.fileDocList[0].name
    this.IsRequred = false;
  }
  documentsave() {
    this.IsRequred = true;
    this.isDocumentFormSubmitted = true;
    this.isBlocked = true;
    var docObj = { MemberId: this.memberId }
    this.memberService.saveDocument(this.fileDocList, docObj).subscribe(res => {
      this.toastr.success(ToasterMessages.save('Document'), toasterHeadings.Success);
      this.isBlocked = false;
      this.documentName = "Choose Documents to Upload...";
      this.getDocuments();
    }, error => {
      this.isBlocked = false;
      this.documentName = "Choose Documents to Upload...";
      this.toastr.error(ToasterMessages.saveError('Document'), toasterHeadings.Error);
    }, () => {
    })
  }
  deleteId(id) {
    this.documentId = id
  }
  private getDocumentForm() {
    return this.fb.group({
      file: ["", [Validators.required]],
    })
  }
  getDocuments() {
    this.isBlocked = true;
    this.memberService.getDocumentsById(this.paginationModel.skip, this.paginationModel.take, this.memberId)
      .subscribe(allmembers => {
        this.isBlocked = false;
        this.DocumentList = allmembers.result.item.items;
        this.DocumentCount = allmembers.result.item.totalRecodeCount;
      },
        error => {
          this.isBlocked = false;
          this.toastr.error('Error in Loading Documents', 'Oops');
        }, () => {
        });
  }
  archiveDocument() {
    this.isBlocked = true;
    this.memberService.archiveDocument(this.documentId).subscribe(res => {
      this.isBlocked = false;
      this.toastr.success(ToasterMessages.delete('Document'), toasterHeadings.Success);
    },
      error => {
        this.isBlocked = false;
        this.toastr.success(ToasterMessages.delete('Document'), toasterHeadings.Success)
        this.getDocuments();
      })
  }

  //Membership 
  isShowTerminate() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Pending ||
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Active) {
      return true;
    }
    return false;
  }

  isShowActivateMembership() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Terminated) {
      return true;
    }
    else false;
  }

  isShowUpdateMembership() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Pending) {
      return true;
    }
    else false;
  }

  isShowRenewMembership() {
    if (this.memberId &&
      this.memberId != 0 &&
      this.subscription != 0 &&
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Inactive) {
      return true;
    }
    else false;
  }

  isDisplayMemberLevel() {
    if (this.memberId == 0 ||
      this.subscribedLevelInfo.membershipStatus == this.membershipState.Pending ||
      this.isDisplayLevels == true) {
      return true;
    }
    else false;
  }

  isDisplayMemberGroup() {
    if (this.memberId == 0) { return true }
    else { return false }
  }

  showLevels() {
    this.isDisplayLevels = true;
  }

  public hideTerminationModal(): void {
    this.terminateNoteModal.hide();
  }

  sendRenewalMail() {
    this.memberSubscriptionService.sendRenewalMail(this.memberId)
      .subscribe(res => {
        this.toastr.success(ToasterMessages.mailSent('Renewal'), toasterHeadings.Success);
      }, error => {
        this.toastr.error(ToasterMessages.mailSendError('Renewal'), toasterHeadings.Error);
      });
  }

  terminateMember() {
    this.terminationModel.MemberId = this.memberId;

    this.isBlocked = true;
    this.memberSubscriptionService.terminateSubscription(this.terminationModel)
      .subscribe(res => {
        this.isBlocked = false;
        this.toastr.success(ToasterMessages.terminate('Subscription'), toasterHeadings.Success);
        this.router.navigate(['/members']);
      },
        error => {
          this.isBlocked = false;
          this.toastr.error(ToasterMessages.terminate('Subscription'), toasterHeadings.Error);
        });
  }

  activateMembership() {
    this.memberSubscriptionService.activateMembership(this.memberId)
      .subscribe(res => {
        this.subscribedLevelInfo = (res.result);
        this.toastr.success(ToasterMessages.update('Subscription'), toasterHeadings.Success);
        this.router.navigate(['/members']);        
      },
        error => {
          this.toastr.error(ToasterMessages.updateError('Subscription'), toasterHeadings.Error);
        })
  }


  getmemberLevelData() {
    this.levelService.getAllMemberLevel(0, 0)
      .subscribe(memberLevels => {
        this.memberLevels = memberLevels.result.items;
      });
  }
  changeMembership() {
    this.IsSubscriptionChange = 'change';
    this.router.navigate(["/members/updateMember/", this.memberId, this.IsSubscriptionChange]);
  }

  renewMembership() {
    this.IsSubscriptionChange = 'renew';
    this.router.navigate(["/members/updateMember/", this.memberId, this.IsSubscriptionChange]);
  }
}
