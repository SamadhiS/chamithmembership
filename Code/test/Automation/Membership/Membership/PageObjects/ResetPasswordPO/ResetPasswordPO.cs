﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;

namespace Membership.PageObjects.ResetPasswordPO
{
    class ResetPasswordPO
    {

        public NgWebElement CurrentPassword(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCurrentPassword"));
        }
        public NgWebElement NewPassword(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtNewPassword"));
        }
        public NgWebElement ConfirmPassword(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtConfirmPassword"));
        }
        private NgWebElement Btn_ResetPassword(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnResetPassword"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ResetPassword_Click(NgWebDriver d)
        {
            this.Btn_ResetPassword(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
