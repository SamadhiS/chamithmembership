﻿using System.Threading.Tasks;

namespace Inx.Module.Membership.Core.Service.Settings.Interface
{
    public interface INotificationBackgroundProcess
    {
        Task AboveToExpire();
        Task HasExpired();
        Task IsPending();
    }
}
