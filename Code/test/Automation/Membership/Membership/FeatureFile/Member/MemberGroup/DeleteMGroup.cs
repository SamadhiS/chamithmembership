﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.MemberPO;
using System.Windows.Forms;
using System;
using System.Diagnostics;

namespace Membership.FeatureFile.Member.MemberGroup
{
    [Binding]
    public sealed class DeleteMGroup
    {

        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private MemberGroupPO groupPO = new MemberGroupPO();

        [Given(@"The application Page opens")]
        public void GivenTheApplicationPageOpens()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }


        [Then(@"Users enters the ""(.*)"" and ""(.*)""")]
        public void ThenUsersEntersTheAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"User click on SignIn")]
        public void ThenUserClickOnSignIn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Navigated to Homepage")]
        public void ThenUserNavigatedToHomepage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(5000);
        }

        [Then(@"User Directed to Members Section")]
        public void ThenUserDirectedToMembersSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);

        }

        [Then(@"User Navigate To Groups")]
        public void ThenUserNavigateToGroups()
        {
            groupPO.Btn_Group_Click(ngWebDriver);
            Thread.Sleep(2000);
        }


        [Then(@"Select a Member Group")]
        public void ThenSelectAMemberGroup()
        {
            groupPO.Btn_viewMembGrp(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"I click on the Archive button")]
        public void ThenIClickOnTheArchiveButton()
        {
            groupPO.Btn_Archive_MemGrp(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"Confirm the Archive")]
        public void ThenConfirmTheArchive()
        {
            groupPO.Btn_ArchYes_MemGrp(ngWebDriver).Click();
            Thread.Sleep(2000);
        }

        [Then(@"LOG Out")]
        public void ThenLOGOut()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }


    }
}
