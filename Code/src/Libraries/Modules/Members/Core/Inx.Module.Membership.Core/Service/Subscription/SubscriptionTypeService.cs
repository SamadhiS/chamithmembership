﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo;
using Inx.Module.Membership.Data.DbContext;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Data.Entity.Subscription;

namespace Inx.Module.Membership.Core.Service.Subscription
{
    public class SubscriptionTypeService : MemberBaseService, ISubscriptionTypeService
    {
        private readonly IUnitOfWokMembership uow;
        public SubscriptionTypeService(IUnitOfWokMembership _uow, ICoreInjector inject) : base(inject)
        {
            uow = (IUnitOfWokMembership) _uow;
        }

        public async Task<SubscriptionTypeBo> Create(Request<SubscriptionTypeBo> req)
        {
            try
            {
                var item = (await uow.SubscriptionTypeRepository.CreateAndSave(
                        req.MapRequestObject<SubscriptionTypeBo, SubscriptionType>()))
                    .MapObject<SubscriptionType, SubscriptionTypeBo>();
                await RemoveCache(req.TenantId);
                return item;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.SubscriptionTypeRepository.DeleteAndSave(req);
                await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<SubscriptionTypeBo>> Read(Search search)
        {
            try
            {
                var ckey = CacheKeyGen(new List<string> {Enums.CacheKey.SubscriptionType.ToString(), search.TenantId.ToString() });
                var result = await cache.GetAsync<PageList<SubscriptionTypeBo>>(ckey);
                return result.IsNull()
                    ? (await uow.SubscriptionTypeRepository.Read<SubscriptionType>(search))
                    .MapPageObject<SubscriptionType, SubscriptionTypeBo>()
                    .CacheSetAndGet(cache, ckey)
                    : result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<SubscriptionTypeBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.SubscriptionTypeRepository.Read(req))
                    .MapObject<SubscriptionType, SubscriptionTypeBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                var ckey = CacheKeyGen(new List<string> { Enums.CacheKey.SubscriptionType.ToCacheKeyValue(), req.TenantId.ToString() });
                var result = await cache.GetAsync<List<KeyValueListItem<int>>>(ckey);
                return result.IsNull()
                    ? (await uow.SubscriptionTypeRepository.ReadKeyValue<SubscriptionType>(req)).CacheSetAndGet(cache,
                        ckey)
                    : result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Update(Request<SubscriptionTypeBo> req)
        {
            try
            {
                await uow.SubscriptionTypeRepository.UpdateAndSave(
                    req.MapRequestObject<SubscriptionTypeBo, SubscriptionType>(req.Item.Id));
                await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        private async Task RemoveCache(int tenantid)
        {
            await cache.Remove(
                new List<string>
                {
                    CacheKeyGen(new List<string> {Enums.CacheKey.Subscription.ToString(), tenantid.ToString()}),
                    CacheKeyGen(new List<string> {Enums.CacheKey.SubscriptionType.ToString(), tenantid.ToString()}),
                    CacheKeyGen(new List<string> {Enums.CacheKey.SubscriptionType.ToCacheKeyValue(), tenantid.ToString()})
                }
            );
            await RemoveContactCacheAsync(tenantid);
        }
    }
}