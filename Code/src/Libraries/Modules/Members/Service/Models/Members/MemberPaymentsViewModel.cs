﻿namespace Inx.Module.Membership.Service.Models.Members
{
    public class MemberPaymentsViewModel
    {
        public int MemberSubscriptionId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}
