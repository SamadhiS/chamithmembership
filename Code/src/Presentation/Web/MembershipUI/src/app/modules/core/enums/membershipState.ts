export enum MembershipState {
    Active = 1,
    Inactive = 2,
    Pending = 3,
    Terminated = 4,
    All = 5
}