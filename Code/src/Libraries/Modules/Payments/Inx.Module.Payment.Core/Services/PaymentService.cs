﻿using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Core.Services.Interfaces;
using Inx.Module.Payment.Data.DbContext;
using Inx.Module.Payment.Data.Entity;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Inx.Utility.Utility.Enums;
using Microsoft.EntityFrameworkCore;
using Inx.Module.Membership.Data.DbContext;

namespace Inx.Module.Payment.Core.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IUnitOfWorkPayment uow;

        private readonly IUnitOfWokMembership uowm;
        private readonly IMemoryCacheManager cache;
        public PaymentService(IUnitOfWorkPayment _uow, IMemoryCacheManager _cache, IUnitOfWokMembership _uowm)
        {
            uow = (IUnitOfWorkPayment)_uow;
            cache = _cache;
            uowm = _uowm;
        }
        public async Task<PaymentBo> Create(Request<PaymentBo> req)
        {
            try
            {
                if (req.Item.PaymentState == PaymentState.Pending && req.Item.PaymentMasterId == 0)
                {
                  return (await uow.PaymentMasterRepository.CreateAndSave(req.MapRequestObject<PaymentBo, PaymentMaster>())).MapObject<PaymentMaster, PaymentBo>();
                }
                else if ((req.Item.PaymentState == PaymentState.Paid || req.Item.PaymentState == PaymentState.PartialyPaid) && req.Item.PaymentMasterId == 0)
                {

                    var paymentMaster = req.MapRequestObject<PaymentBo, PaymentMaster>();
                    paymentMaster.Item.PaymentInfoes.Add(new PaymentInfo
                    {
                        PaymentDate = DateTimeOffset.Now,
                        Amount = req.Item.Amount,
                        PaymentMasterId = paymentMaster.Id,
                        PaymentType = req.Item.PaymentType,
                        Name = string.Empty,
                        TenantId = req.TenantId,
                        CreatedById = req.UserId,
                    });

                    return (await uow.PaymentMasterRepository.CreateAndSave(paymentMaster)).MapObject<PaymentMaster, PaymentBo>();
                }
                else if (req.Item.PaymentMasterId != 0)
                {

                    (await uow.PaymentInfoRepository.CreateAndSave(req.MapRequestObject<PaymentBo, PaymentInfo>())).MapObject<PaymentInfo, PaymentBo>();
                    var paymentMaster = await uow.PaymentMasterRepository.ReadAsTracking(new Request<int>() { TenantId = req.TenantId, UserId = req.UserId, Item = req.Item.PaymentMasterId });
                    paymentMaster.PaymentState = PaymentState.Paid;
                    paymentMaster.EditedOn = DateTime.Now;
                    await uow.SaveAsync();
                    return paymentMaster.MapObject<PaymentMaster, PaymentBo>();

                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.PaymentMasterRepository.DeleteAndSave(req);

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<PaymentBo>> Read(Search search)
        {
            try
            {
                return (await uow.PaymentMasterRepository.Read<PaymentMaster>(search))
                    .MapPageObject<PaymentMaster, PaymentBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }


        public async Task<PaymentBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.PaymentMasterRepository.Read(req))
                   .MapObject<PaymentMaster, PaymentBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PaymentBo> ReadMemberPayment(Request<int> req)
        {
            try
            {
                return (await uow.PaymentMasterRepository.TableAsNoTracking.FirstOrDefaultAsync(a => a.MemberId == req.Id))
                   .MapObject<PaymentMaster, PaymentBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

 
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.PaymentMasterRepository.ReadKeyValue<PaymentMaster>(req);

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<PaymentBo> req)
        {
            try
            {
                await uow.PaymentMasterRepository.UpdateAndSave(req.MapRequestObject<PaymentBo, PaymentMaster>(req.Item.Id));

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        

    }
}
