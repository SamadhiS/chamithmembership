export class MemberLevelRenewalModel {
    id: number;
    IsRenewable: boolean;
    IsSendEmail: boolean;
    RenewalDays: number
}