import { TableTemplate } from './../../core/enums/tableTemplate';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ResponseContentType } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../core/services/base.service';
import { TenantInfoService } from '../../core/services/tenant-info.service';
import { MemberSearchModel } from '../../members/models/memberSearchModel';
import { ContactSearchModel } from '../../contacts/models/contactSearchModel';
import { MembersService } from '../../core/services/members.service';
import { ContactsService } from '../../core/services/contacts.service';
import { LookupService } from '../../core/services/lookup.service';
import 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import * as json2csv from 'json2csv';

@Injectable()
export class GenerateReportsService extends BaseService {

  skip: number = 0;
  take: number = 0;
  orderByTerm: string = "";

  memberSearch = new MemberSearchModel();
  contactSearch = new ContactSearchModel();

  memberDetails = [];
  contactDetails = [];
  contactCategoryIds = [];
  categories = [];

  constructor(
    private datePipe: DatePipe, private http: HttpClient,
    private tenantInfoService: TenantInfoService,
    private memberService: MembersService,
    private contactService: ContactsService,
    private lookUpService: LookupService) {
    super();
  }

  //PDF report Convertion
  pdfReportGenerate(selectedColumns: any[], memberStaus, type) {
    if (type == "Member") {
      this.memberSearch.status = memberStaus;
      this.memberService.getMembers(this.skip, this.take, JSON.stringify(this.memberSearch), this.orderByTerm)
        .subscribe(allmembers => {
          this.memberDetails = allmembers.result.item.items;
          this.pdf(this.memberDetails, selectedColumns, type);
        });
    }

    if (type == "Contact") {
      this.lookUpService.getContactTypes()
        .subscribe(res => {
          this.categories = res.result.item;
          this.contactCategoryIds = this.categories.map(item => item.value);
          this.contactSearch.contactCategoryTypes = this.contactCategoryIds

          this.contactService.getContacts(this.skip, this.take, JSON.stringify(this.contactSearch), this.orderByTerm)
            .subscribe(allcontact => {
              this.contactDetails = allcontact.result.item.items;
              this.pdf(this.contactDetails, selectedColumns, type);
            });
        });
    }
  }

  //CSV report Convertion
  csvReportGenerate(selectedColumns: any[], memberStaus, type) {
    if (type == "Member") {
      this.memberSearch.status = memberStaus;
      this.memberService.getMembers(this.skip, this.take, JSON.stringify(this.memberSearch), this.orderByTerm)
        .subscribe(allmembers => {
          this.memberDetails = allmembers.result.item.items;
          this.CSV(this.memberDetails, selectedColumns, type);
        });
    }
    if (type == "Contact") {
      this.lookUpService.getContactTypes()
        .subscribe(res => {
          this.categories = res.result.item;
          this.contactCategoryIds = this.categories.map(item => item.value);
          this.contactSearch.contactCategoryTypes = this.contactCategoryIds

          this.contactService.getContacts(this.skip, this.take, JSON.stringify(this.contactSearch), this.orderByTerm)
            .subscribe(allcontact => {
              this.contactDetails = allcontact.result.item.items;
              this.CSV(this.contactDetails, selectedColumns, type);
            });
        });
    }
  }

  //PDF report Convertion
  pdf(data, selectedColumns: any[], type) {
    
    let columns = [];
    let requiredFields = [];

    selectedColumns.forEach(element => {
      requiredFields.push(element.replace(/ +/g, ""))
    });
    columns = requiredFields.map(data => { return { "title": data, 'dataKey': this.toLowercaseFirstLetter(data) } });
    columns.unshift({ title: "Name", dataKey: "name" });
    columns.unshift({ title: "Email", dataKey: "email" });

    let doc = new jsPDF('landscape');
    let tenantInfo = this.tenantInfoService.loadTenantData();
    let orgName = tenantInfo.name;

    var replaceStr = /application\/octet-stream/gi;
    let img64String: any = localStorage.getItem("Image_Data");
    let imageData = null;

    //Adding a image to PDF
    if (img64String != null) {
      imageData = img64String.replace(replaceStr, "image/png");
      doc.addImage(imageData, 'png', 10, 10, 18, 18);
    }

    let date = this.datePipe.transform(new Date(), 'dd-MM-yyyy, h:mm a');
    let pageContent = function (data) {
      if (data.pageCount == 1) {
        doc.setFontSize(18);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        doc.text(type + ' Report of ' + orgName, 30, 20);
        doc.setFontSize(12);
        doc.text("Generated Date: " + date, 200, 20);
      }
      doc.setFontSize(10);
      doc.text("page " + data.pageCount, 265, 205);
    }
    doc.autoTable(columns, data, {
      addPageContent: pageContent,
      startY: 30,
      margin: 12,
    });
    doc.save(type + '_' + orgName + '_' + date + '.pdf');
  }

  //CSV Report Convertion
  CSV(data, selectedColumns, type) {
    let date = this.datePipe.transform(new Date(), 'dd-MM-yyyy, h:mm a');
    let requiredColumns = ['email', 'name'];
    selectedColumns.forEach(element => {
      var word = this.toLowercaseFirstLetter(element);
      requiredColumns.push(word.replace(/ +/g, ""))
     // requiredColumns.push();
    });

    let json2csv = require('json2csv').parse;
    let fields = requiredColumns;
    let options = { fields };

    let csv = json2csv(data, options);

    //Downloading the CSV
    var blob = new Blob([csv]);
    if (window.navigator.msSaveOrOpenBlob)  // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
      window.navigator.msSaveBlob(blob, 'Report_' + type + '_' + date + '.csv');
    else {
      var a = window.document.createElement("a");
      a.href = window.URL.createObjectURL(blob);
      a.download = 'Report_' + type + '_' + date + '.csv';
      document.body.appendChild(a);
      a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
      document.body.removeChild(a);
    }
  }

  //Converting image to 64Base 
  getBase64Image(imageUrl, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      var that = this;
      reader.onloadend = function () {
        callback(reader.result);
        localStorage.setItem('Image_Data', reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', imageUrl);
    xhr.responseType = 'blob';
    xhr.send();
  }

  //Making first letter of a string lowercase
  toLowercaseFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
  }
}
