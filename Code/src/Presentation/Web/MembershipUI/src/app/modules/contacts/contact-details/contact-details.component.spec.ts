import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactDetailsComponent } from './contact-details.component';
import { SharedModule } from '../../shared/shared.module';

import { SortBaseService } from '../../core/services/sort-base.service';
import { LookupService } from '../../core/services/lookup.service';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ContactsService } from '../../core/services/contacts.service';


describe('ContactDetailsComponent', () => {
  let component: ContactDetailsComponent;
  let fixture: ComponentFixture<ContactDetailsComponent>;

  let de: DebugElement;
  let el: HTMLElement;

  let lookupService: LookupService;
  let contactService: ContactsService;
  let sortBaseService: SortBaseService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  }

  let getContactsSpy: jasmine.Spy


  let mockContacts = {
    result: {
      items: [
        { name: 'isuru', company: "Inexis", designation: 'SSE' },
        { name: 'mahesh', company: "99x", designation: 'SSE' },
        { name: 'chamith', company: "embla", designation: 'SSE' }
      ]
    }
  }

  let mockContactTypes = {
    result: {
      item: [
        { value: 1, text: 'Staff' },
        { value: 2, text: "Member" },
        { value: 3, text: "Other" }
      ]
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactDetailsComponent],
      imports: [SharedModule, HttpClientModule],
      providers: [ContactsService, SortBaseService, LookupService, { provide: Router, useValue: mockRouter }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactDetailsComponent);
    component = fixture.componentInstance;

    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

    lookupService = fixture.debugElement.injector.get(LookupService);
    contactService = fixture.debugElement.injector.get(ContactsService);

    spyOn(contactService, "getContacts").and.returnValue(Observable.of(mockContacts))
    spyOn(lookupService, "getContactTypes").and.returnValue(Observable.of(mockContactTypes))

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should navigate to create contact after NEW CONTACT button click', () => {

    de = fixture.debugElement.query(By.css('#btnCreateContact'));
    de.triggerEventHandler("click", null);//2nd parameter is event object
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/contacts/createContact']);
  });

  it('should navigate to edit contact after click contact edit', () => {
    component.editContact(1)
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/contacts/createContact', 1]);
  });


  it('contacts table row count should be 3', () => {

    var rows = fixture.debugElement.queryAll(By.css("#tblContacts>tbody>tr"));
    expect(rows.length).toEqual(3)
  });

  it('equal wordds', () => {

    var rows = fixture.debugElement.queryAll(By.css("#tblContacts>tbody>tr"));
    expect("isuru").toEqual(mockContacts.result.items[0].name)
  });


  it('contacts table should display name as isuru company as inexis and designation as SSE', () => {

    el = fixture.debugElement.queryAll(By.css("#tblContacts>tbody>tr"))[0].query(By.css('td:nth-child(2)')).nativeElement;
    expect(el.innerText).toBe(mockContacts.result.items[0].name)
    el = fixture.debugElement.queryAll(By.css("#tblContacts>tbody>tr"))[0].query(By.css('td:nth-child(3)')).nativeElement;
    expect(el.innerText).toEqual(mockContacts.result.items[0].company)
    el = fixture.debugElement.queryAll(By.css("#tblContacts>tbody>tr"))[0].query(By.css('td:nth-child(4)')).nativeElement;
    expect(el.innerText).toBe(mockContacts.result.items[0].designation)
  });


});
