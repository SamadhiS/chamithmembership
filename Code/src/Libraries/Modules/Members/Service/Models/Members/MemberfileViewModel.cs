﻿using System;
using Inx.Service.Base.Models;
namespace Inx.Module.Membership.Service.Models.Members
{
    public class MemberFileViewModel : BaseViewModel
    {
        public int MemberId { get; set; }
        public string FileName { get; set; }
        public string FileSaveName { get; set; }
        public decimal Size { get; set; }
        public DateTime CreatedOn { get; set; }
        [Obsolete]
        public new string Name { get; set; }
        public string DownloadPath { get; set; }
        public void SetFileInfo(string filename,string filesavename,string size)
        {
            FileName = filename;
            FileSaveName = filesavename;
            Size = decimal.Parse(size);
        }
    }
}
