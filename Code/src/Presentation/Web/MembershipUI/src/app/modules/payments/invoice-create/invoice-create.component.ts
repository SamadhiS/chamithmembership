import { LevelService } from './../../core/services/level.service';
import { TimeLine } from '../../members/models/memberTimelineModel';
import { element } from 'protractor';
import { LookupService } from '../../core/services/lookup.service';
import { TenantInfoModel } from '../../settings/models/tenantInfoModel';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';

import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { PaymentModalComponent } from '../../shared/components/payment-modal/payment-modal.component';
import { MemberAllDataModel } from '../../members/models/memberAllDataModel';
import { PaymentService } from '../../core/services/payment.service';
import { PaymentModel } from '../models/paymentModel';
import { PaymentViewModel } from '../models/paymentViewModel';
import { PaymentStates } from '../../core/enums/paymentState';
import { MembersService } from '../../core/services/members.service';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import 'rxjs/add/operator/mergeMap';
import { TenantInfoService } from '../../core/services/tenant-info.service';



@Component({
  selector: 'membership-invoice-create',
  templateUrl: './invoice-create.component.html',
  styleUrls: ['./invoice-create.component.scss']
})
export class InvoiceCreateComponent implements OnInit {

  paymentId = 0;
  memberId = 0;
  paymentViewModel = new PaymentViewModel();
  timeLine = new TimeLine();
  tenanViewModel = new TenantInfoModel();
  countries = [];
  country = "";
  memberCountry = "";
  prefix = "";
  currency = "";
  membershipID: string;
  orgName: string;
  imageSource: string;
  member = new MemberAllDataModel();
  isBlocked = false;
  isRequested = false

  constructor(
    private paymentService: PaymentService,
    private levelService: LevelService,
    private memberService: MembersService,
    private tenantInfoService: TenantInfoService,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private toastr: ToastsManager, vcr: ViewContainerRef,
    private settingService: LookupService, ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.loadUserData();
    this.prefix = localStorage.getItem('Prefix');
    this.currency = localStorage.getItem('Currency');
    this.activateRoute.params.subscribe((params: Params) => {
      this.paymentId = params['paymentId'];
      this.memberId = params['memberId'];

      if (this.paymentId && this.paymentId != 0) {
        this.getMemberInfo();
        this.getTenantInfo();
        this.getCountry();
        this.getPayment();
      }

    });
  }

  loadUserData() {
    let tenantInfo = this.tenantInfoService.loadTenantData();
    this.orgName = tenantInfo.name;
    this.imageSource = tenantInfo.organizationLogo;
  }

  getPayment() {
    this.isBlocked = true;
    this.paymentService.getPayment(this.paymentId).subscribe(res => {

      this.paymentViewModel = res.result.item;
      if (this.paymentViewModel.paymentState == "Paid" || this.paymentViewModel.paymentState == "None") {
        this.isRequested = true;
      }
    }, error => {
      this.isBlocked = false;
      this.toastr.error(ToasterMessages.loadError('Payment'), toasterHeadings.Error);
    }, () => {
      this.isBlocked = false;
    })
  }

  getMemberInfo() {
    this.memberService.getMemberById(this.memberId).subscribe(res => {
      this.member = res.result.item;
      var ID = this.memberId;
      this.membershipID = this.prefix + "-" + ID;
      this.countries.forEach(element => {
        if (element.value == this.member.contactInfo.country) {
          this.memberCountry = element.text
        }
      }), error => {
      }
    })
  }

  getCountry() {
    this.settingService.getCountries()
      .subscribe(countries => {
        this.countries = countries.result.item;
      }, error => {
      })
  }

  getTenantInfo() {
    this.isBlocked = true;
    this.settingService.getTenantInfo().subscribe(res => {
      this.tenanViewModel = res.result;
      this.countries.forEach(element => {
        if (element.value == this.tenanViewModel.country) {
          this.country = element.text
        }
      });
    },
      error => {
        this.isBlocked = false;
      }, () => {
        this.isBlocked = false;
      })
  }

  goToPayments() {
    this.router.navigate(['/payments']);
  }

  printToCart(printSectionId: string) {
    let printContents, popupWin;
    printContents = document.getElementById(printSectionId).innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
          <style>
          @import url('https://fonts.googleapis.com/css?family=Nunito:400,700');
          *{
            margin:0;
            padding:0;
          }
          body{
            font-family: "Nunito", Helvetica, Arial, sans-serif;
            font-size: 16px;
            line-height: 1.42857143;
          }
          .invoice {
            position: relative;
            border: 1px solid #ecedf2;
            border-radius: 4px;
            background-color: #fff;
            color: #252529;
            overflow: hidden;
          }
          
          @media (max-width: 768px) {
            .invoice {
              padding: 50px;
            }
          }
          .invoice-wrapper {
            padding: 0px 100px;
            min-height: 842px;
          }
          
          @media (max-width: 768px) {
            .invoice-wrapper {
              padding: 15px 15px 0;
            }
          }
          .invoice-header {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-bottom: 50px;
            margin-bottom: 50px;
            background-color: #6061e8;
            margin-left: -100px;
            margin-right: -100px;
            padding: 24px 91px;
            color: #fff;
          }
          
          .invoice-title small {
            color: #fff;
            display: block;
            font-size: 14px;
            text-transform: capitalize;
            font-weight: normal;
            margin-bottom: 3px;
          }
          
          @media (max-width: 768px) {
            .invoice-header {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
            }
          }
          .invoice-brand {
            margin-bottom: 30px;
          }
          
          @media (max-width: 768px) {
            .invoice-brand {
              margin: 0;
            }
          }
          .invoice-brand-img {
            max-width: 40px;
            margin-bottom: 15px;
          }
          
          .invoice-title {
            margin-bottom: 0px;
            margin-top: 0;
          }
          
          .invoice-date {
            display: block;
            color: #fff;
            text-transform: uppercase;
          }
          
          .invoice-details {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-bottom: 30px;
          }
          
          .invoice-company-name {
            margin-bottom: 15px;
          }
          
          .invoice-company-address {
            margin-bottom: 15px;
            color: #99a5bd;
          }
          
          .invoice-body {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            margin-bottom: 50px;
          }
          
          @media (max-width: 768px) {
            .invoice-body {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
            }
          }
          .invoice-number {
            color: #99a5bd;
          }
          
          .invoice-table {
            width: 100%;
          }
          
          .invoice-table .table {
            margin-bottom: 10px;
            box-shadow: none;
          }
          
          .invoice-table .table strong {
            color: #252529;
          }
          
          @media (max-width: 768px) {
            .invoice-table .table {
              display: table;
              width: 100%;
            }
          }
          .invoice-terms {
            color: #99a5bd;
          }
          
          .invoice-note {
            margin-bottom: 30px;
          }
          
          .invoice-footer {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-top: 100px;
            padding: 30px 100px;
            color: #fff;
            background: #768093;
            background: linear-gradient(180deg, #768093, #969fb0);
          }
          
          @media (max-width: 576px) {
            .invoice-footer {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
              text-align: center;
            }
          }
          .invoice-footer-brand {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
          }
          
          @media (max-width: 576px) {
            .invoice-footer-brand {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              justify-content: center;
            }
          }
          .invoice {
            position: relative;
            border: 1px solid #ecedf2;
            border-radius: 4px;
            background-color: #fff;
            color: #252529;
            overflow: hidden;
          }
          
          @media (max-width: 768px) {
            .invoice {
              padding: 30px;
            }
          }
          @media (max-width: 768px) {
            .invoice-wrapper {
              padding: 15px 15px 0;
            }
          }
          .invoice-header {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-bottom: 50px;
            background-color: #6061e8;
          }

          .invoice-title{
            float:left;
          }
          .invoice-date{
            float:right;
          }

          .invoice-header .image-circle {
            background-color: #fff;
            border: 3px solid #fff;
            border-radius: 50%;
            height: 70px;
            width: 70px;
            overflow: hidden;
            float:right;
            position:absolute;
            top:20px;
            right:14px;
          }
          
          @media (max-width: 768px) {
            .invoice-header {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
            }
          }
          .invoice-brand {
            margin-bottom: 30px;
          }
          
          @media (max-width: 768px) {
            .invoice-brand {
              margin: 0;
            }
          }
          .invoice-brand-img {
            max-width: 40px;
            margin-bottom: 15px;
          }
          
          .invoice-title {
            text-transform: uppercase;
            font-weight: bold;
          }
          
          .invoice-date {
            display: flex;
            color: #fff;
            text-transform: capitalize;
            align-items: center;
            align-items: flex-end;
            flex-direction: column;
            text-align: right;
            font-size: 13px;
          }
          .invoice-date p {
            margin-bottom: 2px;
            font-weight: bold;
          }
          
          .invoice-details {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-bottom: 30px;
          }
          
          .invoice-company-name {
            margin-bottom: 15px;
          }
          
          .invoice-company-address {
            margin-bottom: 15px;
            color: #99a5bd;
          }
          
          .invoice-body {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            margin-bottom: 50px;
          }
          
          @media (max-width: 768px) {
            .invoice-body {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
            }
          }
          .invoice-number {
            color: #99a5bd;
          }
          
          .invoice-table {
            width: 100%;
          }
          
          .invoice-table .table {
            margin-bottom: 10px;
            box-shadow: none;
          }
          
          .invoice-table .table strong {
            color: #252529;
          }
          
          @media (max-width: 768px) {
            .invoice-table .table {
              display: table;
              width: 100%;
            }
          }
          .invoice-terms {
            color: #99a5bd;
            font-size: 0.875rem;
          }
          
          .invoice-note {
            margin-bottom: 30px;
          }
          
          .invoice-footer {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            margin-top: 100px;
            padding: 30px 100px;
            color: #fff;
            background: #768093;
            background: linear-gradient(180deg, #768093, #969fb0);
          }
          
          @media (max-width: 576px) {
            .invoice-footer {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
              text-align: center;
            }
          }
          .invoice-footer-brand {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
          }
          
          @media (max-width: 576px) {
            .invoice-footer-brand {
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-flow: column;
              flex-flow: column;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              justify-content: center;
            }
          }
          .table > thead > tr {
            background-color: #e7e9f2;
          }
          
          .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 0px solid #ddd;
          }
          
          .payment-info {
            text-align: center;
            padding: 9px;
            border-radius: 4px;
            overflow: auto;
            margin: 15px 0;
          }
          .payment-info.paid {
            border: 1px solid #4daf50;
            background-color: #c2f1c3;
          }
          .payment-info h4 {
            font-weight: bold;
            font-size: 15px;
            margin: 0;
          }
          
          .invoice-table .table-head tr {
            background-color: #eaeaff !important;
          }
          
          .org-title {
            font-size: 18px;
            text-transform: uppercase;
          }
          
          .sub-g {
            background: linear-gradient(to right, #e7eaf3 0%, rgba(255, 255, 255, 0) 100%);
            padding: 10px 15px;
            margin-bottom: 0px;
            background-color: white;
            display: flex;
            align-items: center;
            justify-content: space-between;
          }

          </style>
        </head>
    <body style="width:100%;" onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  settlePaymnet(paymnetModel) {
    this.isBlocked = true;
    this.paymentService.savePaymnets(paymnetModel)
      .mergeMap(payment => {
        return this.levelService.changeMemberStatus(payment.result)
      })
      .mergeMap(timeline => {
        this.timeLine.paymentId = this.paymentId;
        this.timeLine.memberId = this.memberId;
        this.timeLine.name = "Paid";
        return this.memberService.updateTimeLine(this.timeLine)
      }).subscribe(res => {
        this.isRequested = true
        this.paymentViewModel.paymentState = "Paid"
        this.toastr.success(ToasterMessages.save('Payment'), toasterHeadings.Success);
        this.isBlocked = false;
      }, error => {
        this.toastr.error(ToasterMessages.saveError('Payment'), toasterHeadings.Error);
        this.isBlocked = false;
      }, () => {
        this.isBlocked = false;
      })
  }


}
