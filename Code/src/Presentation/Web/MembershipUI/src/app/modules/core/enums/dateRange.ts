export enum DateRange {
    threemonths = 1,
    sixmonths = 2,
    oneyear = 3,
    week = 4
}