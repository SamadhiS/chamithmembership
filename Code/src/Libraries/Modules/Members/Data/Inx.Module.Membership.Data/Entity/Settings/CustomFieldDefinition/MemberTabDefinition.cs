﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition
{
    [Table("MemberTabDefinition", Schema = Constraints.Schema)]
    public class MemberTabDefinition : AuditableEntity
    {
        [Required]
        public Guid Key { get; set; }
        [StringLength(DbConstraints.NameLength),Required]
        public string DisplayName { get; set; }
        public bool IsVisible { get; set; }
        public int Order { get; set; }
        [NotMapped, Obsolete]
        public new string Name { get; set; }
    }
}
