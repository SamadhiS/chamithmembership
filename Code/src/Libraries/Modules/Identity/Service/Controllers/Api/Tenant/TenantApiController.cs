﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Module.Identity.Service.Models.Tenant;
using Inx.Module.Identity.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Inx.Module.Identity.Service.Controllers.Api.Tenant
{
    [Route("api/identity/v1")]
    public class TenantApiController : BaseApiController, IBaseApi<TenantViewModel, int>
    {
        private readonly ITenantService serviceTenant;
        public TenantApiController(ITenantService _serviceTenant, IBaseInjector baseinject) : base(baseinject)
        {
            serviceTenant = _serviceTenant;
        }

        [HttpPost, Route("tenants"), ModelValidation]
        public async Task<IActionResult> Create()
        {
            try
            {
                var httpRequest = HttpContext.Request;
                var tenantViewModel = httpRequest.Form["tenantViewData"];
                var item = JsonConvert.DeserializeObject<TenantViewModel>(tenantViewModel);
                var result = await UploadBlob(Inx.Utility.Utility.Enums.FileType.OrginazationLogoDefault);
                if (result.FileIsThere)
                {
                    await UploadBlob(Inx.Utility.Utility.Enums.FileType.OrginazationLogoDefault, result.FileNames[0]);
                    item.OrganizationLogo = result.FileNames[0];
                }
                var response = await serviceTenant.Create(new Request<TenantBo>()
                {
                    Item = item.MapObject<TenantViewModel, TenantBo>(),
                    TenantId = 0,
                    UserId = UserId
                });
                return Ok(Token.TokenGenarator(response));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("tenants/{subdomain}/available")]
        public async Task<IActionResult> CheckDomain(string subdomain)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = await serviceTenant.CheckDomain(subdomain);
                stopwatch.Stop();
                return Ok(new Response<bool>
                {
                    Item = result,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("tenants")]
        public async Task<IActionResult> Read()
        {
            try
            {
                return Ok(await serviceTenant.Read(Request(0)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }


        [HttpPut, Route("tenants"), ModelValidation]
        public async Task<IActionResult> Update()
        {
            try
            {
                var httpRequest = HttpContext.Request;
                var files = httpRequest.Form.Files;
                var tenantViewModel = httpRequest.Form["tenantViewData"];
                var item = JsonConvert.DeserializeObject<TenantViewModel>(tenantViewModel);
                var result = await UploadBlob(Inx.Utility.Utility.Enums.FileType.OrginazationLogoDefault);
                if (result.FileIsThere)
                {
                    await UploadBlob(Inx.Utility.Utility.Enums.FileType.OrginazationLogoDefault, result.FileNames[0]);
                    item.OrganizationLogo = result.FileNames[0];
                }
                await serviceTenant.Update(Request<TenantViewModel, TenantBo>(item));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        #region not implemented
        [ApiExplorerSettings(IgnoreApi = true), Obsolete]

        public Task<IActionResult> Delete(int id)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true), Obsolete]

        public Task<IActionResult> Read(int id)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true), Obsolete]

        public Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            throw new NotImplementedException();
        }

        [ApiExplorerSettings(IgnoreApi = true), Obsolete]

        public Task<IActionResult> ReadKeyValue()
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public new Task<IActionResult> Create(TenantViewModel item)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true), Obsolete]
        public new Task<IActionResult> Update(TenantViewModel item)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
