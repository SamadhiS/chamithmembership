﻿using System.Threading.Tasks;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Members.interfaces
{
    public interface IMemberInfoTypeService
    {
        Task Create<T>(Request<T> request) where T : AuditableEntity;
    }
}
