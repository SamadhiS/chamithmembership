﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Subscription
{
    public class SubscriptionBo : BaseBo
    {
        public decimal Price { get; set; }
        public bool IsLimited { get; set; }
        public int Capacity { get; set; }
        public DateTime SubscriptionFrom { get; set; }
        public DateTime SubscriptionTo { get; set; }
        public string Description { get; set; }
        public int SubscriptionTypeId { get; set; }
        public string Name { get; set; }
        public bool IsRenewable { get; set; }
        public bool IsSendEmail { get; set; }
        public int RenewalDays { get; set; }
        public int EmaillTemplateId { get; set; }
        public Inx.Utility.Utility.Enums.MembershipExpired MembershipExpired { get; set; }
    }
}
