import { QuillModule } from 'ngx-quill';
import { EditorModule } from 'primeng/editor';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ToasterCustomOptions } from './toasterCustomOptions';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ToastOptions } from 'ng2-toastr';
import { SelectColumnsComponent } from './select-columns/select-columns.component';
import { BlockUiComponent } from './block-ui/block-ui.component';
import { SelectReportColumnsComponent } from './select-report-columns/select-report-columns.component';
import { GenerateReportsService } from './services/generate-reports.service';
import { EmailSendComponent } from './components/email-send/email-send.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { PaymentModalComponent } from './components/payment-modal/payment-modal.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/multiselect.component';
import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { DynamicFormElementComponent } from './dynamic-form-element/dynamic-form-element.component';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { BsDatepickerModule } from '../../../../node_modules/ngx-bootstrap/datepicker';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ToastModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    EditorModule,
    QuillModule,
    AutoCompleteModule,
    AngularMultiSelectModule,
    TranslateModule
  ],
  declarations: [
    DeleteModalComponent,
    SelectColumnsComponent,
    BlockUiComponent,
    SelectReportColumnsComponent,
    EmailSendComponent,
    SafeHtmlPipe,
    PersonalInfoComponent,
    PaymentModalComponent,
    DynamicFormElementComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ToastModule,
    ModalModule,
    TabsModule,
    SafeHtmlPipe,
    PaginationModule,
    ReactiveFormsModule,
    SelectColumnsComponent,
    SelectReportColumnsComponent,
    DeleteModalComponent,
    EmailSendComponent,
    PersonalInfoComponent,
    PaymentModalComponent,
    TranslateModule,
    BlockUiComponent,
    DynamicFormElementComponent],

  entryComponents: [
    PaymentModalComponent
  ],
  providers: [
    DatePipe,
    GenerateReportsService,
    { provide: ToastOptions, useClass: ToasterCustomOptions }]
})
export class SharedModule { }
