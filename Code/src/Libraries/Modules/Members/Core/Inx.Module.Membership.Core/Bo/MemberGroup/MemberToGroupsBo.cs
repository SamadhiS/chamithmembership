﻿using Inx.Utility.Models;
using System.Collections.Generic;

namespace Inx.Module.Membership.Core.Bo.MemberGroup
{
    public class MemberToGroupsBo : BaseBo
    {
        public int MemberId { get; set; }
        public List<int> GroupIds { get; set; }
    }
}
