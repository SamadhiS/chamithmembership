﻿using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Utility.Models;
using Inx.Module.Membership.Core.Bo.Subscription.SearchViewResult;

namespace Inx.Module.Membership.Core.Service.Subscription.Interfaces
{
    public interface ISubscriptionService : IBaseService<SubscriptionBo>
    {
        new Task<PageList<SubscriptionSearchResult>> Read(Search search);
        Task<MemberSubscriptionViewResult> GetSubscriptions(Request<int> item);
        Task UpdateRenewal(Request<SubscriptionRenewalBo> item);
    }
}
