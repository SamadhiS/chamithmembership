﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Messenger.Data.Migrations
{
    public partial class Messenger_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Inexis.Messenger");

            migrationBuilder.CreateTable(
                name: "EmailTemplate",
                schema: "Inexis.Messenger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    EmailBody = table.Column<string>(nullable: true),
                    EmailTemplateTypes = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTemplate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailTemplate_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmailTemplate_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmailTemplate_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplate_CreatedById",
                schema: "Inexis.Messenger",
                table: "EmailTemplate",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplate_EditedById",
                schema: "Inexis.Messenger",
                table: "EmailTemplate",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmailTemplate_TenantId",
                schema: "Inexis.Messenger",
                table: "EmailTemplate",
                column: "TenantId");
 
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailTemplate",
                schema: "Inexis.Messenger");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Tenants",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "TenantTypes",
                schema: "Inexis.Security");
        }
    }
}
