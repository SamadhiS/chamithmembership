using System;
using Inx.Module.Identity.Test.Seed;
using Inx.Module.Identity.Test.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inx.Module.Identity.Test
{
    [TestClass]
    public class Setup
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            new Comman().Context.Database.EnsureDeleted();
            try
            {
                new Comman().Context.Database.EnsureCreated();
            }
            catch (Exception e)
            {
            }
            new Comman().Context.Database.Migrate();
            new SeedData().Seed();
        }
    }
}
