import { LevelService } from './../../core/services/level.service';

/*unit testing official tutorial:https://angular.io/guide/testing*/

import { Observable } from 'rxjs/Observable';
import { RouterTestingModule } from '@angular/router/testing';
import { LookupService } from '../../core/services/lookup.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { MemberCreateComponent } from './member-create.component';
import { MembersService } from '../../core/services/members.service';
import { Subscription } from 'rxjs/Subscription';
import { MemberSubscriptionService } from '../services/member-subscription.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { EmailTemplateService } from '../../core/services/email-template.service';


describe('MemberCreateComponent', () => {
  let component: MemberCreateComponent;
  let fixture: ComponentFixture<MemberCreateComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  let lookupService: LookupService;
  let memberService: MembersService;
  let levelService: LevelService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  }

  let mockCountryData = {
    result: {
      item: [
        { value: 0, text: "SL" },
        { value: 1, text: "AUS" }]
    }
  }

  let mockMartialData = {
    result: [
      { value: 0, text: "Single" },
      { value: 1, text: "Married" }]
  }

  let mockSubscriptionData = {
    result: {
      items: [
        { name: "Platinum" },
        { name: "Gold" }
      ]
    }

  }
  let mockMemberData = {
    result: {
      item:
        {
          memberInfo: { firstName: "user" },
          contactInfo: {},
          workingInfo: {},
          Subscription: 1008
        }
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MemberCreateComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [SharedModule, BsDatepickerModule.forRoot(), HttpClientModule,
        RouterTestingModule],
      providers: [MembersService, LookupService, MemberSubscriptionService, BlockUiService, EmailTemplateService,
        { provide: ActivatedRoute, useValue: { 'params': Observable.of({ 'id': 100 }) } },
        { provide: Router, useValue: mockRouter }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    //The createComponent method returns a ComponentFixture, a handle on the test environment surrounding the created component.
    // The fixture provides access to the component instance itself and to the DebugElement, which is a handle on the component's DOM element.

    fixture = TestBed.createComponent(MemberCreateComponent);
    component = fixture.componentInstance;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

    lookupService = fixture.debugElement.injector.get(LookupService);
    memberService = fixture.debugElement.injector.get(MembersService);


    spyOn(memberService, "getMemberById").and.returnValue(Observable.of(mockMemberData));
    spyOn(levelService,"getAllMemberLevel").and.returnValue(Observable.of(mockSubscriptionData));
    spyOn(lookupService, "getCountries").and.returnValue(Observable.of(mockCountryData));
    spyOn(lookupService, "getMartialStatus").and.returnValue(Observable.of(mockMartialData));

    fixture.detectChanges();//update the view with values in components
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should call save method when click save button', () => {
    component.memberId = 0;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnSaveMember"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.save()).toHaveBeenCalled();
    })
  });

  it('should call update method in person when click update button', () => {
    component.memberId = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnPersonWork"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.updateMember()).toHaveBeenCalled();
    })
  });

  it('should call update method in contact when click update button', () => {
    component.memberId = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnContactWork"));
    de.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.updateContact()).toHaveBeenCalled();
    })
  });

  it('should not display update button in person when memberId equals 0', () => {
    component.memberId = 0;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnContactWork"));
    expect(de).toBeNull();
  });

  it('should display update button in person when memberId greater than 0', () => {
    component.memberId = 10;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css("#btnContactWork"));
    expect(de).toBeTruthy();
  });


  it('personal form invalid when empty', () => {
    component.memberForm.reset();
    expect(component.memberForm.valid).toBeFalsy();
  });

  it('contact form invalid when empty', () => {
    component.contactForm.reset();
    expect(component.contactForm.valid).toBeFalsy();
  });

  it('work form invalid when empty', () => {
    component.workForm.reset();
    expect(component.workForm.valid).toBeFalsy();
  });

  it('subscricption form invalid when empty', () => {
    component.membershipForm.reset();
    expect(component.membershipForm.valid).toBeFalsy();
  });

  it('first name field validity', () => {
    component.memberForm.reset();
    let errors = {};
    let firstName = component.memberForm.controls['firstName'];
    expect(firstName.valid).toBeFalsy();

    // first name field is required
    errors = firstName.errors || {};
    expect(errors['required']).toBeTruthy();

    // Set first name to something
    firstName.setValue("firstName");
    errors = firstName.errors || {};
    expect(errors['required']).toBeFalsy();

  });

  it('last name field validity', () => {
    let errors = {};
    let lastName = component.memberForm.controls['lastName'];
    expect(lastName.valid).toBeFalsy();

    // last name field is required
    errors = lastName.errors || {};
    expect(errors['required']).toBeTruthy();

    // Set last name to something
    lastName.setValue("lastName");
    errors = lastName.errors || {};
    expect(errors['required']).toBeFalsy();

  });


  it('should country drop down list has first value as SL', () => {
    el = fixture.debugElement.query(By.css("#ddCountry")).queryAll(By.css("option"))[0].nativeElement;
    expect(el.textContent).toEqual("SL");
  });


  it('should martial status drop down list has first value as Single', () => {
    el = fixture.debugElement.query(By.css("#ddMartialStatus")).queryAll(By.css("option"))[0].nativeElement;
    expect(el.textContent).toEqual("Single");
  });

  it('should display first name as user when member id is greater than 0', () => {
    const input = fixture.debugElement.query(By.css('#txtfname')).nativeElement;
    expect(input.value).toEqual("user");
  });

  it('should member subscription drop down list has first value as Platinum', () => {
    el = fixture.debugElement.query(By.css("#ddMembershiplevel")).queryAll(By.css("option"))[0].nativeElement;
    expect(el.textContent).toEqual("Platinum");
  });
});
