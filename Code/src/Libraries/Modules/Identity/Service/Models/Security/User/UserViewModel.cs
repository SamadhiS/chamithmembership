﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;

namespace Inx.Module.Identity.Service.Models.Security.User
{
    public class UserViewModel
    {
        [EmailAddress, Required, StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; } 
    }
}
