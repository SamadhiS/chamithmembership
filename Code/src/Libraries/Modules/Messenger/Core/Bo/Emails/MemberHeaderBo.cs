﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Inx.Module.Messenger.Core.Bo.Emails
{
    public class MemberHeaderBo
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Identity { get; set; }
        public Utility.Utility.Enums.MartialStatus MartialStatus { get; set; }
        public Utility.Utility.Enums.MembershipStatus MembershipStatus { get; set; }
        public Utility.Utility.Enums.RecordStatus RecordStatus { get; set; }
        public int TenantId { get; set; }
        //contact
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        public List<string> GetProperties(List<string> exclude)
        {
            var foo = this;
            var lst = foo.GetType().GetProperties().Select(prop => prop.Name).ToList();
            var result = new List<string>();
            foreach (var item in lst)
            {
                if (!exclude.Contains(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }
    }
}
