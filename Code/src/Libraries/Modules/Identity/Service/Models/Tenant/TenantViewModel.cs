﻿using System.ComponentModel.DataAnnotations;
using Inx.Service.Base.Models;
using Inx.Utility;
using Inx.Utility.Utility;

namespace Inx.Module.Identity.Service.Models.Tenant
{
    public class TenantViewModel : BaseViewModel
    {
        [Required, StringLength(50), MinLength(3), RegularExpression(RegularExpression.Name)]
        public string Host { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }
        //[NumberNotZero]
        public int TenantTypeId { get; set; }
        public string OrganizationLogo { get; set; } = Constants.OrganizationNoImage;
    }
}
