import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ContactCategoryService } from './services/ContactCategory.service';
import { SettingService } from './services/setting.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { CreateCategoriesComponent } from './create-categories/create-categories.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { SharedModule } from '../shared/shared.module';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { TranslateModule } from '@ngx-translate/core';
import { PermissionsComponent } from './permissions/permissions.component';
import { PermissionService } from './services/permission.service';
import { PermissionPasswordComponent } from './permission-password/permission-password.component';
import { CustomFieldsComponent } from './custom-fields/custom-fields.component';


@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    TranslateModule,
    SharedModule,
  ],
  declarations: [ CreateCategoriesComponent, AccountSettingsComponent, UserSettingsComponent, PermissionsComponent, PermissionPasswordComponent, CustomFieldsComponent],
  providers:[SettingService, ContactCategoryService,PermissionService]
})
export class SettingsModule { }
