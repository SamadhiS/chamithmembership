﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Membership.PageObjects.NavPage
{
    class NavPage
    {
        public void Load_Dashboard()
        {
            Assert.AreEqual("http://membershipinexis.azurewebsites.net/#/dashboard", "http://membershipinexis.azurewebsites.net/#/dashboard");
            Thread.Sleep(5000);
        }

       

        public void Btn_Contact_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnContacts"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(3000);
        }
        public void Btn_Member_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnMembers"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void Btn_UserSettings_Click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnTabUserSettings"));
            this.ScrollToElem(elem, d);
            elem.Click();
        }
        public void Btn_OrganizationSettings_Click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnOrgSettings"));
            this.ScrollToElem(elem, d);
            elem.Click();
        }
        public void Btn_Settings_Click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSettings"));
            this.ScrollToElem(elem, d);
            elem.Click();
        }
        public void Btn_MemberLevel_click(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnMemberLevelTab"));
            this.ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
