﻿using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
namespace Inx.Module.Contacts.Data.Entity.Utility
{
    [Table("TableTemplates", Schema = Constraints.Schema)]
    public class TableTemplate : AuditableEntity
    {
        public int TemplateType { get; set; }
        public string Keys { get; set; } = "[]";
        [NotMapped]
        public new string Name { get; set; }
    }
}
