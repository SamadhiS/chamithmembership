﻿using System.Threading.Tasks;
using System.Web.Http.Description;
using Inx.Module.Identity.Core.Bo;
using Inx.Module.Membership.Core.Bo.MemberGroup;
using Inx.Module.Membership.Core.Service.MemberGroup.Interface;
using Inx.Module.Membership.Service.Models.Groups;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.MemberGroup
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class MemberGroupApiController : MembershipBaseApiController, IBaseApi<MemberGroupViewModel, int>
    {
        private readonly IMemberGroupService service;
        public MemberGroupApiController(IMemberGroupService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("membergroups"), ModelValidation]
        public async Task<IActionResult> Create([FromBody]  MemberGroupViewModel item)
        {
            return await Create<MemberGroupViewModel, MemberGroupBo, IMemberGroupService>(item, service);
        }

        [HttpPost, Route("membergroups/assigntogroups"), ModelValidation]
        public async Task<IActionResult> AssignToGroups([FromBody]  MemberToGroupsViewModel item)
        {
            await service.AssignToGroups(Request<MemberToGroupsViewModel, MemberToGroupsBo>(item));
            return Ok();
        }

        [HttpDelete, Route("membergroups/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<MemberGroupBo, IMemberGroupService>(id, service);
        }
        [HttpGet, Route("membergroups/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
           
            return Ok(await service.GetMemberGroups(Request(id)));
        }
        [HttpGet, Route("membergroups"), ResponseType(typeof(PageList<MemberGroupViewModel>))]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<MemberGroupViewModel, MemberGroupBo, IMemberGroupService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpGet, Route("membergroups/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<MemberGroupBo, IMemberGroupService>(service);
        }
        [HttpPut, Route("membergroups"), ModelValidation, ResponseType(typeof(void))]
        public async Task<IActionResult> Update([FromBody]  MemberGroupViewModel item)
        {
            return await Update<MemberGroupViewModel, MemberGroupBo, IMemberGroupService>(item, this.service);
        }

    }
}
