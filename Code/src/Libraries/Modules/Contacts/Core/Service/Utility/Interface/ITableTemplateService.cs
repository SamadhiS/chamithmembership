﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Contacts.Core.Bo.Utlity;
using Inx.Utility.Models;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Contacts.Core.Service.Utility.Interface
{
    public interface ITableTemplateService : IBaseService<TableTemplateBo>
    {
        List<string> GetColomHeaders(TableTemplateTypes request);
        new Task<List<string>> Read(Request<int> req);
    }
}
