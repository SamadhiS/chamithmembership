using System;
using Inx.Module.Membership.Test.Seed;
using Inx.Module.Membership.Test.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using Inx.Utility.Utility;
using Inx.Module.Membership.Core.Bo.Groups;
using Inx.Module.Membership.Data.Entity.Groups;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Membership.Data.Entity.Subscription;

namespace Inx.Module.Membership.Test
{
    [TestClass]
    public class Setup
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            new Comman().MemberContext.Database.EnsureDeleted();
            try
            {
                new Comman().MemberContext.Database.EnsureCreated();
            }
            catch (Exception e)
            {
            }
            new Comman().IdentityContext.Database.Migrate();
            new Comman().MemberContext.Database.Migrate();
            new SeedData().Seed();
            //automapper 
            Mapper.Initialize(config =>
            {
                config.CreateMap<MembershipGroup, MembershipGroupBo>().ReverseMap();
            });
            IdentitySeed();
            MemberGroupSeed();
            MemberLevelSeed();
        }

        static void IdentitySeed()
        {
            using (var c = new Comman().IdentityContext)
            {

                c.Users.Add(new User
                {
                    CreatedById = 0,
                    CreatedOn = DateTime.Today,
                    Email = "test@gmail.com",
                    Password = "test@gmail.com",
                    EmailConfirmCode = Guid.Empty,
                    IsEmailConfirmed = true,
                    RecordStatus = Enums.UserRecordStatus.Active,
                    PasswordResetCode = Guid.Empty,

                });
                c.SaveChanges();
            }
        }

        static void MemberGroupSeed()
        {
            using (var c = new Comman().MemberContext)
            {
                c.MembershipGroup.Add(new MembershipGroup
                {
                    Name = "Gym Group",
                    Description = "Created for testing",
                    EditedById = null,
                    EditedOn = null,
                    RecordState = Enums.RecordStatus.Active,
                    TenantId = 1
                });
                c.MembershipGroup.Add(new MembershipGroup
                {
                    Name = "18+",
                    Description = "Created for testing data",
                    EditedById = null,
                    EditedOn = null,
                    RecordState = Enums.RecordStatus.Active,
                    TenantId = 1
                });
                c.SaveChanges();
            }


        }

        static void MemberLevelSeed()
        {
            using (var c = new Comman().MemberContext)
            {
                c.SubscriptionType.Add(new SubscriptionType
                {
                    Name = "Gold",
                    Description = "Test Description",
                    TenantId = 1,
                    EditedById = null,
                    EditedOn = null,
                    RecordState = Enums.RecordStatus.Active
                });
                c.SubscriptionType.Add(new SubscriptionType
                {
                    Name = "Silver",
                    Description = "Test Silver Description",
                    TenantId = 1,
                    EditedById = null,
                    EditedOn = null,
                    RecordState = Enums.RecordStatus.Active
                });
            }
        }

            
        

        static void MemberGroupMemberSeed()
        {
            using (var e = new Comman().MemberContext)
            {
                e.MemberGroup.Add(new MemberGroup
                {
                    Name = "test",
                    TenantId = 1,
                    EditedById = null,
                    EditedOn = null,
                });
                e.MemberGroup.Add(new MemberGroup
                {
                    Name = "test2",
                    TenantId = 1,
                    EditedById = null,
                    EditedOn = null,
                });
                e.SaveChanges();
            }
        }
    }
        
    
}
