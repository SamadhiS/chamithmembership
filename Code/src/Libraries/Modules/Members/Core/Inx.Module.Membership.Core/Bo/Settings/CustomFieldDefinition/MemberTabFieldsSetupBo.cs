﻿using System.Collections.Generic;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberTabFieldsSetupBo
    {
        public List<MemberFieldDefinitionBo> MemberFieldDefinition { get; set; } 
        public List<MemberTabDefinitionBo> MemberTabDefinition { get; set; } 
        public List<MemberTabFieldBo> MemberTabField { get; set; }  
        public List<MemberTypeTabBo> MemberTypeTab { get; set; }
    }
}
