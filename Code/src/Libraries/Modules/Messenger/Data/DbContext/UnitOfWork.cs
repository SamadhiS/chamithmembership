﻿using Inx.Data.Base.Repository;
using Inx.Module.Messenger.Data.Entity;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Messenger.Data.DbContext
{
    public class UnitOfWork: IUnitOfWorkMassengers
    {
        #region fields

        private bool disposed;
        public Microsoft.EntityFrameworkCore.DbContext Context { get; }

        #region contact

        private GenericRepository<EmailTemplate> emailTemplateRepository;
       
        #endregion

        public string ConnectionString => Context.Database.GetDbConnection().ConnectionString;

        #endregion

        #region ctor

        public UnitOfWork(IMessengerDbSet dbcontext)
        {
            Context = (MessengerDbContext)dbcontext;
        }

        #endregion

        #region repositories

        public GenericRepository<EmailTemplate> EmailTemplateRepository =>
            emailTemplateRepository ?? (emailTemplateRepository = new GenericRepository<EmailTemplate>(Context));

          #endregion

        #region methods

        public void Save()
        {
            Context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
