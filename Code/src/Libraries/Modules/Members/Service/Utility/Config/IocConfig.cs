﻿using Abp.Dependency;
using Inx.Core.Base;
using Inx.Core.Base.Service;
using Inx.Core.Base.Service.Interface;
using Inx.Module.Membership.Core.Service.Dashboard;
using Inx.Module.Membership.Core.Service.Dashboard.Interface;
using Inx.Module.Membership.Core.Service.MemberGroup;
using Inx.Module.Membership.Core.Service.MemberGroup.Interface;
using Inx.Module.Membership.Core.Service.Members;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Core.Service.Members.Master;
using Inx.Module.Membership.Core.Service.MemberSubscription;
using Inx.Module.Membership.Core.Service.MemberSubscription.Interface;
using Inx.Module.Membership.Core.Service.Settings;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Core.Service.Settings.Interface;
using Inx.Module.Membership.Core.Service.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Payment.Data.DbContext;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Utility.Files;
using Inx.Utility.Cache.Runtime;
namespace Inx.Module.Membership.Service.Utility.Config
{
    public class IocConfig
    {
        public static void Register(IIocManager iocManager)
        {
            #region data
            iocManager.RegisterIfNot<IMembershipDbSet, MembershipDbContext>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IUnitOfWokMembership, Inx.Module.Membership.Data.DbContext.UnitOfWork>(DependencyLifeStyle.Transient);
            ///

            iocManager.RegisterIfNot<IUnitOfWorkPayment, Inx.Module.Payment.Data.DbContext.UnitOfWork>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMembershipDbSet, MembershipDbContext>(DependencyLifeStyle.Transient);
            #endregion

            iocManager.RegisterIfNot<IMemberInfoService, MemberInfoService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IGroupService, GroupService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ISubscriptionTypeService, SubscriptionTypeService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ISubscriptionService, SubscriptionService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberFileService, MemberFileService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBackgroundProcess, BackgroundProcess>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<INotificationBackgroundProcess, NotificationBackgroundProcess>(DependencyLifeStyle.Transient);
        
            iocManager.RegisterIfNot<INotificationService, NotificationService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<INotificationService, NotificationService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IDashboardService, DashboardService>(DependencyLifeStyle.Transient);

            iocManager.RegisterIfNot<IMemberTabDefinitionService, MemberTabDefinitionService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberFieldDefinitionService, MemberFieldDefinitionService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberTypeTabService, MemberTypeTabService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberTabFieldsSetupService, MemberTabFieldsSetupService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberTabFieldService, MemberTabFieldService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberTypeTabFieldsRenderService, MemberTypeTabFieldsRenderService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberFieldRequiredService, MemberFieldRequiredService>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemberSubscriptionService, MemberSubscriptionService>(DependencyLifeStyle.Transient);
            

            #region genaral
            iocManager.RegisterIfNot<IBlobStorage, Blob>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IMemoryCacheManager, MemoryCacheManager>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBlobStorage, Blob>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<IBaseInjector, BaseInjector>(DependencyLifeStyle.Transient);
            iocManager.RegisterIfNot<ICoreInjector, CoreInjector>();
            iocManager.RegisterIfNot<IMemberGroupService, MemberGroupService>(DependencyLifeStyle.Transient);
            #endregion

        }
    }
}
