﻿using System;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Members;
using Inx.Module.Membership.Core.Bo.Members.Master;
using Inx.Module.Membership.Core.Bo.Members.Master.Person;
using Inx.Module.Membership.Core.Service.Members;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Core.Service.Members.Master;
using Inx.Module.Membership.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inx.Module.Membership.Test.IntegrationTesting
{
    public class MemberTest: CoreTestBase, ICoreTestBase
    {
        private IMemberService service;

        //[TestInitialize]
        public void Initialize()
        {
            service = new MemberInfoService<>(new Comman().IUnitOfWokMembership,new Comman().IUnitOfWorkPayment, new Comman().IUnitOfWorkMassengers, null,null);
        }

        [DataTestMethod]
        [DataRow(1,"Salmaan", "Mohamed", Enums.MartialStatus.Married, "943624062v",1,"23/1,k.d david avenue","maradana","salmaan@inexisconsulting.com","0773074584","Inexis","SE")]
        [DataRow(1,"Some", "Latha", Enums.MartialStatus.Single, "943624062v", 1, "gangarama", "maradana", "some@inexisconsulting.com", "0773074584", "Inexis", "SE")]
        [DataRow(1,"Salmaan", "Mohamed", Enums.MartialStatus.Married, "943624062v", 1, "23/1,k.d david avenue", "maradana", "", "0773074584", "Inexis", "SE")]
        [DataRow(1,"Salmaan", "Mohamed", Enums.MartialStatus.Married, "943624062v", 1, "23/1,k.d david avenue", "maradana", "salmaan@inexisconsulting.com", "", "Inexis", "SE")]
        public async Task Create()
        {
            object[] data = null;
            var bo = new MemberPersonBo
            {
                MemberInfo = new PersonalInfoBo
                {
                    FirstName = Convert.ToString(data[1]),
                    LastName = Convert.ToString(data[2]),
                    DOB = new DateTime(1988, 1, 24),
                    MartialStatus = (Enums.MartialStatus)(data[3]),
                    Identity = Convert.ToString(data[4]),
                },
                ContactInfo = new ContactInfoBo()
                {
                    Address = Convert.ToString(data[6]),
                    City = Convert.ToString(data[7]),
                    Email = Convert.ToString(data[8]),
                    Phone = Convert.ToString(data[9]),
                    Telephone = "",
                    Zip = "",
                    Country = 1
                },
                WorkingInfo = new WorkingInfoBo()
                {
                    Phone = "",
                    Telephone = "",
                    Company = Convert.ToString(data[10]),
                    Designation = Convert.ToString(data[11])
                },
                Subscription = Convert.ToInt32(data[5])
            };
            try
            {
                await base.Create(bo, service);
                True("MemberTest=>Create");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow(1)] //true
        [DataRow(999)] //false
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<MemberPersonBo, IMemberService>(id, service);
                True("MemberTest=>Delete");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")] //true
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<MemberPersonBo, IMemberService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("MemberTest=>Read");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow(1)] //true
        [DataRow(999)] //false
        public async Task ReadById(int id)
        {
            try
            {
                await service.Read(TestRequest(id));
                True("MemberTest=>ReadById");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<MemberPersonBo, IMemberService>(service);
                True("MemberTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")] //true
        public async Task Search(int skip, int take, string search)
        {
            try
            {
                await service.Search(TestSearchRequest(skip: skip,
                    take: take, searchTerm: search));
                True("MemberTest=>Search");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }

        [DataTestMethod]
        [DataRow(1)] //true
        [DataRow(999)]//false
        [DataRow("a")]//false
        public async Task Update()
        {
            try
            {
                await base.Update(new MemberPersonBo(), service);
                True("MemberTest=>Update");
            }
            catch (Exception e)
            {
                Fail(e.Message);
            }
        }
    }
}
