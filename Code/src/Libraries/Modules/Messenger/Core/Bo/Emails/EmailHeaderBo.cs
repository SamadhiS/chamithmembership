﻿using System.Collections.Generic;

namespace Inx.Module.Messenger.Core.Bo.Emails
{
    public class EmailHeaderBo
    {
        public List<string> MemberHeaders { get; set; }
        public List<string> CustomHeaders { get; set; }
    }
}
