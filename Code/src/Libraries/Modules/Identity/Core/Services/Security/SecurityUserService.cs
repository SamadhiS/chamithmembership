﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Core.Bo.User;
using Inx.Module.Identity.Core.Utility;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Inx.Module.Identity.Core.Services.Security
{
    public partial class SecurityService
    {
        public async Task<Bo.User.UserBo> ReadUser(Request<string> request)
        {
            try
            {
                return await (from user in unitOfWorkIdentity.UserRepository.TableAsNoTracking
                        where user.Id == request.UserId
                        select new { user.Email, user.RecordStatus })
                    .AsQueryable().Select(p => new Bo.User.UserBo
                    {
                        Id = request.UserId,
                        Email = p.Email,
                        RecordStatus = p.RecordStatus,
                        TenantId = request.TenantId

                    }).FirstAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        [Description("give email address for permission user")]
        public async Task<RegisterResponse> CreateUser(Request<UserBo> request)
        {

            IDbContextTransaction tra = null;
            try
            {
                tra = await unitOfWorkIdentity.Context.Database.BeginTransactionAsync();
                var token = Guid.NewGuid();
                var @user = new User
                {
                    CreatedById = 0,
                    CreatedOn = DateTime.UtcNow,
                    Email = request.Item.Email,
                    EmailConfirmCode = token,
                    TenantId = request.TenantId,
                    Password = string.Empty,
                    RecordStatus = Enums.UserRecordStatus.Pending,
                    IsEmailConfirmed = false
                };
                await unitOfWorkIdentity.Context.AddAsync(@user);
                await unitOfWorkIdentity.SaveAsync();
                await unitOfWorkIdentity.Context.AddAsync(new UserRole
                {
                    TenantId = @user.TenantId,
                    CreatedById = @user.Id,
                    CreatedOn = DateTime.UtcNow,
                    RecordState = Enums.RecordStatus.Active,
                    UserId = @user.Id,
                    RoleId = 1
                });
                await unitOfWorkIdentity.SaveAsync();
                tra.Commit();
                return new RegisterResponse
                {
                    UserId = @user.Id,
                    Token = token
                };

            }
            catch (DbUpdateException ex)
            {
                tra?.Rollback();
                var item = ex.GetBaseException() as SqlException;
                if (!item.IsNull() && item.Number == 2601)
                {
                    // check is email already there and if there handle error rmessage
                    var result = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == request.Item.Email);
                    throw new UserSecurityException(ErrorResponse(result.RecordStatus));
                }
                throw ex.HandleException();
            }
            catch (Exception ex)
            {
                tra?.Rollback();
                throw ex.HandleException();
            }
        }

        public async Task ChangeUserInitialPassword(Request<ChangePermissionPasswordBo> request)
        {
            try
            {
                var result = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == request.Item.Email && p.EmailConfirmCode == new Guid(request.Item.Token)
                      && !p.IsEmailConfirmed && p.RecordStatus == Enums.UserRecordStatus.Pending);
                if (result.IsNull())
                {
                    throw new RecordNotFoundException();
                }
                result.Password = request.Item.Password.Encrypt();
                result.EmailConfirmCode = Guid.Empty;
                result.IsEmailConfirmed = true;
                result.RecordStatus = Enums.UserRecordStatus.Active;
                await unitOfWorkIdentity.SaveAsync();
            }
            catch (RecordNotFoundException)
            {
                throw new RecordNotFoundException(EMessages.InvaliedCurrentPassword.ToString()).HandleException();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        [Description("get permoission user info")]
        public async Task<List<PermissionUserBo>> ReadUsers(Request<string> request)
        {
            try
            {
                var query = (from user in unitOfWorkIdentity.UserRepository.TableAsNoTracking
                             join userrole in unitOfWorkIdentity.UserRoleRepository.TableAsNoTracking on user.Id equals userrole.UserId
                             join role in unitOfWorkIdentity.RoleRepository.TableAsNoTracking on userrole.RoleId equals role.Id
                             where user.TenantId == request.TenantId
                             select new PermissionUserBo
                             {
                                 Email = user.Email,
                                 RoleName = role.Name,
                                 TenantId = user.TenantId,
                                 RecordStatus = user.RecordStatus,
                                 UserStatus = user.RecordStatus.ToString()
                             }).AsQueryable();

                var res = await query.ToListAsync();
                return res;

            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task NewUserCreateRequestValidation(Request<Dictionary<string, string>> request)
        {
            try
            {
                var result = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == request.Item["email"] && p.EmailConfirmCode == new Guid(request.Item["token"])
                   && !p.IsEmailConfirmed && p.RecordStatus == Enums.UserRecordStatus.Pending);
                if (result.IsNull())
                {
                    throw new RecordNotFoundException();
                }
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
