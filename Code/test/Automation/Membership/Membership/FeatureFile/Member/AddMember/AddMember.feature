﻿Feature: As an Admin, I should be able to add a new member


@mytag
Scenario Outline: As an Admin, I should be able to add a new member
	Given The Application Pg
	Then I User enters "membership.inexis@gmail.com" and "Admin@123"
	And I Click on Submit 
	And I navigated to Homepage 
	And I directed to Members Section
	And I Clicks on Add New Member
	And I enters First name as <_FN>
	And I enters Last name as <_LN>
	And I selects profile Image
	And I enters DOB  as <_DOB>
	And I selects martial Status
	And I enters NIC as <_NIC>
	And I Clicks on the NEXT button 
	And I select a Country
	And I enters City as <_City>
	And I enters Postal as <_Postal>
	And I enters Address as <_Address>
	And I entered Mob as <Mob>
	And I enters Telephone as <_Tele>
	And I enters Email as <_Email>
	And I Clicks on NEXT button 
	And I enters Designation as <_desig>
	And I enters Company as <_Cmpy>
	And I entered Company address as <C_add>
	And I select my work country
	And I entered Ofc Telephone as <_TeleAdd>
	And user enters Ofc Mobile as <_TeleMob>
	And Click one Next Button
	And Select a Membership Level
	And Click on Next button
	And I navigated to Payments
	And I select Transaction type
	And I select Payment Type
	And I Enter Description as <Descrpt>
	And Click on Paynow Button
	And I click on logout
Examples: 
| _FN     | _LN   | _DOB     | _NIC       | _Country   | _City    | _Postal | _Address                  | Mob        | _Tele      | _Email              | _desig    | _Cmpy               | C_add                      | _TeleAdd   | _TeleMob   | Descrpt |
| Krishni | Amaya | 1993-2-4 | 944413329V | New Zeland | Auckland | 2582    | 25, Auckland, New Zealand | 2525444444 | 3233333322 | krishni@hotmail.com | HR Intern | More Octane Fitness | 25 Postman Suite, New York | 0827338125 | 0266735292 | Membership Payment      |

##Scenario Outline: As an Admin, I should not be able to add a new member
##	Given The membership portal
##	Then Entered the "membership.inexis@gmail.com" and "Admin@123"
##	And I Click on Submit button
##	And I navigated to Homepage page
##	And I directed to Member Section
##	And I Click on Add New Member
##	And I entered First name as <_FN>
##	And I entered Last name as <_LN>
##	And I selected profile Image
##	And I entered DOB  as <_DOB>
##	And I selected martial Status
##	And I have enter NIC as <_NIC>
##	And I Clicked on the NEXT button and display the error message
##	And I clicked on logout button
##	
##Examples: 
##| _FN     | _LN  | _DOB     | _NIC |
##| Rocheal | Stew | 1992-2-4 |      |

#Scenario Outline: Enter Invalid Contact Information
#	Given The Application Page load
#	Then User entered un & pw "membership.inexis@gmail.com" and "Admin@123"
#	And Clicks on Submit btn
#	And is navigated to Homepage page
#	And User navigates to Members Section
#	And I Click on Add New Member button
#	And I entered Firstname as <_FN>
#	And I entered Lastname as <_LN>
#	And User selected profile Image
#	And User entered DOB  as <_DOB>
#	And User selected martial Status
#	And User have enter NIC as <_NIC>
#	And User Clicked on the NEXT button
#	And I selected a Country
#	And User enter City as <_City>
#	And User enter Postal as <_Postal>
#	And User enter Address as <_Address>
#	And User enters Mob as <Mob>
#	And User enter Telephone as <_Tele>
#	And User enter Email as <_Email>
#	And I Clicked on NEXT button and display error msg
#	And User clicked on logout button
#
#	Examples: 
#	| _FN      | _LN      | _DOB      | _NIC       | _Country | _City   | _Postal | _Address          | Mob     | _Tele     | _Email                |
#	| Liallian | Salvator | 1998-4-13 | 926723229V | Kuwait   | Jabriya | 9090    | 22, Bayan Streeet | 99631223 | 257243213 | salvatory@gmal. | 