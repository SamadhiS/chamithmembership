export class TimeLine
{
    memberId: number;
    subscriptionName: string;
    subscriptionId: number;
    paymentId: number;
    amount: number;
    templateType : number;
    name : string;
}
