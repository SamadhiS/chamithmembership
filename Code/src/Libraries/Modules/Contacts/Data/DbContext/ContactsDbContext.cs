﻿using System.Threading;
using System.Threading.Tasks;
using Inx.Module.Contacts.Data.Entity.User;
using Inx.Module.Contacts.Data.Entity.Utility;
using Inx.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Contacts.Data.DbContext
{
    public class ContactsDbContext : Microsoft.EntityFrameworkCore.DbContext, IContactsDbSet
    {
        private readonly string connectionString;
        #region contacts
        public DbSet<PersonInfo> PersonInfo { get; set; }
        public DbSet<WorkingInfo> WorkingInfo { get; set; }
        public DbSet<ContactInfo> ContactInfo { get; set; }
        public DbSet<ContactCategory> ContactCategory { get; set; }
        public DbSet<Contactfile> Contactfile { get; set; }
        public DbSet<TableTemplate> TableTemplate { get; set; }
        #endregion

        public ContactsDbContext()
        {
            connectionString = GlobleConfig.ConnectionString;
        }

        public ContactsDbContext(string _connectionstring)
        {
            connectionString = _connectionstring;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContactInfo>()
                .HasIndex(c => new { c.Phone, c.TenantId }).IsUnique();
            modelBuilder.Entity<ContactCategory>()
                .HasIndex(c => new { c.Name, c.TenantId }).IsUnique();
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {  
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
