﻿using Inx.Service.Base.Models;
using System.Collections.Generic;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Service.Models.CustomFieldDefinition
{
    public class MemberTabDefinitionViewModel : BaseViewModel
    {
        public string DisplayName { get; set; }
        public bool IsVisible { get; set; }
        public List<Enums.MemberType> MemberType { get; set; } = new List<Enums.MemberType>();
    }
}
