export class PaymentViewModel {

    paymentMasterId:number;
    amount: number;
    remaining: number;
    transActionType: string;
    paymentType: string;
    paymentState: string;
    paymentDate:Date;
    description: string
    name:string;
    createdDate:any;
    createdOn:any;
   editedOn :Date;
}