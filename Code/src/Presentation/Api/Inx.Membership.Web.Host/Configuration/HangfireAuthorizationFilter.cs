﻿using Hangfire.Dashboard;

namespace Inx.Membership.Web.Host.Configuration
{
    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}
