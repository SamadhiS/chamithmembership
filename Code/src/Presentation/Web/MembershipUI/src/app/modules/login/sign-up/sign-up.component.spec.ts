import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { SignUpComponent } from './sign-up.component';
import { SharedModule } from '../../shared/shared.module';
import { RegisteruserService } from '../service/registeruser.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;
  let submit: DebugElement;  
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignUpComponent],
      imports: [SharedModule, RouterTestingModule, HttpClientModule],
      providers: [RegisteruserService]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    submit = fixture.debugElement.query(By.css('button'));
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('invalid signupForm disables the submit button', () => {
    !(component.signupForm.valid);
    fixture.detectChanges();
    expect(submit.nativeElement.disabled).toBeTruthy();
  });

  it('should call signup method when click signup button', () => {
    de = fixture.debugElement.query(By.css("#signUp"));
    de.triggerEventHandler("click", null);
    fixture.whenStable().then(() => {
      expect(component.signupUser()).toHaveBeenCalled();
    })
  });
  it('form invalid when empty', () => {
    expect(component.signupForm.valid).toBeFalsy();
  });
  it('email field validity', () => {
    let errors = {};
    let email = component.signupForm.controls['userMail'];
    expect(email.valid).toBeFalsy();

    // Email field is required
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy();

    // Set email to something correct
    email.setValue("test@example.com");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
  });
});
