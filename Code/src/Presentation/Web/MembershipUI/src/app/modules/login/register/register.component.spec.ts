import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RegisteruserService } from '../service/registeruser.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let de: DebugElement;
  let submit: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports: [SharedModule,RouterTestingModule,HttpClientModule],
      providers: [RegisteruserService]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    submit = fixture.debugElement.query(By.css('button'));
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('form invalid when empty', () => {
    expect(component.registerForm.valid).toBeFalsy();
  });
  it('invalid registerForm disables the submit button', () => {
    !(component.registerForm.valid);
    fixture.detectChanges();
    expect(submit.nativeElement.disabled).toBeTruthy();
  });
  it('should call createAccount method when click Create Account button', () => {
    de = fixture.debugElement.query(By.css("#createAccount"));
    de.triggerEventHandler("click", null);
    fixture.whenStable().then(() => {
      expect(component.createAccount()).toHaveBeenCalled();
    })
  });
  it('fields validity', () => {
    let errors = {};
    let orgName = component.registerForm.controls['organizationName'];
    let orgType = component.registerForm.controls['organizationType'];
    let hostName = component.registerForm.controls['hostName']
    let isAgreement = component.registerForm.controls['IsAgreement']

    //orgName field is required
    errors = orgName.errors || {};
    expect(errors['required']).toBeTruthy();

    //orgType field is required
    errors = orgType.errors || {};
    expect(errors['required']).toBeTruthy();

     //hostName field is required
     errors = hostName.errors || {};
     expect(errors['required']).toBeTruthy();
 
     //isAgreement field is required
     errors = isAgreement.errors || {};
     expect(errors['required']).toBeTruthy();
  });
});
