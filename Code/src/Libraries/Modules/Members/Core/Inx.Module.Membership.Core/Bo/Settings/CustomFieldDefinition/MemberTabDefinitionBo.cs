﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberTabDefinitionBo: BaseBo
    {
        public Guid Key { get; set; }
        public string DisplayName { get; set; }
        public bool IsVisible { get; set; }
        public int Order { get; set; }
    }
}
