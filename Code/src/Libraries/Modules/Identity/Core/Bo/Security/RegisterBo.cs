﻿using Inx.Utility.Models;

namespace Inx.Module.Identity.Core.Bo.Security
{
    public class RegisterBo : BaseBo
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
