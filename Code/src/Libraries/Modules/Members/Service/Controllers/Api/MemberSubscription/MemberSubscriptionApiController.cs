﻿using System;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Bo;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.MemberSubscription.Interface;
using Inx.Module.Membership.Service.Models.Subscription;
using Inx.Module.Membership.Service.Utility;
using Inx.Module.Payment.Core.Bo;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.MemberSubscription
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public  class MemberSubscriptionApiController : MembershipBaseApiController
    {
        private readonly IMemberSubscriptionService service;
        public MemberSubscriptionApiController(IMemberSubscriptionService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("members/subscriptions")]
        public async Task<IActionResult> Create([FromBody] MemberSubscriptionViewModel item)
        {
            return await Create<MemberSubscriptionViewModel, MemberSubscriptionBo, IMemberSubscriptionService>(item, service);
        }

        [HttpPost, Route("members/subscriptions/terminate")]
        public async Task<IActionResult> TerminateSubscription([FromBody] TerminationNoteBo item)
        {
            try
            {
                await service.TerminateSubscription(Request(item));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("members/{id:int}/subscription")]
        public async Task<IActionResult> ReadMemberSubscription(int id)
        {
            try
            {
                return Ok(await service.ReadMemberSubscription(Request(id)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPut, Route("members/subscriptions"), ModelValidation]
        public async Task<IActionResult> ChangeMemberSubscription([FromBody] MemberSubscriptionBo item)
        {
            try
            {
                return Ok(await service.ChangeMemberSubscription(Request(item)));

            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPut, Route("members/{id:int}/subscriptions/activate")]
        public async Task<IActionResult> ActivateSubscription(int id)
        {
            try
            {
                await service.ActivateSubscription(Request(id));
                return Ok(await service.ReadMemberSubscription(Request(id)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPost, Route("members/{id:int}/subscriptions/renew"), ModelValidation]
        public async Task<IActionResult> RenewSubscription([FromBody] MemberSubscriptionBo item)
        {
            try
            {
                return Ok(await service.RenewSubscription(Request(item)));
                //return Ok(await service.ReadMemberSubscription(Request(item.MemberId)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPost, Route("members/{id:int}/subscriptions/sendrenewalemail")]
        public async Task<IActionResult> SendRenewalMail(int id)
        {
           // await service.SendRenewalMail(Request(id));
            return Ok();
        }

        [HttpPost, Route("members/subscriptions/{id:int}/changestate")]
        public async Task<IActionResult> ChangeState([FromBody] PaymentBo item)
        {
            await service.ChangeSubscriptionState(Request(item));
            return Ok();
        }
    }
}
