﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;

namespace Membership.StepDefinition.ContactStepsDef
{
    [Binding]
    public sealed class SearchContactSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();
        private SearchContactPO searchContactPO = new SearchContactPO();

        [Given(@"The Application Page Load")]
        public void GivenTheApplicationPageLoad()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"User entered ""(.*)"" and ""(.*)""")]
        public void ThenUserEnteredAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"Clicks on Submit button")]
        public void ThenClicksOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Navigated to Dashboard page")]
        public void ThenNavigatedToDashboardPage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I navigates to Contacts Section")]
        public void ThenINavigatesToContactsSection()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I search firstname as ""(.*)""")]
        public void ThenISearchFirstnameAs(string p0)
        {
            searchContactPO.Search(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Display the results")]
        public void ThenDisplayTheResults()
        {
            Thread.Sleep(4000);
        }

        [Then(@"click on logout button")]
        public void ThenClickOnLogoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //Lastname
        [Given(@"The Application Page")]
        public void GivenTheApplicationPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"I entered ""(.*)"" and ""(.*)""")]
        public void ThenIEnteredAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"Clicked on Submit button")]
        public void ThenClickedOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I Navigated to Dashboard page")]
        public void ThenINavigatedToDashboardPage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I navigated to Contacts Section")]
        public void ThenINavigatedToContactsSection()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I searched lastname as ""(.*)""")]
        public void ThenISearchedLastnameAs(string p0)
        {
            searchContactPO.Search(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Display the related results")]
        public void ThenDisplayTheRelatedResults()
        {
            Thread.Sleep(4000);
        }

        [Then(@"click on logout bttn")]
        public void ThenClickOnLogoutBttn()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        // Mobile
        [Given(@"The membership login page")]
        public void GivenTheMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"Entered ""(.*)"" and ""(.*)""")]
        public void ThenEnteredAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"I Clicked on Submit button")]
        public void ThenIClickedOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I Navigat to Dashboard page")]
        public void ThenINavigatToDashboardPage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I navigat to Contacts Section")]
        public void ThenINavigatToContactsSection()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I searched Mobile as ""(.*)""")]
        public void ThenISearchedMobileAs(string p0)
        {
            searchContactPO.Search(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Display the related result")]
        public void ThenDisplayTheRelatedResult()
        {
            Thread.Sleep(4000);
        }

        [Then(@"click on logout")]
        public void ThenClickOnLogout()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //Email

        [Given(@"The membership page")]
        public void GivenTheMembershipPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }
       
        [Then(@"Enters ""(.*)"" and ""(.*)""")]
        public void ThenEntersAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"User Clicked on Submit button")]
        public void ThenUserClickedOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"User Navigat to Dashboard page")]
        public void ThenUserNavigatToDashboardPage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"User navigat to Contacts Section")]
        public void ThenUserNavigatToContactsSection()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"User searched Email as ""(.*)""")]
        public void ThenUserSearchedEmailAs(string p0)
        {
            searchContactPO.Search(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Displays related result")]
        public void ThenDisplaysRelatedResult()
        {
            Thread.Sleep(4000);
        }

        [Then(@"User click on logout button")]
        public void ThenUserClickOnLogoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
            
        }
        // Organization

        [Given(@"The membership home page")]
        public void GivenTheMembershipHomePage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"I enters ""(.*)"" and ""(.*)""")]
        public void ThenIEntersAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }


        [Then(@"I click on Submit button")]
        public void ThenIClickOnSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigate to Dashboard page")]
        public void ThenINavigateToDashboardPage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I navigate to Contacts Section")]
        public void ThenINavigateToContactsSection()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I search company as ""(.*)""")]
        public void ThenISearchCompanyAs(string p0)
        {
            searchContactPO.Search(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Displays related results")]
        public void ThenDisplaysRelatedResults()
        {
            Thread.Sleep(4000);
        }

        [Then(@"User click on logout btn")]
        public void ThenUserClickOnLogoutBtn()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        //Negative

        [Given(@"Membership home page")]
        public void GivenMembershipHomePage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"Enter ""(.*)"" and ""(.*)""")]
        public void ThenEnterAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"Click Submit button")]
        public void ThenClickSubmitButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Navigated Dashboard")]
        public void ThenNavigatedDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I navigated to Contact Section")]
        public void ThenINavigatedToContactSection()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I searched company (.*)")]
        public void ThenISearchedCompanyV(string p0)
        {
            searchContactPO.Search(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Display related results")]
        public void ThenDisplayRelatedResults()
        {
            Thread.Sleep(4000);
        }

        [Then(@"User clicked on logout btn")]
        public void ThenUserClickedOnLogoutBtn()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}

