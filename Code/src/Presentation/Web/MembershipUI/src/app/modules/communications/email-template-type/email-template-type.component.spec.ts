import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTemplateTypeComponent } from './email-template-type.component';

describe('EmailTemplateTypeComponent', () => {
  let component: EmailTemplateTypeComponent;
  let fixture: ComponentFixture<EmailTemplateTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTemplateTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTemplateTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
