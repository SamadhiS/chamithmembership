import { LevelService } from './../../core/services/level.service';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { MembersService } from '../../core/services/members.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { BlockUiService } from '../../core/services/block-ui.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-member-levels',
  templateUrl: './member-levels.component.html',
  styleUrls: ['./member-levels.component.scss']
})
export class MemberLevelsComponent implements OnInit {

  skip: number;
  take: number;
  memberLevelId = 0;
  memberlevelCount = 0;
  memberlevelList = [];
  isBlocked = false;

  @ViewChild('levelModal') public groupModal: ModalDirective;
  @Output() group: EventEmitter<any> = new EventEmitter();
   @ViewChild('terminateNoteModal') public terminateNoteModal: ModalDirective;

  constructor(private memberService: MembersService,
    private router: Router,
    translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private blockUiService: BlockUiService,
    private levelService: LevelService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.skip = 0;
    this.take = 0;
    this.getMemberLevels();
  }
  getMemberLevels() {
    this.isBlocked = true;
    this.levelService.getAllMemberLevel(this.skip, this.take)
      .subscribe(allmemberLevel => {
        this.memberlevelList = allmemberLevel.result.items;
        this.memberlevelCount = allmemberLevel.result.totalRecodeCount
        if (allmemberLevel.result.totalRecodeCount == 0) {
        }
        this.isBlocked = false;
      },
      error => {
        this.isBlocked = false;
        this.toastr.error('Error in Loading Group', 'Oops');
      });
  }
  getMemberLevel(id){
    this.router.navigate(["/members/memberlevelInformation", id])
  }
  public hideTerminationModal(): void {
    this.terminateNoteModal.hide();
  }

    
}
