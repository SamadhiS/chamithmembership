﻿using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inx.Module.Membership.Data.Entity.Members
{
    [Table("MembershipTimeline", Schema = Constraints.Schema)]
    public class MembershipTimeline : AuditableEntity
    {
        [NumberNotZero] public int MemberId { get; set; }
        public string SubscriptionName { get; set; }
        public int? SubscriptionId { get; set; } = 0;
        public int? PaymentId { get; set; } = 0;
        public double? Amount { get; set; } = 0;
        public int TemplateType { get; set; }
    }
}
