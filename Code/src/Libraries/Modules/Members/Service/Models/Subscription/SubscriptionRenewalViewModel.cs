﻿using Inx.Service.Base.Models;

namespace Inx.Module.Membership.Service.Models.Subscription
{
    public class SubscriptionRenewalViewModel : BaseViewModel
    {
        public bool IsRenewable { get; set; }
        public bool IsSendEmail { get; set; }
        public int RenewalDays { get; set; }
        public int EmaillTemplateId { get; set; }
    }
}
