﻿using Inx.Test.Automation.Base.Element;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;

namespace Membership.test.StepDefinition
{
    [Binding]
    public sealed class CreatePayment
    {
        private static NgWebDriver driver = new NgWebDriver(new ChromeDriver());

        [Given(@"System application page is opened")]
        public void GivenSystemApplicationPageIsOpened()
        {
            
        }

        [Given(@"Enter (.*) and (.*)")]
        public void GivenEnterAnd(string p0, string p1)
        {
            driver.SetValue("txtEmail", p0);
            driver.SetValue("txtPassword", p1);
        }

        [Given(@"click on login")]
        public void GivenClickOnLogin()
        {
            driver.Click("btnLogin", 6000);
        }

        [Given(@"Naviagate to Homepage")]
        public void GivenNaviagateToHomepage()
        {
            Thread.Sleep(5000);
        }

        [Given(@"click on Payments")]
        public void GivenClickOnPayments()
        {
            driver.Click("btnPayment", true, 6000);
        }

        [Given(@"select the payment status as (.*)")]
        public void GivenSelectThePaymentStatusAs(string p0)
        {
            // Read the Status 
            // Click on View  
            driver.Click("btnViwePayment", true, 6000);
        }

        [Given(@"click on Settle")]
        public void GivenClickOnSettle()
        {
           
        }

        [Given(@"Select Payment Type as ""(.*)""")]
        public void GivenSelectPaymentTypeAs(string p0)
        {
           
        }

        [Given(@"Click on Pay Now")]
        public void GivenClickOnPayNow()
        {
            
        }

        [Given(@"Success message appears")]
        public void GivenSuccessMessageAppears()
        {
            
        }

        [Given(@"Navigate back to Payment section")]
        public void GivenNavigateBackToPaymentSection()
        {
            
        }

        [Given(@"Log out")]
        public void GivenLogOut()
        {
            
        }

        [Given(@"there is no Settle Button")]
        public void GivenThereIsNoSettleButton()
        {
            
        }

    }
}
