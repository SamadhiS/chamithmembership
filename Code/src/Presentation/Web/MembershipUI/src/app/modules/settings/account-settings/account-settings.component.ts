import { TenantInfoModel } from './../models/tenantInfoModel';
import { error } from 'protractor';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { SettingService } from './../services/setting.service';
import { ChangePassword } from './../models/passwordModel';
import { PatternModel } from './../../shared/patternvalidator/pattern';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { TenantInfoService } from '../../core/services/tenant-info.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { LookupService } from '../../core/services/lookup.service';
import { Observable } from 'rxjs/Observable';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {

  imageLogoSource = 'https://cdn.yoreparo.com/uploads/imagen_nodo/0001/01/thumb_69_imagen_nodo_big.jpeg';
  tenantInfoForm: FormGroup;

  organizationLogoFile: FileList;
  organizationLogoName: string;
  tenantInfo = new TenantInfoModel();
  patternModel = new PatternModel();
  isBlocked = true;
  orgImageName = 'Select a Logo For Organization';
  isLoaded = false;
  isCreateTenantInfoFormSubmitted = false;
  countries = [];
  languages =[];
  currencys = [];
  countryCode: any;
  language :string;
  currency :string;

  constructor(private fb: FormBuilder, vcr: ViewContainerRef, public toastr: ToastsManager,
    private settingService: SettingService,
    private lookUpService: LookupService,
    private translateService: TranslateService,
    private tenantInfoService: TenantInfoService,
    private blockUiService: BlockUiService, ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  fileUpload(event) {
    this.organizationLogoFile = event.target.files;
    this.orgImageName= this.organizationLogoFile[0].name
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageLogoSource = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  loadOrganizationLogo() {
    let tenantObj = localStorage.getItem('tenantObj');
    let tenantJson = JSON.parse(tenantObj);
    if (this.getImageName(tenantJson.organizationLogo) != 'noorganizationlogo.png') {
      this.imageLogoSource = tenantJson.organizationLogo;
      this.organizationLogoName = this.getImageName(this.imageLogoSource);
    }
  }

  updateOrganization() {
    this.isBlocked = true;
    this.tenantInfoService.getTenantInfo()
      .subscribe(tenant => {
        let tenantInfo = tenant.result;
        localStorage.setItem('Prefix', this.tenantInfo.prefix)
        tenantInfo.organizationLogo = this.organizationLogoName;
        this.tenantInfoService.updateTenant(this.organizationLogoFile, tenantInfo)
          .subscribe(res => {
            this.tenantInfoService.getTenantInfo()
              .subscribe(res => {
                localStorage.setItem('tenantObj', JSON.stringify(res.result));
                localStorage.removeItem('Image_Data');
                this.isBlocked = false;
                this.orgImageName = 'Select a Logo For Organization';
                this.toastr.success('Organization Logo is successfully updated', 'Success');
                location.reload();
              });
          }, error => {
            this.isBlocked = false;
            this.toastr.error('Error in uploading organization Logo', 'Oops');
          });
      });
  }

  private getImageName(profileImage) {
    let splitFilePath = profileImage.split("/");
    return splitFilePath[splitFilePath.length - 1]
  }

  countryCodechange(event) {
    this.countries.forEach(element => {
      if (element.text.trim() == event.label) {
        this.countryCode = element.phoneCode
      }
    });
  }


  changelanguage(eventno :number){
    if(eventno == 2)
    {
      this.language = 'sh';
    }
   else if(eventno == 3)
   {
    this.language = 'tm';
   }
   else 
   {
    this.language = 'en';
   }
    this.translateService.use(this.language);
    this.translateService.setDefaultLang(this.language);
  
  }

  createTenantInfoForm() {
    this.isCreateTenantInfoFormSubmitted = false;
    this.tenantInfoForm = this.fb.group({
      email: ['', [patternValidator(this.patternModel.emailRegex)]],
      address: ['',[Validators.required]],
      language: [1, [Validators.required]],
      currency: [1, [Validators.required]],
      city: [''],
      state: [''],
      zip: [''],
      country: [57],
      phone: ["", [Validators.required, patternValidator(this.patternModel.phoneregx)]],
      fax: [''],
      prefix:['',[Validators.required]],
    });
  }

  saveTenantInfo() {
    this.isCreateTenantInfoFormSubmitted = true;    
    if (this.tenantInfoForm.invalid) return;
    this.isBlocked = true;
    var tenantInfo = Object.assign({}, this.tenantInfo, this.tenantInfoForm.value);

    this.settingService.saveTenantInfo(tenantInfo)
      .subscribe(res => {
        this.isBlocked = false;
        localStorage.setItem('Language', this.language);
        localStorage.setItem('Currency', "Rs")    
        localStorage.setItem('Prefix', this.tenantInfo.prefix)
        this.toastr.success(ToasterMessages.save('account owner info'), toasterHeadings.Success);
      }, error => {
        this.isBlocked = false;
        this.toastr.success(ToasterMessages.saveError('account owner info'), toasterHeadings.Error);
      })
  }

  getTenantInfo() {
    this.settingService.getTenantInfo().subscribe(res => {
      this.patchTenantInfoForm(res.result);
      if(res.result.language == 2)
      {
        this.language = 'sh';
      }
     else if(res.result.language == 3)
     {
      this.language = 'tm';
     }
     else 
     {
      this.language = 'en';
     }
      localStorage.setItem('Language', this.language);
      localStorage.setItem('Prefix', res.result.prefix)
    },
      error => {
        this.isBlocked = false;
      }, () => {
        this.isBlocked = false;
      })
  }

  private patchTenantInfoForm(tenantInfo: TenantInfoModel) {
    this.isBlocked = true;
    if(tenantInfo.language == 2)
    {
      this.language = 'sh';
    }
   else if(tenantInfo.language == 3)
   {
    this.language = 'tm';
   }
   else 
   {
    this.language = 'en';
   }
    this.translateService.use(this.language);
    this.translateService.setDefaultLang(this.language);
    localStorage.setItem('Language', this.language);
    this.tenantInfo = tenantInfo; 
    localStorage.setItem('Prefix', this.tenantInfo.prefix)

    if(this.tenantInfo == null){
      this.isLoaded = false;
    }else{
      this.isLoaded = true;
    }

    this.countries.forEach(element => {
      if (element.value == tenantInfo.country) {
        this.countryCode = element.phoneCode
      }
    });
    this.tenantInfoForm.patchValue({
      email: tenantInfo.email,
      address: tenantInfo.address,
      city: tenantInfo.city,
      state: tenantInfo.state,
      zip: tenantInfo.zip,
      currency: tenantInfo.currency,
      country: tenantInfo.country,
      phone: tenantInfo.phone,
      fax: tenantInfo.fax,
      language: tenantInfo.language,
      prefix:tenantInfo.prefix
    })
    this.isBlocked = false;
  }

  updateTenantInfo() {
    this.isCreateTenantInfoFormSubmitted = true;    
    if (this.tenantInfoForm.invalid) return;
    this.isBlocked = true;
    this.tenantInfo = Object.assign({}, this.tenantInfo, this.tenantInfoForm.value);
    localStorage.setItem('Prefix', this.tenantInfo.prefix)
    localStorage.setItem('Language', this.language);
    localStorage.setItem('Currency', "Rs")
    if(this.tenantInfo.currency == 1){
    }
    this.settingService.updateTenantInfo(this.tenantInfo)
      .subscribe(res => {
        this.isBlocked = false;
        this.toastr.success(ToasterMessages.update('Account Owner Info'), toasterHeadings.Success);
      }, error => {
        this.isBlocked = false;
        this.toastr.success(ToasterMessages.updateError('Account Owner Info'), toasterHeadings.Error);
      })
  }

  private getContactLookupData() {
    this.lookUpService.getCountries()
      .subscribe(countries => {
        this.countryCode = "94";
        this.countries = countries.result.item;
      })
      
    this.lookUpService.getLanguages()
      .subscribe(languages => {
        this.languages = languages.result;
      })
      this.lookUpService.getCurrency()
      .subscribe(currency => {
        this.currencys = currency.result;
      })
  }

  ngOnInit() {
    this.loadOrganizationLogo();
    this.createTenantInfoForm();
    this.getTenantInfo();
    this.getContactLookupData(); 
  }
}
