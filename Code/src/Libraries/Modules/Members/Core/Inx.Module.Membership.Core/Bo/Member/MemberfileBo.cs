﻿using System;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Member
{
    public class MemberFileBo : BaseBo
    {
        public int MemberId { get; set; }
        public string FileName { get; set; }
        public string FileSaveName { get; set; }
        public decimal Size { get; set; }
        public DateTime CreatedOn { get; set; }
        public string DownloadPath { get; set; }
        [Obsolete]
        public new string Name { get; set; }
    }
}
