﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Core.Bo.Security.User;
using Inx.Module.Identity.Core.Bo.User;
using Inx.Module.Identity.Core.Utility;
using Inx.Module.Identity.Data.DbContext;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using UserBo = Inx.Module.Identity.Core.Bo.Security.User.UserBo;

namespace Inx.Module.Identity.Core.Services.Security
{
    public partial class SecurityService : BaseService, ISecurityService
    {
        private readonly IUnitOfWorkIdentity unitOfWorkIdentity;

        public SecurityService(IUnitOfWorkIdentity _unitOfWorkIdentity)
        {
            unitOfWorkIdentity = _unitOfWorkIdentity;
        }

        public async Task<AuthResponse> Authentication(AuthenticateBo request)
        {
            try
            {
                var result = (from user in unitOfWorkIdentity.UserRepository.TableAsNoTracking
                    where user.Email == request.Email &&
                          user.Password == request.Password
                    select new
                    {
                        user.IsEmailConfirmed,
                        user.RecordStatus,
                        user.TenantId,
                        userId = user.Id
                    }).AsNoTracking().FirstOrDefault();
                if (result.IsNull())
                {
                    throw new UserSecurityException(new ComplexBadResponse
                    {
                        Message = Bo.Enums.SecurityErrorTypes.InvaliedUsernameOrPassword.ToString(),
                        State = (int) Bo.Enums.SecurityErrorTypes.InvaliedUsernameOrPassword
                    });
                }
                else if (!new[]{Enums.UserRecordStatus.Active,Enums.UserRecordStatus.EmailValidated}
                    .Contains(result.RecordStatus))
                {
                    throw new UserSecurityException(ErrorResponse(result.RecordStatus));
                }
                //access claims
                var roleresult = await (from userroles in unitOfWorkIdentity.UserRoleRepository.TableAsNoTracking
                    join roles in unitOfWorkIdentity.RoleRepository.TableAsNoTracking on userroles.RoleId equals roles.Id
                    where userroles.UserId == result.userId 
                    select new
                    {
                        roles.Name
                    }).AsNoTracking().ToListAsync();
                var rolex = new List<string>();
                foreach (var item in roleresult)
                {
                    rolex.Add(item.Name.ToString());
                }
                return new AuthResponse
                {
                    TenantId = result.TenantId,
                    Id = result.userId,
                    Email = request.Email,
                    Roles = rolex,
                    UserState = result.RecordStatus
                };
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public Task<RegisterResponse> CreateUser(Request<UserBo> request)
        {
            throw new NotImplementedException();
        }

        [Description("give email address for register user")]
        public async Task<RegisterResponse> Register(Request<RegisterBo> request)
        {
            IDbContextTransaction tra = null;
            try
            {
                tra = await unitOfWorkIdentity.Context.Database.BeginTransactionAsync();
                var token = Guid.NewGuid();
                var @user = new User
                {
                    CreatedById = 0,
                    CreatedOn = DateTime.UtcNow,
                    Email = request.Item.Email,
                    EmailConfirmCode = token,
                    TenantId = 0,
                    Password = request.Item.Password,
                    RecordStatus = Enums.UserRecordStatus.Active
                };
                await unitOfWorkIdentity.Context.AddAsync(@user);
                await unitOfWorkIdentity.SaveAsync();
                await unitOfWorkIdentity.Context.AddAsync(new UserRole
                {
                    TenantId = 0,
                    CreatedById = @user.Id,
                    CreatedOn = DateTime.UtcNow,


                    RecordState = Enums.RecordStatus.Active,
                    UserId = @user.Id,
                    RoleId = 1
                });
                await unitOfWorkIdentity.SaveAsync();
                tra.Commit();
                return new RegisterResponse
                {
                    UserId = @user.Id,
                    Token = token
                };
            }
            catch (DbUpdateException ex)
            {
                tra?.Rollback();
                var item = ex.GetBaseException() as SqlException;
                if (!item.IsNull() && item.Number == 2601)
                {
                    // check is email already there and if there handle error rmessage
                    var result = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == request.Item.Email);
                    throw new UserSecurityException(ErrorResponse(result.RecordStatus));
                }
                throw ex.HandleException();
            }
            catch (Exception ex)
            {
                tra?.Rollback();
                throw ex.HandleException();
            }
        }

        [Description("registeration,email confirm,resend email")]
        public async Task<Guid> ResendRegisterValidateEmail(Request<string> email)
        {
            try
            {
                var token = Guid.NewGuid();
                var item = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == email.Item);
                if (item.IsEmailConfirmed)
                {
                    throw new UserSecurityException(ErrorResponse(item.RecordStatus));
                }
                item.EmailConfirmCode = token;
                await unitOfWorkIdentity.SaveAsync();
                return token;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        [Description("validate email address by token")]
        public async Task<int> ValidateEmailAddress(Request<Dictionary<string, string>> request)
        {
            try
            {
                var item = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == request.Item["email"]);
                if (item.IsNull())
                {
                    throw new UserSecurityException(new ComplexBadResponse
                    {
                        State = (int)Bo.Enums.SecurityErrorTypes.EmailNotFoundedInSystem,
                        Message = Bo.Enums.SecurityErrorTypes.EmailNotFoundedInSystem.ToString()
                    });
                }
                if (item.IsEmailConfirmed)
                {
                    throw new UserSecurityException(ErrorResponse(item.RecordStatus));
                }
                if (!item.IsEmailConfirmed && item.EmailConfirmCode == request.Item["token"].ToValiedGuid())
                {
                    item.IsEmailConfirmed = true;
                    item.EmailConfirmCode = Guid.Empty;
                    item.RecordStatus = Enums.UserRecordStatus.EmailValidated;
                    await unitOfWorkIdentity.SaveAsync();
                    return item.Id;
                }
                else
                {
                    throw new UserSecurityException(new ComplexBadResponse
                    {
                        State = (int)Bo.Enums.SecurityErrorTypes.InvaliedEmailOrToken,
                        Message = Bo.Enums.SecurityErrorTypes.InvaliedEmailOrToken.ToString()
                    });
                }
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
       
        public async Task ChangePassword(Request<ChangePasswordBo> request)
        {
            try
            {
                var currentpw = request.Item.CurrentPassword.Encrypt();
                var item = await unitOfWorkIdentity.UserRepository.Read(p => p.Password
                                                              == currentpw && p.Id == request.UserId, request.TenantId);
                item.Password = request.Item.NewPassword.Encrypt();
                await unitOfWorkIdentity.SaveAsync();
            } 
            catch (RecordNotFoundException)
            {
                throw new RecordNotFoundException(EMessages.InvaliedCurrentPassword.ToString())
                    .HandleException();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        #region forget password
        public async Task<Guid> ForgetPasswordRequest(Request<string> email)
        {
            try
            {
                var item = await unitOfWorkIdentity.UserRepository.Table.FirstOrDefaultAsync(p => p.Email == email.Item);
                if (item.IsNull())
                {
                    throw new UserSecurityException(new ComplexBadResponse
                    {
                        Message = Bo.Enums.SecurityErrorTypes.EmailNotFoundedInSystem.ToString(),
                        State = (int)Bo.Enums.SecurityErrorTypes.EmailNotFoundedInSystem
                    });
                }else if (!item.IsEmailConfirmed)
                {
                    throw new UserSecurityException(new ComplexBadResponse
                    {
                        Message = Bo.Enums.SecurityErrorTypes.EmailSubmitedWithoutValidating.ToString(),
                        State = (int)Bo.Enums.SecurityErrorTypes.EmailSubmitedWithoutValidating
                    });
                }
                item.PasswordResetCode = Guid.NewGuid();
                await unitOfWorkIdentity.SaveAsync();
                return item.PasswordResetCode;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<AuthResponse> ForgetPasswordValidateToken(Request<Dictionary<string, string>> request)
        {
            try
            {
                var item = await unitOfWorkIdentity.UserRepository.Read(p => p.Email == request.Item["email"] &&
                                                              p.PasswordResetCode == request.Item["token"].ToValiedGuid());

                item.PasswordResetCode = Guid.Empty;
                var pw = item.Password;
                await unitOfWorkIdentity.SaveAsync();
                return await Authentication(new AuthenticateBo
                {
                    Email = request.Item["email"],
                    Password = item.Password
                });
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task ForgetPasswordChangePassword(Request<string> request)
        {
            try
            {
                var item = await unitOfWorkIdentity.UserRepository.Read(p => p.Id == request.UserId, request.TenantId);
                item.Password = request.Item.Encrypt();
                await unitOfWorkIdentity.SaveAsync();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        #endregion

      
        #region utility

        ComplexBadResponse ErrorResponse(Enums.UserRecordStatus userRecordStatus)
        {
            var complexResponse = new ComplexBadResponse();
            switch (userRecordStatus)
            {
                case Enums.UserRecordStatus.EmailSubmit:
                    complexResponse = new ComplexBadResponse
                    {
                        State = (int)Bo.Enums.SecurityErrorTypes.EmailSubmitedWithoutValidating,
                        Message = Bo.Enums.SecurityErrorTypes.EmailSubmitedWithoutValidating.ToString()
                    };
                    break;
                case Enums.UserRecordStatus.EmailValidated:
                    complexResponse = new ComplexBadResponse
                    {
                        State = (int)Bo.Enums.SecurityErrorTypes.EmailRegistedWithoutTenant,
                        Message = Bo.Enums.SecurityErrorTypes.EmailRegistedWithoutTenant.ToString()
                    };
                    break;
                default:
                    if (new[] { Enums.UserRecordStatus.Archive, Enums.UserRecordStatus.Delete, Enums.UserRecordStatus.Inactive }.Contains(userRecordStatus))
                    {
                        complexResponse = new ComplexBadResponse
                        {
                            State = (int)Bo.Enums.SecurityErrorTypes.AlreadyRegstedButInactive,
                            Message = Bo.Enums.SecurityErrorTypes.AlreadyRegstedButInactive.ToString(),
                            Description = "Contact admin"
                        };
                    }
                    else
                    {
                        complexResponse = new ComplexBadResponse
                        {
                            State = (int)Bo.Enums.SecurityErrorTypes.AlreadyRegisted,
                            Message = Bo.Enums.SecurityErrorTypes.AlreadyRegisted.ToString()
                        };
                    }
                    break;
            }
            return complexResponse;
        }
        #endregion
    }
}
