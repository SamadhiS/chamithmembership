import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from '../model/user';

@Injectable()
export class RegisteruserService extends BaseService {

  user = new User();
  constructor(private http: HttpClient) {
    super();
  }
  sendMail(user) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/register`,
      user, this.httpOptions)
      .map(this.extractData)
      .catch(this.registerErrorHandler)
  }
  resendMail(resendMailObj) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/register/resendregistervalidateemail`,
      resendMailObj, this.httpOptions)
      .map(this.extractData)
      .catch(this.errorHandler)
  }
  validateMail(emailToken) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/register/validateemailaddresstoken?email=` + emailToken.email + `&token=` + emailToken.token,
      emailToken, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  getOrganizationtypes() {
    return this.http.get(`${this.baseApiEndPoint}identity/v1/tenant/lookups/organizationtypes`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  verifyHostName(hostObj) {
    return this.http.get(`${this.baseApiEndPoint}identity/v1/tenants/`+ hostObj +`/available`, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  register(files, user) {
    let formData: FormData = new FormData();
    
    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('tenantViewData', JSON.stringify(user));
    return this.http.post(`${this.baseApiEndPoint}identity/v1/tenants`,
      formData, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  authenticate(userLogin) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/authentication`,
      userLogin, this.httpOptions)
      .map((response) => response)
      .catch(this.registerErrorHandler)
  }
  sendForgotpasswordMail(email) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/forgetpasswordrequest`,
      email, this.httpOptions)
      .map(this.extractData)
      .catch(this.errorHandler)
  }
  validateResetEmail(emailToken) {
    return this.http.post(`${this.baseApiEndPoint}identity/v1/security/forgetpasswordvalidatetoken?email=` + emailToken.email + `&token=` + emailToken.token,
      emailToken, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  resetforgetpassword(pwdObj) {
    return this.http.put(`${this.baseApiEndPoint}identity/v1/security/forgetpasswordsetnewpassword`,
      pwdObj, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
}
