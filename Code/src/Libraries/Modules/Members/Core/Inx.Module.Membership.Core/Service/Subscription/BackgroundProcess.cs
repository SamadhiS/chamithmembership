﻿using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Utility.Utility;
using System;
using System.Threading.Tasks;
using System.Linq;
using Inx.Module.Messenger.Data.DbContext;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Microsoft.EntityFrameworkCore;
using Inx.Module.Payment.Data.DbContext;

namespace Inx.Module.Membership.Core.Service.Subscription
{
    public class BackgroundProcess : MemberBaseService, IBackgroundProcess
    {
        private readonly IUnitOfWokMembership uowMembership;
        private readonly IUnitOfWorkPayment uowPayment;
        private readonly IUnitOfWorkMassengers uowmaMassengers;
       // private readonly IMemberService memberService;

        public BackgroundProcess(IUnitOfWokMembership _uowMembership, IUnitOfWorkPayment _uowPayment, IUnitOfWorkMassengers _uowmaMassengers)//, IMemberService _memberService) : base()
        {
            uowMembership = _uowMembership;
            uowmaMassengers = _uowmaMassengers;
            uowPayment = _uowPayment;
           // memberService = _memberService;
        }

        public async Task ChangeSubscriptionStatus()
        {
            var memberSubscription = await (from memberSubs in uowMembership.MemberSubscriptionRepository.Table
                                            where memberSubs.RenewDate < DateTime.UtcNow && memberSubs.MembershipStatus == Enums.MembershipStatus.Active
                                            select memberSubs).ToListAsync();

            foreach (var rec in memberSubscription)
            {
                var paymentMaster = await (from payMaster in uowPayment.PaymentMasterRepository.Table
                                           where payMaster.MemberSubscriptionId == rec.Id
                                           select payMaster).FirstOrDefaultAsync();
                if (paymentMaster != null)
                {
                    paymentMaster.PaymentState = Enums.PaymentState.None;
                    await uowPayment.SaveAsync();

                    rec.MembershipStatus = Enums.MembershipStatus.Inactive;
                    await uowMembership.SaveAsync();
                }
                
            };
        }

        public async Task SendRenewalEmail()
        {
            //TimeSpan RemainingDays;
            //var activeSubscriptions = await (from membersubs in uowMembership.MemberSubscriptionRepository.Table
            //                                 join subs in uowMembership.SubscriptionRepository.Table on membersubs.SubscriptinId equals subs.Id
            //                                 join contact in uowMembership.ContactInfoRepository.Table on membersubs.MemberId equals contact.MemberId
            //                                 where membersubs.MembershipStatus == Enums.MembershipStatus.Active && subs.IsSendEmail == true
            //                                 select new
            //                                 {
            //                                     membersubs.Id,
            //                                     membersubs.MemberId,
            //                                     membersubs.JoinDate,
            //                                     membersubs.RenewDate,
            //                                     subs.SubscriptionTypeId,
            //                                     subs.RenewalDays,
            //                                     subs.EmaillTemplateId,
            //                                     contact.Email
            //                                 }).ToListAsync();


            //foreach (var activeSubs in activeSubscriptions)
            //{
            //    RemainingDays = activeSubs.RenewDate.Subtract(DateTime.UtcNow);

            //    if (RemainingDays.Days == activeSubs.RenewalDays)
            //    {
            //        var emailTemplateInfo = await (from emailTemplate in uowmaMassengers.EmailTemplateRepository.Table
            //                                 where emailTemplate.Id == activeSubs.EmaillTemplateId
            //                                 select emailTemplate).FirstOrDefaultAsync();

            //        var emailSendingInfoModel = new Messenger.Core.Bo.Emails.EmailSendingInfoModel
            //        {
            //            EmailBody = emailTemplateInfo.EmailBody,
            //            Subject = "Renewal Email",
            //            EmailSendType = Enums.EmailSendTypes.Single,
            //            MemberLevelId = activeSubs.SubscriptionTypeId,
            //            MemberId = activeSubs.MemberId,
            //            TemplateId = activeSubs.EmaillTemplateId,
            //            ToAddress = activeSubs.Email,
            //        };

            //        //await memberService.SendEmails(new Inx.Utility.Models.Request<Messenger.Core.Bo.Emails.EmailSendingInfoModel>()
            //        //{
            //        //    Item = emailSendingInfoModel,
            //        //    TenantId = emailTemplateInfo.TenantId,
            //        //    UserId = emailTemplateInfo.CreatedById
            //        //});
            //    };
           // }
        }
    }
}
