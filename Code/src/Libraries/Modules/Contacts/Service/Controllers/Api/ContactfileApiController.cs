﻿using System.Threading.Tasks;
using Inx.Module.Contacts.Service.Utility;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Inx.Service.Base.Attributes;
using Inx.Utility.Utility;
using Inx.Module.Contacts.Service.Models.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Newtonsoft.Json;
using System;
namespace Inx.Module.Contacts.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class ContactfileApiController : ContactsBaseApiController, IBaseApi<ContactfileViewModel, int>
    {

        private readonly IContactFileService service;
        public ContactfileApiController(IContactFileService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("contactfiles")]
        public async Task<IActionResult> Create()
        {
            string[] filename = null;
            try
            {
                var httpRequest = HttpContext.Request;
                var contactViewData = httpRequest.Form["contactFileViewData"];
                var item = JsonConvert.DeserializeObject<ContactfileViewModel>(contactViewData);
                var result = await UploadBlobFiles(Enums.FileType.ContactsFiles);
                filename = result.FileNamesKeyValue[0];
                item.SetFileInfo(filename[0], filename[1], filename[2]);
                return await Create<ContactfileViewModel, ContactfileBo, IContactFileService>(item, service);
            }
            catch (Exception e)
            {
                //if any happen delete file if exist.
                if (!string.IsNullOrEmpty(filename?[1]))
                {
                    Hangfire.BackgroundJob.Enqueue(() => DeleteBlob(Enums.FileType.MembersFiles, filename[1]));
                }

                return await HandleException(e);
            }
        }
        [HttpDelete, Route("contactfiles/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var result = await service.Delete(Request(id));
                Hangfire.BackgroundJob.Enqueue(() => DeleteBlob(Enums.FileType.ContactsFiles, result));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("contactfiles")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<ContactfileViewModel, ContactfileBo, IContactFileService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }

        #region ignored

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<ContactfileViewModel, ContactfileBo, IContactFileService>(id, this.service);
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<ContactfileBo, IContactFileService>(service);
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public new Task<IActionResult> Create(ContactfileViewModel item)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Update([FromBody]  ContactfileViewModel item)
        {
            return await Update<ContactfileViewModel, ContactfileBo, IContactFileService>(item, this.service);
        }
        #endregion

    }
}