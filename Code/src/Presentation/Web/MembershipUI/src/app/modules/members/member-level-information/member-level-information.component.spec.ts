import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberLevelInformationComponent } from './member-level-information.component';

describe('MemberLevelInformationComponent', () => {
  let component: MemberLevelInformationComponent;
  let fixture: ComponentFixture<MemberLevelInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberLevelInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberLevelInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
