import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable()
export class PaymentService extends BaseService {

  private apiEndPoint = `${this.baseApiEndPoint}membership/v1/payments`;
  private apiPaymentEndPoint = `${this.baseApiEndPoint}payments/v1/payments`;
  constructor(private http: HttpClient) {
    super()
  }

  getPayments(skip, take, search, orderby) {
    return this.http.get(`${this.apiEndPoint}?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getPayment(id) { 
    return this.http.get(`${this.apiPaymentEndPoint}/${id}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  savePaymnets(payment) {
    return this.http.post(this.apiPaymentEndPoint, payment, this.httpOptions)
    .map(res => res).catch(this.errorHandler)
  }

  getTransActionTypes() {
    return this.http.get(`${this.apiPaymentEndPoint}/transactiontypes`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getPaymentTypes() {
    return this.http.get(`${this.apiPaymentEndPoint}/paymenttypes`)
      .map((response) => response)
      .catch(this.errorHandler)
  }


  getMemberPayment(memberId) {
    return this.http.get(`${this.apiEndPoint}/${memberId}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }


}
