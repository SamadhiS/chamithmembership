﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo;
using Inx.Module.Membership.Core.Bo.Member.Master;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.Members.Master;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = Inx.Utility.Utility.Enums;

namespace Inx.Module.Membership.Service.Controllers.Api.Members.Master
{
    [Route(Constraints.ApiPrefix)/*, AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)*/]
    public class MemberInfoApiController: MembershipBaseApiController,IBaseApi<MemberInfoViewModel,int>
    {
        private IMemberInfoService memberInfoService;
        private IMemberFieldRequiredService memberFieldRequiredService;
        public MemberInfoApiController(IMemberInfoService _memberInfoService, IMemberFieldRequiredService _memberFieldRequiredService, 
            IBaseInjector baseinject) :base(baseinject)
        {
            memberInfoService = _memberInfoService;
            memberFieldRequiredService = _memberFieldRequiredService;
        }

        [HttpPost, Route("members"), ModelValidation]
        public async Task<IActionResult> Create([FromBody] MemberInfoViewModel item)
        {
            try
            {
                var systemFields = await memberFieldRequiredService.Read();
                var memberid = 0;
                if (item.MemberType == Inx.Utility.Utility.Enums.MemberType.Person)
                {
                    systemFields = systemFields.Where(p => p.MemberType == Enums.MemberType.Person).ToList();
                    var requestItem = new MemberInfoBo<PersonalInfoBo> {MemberType = Enums.MemberType.Person};
                    foreach (var field in systemFields)
                    {
                        var obj = item.MemberFieldKeyValue.FirstOrDefault(p => p.Key == field.Key);
                        if (obj.IsNull())
                        {
                            throw new DataInconsistencyClientException();
                        }
                        requestItem.GetType().GetProperty(field.Name).SetValue(requestItem, obj.Value);
                        requestItem.MemberFieldKeyValue = item.MemberFieldKeyValue
                            .MapListObject<MemberFieldKeyValueViewModel, MemberFieldKeyValueBo>().ToList();
                    }
                    memberid = await memberInfoService.Create(Request(requestItem));
                }
                else
                {
                    systemFields = systemFields.Where(p => p.MemberType == Enums.MemberType.Organization).ToList();
                    var  requestItem = new MemberInfoBo<OrganizationInfoBo> {MemberType = Enums.MemberType.Organization};
                    foreach (var field in systemFields)
                    {
                        var obj = item.MemberFieldKeyValue.FirstOrDefault(p => p.Key == field.Key);
                        if (obj.IsNull())
                        {
                            throw new DataInconsistencyClientException();
                        }
                        requestItem.GetType().GetProperty(field.Name).SetValue(requestItem, obj.Value);
                        requestItem.MemberFieldKeyValue = item.MemberFieldKeyValue
                            .MapListObject<MemberFieldKeyValueViewModel, MemberFieldKeyValueBo>().ToList();
                    }
                    memberid = await memberInfoService.Create(Request(requestItem));
                }
                return Ok(memberid);
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpDelete, Route("members/{id:int}"), Description("member will be archive")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await memberInfoService.Delete(Request(id));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            try
            {
                return Ok(await memberInfoService.Search(SearchRequest(skip, take, searchTerm: search,
                    orderByTerm: orderby)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            try
            {
                return Ok(await memberInfoService.Read(Request(id)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("members/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            try
            {
                return Ok(await memberInfoService.ReadKeyValue(SearchRequest()));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpPut, Route("members")]
        public new Task<IActionResult> Update([FromBody] MemberInfoViewModel item)
        {
            throw new NotImplementedException();
        }
    }
}
