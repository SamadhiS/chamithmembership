﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base.Bo.Notification;
using Inx.Core.Base.Service.Interface;
using Inx.Data.Base.Entity.Notification;
using Inx.Module.Identity.Core.Bo;
using Inx.Module.Membership.Core.Service.Settings.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Payment.Data.DbContext;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Settings
{
    public class NotificationBackgroundProcess : INotificationBackgroundProcess
    {
        private readonly IUnitOfWokMembership uow;
        private readonly INotificationService notificationService;
        private readonly IUnitOfWorkPayment uowp;

        public NotificationBackgroundProcess(IUnitOfWokMembership _uow, INotificationService _notificationService, IUnitOfWorkPayment _uowp) : base()
        {
            uow = _uow;
            notificationService = _notificationService;
            uowp = _uowp;
        }

        public async Task AboveToExpire()
        {
            try
            {
                //TODO what is this ?
                var ff = "2018-05-17 11:18:24.3435676";


                var membershipRenewal = (from memberSubscription in uow.MemberSubscriptionRepository.TableAsNoTracking
                    join memberInfo in uow.MemberRepository.TableAsNoTracking on memberSubscription.MemberId equals
                        memberInfo.Id
                    join subscription in uow.SubscriptionRepository.TableAsNoTracking on memberSubscription
                        .SubscriptinId equals subscription.Id
                    join subscriptionType in uow.SubscriptionTypeRepository.TableAsNoTracking on subscription
                        .Id equals subscriptionType.Id
                    where (memberSubscription.MembershipStatus == Enums.MembershipStatus.Active) &&
                          memberSubscription.RenewDate == Convert.ToDateTime(ff)
                    //notifyday
                    select new
                    {
                        membershipStatus = memberSubscription.MembershipStatus,
                        memberId = memberSubscription.MemberId,
                        renewalDate = memberSubscription.RenewDate,
                        membershipLevel = subscriptionType.Name,
                        memberSubscription.TenantId,
                        memberName = memberInfo.Name,
                    });

                var res = await membershipRenewal.Select(p => new NotificationCreateRequest
                {
                    NotificationGroup = new NotificationGroup
                    {
                        NotificationTypeId = 1,
                        CreatedOn = DateTime.UtcNow,
                        TenantId = p.TenantId,
                        UserRole = 1,
                        RecipientId = 0,
                    },
                    Notification = new List<Notification>
                    {
                        new Notification
                        {
                            SenderId = p.memberId
                        }
                    }
                }).FirstOrDefaultAsync();
                await notificationService.Create(new Request<NotificationCreateRequest>
                {
                    Item = res
                });

            }
            catch (Exception e)
            {
                throw;
            }

        }

        public async Task HasExpired()
        {
            try
            {
                var notifyday = DateTime.UtcNow.AddDays(-1).Date;
                //  var ff = "2018-05-17 11:18:24.3435676";

                var HasExpired = (from memberSubscription in uow.MemberSubscriptionRepository.TableAsNoTracking
                    join memberInfo in uow.MemberRepository.TableAsNoTracking on memberSubscription.MemberId equals
                        memberInfo.Id
                    join subscription in uow.SubscriptionRepository.TableAsNoTracking on memberSubscription
                        .SubscriptinId equals subscription.Id
                    join subscriptionType in uow.SubscriptionTypeRepository.TableAsNoTracking on subscription
                        .Id equals subscriptionType.Id
                    where (memberSubscription.MembershipStatus == Enums.MembershipStatus.Active) &&
                          subscription.SubscriptionTo > notifyday
                    select new
                    {
                        memberSubscription.MembershipStatus,
                        memberSubscription.MemberId,
                        memberSubscription.RenewDate,
                        subscriptionType.Name,
                        memberSubscription.TenantId,
                        memberName = memberInfo.Name,
                    }).AsQueryable().Select(p => new NotificationCreateRequest
                {
                    NotificationGroup = new NotificationGroup
                    {
                        NotificationTypeId = 2,
                        CreatedOn = DateTime.UtcNow,
                        TenantId = p.TenantId,
                        UserRole = 1,
                        RecipientId = 0,
                    },
                    Notification = new List<Notification>
                    {
                        new Notification
                        {
                            SenderId = p.MemberId
                        }
                    }
                }).FirstOrDefault();

                await notificationService.Create(new Request<NotificationCreateRequest>
                {
                    Item = HasExpired
                });
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task IsPending()
        {
            try
            {
                var notifyday = DateTime.UtcNow.AddDays(-5).Date;
                //  var ff = "2018-05-17 11:18:24.3435676";

                var paymentContext = (from paymentMaster in uowp.PaymentMasterRepository.TableAsNoTracking
                                   select paymentMaster).ToList();
                var Ispending = (from memberSubscription in uow.MemberSubscriptionRepository.TableAsNoTracking
                    join memberInfo in uow.MemberRepository.TableAsNoTracking on memberSubscription.MemberId equals
                        memberInfo.Id
                    join subscription in uow.SubscriptionRepository.TableAsNoTracking on memberSubscription
                        .SubscriptinId equals subscription.Id
                    join subscriptionType in uow.SubscriptionTypeRepository.TableAsNoTracking on subscription
                        .Id equals subscriptionType.Id
                    join paymentMaster in paymentContext on memberSubscription.Id equals paymentMaster
                        .MemberSubscriptionId
                    where (memberSubscription.MembershipStatus == Enums.MembershipStatus.Active) && (paymentMaster
                                                                                                         .PaymentState ==
                                                                                                     Enums.PaymentState
                                                                                                         .Pending)
                                                                                                 && (paymentMaster
                                                                                                         .CreatedOn ==
                                                                                                     notifyday)
                    select new
                    {
                        memberSubscription.MembershipStatus,
                        memberSubscription.MemberId,
                        paymentMaster.CreatedOn,
                        subscriptionType.Name,
                        memberSubscription.TenantId,
                        memberName = memberInfo.Name,
                    }).Select(p => new NotificationCreateRequest
                {
                    NotificationGroup = new NotificationGroup
                    {
                        NotificationTypeId = 3,
                        CreatedOn = DateTime.UtcNow,
                        TenantId = p.TenantId,
                        UserRole = 1,
                        RecipientId = 0,
                    },
                    Notification = new List<Notification>
                    {
                        new Notification
                        {
                            SenderId = p.MemberId
                        }
                    }
                }).FirstOrDefault();

                await notificationService.Create(new Request<NotificationCreateRequest>
                {
                    Item = Ispending
                });
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
