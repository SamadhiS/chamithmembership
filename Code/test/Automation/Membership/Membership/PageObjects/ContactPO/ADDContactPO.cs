﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ContactPO
{
    public class ADDContactPO
    {
        //private NgWebDriver ngDriver;
        public void ContactInfor(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnCreateContact"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }


     
        public void Btn_AddContact_Click(NgWebDriver d)
        {
            this.Btn_AddContact(d).Click();
        }
        private NgWebElement Btn_AddContact(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnCreateContacts"));
            ScrollToElem(elem, d);
            return elem;
        }
        public NgWebElement fName(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtFname"));
        }

        public NgWebElement lName(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtLastName"));
        }
        private NgWebElement Btn_ProfileImage(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#btnPersonal > div > div > div > form > fieldset > div:nth-child(1) > div:nth-child(3) > label.fileContainer"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ProfileImage_Click(NgWebDriver d)
        {
            this.Btn_ProfileImage(d).Click();
        }
        public NgWebElement dob(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDob"));
        }
        public NgWebElement nic(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtIdentity"));
        }
        public void maritalStat(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("ddMartialStatus"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(1000);
        }
                    public NgWebElement mStatType(NgWebDriver d)
                    {
                        return d.FindElement(By.Id("ddMartialStatus")).FindElement(By.CssSelector("option[value='2']")); ;
                    }
        public void contactType(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("ddContactType"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(3000);
        }
                    public NgWebElement categoryTyp(NgWebDriver d) //Manager
                    {
                        return d.FindElement(By.Id("ddContactType")).FindElement(By.CssSelector("option[value='370']")); ;
                    }


        public void nextBtn_P(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#btnPersonal > div > div > div > form > fieldset > div.col-md-12 > div > a"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(3000);
        }

        public void countryBtn(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("ddCountry"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(3000);
        }

                    public NgWebElement countryTyp(NgWebDriver d)
                    {
                        return d.FindElement(By.Id("ddCountry")).FindElement(By.CssSelector("option[value='199']")); ;
                    }

        public NgWebElement city(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCity"));
        }
        public NgWebElement postalCode(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtZip"));
        }
        public NgWebElement address(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtAddress"));
        }
        public NgWebElement phone(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtPhone"));
        }
        public NgWebElement telephone(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtTelephone"));
        }
        public NgWebElement email(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtEmail"));
        }

        public void nextBtn_C(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNextWork"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(1000);
        }
        public NgWebElement designation(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDesignation"));
        }
        public NgWebElement company(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCompany"));
        }
        public NgWebElement companyAdd(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComAddress"));
        }
        public void CompanycountryBtn(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("ddComCountry"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(1000);
        }

        public NgWebElement CompanycountryTyp(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddComCountry")).FindElement(By.CssSelector("option[value='110']")); ;
        }
        public NgWebElement workTele(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComtelephone"));
        }

        public NgWebElement workMob(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComphone"));
        }

        public void Clear_field(NgWebDriver d)
        {
            for (int i = 0; i < 20; i++)
            {
                dob(d).SendKeys(Keys.Backspace);
            }
        }

                    public NgWebElement mStatTypeB(NgWebDriver d)
                    {
                        return d.FindElement(By.Id("martialStatus")).FindElement(By.CssSelector("option[value='2']")); ;
                    }

                    public NgWebElement categoryTypeB(NgWebDriver d) //Cateogry Operational Manager
                    {
                        return d.FindElement(By.Id("contactType")).FindElement(By.CssSelector("option[value='379']")); ;
                    }


        public void saveContact(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSaveContact"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(1000);
        }

        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }

    }
}
