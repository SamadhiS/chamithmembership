import { LevelService } from './../../../core/services/level.service';
import { toasterHeadings } from './../../../core/utility/toasterHeadings';
import { Observable } from 'rxjs/Observable';

import { ToastsManager } from 'ng2-toastr';
import { PaginationModel } from './../../../core/models/paginationModel';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EmailTemplateService } from './../../../core/services/email-template.service';
import { Component, OnInit, ViewContainerRef, Input, OnChanges } from '@angular/core';
import { MembersService } from '../../../core/services/members.service';
import { EmailSendModel } from '../../models/emailSendModel';
import { EmailSendType } from '../../../core/enums/emailSendType';
import { ToasterMessages } from '../../../core/utility/toasterMessages';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
Quill.register('modules/imageResize', ImageResize);

@Component({
  selector: 'membership-email-send',
  templateUrl: './email-send.component.html',
  styleUrls: ['./email-send.component.scss']
})
export class EmailSendComponent implements OnInit {

  emailSendModel = new EmailSendModel();
  emailTemplates = [];
  emailSendForm: FormGroup;
  memberLevels = [];
  memberStatus = [];
  noTemplateId = 0;
  isEmailBodyDisabled = true;
  isEmailSendFormSubmit = false;
  isBlocked = false;
  language='';
  _text:any;
  dropdownSettings = {};
  @Input() isSingle = false;
  @Input() memberId: number;
  isIndividual = true;
  isMultiple = false;
  isALL = false;
  emailSendTypes: typeof EmailSendType = EmailSendType;
  members = [];
  selectedItems = [];
  select =[];
  selectAll =[];
  selected = false;
  isRequested = false;
  editor_modules = {}

  constructor(private emailTemplateService: EmailTemplateService,
    private memberService: MembersService,
    private toastr: ToastsManager,
    private levelService: LevelService,
    private fb: FormBuilder, vcr: ViewContainerRef) {
   
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    if (!this.isSingle)//member related info not needed when sending emails through member section
      this.getMemberRelatedData();
    this.getEmailTemplates();
    this.emailSendForm = this.getEmailTemplateForm();
    this.setValidations(EmailSendType.single);
    this.dropdownSettings = {
            singleSelection: false,
            placeholder: "Search Memebrs to Add...",
            text: "Select Members",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        
          };

          //Message editor
          this.editor_modules = {
            toolbar: {
              container: [
                [{ 'font': [] }],
                [{ 'size': ['small', false, 'large', 'huge'] }],
                ['bold', 'italic', 'underline', 'strike'],
                [{ 'header': 1 }, { 'header': 2 }],
                [{ 'color': [] }, { 'background': [] }],
                [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                [{ 'align': [] }],
                ['link', 'image']
              ]
            },
            imageResize: true,
          };
  }

    OnItemDeSelect(item: any) { 
        this.select = item;
        if(this.select ==null)
        {
          this.selected = false;
        }
      }
    
      onDeSelectAll(items: any) {
        this.selected = false;
       }
      
       onItemSelect(item: any) {
        this.select = item;
        if(this.select !=null)
        {
          this.selected = true;
        }
         this.emailSendModel.memberIds = this.selectedItems.map(data => data.id)
       }
     
       onSelectAll(items: any) {
        this.selectAll = items;
        if(this.selectAll !=null)
        {
          this.selected = true;
        }
         this.emailSendModel.memberIds = items.map(data => data.id)
       }
  

  emailSend() {
    if(this.isIndividual== true && !this.isSingle)
    {//check is individual
      this.isRequested =this.selected;
      if( this.isRequested == false)
      {
        this.toastr.error('Select Members!', 'Oops');
        this.isRequested = false; 
        this.selected = false;
        this.isBlocked = false;  
        this.getMemberRelatedData();
        return;
      }
      else{
        this.isEmailSendFormSubmit = true;
        if (!this.emailSendForm.valid) return;
        this.isBlocked = true;
        this.emailSendModel.memberId = this.memberId;
        let email: EmailSendModel = Object.assign({}, this.emailSendModel, this.emailSendForm.value)
        if (this.isSingle) { email.emailSendType = EmailSendType.single }
        if(this.emailSendModel.templateId == undefined){
          this.emailSendModel.templateId = 0;
          this.getMemberRelatedData();
        }
        this.emailTemplateService.sendEmail(email).subscribe(res => {
          this.isBlocked = true;
          this.emailSendForm.disable();
          this.toastr.success("Emails sent successfully", "Success")
          this.emailSendForm.enable();
          this.emailSendForm = this.getEmailTemplateForm();
          this.isEmailSendFormSubmit = false;
          this.isRequested = false; 
          this.selected = false;
          this.isBlocked = false;  
          this.getMemberRelatedData();        
        }, error => {
          this.isBlocked = false;
          this.getMemberRelatedData();
          this.toastr.error(error)
        }, () => {
          this.isBlocked = false;
          this.getMemberRelatedData();
        })
      }
    }
    else{
      //check if it is multiple or all
      this.isEmailSendFormSubmit = true;
      if (!this.emailSendForm.valid) return;
      this.isBlocked = true;
      this.emailSendModel.memberId = this.memberId;
      let email: EmailSendModel = Object.assign({}, this.emailSendModel, this.emailSendForm.value)
      if (this.isSingle) { email.emailSendType = EmailSendType.single }
      if(this.emailSendModel.templateId == undefined){
        this.emailSendModel.templateId = 0;
        this.getMemberRelatedData();
      }
      this.emailTemplateService.sendEmail(email).subscribe(res => {
        this.isBlocked = true;
        this.toastr.success("Emails sent successfully", "Success")
        this.emailSendForm.reset();
        this.emailSendForm = this.getEmailTemplateForm();
        this.getMemberRelatedData();
        this.isEmailSendFormSubmit = false;
        this.isEmailBodyDisabled = false;
        this.isBlocked = false;
        
      }, error => {
        this.isBlocked = false;
        this.getMemberRelatedData();
        this.toastr.error(error)
      }, () => {
        this.isBlocked = false;
        this.getMemberRelatedData();
      })
    }
  }


  changeEmailSendType(data) {
    this.isEmailSendFormSubmit = false;
    if (data == EmailSendType.individuals) {
      this.isMultiple = false;
      this.isIndividual = true;
      this.isALL = false;
    }
    else if (data == EmailSendType.multiple) {
      this.isMultiple = true;
      this.isIndividual = false;
      this.isALL = false;
    }
    else if (data == EmailSendType.all) {
      this.isMultiple = false;
      this.isIndividual = false;
      this.isALL = true;
    }

    this.setValidations(data);
  }

  getTemplate() {
    
    let templateId = this.emailSendForm.get("templateId").value;    
    if(templateId == "null"){
      this.isEmailBodyDisabled = false;
      this.emailSendForm.patchValue({
            emailBody: ""
          })
    }
    else{
    this.emailTemplateService.getTemplate(templateId).subscribe(res => {
      this.isEmailBodyDisabled = true;      
      this.emailSendForm.patchValue({
        emailBody: res.result.item.emailBody
      })
    }, error => {

    })
  }
  }

  getEmailTemplates() {
    this.isEmailBodyDisabled = false;
    this.isBlocked = true;
    this.emailTemplateService.getAllTemplates().subscribe(data => {
      this.emailTemplates = data.result.item
      this.isBlocked = false;
    }, error => {
      this.toastr.error(ToasterMessages.loadError('Email template'), toasterHeadings.Error)
    })
  }

  private getMemberRelatedData() {
    this.isBlocked = true;
    Observable.forkJoin(

      this.memberService.getMembersKeyValue(),
      this.levelService.getMemberLevelKeyValues(),
      this.memberService.getMemberStatus(),
    ).subscribe(data => {
      this.members = data[0].result.item.map(data => { return { "id": data.value, "itemName":data.value +": "+ data.text } });
      this.memberLevels = data[1].result;
      this.memberStatus = data[2].result.item;

    },
      error => {
       
        this.toastr.error(ToasterMessages.loadError('Email template data'), toasterHeadings.Error)
        this.isBlocked = false;
      },
      () => {
        this.isBlocked = false;
      })
  }


  private setValidations(data) {

    let memberLevel = this.emailSendForm.get('memberLevelId');
    let memberStatus = this.emailSendForm.get('memberStatusId');

    if (data == EmailSendType.multiple) {
      memberLevel.setValidators(Validators.required)
      memberStatus.setValidators(Validators.required)
    }
    else {
      memberLevel.clearValidators();
      memberStatus.clearValidators();
    }

    memberLevel.updateValueAndValidity();
    memberStatus.updateValueAndValidity();

  }

  private getEmailTemplateForm() {

    return this.fb.group({
      emailSendType: [EmailSendType.individuals, [Validators.required]],
      emailBody: ["", [Validators.required]],
      subject: ["", [Validators.required]],
      members: [[], []],
      status: [""],
      memberLevelId: [""],
      memberStatusId: [""],
      templateId: [],
      toAddress: [""],
    })
  }
}
