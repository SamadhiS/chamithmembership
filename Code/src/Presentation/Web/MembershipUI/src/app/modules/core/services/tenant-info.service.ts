import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TenantInfoService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }
  getTenantInfo() {
    return this.http.get(`${this.baseApiEndPoint}identity/v1/tenants`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  getUserInfo() {
    return this.http.get(`${this.baseApiEndPoint}identity/v1/security/users`,
      this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  updateTenant(files, tenant) {
    let formData: FormData = new FormData();

    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('tenantViewData', JSON.stringify(tenant));
    return this.http.put(`${this.baseApiEndPoint}identity/v1/tenants`,
      formData, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  loadTenantData() {
    let tenantObj = localStorage.getItem('tenantObj');
    let tenantInfo = JSON.parse(tenantObj);
    return tenantInfo;
  }

  loadUserData() {
    let userObj = localStorage.getItem('userObj');
    let userInfo = JSON.parse(userObj);
    return userInfo
  }

  getBase64ImageData() {
    return (localStorage.getItem('Image_Data'));
  }
}
