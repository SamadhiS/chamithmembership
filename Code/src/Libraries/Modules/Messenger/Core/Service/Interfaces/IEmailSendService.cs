﻿using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Utility.Models;
using System.Threading.Tasks;

namespace Inx.Module.Messenger.Core.Service.Interfaces
{
    public interface IEmailSendService
    {
        Task SendEmails(Request<EmailSendingInfoModel> emailInfoModel);
        Task SendEmail(EmailSendingInfoModel model);
    }
}
