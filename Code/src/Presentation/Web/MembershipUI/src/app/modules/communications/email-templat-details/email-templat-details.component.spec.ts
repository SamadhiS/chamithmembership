import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTemplatDetailsComponent } from './email-templat-details.component';

describe('EmailTemplatDetailsComponent', () => {
  let component: EmailTemplatDetailsComponent;
  let fixture: ComponentFixture<EmailTemplatDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTemplatDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTemplatDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
