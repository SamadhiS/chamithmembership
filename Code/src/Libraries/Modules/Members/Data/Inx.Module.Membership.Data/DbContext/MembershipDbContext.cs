﻿using System.Threading;
using System.Threading.Tasks;
using Inx.Module.Membership.Data.Entity.MemberGroup;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Module.Membership.Data.Entity.Settings;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Data.Entity.Subscription;
using Inx.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Data.DbContext
{
    public class MembershipDbContext : Microsoft.EntityFrameworkCore.DbContext, IMembershipDbSet
    {
        private readonly string connectionString;
        #region contacts
        public DbSet<Member> Member { get; set; }
        public DbSet<OrganizationInfo> OrganizationInfo { get; set; }
        public DbSet<PersonalInfo> PersonalInfo { get; set; }
        public DbSet<MemberFieldKeyValue> MemberFieldKeyValue { get; set; }

        public DbSet<Group> MembershipGroup { get; set; }
        public DbSet<SubscriptionType> SubscriptionType { get; set; }
        public DbSet<Subscription> Subscription { get; set; }
        public DbSet<MemberFile> MemberFile { get; set; }
        public DbSet<MemberSubscription> MemberSubscription { get; set; } 
        public DbSet<MemberGroup> MemberGroup { get; set; }
        public DbSet<TerminationNote> TerminationNote { get; set; }
        public DbSet<MembershipTimeline> MembershipTimeline { get; set; }
      
        //definitions
        public DbSet<MemberTabDefinition> MemberTabDefinition { get; set; }
        public DbSet<MemberFieldDefinition> MemberFieldDefinition { get; set; }
        public DbSet<MemberTypeTab> MemberTypeTab { get; set; }
        public DbSet<MemberTabField> MemberTabField { get; set; }
        public DbSet<MemberFieldRequired> MemberFieldRequired { get; set; }
        #endregion

        public MembershipDbContext()
        {
            connectionString = GlobleConfig.ConnectionString;
        }

        public MembershipDbContext(string _connectionstring)
        {
            connectionString = _connectionstring;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>()
               .HasIndex(c => new { c.Name, c.TenantId }).IsUnique();
            modelBuilder.Entity<SubscriptionType>()
                .HasIndex(c => new { c.Name, c.TenantId }).IsUnique();
            modelBuilder.Entity<MemberFieldKeyValue>()
                .HasIndex(c => new { c.Key, c.TenantId,c.MemberId }).IsUnique();
            modelBuilder.Entity<MemberFieldRequired>()
                .HasIndex(c => new { c.Key, c.TenantId, c.MemberType }).IsUnique();
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
