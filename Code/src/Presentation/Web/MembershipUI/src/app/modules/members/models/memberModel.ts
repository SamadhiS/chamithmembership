export class MemberModel {
    id: number;
    firstName: string;
    lastName: string;
    middleName: string;
    dob: Date;
    martialStatus: string;
    identity: string;
    contactCategoryId: string;
    profileImage: string;
    isaMember: boolean;

    getImageName() {
        if (this.profileImage) {
            let splitFilePath = this.profileImage.split("/");
            this.profileImage = splitFilePath[splitFilePath.length - 1]
        }
    }
}