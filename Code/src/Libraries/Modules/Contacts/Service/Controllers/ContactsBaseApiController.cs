﻿using Inx.Service.Base.Controllers;

namespace Inx.Module.Contacts.Service.Controllers
{
    public class ContactsBaseApiController : BaseApiController
    {
        public ContactsBaseApiController(IBaseInjector baseinject) : base(baseinject)
        {
        }
    }
}
