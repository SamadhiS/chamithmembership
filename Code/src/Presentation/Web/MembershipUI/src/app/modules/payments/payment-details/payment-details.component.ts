import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { PaginationModel } from '../../core/models/paginationModel';
import { PaymentSearch } from '../models/paymentSearchModel';
import { PaymentService } from '../../core/services/payment.service';


@Component({
  selector: 'membership-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.scss']
})
export class PaymentDetailsComponent implements OnInit {

  paymentSearchForm: FormGroup;
  maxDate = new Date();
  fromDate = new Date(new Date().setDate(new Date().getDate() -30));
  paginationModel = new PaginationModel(20, '', false)
  searchModel = new PaymentSearch();
  searchTerm$ = new Subject<string>();
  paymentDetails = [];
  isBlocked = false;
  maxSize = 10;
  currency = "";

  constructor(private fb: FormBuilder, private paymentService: PaymentService, private router: Router) {

    this.searchTerm$.debounceTime(300)
      .distinctUntilChanged().subscribe(data => {
        if (data != "" && data) {

          this.searchModel.search = data;
        }
        else {
          this.searchModel.search = '';
        }

        this.getPayments();
      })


  }

  ngOnInit() {
    this.currency  = localStorage.getItem('Currency');
    this.paymentSearchForm = this.getSearchForm();
    this.paymentSearchForm.get("fromDate").valueChanges.subscribe(res => {
      this.searchModel.fromDate = res;
      this.getPayments();
    })

    this.paymentSearchForm.get("toDate").valueChanges.subscribe(res => {
      this.searchModel.toDate = res;
      this.getPayments();
    })


    this.getPayments();
  }


  getPayments() {
    this.isBlocked = true;
    this.paymentService.getPayments(this.paginationModel.skip, this.paginationModel.take, JSON.stringify(this.searchModel), this.paginationModel.orderByTerm).subscribe(res => {
      this.paymentDetails = res.result.items;
      this.paginationModel.totalRecords = res.result.totalRecodeCount;
    }, error => {
      this.isBlocked = false;
    }, () => {
      this.isBlocked = false;
    })
  }

  goToInvoice(paymentId, memberId) {
    this.router.navigate(['/payments/invoice', paymentId, memberId])
  }


  public pageChanged(event: any): void {
    this.paginationModel.skip = (event.page - 1) * this.paginationModel.take;
    this.paginationModel.currentPage = event.page;
    this.getPayments();
  }

  private getSearchForm() {

    return this.fb.group({
      search: [""],
      toDate: [new Date()],
      fromDate: [this.fromDate],
    })

  }

}
