﻿using System;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Service.Models.CustomFieldDefinition;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.Settings.CustomFieldDefinition
{
    [Route(Constraints.ApiPrefix)]
    public class MemberTabDefinitionApiController : MembershipBaseApiController, IBaseApi<MemberTabDefinitionViewModel, int>
    {
        private  IMemberTabDefinitionService serviceMemberTabDefinition;
        private  IMemberTypeTabService serviceIMemberTypeTabService;
        private readonly IUnitOfWokMembership uowMembership;
        private readonly ICoreInjector coreInjector;
        public MemberTabDefinitionApiController(IUnitOfWokMembership _uowMembership, IBaseInjector baseinject, ICoreInjector _coreInjector) : base(baseinject)
        {
            uowMembership = _uowMembership;
            coreInjector = _coreInjector;
            serviceMemberTabDefinition = new MemberTabDefinitionService(uowMembership, coreInjector);
            serviceIMemberTypeTabService = new MemberTypeTabService(uowMembership, coreInjector);
        }

        [HttpPost, Route("tabdefinition"), ModelValidation]
        public async Task<IActionResult> Create(MemberTabDefinitionViewModel item)
        {
            using (var transaction = await uowMembership.Context.Database.BeginTransactionAsync())
            {
                try
                {
                    var tabs = await serviceMemberTabDefinition.Create(Request<MemberTabDefinitionViewModel, MemberTabDefinitionBo>(item));
                    foreach (var field in item.MemberType)
                    {
                        await serviceIMemberTypeTabService.Create(Request(new MemberTypeTabBo(tabs.Id, field)));
                    }
                    transaction.Commit();
                    return Ok();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return await HandleException(e);
                }
            }
           
        }
        [HttpPut, Route("tabdefinition"), ModelValidation]
        public async Task<IActionResult> Update(MemberTabDefinitionViewModel item)
        {
            return await Update<MemberTabDefinitionViewModel, MemberTabDefinitionBo, IMemberTabDefinitionService>(item, this.serviceMemberTabDefinition);
        }
        [HttpGet, Route("tabdefinition/{id:int}"), ModelValidation]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<MemberTabDefinitionViewModel, MemberTabDefinitionBo, IMemberTabDefinitionService>(id, serviceMemberTabDefinition);
        }
        [HttpDelete, Route("tabdefinition/{id:int}"), ModelValidation]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<MemberTabDefinitionBo, IMemberTabDefinitionService>(id, serviceMemberTabDefinition);
        }
        [HttpGet, Route("tabdefinition"), ModelValidation]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            try
            {
                return null;
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("tabdefinition/keyvalue"), ModelValidation]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<MemberTabDefinitionBo, IMemberTabDefinitionService>(serviceMemberTabDefinition);
        }


    }
}
