﻿using Inx.Service.Base.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Service.Models.CustomFieldDefinition
{
    public class MemberTypeTabViewModel: BaseViewModel
    {
        public int TabId { get; set; }
        public Enums.MemberType MemberType { get; set; }
    }
}
