﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base; 
using Inx.Module.Membership.Core.Bo.Member.Master;
using Inx.Module.Membership.Core.Bo.Member.Master.ViewResult;
using Inx.Module.Membership.Core.Bo.Member.SearchViewResult.ViewResult;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Members.interfaces
{
    public interface IMemberInfoService:ISearchService<MembersViewResult>
    {
        Task<int> Create<T>(Request<MemberInfoBo<T>> request) where T : BaseBo;
        Task Delete(Request<int> request);
        Task<PageList<MembersViewResult>> Search(Search request);
        Task<MemberInfoViewResult> Read(Request<int> request);
        Task<List<KeyValueListItem<int>>> ReadKeyValue(Search request);
    }
}
