
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RegisteruserService } from '../service/registeruser.service';
import { ToastsManager } from 'ng2-toastr';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator'
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { UserLoginModel } from '../model/userlogin';

@Component({
  selector: 'membership-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  isMailSent = false;
  isRequested = false;
  isAlreadyMember = false;
  emailValidated = false;
  isEmailVerifiedWithoutTenant = false;
  userLogin = new UserLoginModel();
  patternModel = new PatternModel();
  constructor(private fb: FormBuilder, private router: Router, private registerUser: RegisteruserService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  createForm() {
    this.signupForm = this.fb.group({
      email: ['', [Validators.required, patternValidator(this.patternModel.emailRegex)]],
      password: ['', [Validators.required, patternValidator(this.patternModel.passwordRegex)]],
      confirmPassword:['', [Validators.required]]
    },{ validator: this.comparePasswords });
  }
  comparePasswords(group: FormGroup) {
    let password = group.controls.password.value;
    let confirmPassword = group.controls.confirmPassword.value;
    return (password === confirmPassword) ? null : { isPasswordNotSame: true }
  }
  signupUser() {
    if (!this.signupForm.valid) return;
    this.isRequested = true;
    this.userLogin = Object.assign({}, this.userLogin, this.signupForm.value);
    this.registerUser.sendMail(this.userLogin)
      .subscribe(res => {
        this.isRequested = false;
        this.isMailSent = true;
      }, error => {
        this.isRequested = false;
        if (error.state == 1) {
          this.emailValidated = true;
          this.resendMail();
        }
        if (error.state == 2) {
          this.isEmailVerifiedWithoutTenant = true;
        }
        if (error.state == 3) {
          this.isAlreadyMember = true;
        }
      })
  }
  login() {
    this.router.navigate(['/login'])
  }
  goBack() {
    this.isMailSent = false;
  }
  resendMail() {
    this.isRequested = true;
    var userMail = this.signupForm.value.email;
    var resendMailObj = { Value: userMail }
    this.registerUser.resendMail(resendMailObj)
      .subscribe(res => {
        this.isRequested = false;
        this.isMailSent = true;
      }, error => {
        this.isRequested = false;
        this.isMailSent = false;
      })
  }
  ngOnInit() {
    this.createForm();
  }
}
