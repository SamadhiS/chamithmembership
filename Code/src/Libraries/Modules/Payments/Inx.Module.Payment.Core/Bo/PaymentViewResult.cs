﻿using System;

namespace Inx.Module.Payment.Core.Bo
{
    public class PaymentViewResult
    {
        public int MemberSubscriptionId { get; set; }
        public int PaymentMasterId { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public decimal Amount { get; set; }
        public decimal Remaining { get; set; }
        public string PaymentType { get; set; }
        public string TransActionType { get; set; }
        public string PaymentState { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset? PayedDate { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? EditedOn { get; set; }
    }
}
