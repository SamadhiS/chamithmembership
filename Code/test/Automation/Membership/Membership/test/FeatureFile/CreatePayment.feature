﻿Feature: CreatePayment
		As an Admin. I should be able to create new payments

@mytag
Scenario Outline: Make a payment in Payment section for a pending status 
	Given System application page is opened
	And Enter <userName> and <password>
	And click on login 
	And Naviagate to Homepage 
	And click on Payments 
	And select the payment status as <stat> 
	And click on Settle
	And Select Payment Type as "Cash" 
	And Click on Pay Now 
	And Success message appears
	And Navigate back to Payment section
	And Log out 
Examples:
| _userName                   | _password |_stat	|
| membership.inexis@gmail.com | Admin@123 | pending |


Scenario Outline: Make payments in Payment section for a paid status
	Given System application page is opened
	And Enter <userName> and <password>
	And click on login 
	And Naviagate to Homepage 
	And click on Payments 
	And select the payment status as <stat> 
	And there is no Settle Button 
	And Navigate back to Payment section
	And Log out 
Examples:
| _userName                   | _password | _stat	|
| membership.inexis@gmail.com | Admin@123 | paid	|




#Scenario Outline: Create payments in Member section 
#	Given System application page is opened
#	And Enter <userName> and <password>
#	And click on login 
#	And Naviagate to Homepage 
#	And Click on Members
#	And Click on Pending status as <status>
#	And Go to Payment Tab 
#
#	And Log out 
#Examples:
#| _userName                   | _password | _status			|
#| membership.inexis@gmail.com | Admin@123 | pending			|