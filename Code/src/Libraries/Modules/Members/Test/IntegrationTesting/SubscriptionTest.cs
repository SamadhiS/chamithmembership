﻿using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Test.IntegrationTesting
{
    public class SubscriptionTest : CoreTestBase, ICoreTestBase
    {
        private ISubscriptionService service;
        //[TestInitialize]
        public void Initialize()
        {
            base.MockCache<PageList<SubscriptionBo>>();
            service = new SubscriptionService(new Comman().IUnitOfWokMembership, null);
        }

        [DataTestMethod]
        [DataRow("test Gold", "test description", 0 , 2 , 100.00, true, false, true)] //Pass
        [DataRow("test Silver", "testdtest description", 0, 2, 99, true, false, true)] //Pass
        [DataRow("test Gold", "testdescription", 0, 2, 85, true, false, true)] //Dupicate Fail
        [DataRow("", "testdescription", 0, 2, 100.00, true, false, true)] //Name Null Fail
        [DataRow("test Gold", "testdescription", 0, 2, 150, true, false, true)] //Name Lengh Fail
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Create(string name, string description, int capacity, int RenewalDays, decimal price,bool isSendEmail, bool isLimited, bool isRenewable)
        {
            try
            {
                var mock = new Mock<IMemoryCacheManager>();
              //  mock.Setup(foo => foo.Remove(It.IsAny<string>())).Returns(Task.FromResult(true));
                var bo = new SubscriptionBo
                {
                   Name = name,
                   Description = description,
                   Capacity = capacity,
                   EmaillTemplateId = 1,
                   IsLimited = isLimited,
                   IsRenewable = isRenewable,
                   IsSendEmail = isSendEmail,
                   Price = price,
                   RenewalDays = 2
                    
                };
                await base.Create(bo, service);
                True("MemebrLevelTest=>Create");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)] //Pass
        [DataRow(1545)] //Record Not Found Fail
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<SubscriptionBo, ISubscriptionService>(Convert.ToInt32(id), service);
                True("MemebrLevelTest=>Delete");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")] //SearchTeam 
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<SubscriptionBo, ISubscriptionService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("MemebrLevelTest=>Read");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(1)] //Pass
        [DataRow(1000)] //Record Not Found Fail
        public async Task ReadById(int id)
        {
            try
            {
                await service.Read(TestRequest(id));
                True("MemebrLevelTest=>ReadById");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<SubscriptionBo, ISubscriptionService>(service);
                True("MemebrLevelTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Update()
        {
            try
            {
                await base.Update(new SubscriptionBo()
                {
                }, service);
                True("MemebrLevelTest=>Update");
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public Task Search(int skip, int take, string search)
        {
            throw new NotImplementedException();
        }

        public new Task Create()
        {
            throw new NotImplementedException();
            //test 2
        }
    }
}
