import { Component, OnInit, ViewContainerRef} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { PermissionModel } from '../models/permission';
import { PermissionService } from '../services/permission.service';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';

@Component({
  selector: 'membership-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {

  permissionForm :FormGroup;
  isRequested = false;
  isMailSent = false;
  permission = new PermissionModel();
  patternModel = new PatternModel();
  isBlocked = false;
  isAlreadyMember = false;
  permissionUserList = new  PermissionModel();

  constructor(private fb: FormBuilder,private router: Router,private permissionService: PermissionService,public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  createpermissionForm() {
    this.permissionForm = this.fb.group({
      email: ['', [Validators.required, patternValidator(this.patternModel.emailRegex)]],
    //  password:['Q1234q1234',[ patternValidator(this.patternModel.passwordRegex)]]
    });
  }

  ngOnInit() {
    this.createpermissionForm();
    this.getPermissionUsers();
  }

  sendPermission(){
    this.isBlocked = true;
    if (!this.permissionForm.valid) return;
    this.isRequested = true;
    this.permission = Object.assign({}, this.permission, this.permissionForm.value);
    this.permissionService.sendInvite(this.permission)
    .subscribe(res => {
      this.isRequested = false;
      this.isMailSent = true;
      this.isBlocked = false;
      this.toastr.success('Email has been send successfully', 'Success');
      this.isBlocked = false;
      this.permissionForm.reset();
      this.permissionForm.enable();
      this.getPermissionUsers();
    }, error => {
      this.isRequested = false;
      this.isMailSent = false;
      this.isBlocked = false;
      this.permissionForm.reset();
      if (error.state == 3) {
        this.isAlreadyMember = true;
      }
      this.isBlocked = false;
    })
   
  }

  getPermissionUsers() {
    this.isBlocked = true;
    this.permissionService.getAllInviters()
      .subscribe(res => {
        this.permissionUserList = res.result;
        if (res.result.item.totalRecodeCount == 0) {
        }
        this.isBlocked = false;
      },
      error => {
        this.toastr.error(ToasterMessages.loadError('PermissionUser'), toasterHeadings.Error);
        this.isBlocked = false;
      });
  }

  
}
