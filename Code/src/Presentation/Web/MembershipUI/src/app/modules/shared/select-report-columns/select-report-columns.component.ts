import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TableTemplateService } from './../../core/services/table-template.service';
import { TableTemplate } from './../../core/enums/tableTemplate';
import { Component, OnInit, ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { TableTemplateModel } from '../models/tableTemplateModel';
import { ReportFormats } from '../../core/enums/reportFormat';
import { GenerateReportsService } from '../services/generate-reports.service';
import { TenantInfoService } from '../../core/services/tenant-info.service';

@Component({
  selector: 'membership-select-report-columns',
  templateUrl: './select-report-columns.component.html',
  styleUrls: ['./select-report-columns.component.scss']
})
export class SelectReportColumnsComponent implements OnInit {

  allColumns = [];
  displayAllColumns = [] ;

  @ViewChild('reportTemplateModal') reportTemplateModal: ModalDirective;
  @Output() show: EventEmitter<any> = new EventEmitter();
  @Input() tableTemplate: number;
  @Input() memberStaus: number;
  tableTemplateModel = new TableTemplateModel();
  public header: string;
  public reportFormat: string;
  public type: string;

  constructor(private tableTemplateService: TableTemplateService,
    private spinnerService: Ng4LoadingSpinnerService,
    private generateReportsService: GenerateReportsService,
    private tenantInfoService: TenantInfoService) { }

  ngOnInit() {
    this.getColumns();
  }

  public setHeader(reportFormat) {
    switch (this.tableTemplate) {
      case TableTemplate.ContactTableTemplate: {
        this.header = 'Contact ' + reportFormat + ' Report';
        this.type = 'Contact';
        break;
      }
      case TableTemplate.MemberTableTemplate: {
        this.header = 'Membership ' + reportFormat + ' Report';
        this.type = 'Member';
        break;
      }
    }
  }

  public showReportTemplateModal(reportFormat) {
    this.reportFormat = reportFormat;
    console.log("reportFormat" ,this.reportFormat)
    this.setHeader(reportFormat);
    this.reportTemplateModal.show();
  }

  hideTemplateModal() {
    this.reportTemplateModal.hide();
  }


  generateReport() {

    let selectedColumns = this.allColumns.filter(data => data.isSelected).map(data => data.name);

    //Loading Organization Logo
    let tenantInfo = this.tenantInfoService.loadTenantData();
    let imageData = this.tenantInfoService.getBase64ImageData();
    if (imageData == null) {
      if (this.getImageName(tenantInfo.organizationLogo) != 'noorganizationlogo.png') {
        this.generateReportsService.getBase64Image(tenantInfo.organizationLogo, function (orglogo) {
        })
      }
    }

    switch (this.reportFormat) {
      case ReportFormats.PDF: {
        this.generateReportsService.pdfReportGenerate(selectedColumns, this.memberStaus, this.type);
        this.hideTemplateModal();
        break;
      }
      case ReportFormats.CSV: {
        this.generateReportsService.csvReportGenerate(selectedColumns, this.memberStaus, this.type);
        this.hideTemplateModal();
        break;
      }
    }
  }

  private getImageName(profileImage) {
    let splitFilePath = profileImage.split("/");
    return splitFilePath[splitFilePath.length - 1]
  }

  showColumns() {
    let selectedColumns = this.allColumns.filter(data => data.isSelected).map(data => data.name)
    this.show.emit(selectedColumns);
    this.reportTemplateModal.hide();
  }

  getColumns() {
    this.tableTemplateService.getTemplateColumns(this.tableTemplate).subscribe(data => {
      let columns = [];
      columns = data.result.item;
      this.allColumns = columns.map(item => { return { name: item, isSelected: false } });

      this.displayAllColumns = this.allColumns;
      this.displayAllColumns.forEach(element => {
        element.name = element.name.replace(/([A-Z])/g, ' $1').trim()
      });

      this.getSavedTemplateColumns();
    })
  }

  getSavedTemplateColumns() {
    this.tableTemplateService.getSavedTemplateColumns(this.tableTemplate).subscribe(data => {
      let savedColumns = [];
      savedColumns = data.result.item;
      savedColumns.forEach(col => {

        let column = this.allColumns.find(data => data.name == col)
        if (column)
          column.isSelected = true;
      })
    })
  }

}
