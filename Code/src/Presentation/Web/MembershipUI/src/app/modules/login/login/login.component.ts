import { UserLoginModel } from '../model/userlogin';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Router, RouterModule, RouterLink } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { RegisteruserService } from '../service/registeruser.service';
import { ToastsManager } from 'ng2-toastr';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator'
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { TenantInfoService } from '../../core/services/tenant-info.service';

@Component({
  selector: 'membership-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isRequested = false;
  isMailSent = false;
  userLogin = new UserLoginModel();
  patternModel = new PatternModel();
  userLoginForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private registerUser: RegisteruserService,
    public toastr: ToastsManager, vcr: ViewContainerRef,private tenantInfoService:TenantInfoService) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  createForm() {
    this.userLoginForm = this.fb.group({
      email: ['', [Validators.required, patternValidator(this.patternModel.emailRegex)]],
      password: ['', [Validators.required]],
    });
  }
  authenticate() {
    if (!this.userLoginForm.valid) return;
    this.isRequested = true;

    this.userLogin = Object.assign({}, this.userLogin, this.userLoginForm.value);
    this.registerUser.authenticate(this.userLogin)
      .subscribe(res => {
        //userState 1: Active Users 6:Email Verified Without tenant registration
        if (res.result.item.userState == 6) {
          this.isRequested = false;
          localStorage.setItem('Token_id', res.result.item.token);
          this.router.navigate(['/register']);

        } else if (res.result.item.userState == 1) {
          localStorage.setItem('Token_id', res.result.item.token);
          this.tenantInfoService.getTenantInfo()
            .subscribe(res => {
              localStorage.setItem('tenantObj', JSON.stringify(res.result));
              this.tenantInfoService.getUserInfo()
                .subscribe(user => {
                  this.isRequested = false;
                  localStorage.setItem('userObj', JSON.stringify(user.result));
                  this.router.navigate(['/dashboard']);
                });
            });
        }
      }, error => {
        this.isRequested = false;
        if (error.state == 1) {
          this.resendMail();
        }
        if (error.state == 5) {
          this.toastr.error('Invalid Username or Password', 'Oops');
        }
      })
  }
  resendMail() {
    var userMail = this.userLoginForm.value.email;
    var resendMailObj = { Value: userMail }
    this.registerUser.resendMail(resendMailObj)
      .subscribe(res => {
        this.isMailSent = true;
      }, error => {
        this.isMailSent = false;
      })
  }
  ngOnInit() {
    this.createForm();
  }

}
