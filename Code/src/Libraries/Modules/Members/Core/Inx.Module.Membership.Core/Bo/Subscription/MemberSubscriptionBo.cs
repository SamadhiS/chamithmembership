﻿using Inx.Utility.Models;
using System;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Membership.Core.Bo.Subscription
{
    public class MemberSubscriptionBo : BaseBo
    {
        public int MemberId { get; set; }
        public int SubscriptionId { get; set; }
        public int SubscriptionTypeId { get; set; }
        public DateTime? RenewDate { get; set; } = DateTime.UtcNow;
        public PaymentState PaymentState { get; set; } = PaymentState.Pending;
        public MembershipStatus MembershipStatus { get; set; } = MembershipStatus.Pending;
        public MembershipExpired MembershipExpired { get; set; }
        public DateTime? JoinDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string TerminationNote { get; set; }

    }
}
