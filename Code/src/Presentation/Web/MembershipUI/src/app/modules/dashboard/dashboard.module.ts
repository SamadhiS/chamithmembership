import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';


import { HttpClientModule } from '@angular/common/http';
import { DashboardService } from './service/dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [DashboardComponent],
  providers:[DashboardService]
})
export class DashboardModule { }
