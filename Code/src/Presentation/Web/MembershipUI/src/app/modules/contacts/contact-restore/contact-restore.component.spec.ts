import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactRestoreComponent } from './contact-restore.component';

describe('ContactRestoreComponent', () => {
  let component: ContactRestoreComponent;
  let fixture: ComponentFixture<ContactRestoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactRestoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactRestoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
