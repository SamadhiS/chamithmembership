﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;

namespace Membership.StepDefinition.Category
{
    [Binding]
    public sealed class DeleteCategorySteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();

        [Given(@"Load Membership page")]
        public void GivenLoadMembershipPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I entered username & password ""(.*)"" and ""(.*)""")]
        public void GivenIEnteredUsernamePasswordAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I Clicked on Sign in button")]
        public void GivenIClickedOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Navigate to Dashboard")]
        public void ThenNavigateToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I Clicked on Settings")]
        public void ThenIClickedOnSettings()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I select a row and click on delete button")]
        public void ThenISelectARowAndClickOnDeleteButton()
        {
            categoryPO.Btn_DeleteCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I confirmed the delete")]
        public void ThenIConfirmedTheDelete()
        {
            categoryPO.Btn_DelConfirm_Click(ngWebDriver);
        }

        [Then(@"Clicked on logout button")]
        public void ThenClickedOnLogoutButton()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }
        // Cancel the delete
        [Given(@"Load Membership pg")]
        public void GivenLoadMembershipPg()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I entered ""(.*)"" and ""(.*)""")]
        public void GivenIEnteredAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I Clicked on Sign in btn")]
        public void GivenIClickedOnSignInBtn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Navigate to Dashboard page")]
        public void ThenNavigateToDashboardPage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I Clicked on Settings button")]
        public void ThenIClickedOnSettingsButton()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I select a row and click on delete btn")]
        public void ThenISelectARowAndClickOnDeleteBtn()
        {
            categoryPO.Btn_DeleteCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I cancel the delete")]
        public void ThenICancelTheDelete()
        {
            categoryPO.Btn_DelCancel_Click(ngWebDriver);
        }

        [Then(@"Clicked on logout btn")]
        public void ThenClickedOnLogoutBtn()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

        //Close delete

        [Given(@"Load the membership")]
        public void GivenLoadTheMembership()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I entered credentials ""(.*)"" and ""(.*)""")]
        public void GivenIEnteredCredentialsAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"I Clicked on SignIn")]
        public void GivenIClickedOnSignIn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Navigate to Dashboard pg")]
        public void ThenNavigateToDashboardPg()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I Clicked on Settings btn")]
        public void ThenIClickedOnSettingsBtn()
        {
            navPage.Btn_Settings_Click(ngWebDriver);
            Thread.Sleep(3000);
        }

        [Then(@"I select a row and click on delete bttn")]
        public void ThenISelectARowAndClickOnDeleteBttn()
        {
            categoryPO.Btn_DeleteCategory_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I close the delete")]
        public void ThenICloseTheDelete()
        {
            categoryPO.Btn_DelCancel_Click(ngWebDriver);
        }

        [Then(@"Clicked on logout bttn")]
        public void ThenClickedOnLogoutBttn()
        {
            Thread.Sleep(7000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
