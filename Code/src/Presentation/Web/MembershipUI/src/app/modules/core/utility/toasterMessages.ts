export abstract class ToasterMessages {

    public static save(entity): string {
        let message = `${entity} saved successfully !`
        return message;
    }

    public static saveError(entity): string {
        let message = `${entity} saving error !`
        return message;
    }

    public static alreadyExistError(entity): string {
        let message = `${entity} already exist error !`
        return message;
    }

    public static update(entity): string {
        let message = `${entity} updated successfully !`
        return message;
    }

    public static updateError(entity): string {
        let message = `${entity} updating error !`
        return message;
    }

    public static delete(entity): string {
        let message = `${entity} deleted successfully !`
        return message;
    }

    public static deleteError(entity): string {
        let message = `${entity} deleting error !`
        return message;
    }

    public static load(entity): string {
        let message = `${entity} loaded successfully !`
        return message;
    }

    public static loadError(entity): string {
        let message = `${entity} loading error !`
        return message;
    }

    public static terminate(entity): string {
        let message = `${entity} has been successfully terminated!`
        return message;
    }

    public static terminateError(entity): string {
        let message = `${entity} terminating error !`
        return message;
    }

    public static renewSubscription(entity): string {
        let message = `${entity} has been successfully renewed!`
        return message;
    }

    public static renewSubscriptionError(entity): string {
        let message = `${entity} cannot be renewed since there is a Active subscription !`
        return message;
    }

    public static mailSent(entity): string {
        let message = `${entity} email has been successfully sent!`
        return message;
    }

    public static mailSendError(entity): string {
        let message = `Error in sending ${entity} email!`
        return message;
    }
}