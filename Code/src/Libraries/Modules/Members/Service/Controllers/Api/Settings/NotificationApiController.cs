﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Service.Settings.Interface;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.Settings
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class NotificationApiController: MembershipBaseApiController
    {
        private IMembershipNotificationService service;
        public NotificationApiController(IMembershipNotificationService _service,IBaseInjector baseinject) :base(baseinject)
        {
            service = _service;
        }
        [HttpPut, Route("notifications/makeasread")]
        public async Task<IActionResult> MakeAsRead(List<int>request)
        {
            try
            {
                await service.MakeAsRead(Request(request));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("notifications/count")]
        public async Task<IActionResult> NotificationCount()
        {
            try
            {
                return Ok(await service.NotificationCount(SearchRequest(searchTerm: base.RoleId.ToString())));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpGet, Route("notifications")]
        public async Task<IActionResult> Read(int skip=0,int take=100)
        {
            try
            {
                return Ok(await service.Read(SearchRequest(skip, take, searchTerm: base.RoleId.ToString())));                
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
    }
}
