﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;

namespace Membership.PageObjects.CategoryPO
{
    class CategoryPO
    {

        public NgWebElement Category(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCatName"));
        }
        public void Btn_AddCategory_Click(NgWebDriver d)
        {
            this.Btn_AddCategory(d).Click();
        }
        private NgWebElement Btn_AddCategory(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSendCategory"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_EditCategory_Click(NgWebDriver d)
        {
            this.Btn_EditCategory(d).Click();
        }
        private NgWebElement Btn_EditCategory(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnUpdate"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_UpdateCategory_Click(NgWebDriver d)
        {
            this.Btn__UpdateCategory(d).Click();
        }
        private NgWebElement Btn__UpdateCategory(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnUpdateCategory"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_CancelCategory_Click(NgWebDriver d)
        {
            this.Btn_CancelCategory(d).Click();
        }
        private NgWebElement Btn_CancelCategory(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnCancelCategory"));
            ScrollToElem(elem, d);
            return elem;
        } 
      

        private NgWebElement Btn_DeleteCategory(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnDelete"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_DeleteCategory_Click(NgWebDriver d)
        {
            this.Btn_DeleteCategory(d).Click();
        }
       
          private NgWebElement Btn_DelConfirm(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnYes"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_DelConfirm_Click(NgWebDriver d)
        {
            this.Btn_DelConfirm(d).Click();
        }

        public void Btn_DelCancel_Click(NgWebDriver d)
        {
            this.Btn_DelCancel(d).Click();
        }
        private NgWebElement Btn_DelCancel(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnNo"));
            ScrollToElem(elem, d);
            return elem;
        }
        private NgWebElement Btn_Close(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnHide"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Close_Click(NgWebDriver d)
        {
            this.Btn_Close(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
