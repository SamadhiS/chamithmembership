﻿using System;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Data.Entity.Tenant;
using Inx.Module.Identity.Test.Utility;
using Inx.Utility.Utility;

namespace Inx.Module.Identity.Test.Seed
{
    public class SeedData
    {
        public void Seed()
        {
            SeedUser();
            Console.WriteLine("Data seeded");
        }

        public void SeedUser()
        {
            var tdata = new Tenant
            {
                Host = "http://mstest.com",
                Name = "Test Tenant",
                TenantTypeId = 2,
            };
            new Comman().Context.Tenants.Add(tdata);
            new Comman().Context.SaveChanges();
            new Comman().Context.Users.Add(new User
            {
                Email = "test@gmail.com",
                TenantId = tdata.Id,
                CreatedById = 0,
                CreatedOn = DateTime.UtcNow,
                EmailConfirmCode = Guid.Empty,
                IsEmailConfirmed = true,
                Password = "123".Encrypt(),
                RecordStatus = Enums.UserRecordStatus.Active,                
            });
            new Comman().Context.SaveChanges();
        }
    }
}
