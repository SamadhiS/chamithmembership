﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Bo.Security;
using Inx.Module.Identity.Core.Services.Security;
using Inx.Module.Identity.Core.Utility;
using Inx.Module.Identity.Service.Models.Security.ChangePassword;
using Inx.Module.Identity.Service.Models.Security.Login;
using Inx.Module.Identity.Service.Models.Security.Register;
using Inx.Module.Identity.Service.Utility;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Inx.Utility;
using Inx.Utility.Extentions;
using Inx.Utility.Messenger.Email.Modules.Email;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Inx.Module.Identity.Core.Bo.User;
using Inx.Module.Identity.Service.Models;
using Inx.Service.Base.Attributes;

namespace Inx.Module.Identity.Service.Controllers.Api.Security
{
    [Route("api/identity/v1/security")]
    public partial class SecurityApiController : IdentityBaseApiController, ISecurityApiController
    {
        private readonly ISecurityService serviceSecurity;
        public SecurityApiController(ISecurityService _serviceSecurity, IBaseInjector baseinject) : base(baseinject)
        {
            serviceSecurity = _serviceSecurity;
        }

        [HttpPost, Route("authentication"), AllowAnonymous, ModelValidation]
        public async Task<IActionResult> Authentication([FromBody]AuthenticateViewModel request)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = await serviceSecurity.Authentication(request.MapObject<AuthenticateViewModel, AuthenticateBo>());
                stopwatch.Stop();
                return Ok(Token.TokenGenarator(result, stopwatch.ElapsedMilliseconds, result.UserState));
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }
        #region registeration process

        [HttpPost, Route("register"), AllowAnonymous, ModelValidation]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel request)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var res = await serviceSecurity.Register(Request<RegisterViewModel, RegisterBo>(request, true));
                //send confirmation email
                await SendEmailValidationEmail(request.Email, res.Token);
                //BackgroundJob.Enqueue(() => SendEmailValidationEmail(request.Email, res.Token));
                stopwatch.Stop();
                return Ok(new Response<bool>()
                {
                    Item = true,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpPost, Route("register/resendregistervalidateemail"), AllowAnonymous, ModelValidation]
        public async Task<IActionResult> ResendRegisterValidateEmail([FromBody]PostValue<string> email)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var token = await serviceSecurity.ResendRegisterValidateEmail(Request(email.Value, true));
                //resend confirmation email
                await SendEmailValidationEmail(email.Value, token);
                // BackgroundJob.Enqueue(() => SendEmailValidationEmail(email.Value, token));
                stopwatch.Stop();
                return Ok(new Response<bool>()
                {
                    Item = true,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpPost, Route("register/validateemailaddresstoken"), AllowAnonymous, ModelValidation]
        public async Task<IActionResult> ValidateEmailAddress(string email, string token)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var userid = await serviceSecurity.ValidateEmailAddress(
                    Request(new Dictionary<string, string> { { "email", email.TrimAndToLower() }, { "token", token } }, true));
                stopwatch.Stop();
                return Ok(Token.TokenGenarator(new AuthResponse
                {
                    TenantId = 0,
                    Email = email.TrimAndToLower(),
                    Name = Constants.DefaultUserDisplayName,
                    ProfileImage = Constants.ProfileNoImage,
                    Id = userid,
                    UserState = Enums.UserRecordStatus.EmailSubmit,
                    Roles = new List<string>() { Constants.SuperAdmin }
                }, stopwatch.ElapsedMilliseconds));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        #region emails
        [ApiExplorerSettings(IgnoreApi = true)]
        async Task SendEmailValidationEmail(string email, Guid token)
        {
            await Email.Generate(await Lang("Hi there,", HttpContext),
                await Lang("We know our registration process takes few minutes of your time, we hope it'll be worth your while... To proceed please help us verify your email address.", HttpContext))
                 .Sender(await Lang("Email Confirmation", HttpContext), $"Hi {email},",
                     new Inx.Utility.Messenger.Email.Modules.Application.Tenant
                     {
                         Name = "Team Accocify",
                         Host = "sa",
                         LogoFileName = ""
                     }, new List<string>() { email })
                 .AddButton(new Button
                 {
                     Title = await Lang("Verify and Continue", HttpContext),
                     Type = ButtonType.Apply,
                     Url = $"{GlobleConfig.ApiUrl}/#/register?email={email}&token={token.ToString()}"

                 }).Send();
        }
        #endregion
        [HttpPut, Route("changepassword"), ModelValidation]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel request)
        {
            try
            {
                await serviceSecurity.ChangePassword(Request<ChangePasswordViewModel, ChangePasswordBo>(request));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPut,Route("register/validaterequest"),AllowAnonymous]
        public async Task<IActionResult> NewUserCreateRequestValidation(string email,string token) {
            try
            {
                await serviceSecurity.NewUserCreateRequestValidation( Request (new Dictionary<string, string> { { "email", email.TrimAndToLower() },{ "token", token } },true));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPut, Route("changepermissionspassword"), AllowAnonymous, ModelValidation]
        public async Task<IActionResult> ChangePermissionsPassword([FromBody] ChangePermissionPasswordViewModel request)
        {
            try
            {
                await serviceSecurity.ChangeUserInitialPassword(Request<ChangePermissionPasswordViewModel, ChangePermissionPasswordBo>(request,true));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        #region forget password

        [HttpPost, Route("forgetpasswordrequest"), AllowAnonymous
         , Description("for resend email this method also use"), ModelValidation]
        public async Task<IActionResult> ForgetPasswordRequest([FromBody] PostValue<string> email)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var token = await serviceSecurity.ForgetPasswordRequest(Request(email.Value, true));
                //BackgroundJob.Enqueue(() => SendForgetPasswordTokenEmail(email.Value, token));
                await SendForgetPasswordTokenEmail(email.Value, token);
                stopwatch.Stop();
                return Json(new Response<bool>
                {
                    Item = true,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPost, Route("forgetpasswordvalidatetoken"), AllowAnonymous]
        public async Task<IActionResult> ForgetPasswordValidateToken(string email, string token)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = await serviceSecurity.ForgetPasswordValidateToken(
                    Request(new Dictionary<string, string> { { "email", email.TrimAndToLower() }, { "token", token } }, true));
                stopwatch.Stop();
                return Ok(Token.TokenGenarator(result, stopwatch.ElapsedMilliseconds));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPut, Route("forgetpasswordsetnewpassword"), ModelValidation]
        public async Task<IActionResult> ForgetPasswordSetNewPassword([FromBody] PostValue<string> password)
        {
            try
            {
                await serviceSecurity.ForgetPasswordChangePassword(Request(password.Value));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task SendForgetPasswordTokenEmail(string email, Guid token)
        {
            await Email.Generate(await Lang("Reset Password Request", HttpContext),
                    await Lang("Click following button to reset the Password", HttpContext))
                .Sender(await Lang("Reset Password", HttpContext), $"Hi {email},",
                    new Inx.Utility.Messenger.Email.Modules.Application.Tenant
                    {
                        Name = "Membership application",
                        Host = "sa",
                        LogoFileName = ""
                    }, new List<string>() { email })
                .AddButton(new Button
                {
                    Title = await Lang("Reset Password", HttpContext),
                    Type = ButtonType.Apply,
                    Url = $"{GlobleConfig.ApiUrl}#/resetpassword?email={email}&token={token.ToString()}"
                }).Send();
        }
        #endregion

       


    }
}
#endregion
