﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.ContactPO;
using OpenQA.Selenium;

namespace Membership.StepDefinition.ContactStepsDef
{
    [Binding]
    public sealed class ArchiveContactSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();
        private EditContactPO editContactsPO = new EditContactPO();
        private ARCHIVEcontactPO aRCHIVEcontactPO = new ARCHIVEcontactPO();

        [Given(@"Load the membership Application")]
        public void GivenLoadTheMembershipApplication()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }
        [Then(@"User enter ""(.*)"" and ""(.*)""")]
        public void ThenUserEnterAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
            Thread.Sleep(3000);
        }

        [Then(@"I Clicks on Submit btn")]
        public void ThenIClicksOnSubmitBtn()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);

        }

        [Then(@"direct to Dashboard")]
        public void ThenDirectToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I clicked on Contacts pg")]
        public void ThenIClickedOnContactsPg()
        {
            navPage.Btn_Contact_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"I select a contact and clicked on Edit btn")]
        public void ThenISelectAContactAndClickedOnEditBtn()
        {
            editContactsPO.Btn_Edit_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I clicked on Archive btn")]
        public void ThenIClickedOnArchiveBtn()
        {
            aRCHIVEcontactPO.archiveBtn(ngWebDriver);
            Thread.Sleep(2000);
        }
        [Then(@"I confirm")]
        public void ThenIConfirm()
        {
            aRCHIVEcontactPO.confirmArchive(ngWebDriver);
            Thread.Sleep(5000);
        }
        [Then(@"I click no Button")]
        public void ThenIClickNoButton()
        {
            aRCHIVEcontactPO.ConfirmArchive_no(ngWebDriver);
            Thread.Sleep(5000);
        }


        [Then(@"Click on Logout btn")]
        public void ThenClickOnLogoutBtn()
        {
            Thread.Sleep(4000);
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
