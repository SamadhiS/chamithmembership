import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberLevelSubscribeComponent } from './member-level-subscribe.component';

describe('MemberLevelSubscribeComponent', () => {
  let component: MemberLevelSubscribeComponent;
  let fixture: ComponentFixture<MemberLevelSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberLevelSubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberLevelSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
