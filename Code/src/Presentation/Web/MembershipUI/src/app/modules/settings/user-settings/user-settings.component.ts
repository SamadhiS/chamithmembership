import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { ChangePassword } from '../models/passwordModel';
import { SettingService } from '../services/setting.service';
import { ToastsManager } from 'ng2-toastr';
import { TenantInfoService } from '../../core/services/tenant-info.service';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { BlockUiService } from '../../core/services/block-ui.service';

@Component({
  selector: 'membership-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  resetPasswordForm: FormGroup;
  patternModel = new PatternModel();
  password = new ChangePassword();
  isBlocked = false;

  constructor(private fb: FormBuilder, vcr: ViewContainerRef, public toastr: ToastsManager,
    private settingService: SettingService, 
    private tenantInfoService: TenantInfoService,
    private blockUiService: BlockUiService,) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  createResetForm() {
    this.resetPasswordForm = this.fb.group({
      currentPassword: ["", [Validators.required]],
      newPassword: ["", [Validators.required, patternValidator(this.patternModel.passwordRegex)]],
      confirmPassword: ["", [Validators.required]],
    }, { validator: this.comparePasswords });
  }
  comparePasswords(group: FormGroup) {
    let password = group.controls.newPassword.value;
    let confirmPassword = group.controls.confirmPassword.value;
    return (password === confirmPassword) ? null : { isPasswordNotSame: true }
  }

  resetPassword() {
    this.isBlocked = true;
    this.password = Object.assign({}, this.password, this.resetPasswordForm.value)
    this.settingService.changePassword(this.password)
      .subscribe(res => {
        this.isBlocked = false;
        this.toastr.success('Password has been successfully updated', 'Success');
        this.resetPasswordForm.reset();
      },
        error => {
          this.isBlocked = false;
          this.toastr.error('Current Password is Invalid', 'Oops');
        });
  }
  ngOnInit() {
    this.createResetForm();
  }

}
