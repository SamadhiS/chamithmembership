﻿// Place this code for praticular service layer [Inx.Module.Membership.Service.Controllers.Api]
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Inx.Service.Base.Attributes;
using Inx.Utility.Utility;
using Inx.Core.Base;
using Inx.Service.Base.Controllers;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Models;
using Inx.Utility.Models;
using Inx.CORE;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Data.Base.Repository;
using Inx.DATA;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Microsoft.EntityFrameworkCore;
using System.Web.Http.Description;
namespace Inx.Module.Membership.Service.Controllers.Api
{
  [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
  public class DDDApiController: MembershipBaseApiController, IBaseApi< DDDViewModel, int>{
      
	    private readonly IDDDService service;
        public DDDApiController(IDDDService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("membergroups"),ModelValidation, ResponseType(typeof(DDDViewModel))]
        public async Task<IActionResult> Create([FromBody]  DDDViewModel item)
        {
            return await Create<DDDViewModel, DDDBo, IDDDService>(item, service);
        }
        [HttpDelete, Route("membergroups/{id:int}"),ResponseType(typeof(void))]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<DDDBo, IDDDService>(id, this.service);
        }
        [HttpGet, Route("membergroups/{id:int}"), ResponseType(typeof(DDDViewModel))]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<DDDViewModel,  DDDBo, IDDDService>(id, this.service);
        }
        [HttpGet, Route("membergroups"),ResponseType(typeof(PageList<DDDViewModel>))]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<DDDViewModel,  DDDBo, IDDDService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }
		//Following code for search.
		//[HttpGet, Route("membergroups"),ResponseType(typeof(PageList<MembersViewResult>))]
        // public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        // {
        //     return await Search<MembersViewResult, IMemberService>(SearchRequest(skip, take, searchTerm: search, orderByTerm: orderby), this.service);
        // }
        [HttpGet, Route("membergroups/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<DDDBo, IDDDService>(service);
        }
        [HttpPut, Route("membergroups"),ModelValidation,ResponseType(typeof(void))]
        public async Task<IActionResult> Update([FromBody]  DDDViewModel item)
        {
            return await Update<DDDViewModel,  DDDBo, IDDDService>(item, this.service);
        }
	   
  }
}
//place following code inside core [Inx.Module.Membership.Core.Bo.DDD]
	public class  DDDViewModel:BaseViewModel{
		//TODO
	}

	//CORE
namespace Inx.CORE
{

	//place following code inside core [Inx.Module.Membership.Core.Service.DDD]
    public interface IDDDService:IBaseService<DDDBo>,ISearchService<SearchResult>
	{
		 
    }
	public class SearchResult{
	
	}
		//place following code inside core [Inx.Module.Membership.Core.Bo.DDD]
		public class  DDDBo:BaseBo{
		   //TODO
		}

	  public class  DDDService : BaseService, IDDDService
	  {
        private readonly IUnitOfWokMembership uow;
        private readonly IMemoryCacheManager cache;
        public  DDDService(IUnitOfWokMembership _uow, IMemoryCacheManager _cache)
        {
            uow = (IUnitOfWokMembership)_uow;
            cache = _cache;
        }
        public async Task<DDDBo> Create(Request<DDDBo> req)
        {
            try
            {
                return (await uow.DDDRepository.CreateAndSave(req.MapRequestObject<DDDBo, DDD>())).MapObject< DDD,  DDDBo>();
			
			// IF THERE IS CACHE
			// var item = await uow.DDDRepository.CreateAndSave(
           //         req.MapRequestObject<DDDBo, DDD>());
           //     await RemoveCache(req.TenantId);
           //     return item.MapObject<DDD, DDDBo>();		
		   
		   }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Delete(Request<int> req)
        {
            try
            {
                await uow.DDDRepository.DeleteAndSave(req); 
				//IF THERE CACHE
				//await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<PageList<DDDBo>> Read(Search search)
        {
            try
            {
                return (await uow.DDDRepository.Read<DDD>(search))
                    .MapPageObject<DDD, DDDBo>();

			 // IF THERE IS CACHE
			  /*var ckey = CacheKeyGen(new List<string> {"YOUR CACHE KEY",search.TenantId.ToString() });
                var result = await cache.GetAsync<PageList<DDDBo>>(ckey);
                return result.IsNull()
                    ? (await uow.DDDRepository.Read<DDD>(search))
                    .MapPageObject<DDD, DDDBo>()
                    .CacheSetAndGet(cache, ckey)
                    : result;*/
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<DDDBo> Read(Request<int> req)
        {
            try
            {
                return (await uow.DDDRepository.Read(req))
                   .MapObject<DDD, DDDBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                return await uow.DDDRepository.ReadKeyValue<DDD>(req);

				//IF THERE IS CACHE
				/*var ckey = CacheKeyGen(new List<string> { "cachekey".ToString(), req.TenantId.ToString() });
                var result = await cache.GetAsync<List<KeyValueListItem<int>>>(ckey);
                return result.IsNull()
                    ? (await uow..DDDRepository.ReadKeyValue<DDD>(req)).CacheSetAndGet(cache,
                        ckey)
                    : result;*/
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        public async Task Update(Request<DDDBo> req)
        {
            try
            {
                await uow.DDDRepository.UpdateAndSave(req.MapRequestObject<DDDBo, DDD>(req.Item.Id));
				 //IF THERE CACHE
				//await RemoveCache(req.TenantId);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

	  public async Task<PageList<SearchResult>> Search(Search request)
        {
            throw new NotImplementedException();
        }

	  private async Task RemoveCache(int tenantid)
        {
           // await cache.Remove(CacheKeyGen(new List<string>{Enums.CacheKey.ContactCategory.ToString(),tenantid.ToString()}));
          //  await cache.Remove(
              //  CacheKeyGen(new List<string> {Enums.CacheKey.ContactCategoryKeyValue.ToString(), tenantid.ToString()}));
        }
    }
 }
	// DATA
namespace Inx.DATA
{
    public interface IUnitOfWokMembership : IUnitOfWork
    {
        GenericRepository<DDD> DDDRepository { get; } 
    } 
	[Table("DDDs"/*, Schema = Constraints.Schema*/)]
	public class  DDD:AuditableEntity{
	   //TODO
	}
  public interface IMembershipDbSet
    {
        DbSet<DDD> DDD { get; set; }
    }
  public partial class UnitOfWork : IUnitOfWokMembership
    {

	    // place this code
        private GenericRepository<DDD> _DDDRepository;
	
	    public DbContext Context => throw new NotImplementedException();

        public string ConnectionString => throw new NotImplementedException();

         public GenericRepository<DDD>  DDDRepository =>
                _DDDRepository ?? (_DDDRepository
				= new GenericRepository<DDD>(Context));

        public void Save()
        { 
            throw new NotImplementedException();
        }

        public Task SaveAsync()
        {
            throw new NotImplementedException();
        }
    }
	   public class  MembershipDbContext {
	     public DbSet<DDD> DDD { get; set; } 
	   }
}
//ioc
 // iocManager.RegisterIfNot<IDDDService,  DDDService>();

 //automapper
 //  cfg.CreateMap<DDD, DDDBo>().ReverseMap();
 //cfg.CreateMap<DDDViewModel, DDDBo>().ReverseMap();


