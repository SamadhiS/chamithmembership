﻿using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inx.Module.Identity.Data.Entity.Tenant
{
    [Table("TenantInfo", Schema = Constraints.Schema)]
    public class TenantInfo: AuditableEntity
    {
        [EmailAddress, StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [StringLength(DbConstraints.AddressLength)]
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Country { get; set; }
        [StringLength(DbConstraints.PhoneLength)]
        public string Phone { get; set; }
        [StringLength(DbConstraints.FaxLength)]
        public string Fax { get; set; }
        [Obsolete,NotMapped]
        public new string Name { get; set; }
        public int Language { get; set; }
        public int Currency { get; set; }
        public string Prefix { get; set; }
    }
}
