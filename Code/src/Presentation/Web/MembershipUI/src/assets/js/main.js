// $('.menu-switch').on('click',function(){
// 	$('body').toggleClass('close-sidebar');
   
// });

// $(document).ready(function(){
// 	 if(($(window).width()) < 991){
//         $('body').addClass('close-sidebar');
//     }

//         $('.star').on('click', function () {
//       $(this).toggleClass('star-checked');
//     });

//     $('.ckbox label').on('click', function () {
//       $(this).parents('tr').toggleClass('selected');
//     });

//     $('.btn-filter').on('click', function () {
//       var $target = $(this).data('target');
//       if ($target != 'all') {
//         $('.table tr').css('display', 'none');
//         $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
//       } else {
//         $('.table tr').css('display', 'none').fadeIn('slow');
//       }
//     });

// $('#add').on('click',function(){
//     $('.addNew').slideToggle();

    
// });

// $('.mail-timeline .small-card').on('click', function() {
//     $(this).toggleClass('selected');
// });

// });

// Ps.initialize(timeline);

// setInterval(addNotify, 20000);


// function addNotify(){

// var node = $('.first');
// var elem = '<div class="small-card animated"><div class="image-circle active-border"><img class="img img-responsive" src="https://randomuser.me/api/portraits/women/8.jpg" alt="Adelle Charles"></div><div class="info"><h5>Adelle Charles</h5><p>Paid 55$ as Annual Memebership Payment</p></div></div>'

// node.prepend(elem);

// };


var ctx = document.getElementById("members").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["July", "Aug", "Sep"],
        datasets: [{
            label: 'Members',
            data: [2590, 2890, 3260],
            backgroundColor: [
                 'rgba(54, 162, 235, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)','rgba(54, 162, 235, 1)','rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
       legend: {
            display: false,
          },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



var ctx = document.getElementById("types").getContext('2d');
var myChart = new Chart(ctx, {
     type: 'doughnut',
    data: {
        labels: ["Gold", "Sliver", "Bornze", "Paltinum","Basic"],
        datasets: [{

            data: [2590, 2890, 3260, 1000,2000],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
options: {
      responsive: true,
     legend: {
            display: true,
            fullWidth: false,
            labels: {
                //fontColor: 'rgb(255, 99, 132)',
                boxWidth: 10,
            }
        }
                  }
});


var ctx = document.getElementById("revenue").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["July", "Aug", "Sep"],
        datasets: [{
            label: 'Members',
            data: [202000, 236000, 286000],
            backgroundColor: [
                 'rgba(54, 162, 235, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(54, 162, 235, 0.2)',
               
            ],
            borderColor: [
                 'rgba(54, 162, 235, 1)', 'rgba(54, 162, 235, 1)', 'rgba(54, 162, 235, 1)',
              
            ],
            borderWidth: 1
        }]
    },
    options: {
       legend: {
            display: false,
          },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



var ctx = document.getElementById("gold").getContext('2d');
var myChart = new Chart(ctx, {
     type: 'doughnut',
    data: {
        labels: ["Active", "Inactive",],
        datasets: [{

            data: [2590, 123],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
               'rgba(54, 162, 235, 1)',
                'rgba(255,99,132,1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
options: {
      responsive: true,
     legend: {
            display: true,
            fullWidth: false,
            labels: {
                //fontColor: 'rgb(255, 99, 132)',
                boxWidth: 10,
            }
        }
                  }
});

var ctx = document.getElementById("sliver").getContext('2d');
var myChart = new Chart(ctx, {
     type: 'doughnut',
    data: {
        labels: ["Active", "Inactive",],
        datasets: [{

            data: [2590, 500],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255,99,132,1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
options: {
      responsive: true,
     legend: {
            display: true,
            fullWidth: false,
            labels: {
                //fontColor: 'rgb(255, 99, 132)',
                boxWidth: 10,
            }
        }
                  }
});

var ctx = document.getElementById("bronze").getContext('2d');
var myChart = new Chart(ctx, {
     type: 'doughnut',
    data: {
        labels: ["Active", "Inactive",],
        datasets: [{

            data: [2290, 120],
            backgroundColor: [
                 'rgba(54, 162, 235, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255,99,132,1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
options: {
      responsive: true,
     legend: {
            display: true,
            fullWidth: false,
            labels: {
                //fontColor: 'rgb(255, 99, 132)',
                boxWidth: 10,
            }
        }
                  }
});


