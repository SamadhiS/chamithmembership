﻿Feature: As an Admin, I should be able to Update Organization Information with valid data

@mytag
Scenario Outline:As an Admin, I should be able to Update Organization Information with valid data

Given Load membership login
And I have entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And I Click on Sign in button 
Then I have Navigated to Dashboard
And Click on Settings btn
And I click on Organization Settings
And I click on image browser button
And I click on image Upload button
And I have entered email address as <email>
And I have entered phone no as <phone>
And I select currency
And I select country
And I have entered fax no as <fax>
And I have entered prefix as <prefix>
And I have entered address as <address>
And I have entered state as <state>
And I select language
And I have entered city as <city>
And I have entered zip as <zip>
And I click on Update Btn
And I click on signout
Examples:
| email          | phone     | fax       | prefix | address                    | state       | city       | zip   |
| memb@gmail.com | 777885999 | 751222333 | Mem    | No:24, Nugegoda, Sri Lanka | Western Pro | Maharagama | 10232 |


Scenario Outline:As an Admin, I should not be able to Update Organization Information with invalid data

Given Load membership login
And I have entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And I Click on Sign in button 
Then I have Navigated to Dashboard
And Click on Settings btn
And I click on Organization Settings
And I click on image browser button
And I click on image Upload button
And I have entered email address as <email>
And I have entered phone no as <phone>
And I select currency
And I select country
And I have entered fax no as <fax>
And I have entered prefix as <prefix>
And I have entered address as <address>
And I have entered state as <state>
And I select language
And I have entered city as <city>
And I have entered zip as <zip>
And I click on Update Btn
And I click on signout
Examples:
| email            | phone     | fax       | prefix | address | state   | city       | zip   |
| membin@gmail.com | 777885990 | 781222333 |        |         | Western | Maharagama | 10300 |
| memb             | abcdefghi | acccccc   |        |         | Western | Nugegoda   | 11200 |