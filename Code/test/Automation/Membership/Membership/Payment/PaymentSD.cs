﻿using Membership.PageObjects;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.PaymentPO;
using System.Windows.Forms;
using System;
using System.Diagnostics;


namespace Membership.Payment
{
    [Binding]
    public sealed class PaymentSD
    {
        private static NgWebDriver driver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(driver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private PageObjects.MemberPO.MemberPO memberPO = new PageObjects.MemberPO.MemberPO();
        private paymentPO payPO = new paymentPO();

        [Given(@"System application page is opened")]
        public void GivenSystemApplicationPageIsOpened()
        {
            loginPage.LoginPageLoad(driver);
        }

        [Given(@"Enter input for Login ""(.*)"" and ""(.*)""")]
        public void GivenEnterInputForLoginAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"click on login")]
        public void GivenClickOnLogin()
        {
            loginPO.Btn_SignIn_Click(driver);
            Thread.Sleep(2000);
        }

        [Given(@"Naviagate to Homepage")]
        public void GivenNaviagateToHomepage()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(5000);
        }

        [Given(@"click on Payments")]
        public void GivenClickOnPayments()
        {
            payPO.Btn_PaySec_Click(driver);
            Thread.Sleep(5000);
        }

        [Given(@"search the payment status ""(.*)""")]
        public void GivenSearchThePaymentStatusAs(string p0 )
        {  
            payPO.searchField(driver).SendKeys(p0);
            Thread.Sleep(5000);

            payPO.Btn_payview_click(driver);
            Thread.Sleep(5000);
        }

        [Given(@"click on Settle")]
        public void GivenClickOnSettle()
        {
            payPO.Btn_Settle_Click(driver);
            Thread.Sleep(3000);
        }

        [Given(@"Select Payment Type as Cash")]
        public void GivenSelectPaymentTypeAs()
        {
            payPO.Btn_payType_Click(driver);
            Thread.Sleep(3000);
        }

        [Given(@"Click on Pay Now")]
        public void GivenClickOnPayNow()
        {
            Thread.Sleep(3000);
                payPO.Btn_PayNow_Click(driver);
        }

        [Given(@"Success message appears")]
        public void GivenSuccessMessageAppears()
        {
            Thread.Sleep(3000);
        }

        [Given(@"Navigate back to Payment section")]
        public void GivenNavigateBackToPaymentSection()
        {
            payPO.Btn_Back_Click(driver);
        }

        [Given(@"Log out")]
        public void GivenLogOut()
        {
            Thread.Sleep(4000);
            loginPO.Btn_Logout_Click(driver);
        }

        [Given(@"there is no Settle Button")]
        public void GivenThereIsNoSettleButton()
        {
            Thread.Sleep(4000);
        }

        [Given(@"Click on Members")]
        public void GivenClickOnMembers()
        {
            navPage.Btn_Member_click(driver);
            Thread.Sleep(5000);
        }

        [Given(@"Filter by payment status pending")]
        public void GivenFilterByPaymentStatus()
        {
            payPO.Btn_filter_Click(driver);
            Thread.Sleep(2000);
                payPO.Btn_filterPend_Click(driver);
                Thread.Sleep(2000);
        }

        [Given(@"Click on the member View")]
        public void GivenClickOnTheMemberView()
        {
            memberPO.Btn_Edit_Click(driver);
            Thread.Sleep(3000);
        }

        [Given(@"Go to Payment Tab")]
        public void GivenGoToPaymentTab()
        {
            payPO.Btn_memPay_Click(driver);
            Thread.Sleep(2000);
        }

        [Given(@"Click on view INvoice")]
        public void GivenClickOnViewINvoice()
        {
            payPO.Btn_view_click(driver);
            Thread.Sleep(2000);
        }




    }
}
