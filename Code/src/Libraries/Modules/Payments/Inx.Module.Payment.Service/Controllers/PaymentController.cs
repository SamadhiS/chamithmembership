﻿using Inx.Module.Payment.Core.Bo;
using Inx.Module.Payment.Core.Services.Interfaces;
using Inx.Module.Payment.Service.Models;
using Inx.Module.Payment.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Payment.Service.Controllers
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class PaymentController : PaymentBaseApiController, IBaseApi<PaymentViewModel, int>
    {
        private readonly IPaymentService service;
        public PaymentController(IPaymentService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("payments"), ModelValidation]
        public async Task<IActionResult> Create([FromBody]  PaymentViewModel item)
        {
            return await Create<PaymentViewModel, PaymentBo, IPaymentService>(item, service);
        }
        [HttpDelete, Route("payments/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<PaymentBo, IPaymentService>(id, this.service);
        }
        [HttpGet, Route("payments/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<PaymentViewResult, PaymentBo, IPaymentService>(id, this.service);
        }

        [HttpGet, Route("payments/member/{id:int}")]
        public async Task<IActionResult> ReadMemberPayment(int id)
        {
            return Ok((await service.ReadMemberPayment(Request(id))).MapObject<PaymentBo, PaymentViewResult>());
        }

        [HttpGet, Route("payments")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<PaymentViewModel, PaymentBo, IPaymentService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }


        [HttpGet, Route("payments/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<PaymentBo, IPaymentService>(service);
        }
        [HttpPut, Route("payments"), ModelValidation]
        public async Task<IActionResult> Update([FromBody]  PaymentViewModel item)
        {
            return await Update<PaymentViewModel, PaymentBo, IPaymentService>(item, this.service);
        }

        [HttpGet, Route("payments/transactiontypes")]
        public async Task<IActionResult> ReadTransActionTypes()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var transactionTypes = EnumsExtention.GetList<TransActionType>();
                stopwatch.Stop();
                return Ok(new Response<List<EnumMember>>
                {
                    Item = transactionTypes,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("payments/paymenttypes")]
        public async Task<IActionResult> ReadPaymentTypes()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var paymentTypes = EnumsExtention.GetList<PaymentTypes>();
                stopwatch.Stop();
                return Ok(new Response<List<EnumMember>>
                {
                    Item = paymentTypes,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
    }
}
