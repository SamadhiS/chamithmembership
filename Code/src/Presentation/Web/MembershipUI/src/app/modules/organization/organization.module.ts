import { OrganizationService } from './services/organization.service';
import { MemberSubscriptionService } from '../members/services/member-subscription.service';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationCreateComponent } from './organization-create/organization-create.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    SharedModule,
    TranslateModule,
    AngularMultiSelectModule,
    BsDatepickerModule.forRoot(),
  ],
  declarations: [OrganizationCreateComponent],
  providers: [MemberSubscriptionService,OrganizationService]
})
export class OrganizationModule {
  
 }
