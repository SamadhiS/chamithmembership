﻿using Inx.Module.Identity.Core.Bo;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Module.Identity.Service.Models.Tenant;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Inx.Module.Identity.Service.Controllers.Api.Tenant
{
    [Route("api/identity/v1"), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class TenantInfoApiController : BaseApiController, IBaseApi<TenantInfoViewModel, int>
    {
        private readonly ITenantInfoService serviceTenantInfo;
        public TenantInfoApiController(ITenantInfoService _serviceTenantInfo, IBaseInjector baseinject) : base(baseinject)
        {
            serviceTenantInfo = _serviceTenantInfo;
        }
        [HttpPost, Route("tenantinfo"), ModelValidation]
        public async Task<IActionResult> Create([FromBody]TenantInfoViewModel item)
        {
            return await Create<TenantInfoViewModel, TenantInfoBo, ITenantInfoService>(item, serviceTenantInfo);
        }

        [HttpGet, Route("tenantinfo")]
        public async Task<IActionResult> Read(int id)
        {
            return Ok(await serviceTenantInfo.GetTenantInfo(Request(id)));
        }

        [HttpPut, Route("tenantinfo")]
        public async Task<IActionResult> Update([FromBody]TenantInfoViewModel item)
        {
            return Ok(await Update<TenantInfoViewModel, TenantInfoBo, ITenantInfoService>(item, serviceTenantInfo));
        }

        #region not implemented
        [ApiExplorerSettings(IgnoreApi = true), Obsolete]
        public new Task<IActionResult> Delete(int id)
        {
            throw new NotImplementedException();
        }

        [ApiExplorerSettings(IgnoreApi = true), Obsolete]
        public new Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true), Obsolete]
        public new Task<IActionResult> ReadKeyValue()
        {
            throw new NotImplementedException();
        }
        
        #endregion
    }
}
