﻿using Inx.Data.Base.Repository;
using Inx.Module.Messenger.Data.Entity;


namespace Inx.Module.Messenger.Data.DbContext
{
    public interface IUnitOfWorkMassengers: IUnitOfWork
    {
        GenericRepository<EmailTemplate> EmailTemplateRepository { get; }
    }
}
