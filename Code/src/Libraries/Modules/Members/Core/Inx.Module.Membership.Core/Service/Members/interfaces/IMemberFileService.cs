﻿using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Member;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Members.interfaces
{
    public interface IMemberFileService : IBaseService<MemberFileBo>
    {
        new Task<string> Delete(Request<int> req);
    }
}
