export class SubscribedLevelInfo {
    memberId: number;
    subscriptionId: number;
    subscriptionTypeId: number;
    isActive: boolean;
    renewDate: Date;
    membershipStatus: number;
    paymentState: number;
    joinDate: Date;
    name: string;
    description: string;
    price: number
    terminationNote:string;
    membershipExpired:number;
}