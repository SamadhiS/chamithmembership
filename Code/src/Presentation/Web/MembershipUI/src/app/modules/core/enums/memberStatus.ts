export enum MemberStatusEng {
    all = "all",
    active = "active",
    inactive = "inactive",
    terminated = "terminated",
    pending = "pending",
}
export enum MemberStatusSin {
    allS = "සියලු",
    activeS = "සක්රීයයි",
    inactiveS = "අක්රියයි",
    terminatedS = "විභාග වෙමින්",
    pendingS = "අවලංගුයි",
}
export enum MemberStatusTam {
    allT = "அனைத்து",
    activeT = "செயலில்",
    inactiveT = "செயல்படா",
    terminatedT = "நிலுவையில்",
    pendingT = "நிறுத்தப்பட்டது",
}