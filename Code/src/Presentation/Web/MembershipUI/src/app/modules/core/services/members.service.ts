import { RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MembersService extends BaseService {

  private apiEndPoint = `${this.baseApiEndPoint}membership/v1/members`;
  private archiveMembersEndPoint = `${this.baseApiEndPoint}membership/v1/membersArchive`;
  private apiEndPointMemberFile = `${this.baseApiEndPoint}membership/v1/memberfiles`;
  private apiEndPointMemberTimeLine = `${this.baseApiEndPoint}membership/v1/membershiptimeline`;

  constructor(private http: HttpClient) {
    super();
  }
  saveTimeLine(timeLine) {
    return this.http.post(`${this.apiEndPointMemberTimeLine}`, (timeLine), this.httpOptions).map(res => res).catch(this.registerErrorHandler)
  }
  getTimeLineById(memberId) {
    return this.http.get(`${this.apiEndPointMemberTimeLine}/${memberId}`).map(res => res).catch(this.errorHandler)
  }
  updateTimeLine(timeLine) {
    return this.http.put(`${this.apiEndPointMemberTimeLine}`, (timeLine)).map(res => res).catch(this.errorHandler)
  }
  getMembers(skip, take, search, orderby) {
    return this.http.get(`${this.apiEndPoint}?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`).map((response) => response).catch(this.errorHandler)
  }
  getArchiveMembers(skip, take, search, orderby) {
    return this.http.get(`${this.archiveMembersEndPoint}?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`).map((response) => response).catch(this.errorHandler)
  }
  getDocumentsById(skip, take, memberId) {
    return this.http.get(`${this.apiEndPointMemberFile}?skip=${skip}&take=${take}&search=${memberId}`).map(res => res).catch(this.errorHandler)
  }
  getMemberById(id) {
    return this.http.get(`${this.apiEndPoint}/${id}`).map(res => res).catch(this.errorHandler)
  }
  getMembersKeyValue() {
    return this.http.get(`${this.apiEndPoint}/keyvalue`).map(res => res).catch(this.errorHandler)
  }
  getGroupById(groupId) {
    return this.http.get(`${this.baseApiEndPoint}membership/v1/groups/${groupId}`).map(res => res).catch(this.errorHandler)
  }
  saveMember(files, memberData) {
    let formData: FormData = new FormData();
    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('memberViewData', JSON.stringify(memberData));
    return this.http.post(this.apiEndPoint, formData).map(res => res).catch(this.errorHandler)
  }
  verifyPhone(contactObj) {
    return this.http.post(`${this.apiEndPoint}/mobilecheck`,
      contactObj, this.httpOptions)
      .map((response) => response)
      .catch(this.errorHandler)
  }
  verifyemail(contactObj) {
    return this.http.post(`${this.apiEndPoint}/emailcheck`, contactObj, this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
  saveDocument(files, docObj) {
    let formData: FormData = new FormData();
    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('memberFileViewData', JSON.stringify(docObj));
    return this.http.post(`${this.apiEndPointMemberFile}`, formData, this.httpOptions).map(res => res).catch(this.errorHandler)
  }
  updateContact(contact) {
    return this.http.put(`${this.apiEndPoint}/contacts`, contact, this.httpOptions).map(res => res).catch(this.errorHandler)
  }
  archiveMember(memberId) {
    return this.http.delete(`${this.apiEndPoint}/${memberId}`, this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
  restoreMember(memberId) {
    return this.http.put(`${this.apiEndPoint}/${memberId}/restore`, this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
  updateWork(work) {
    return this.http.put(`${this.apiEndPoint}/working`, work, this.httpOptions).map(res => res).catch(this.errorHandler)
  }
  updateMember(files, memberData) {

    let formData: FormData = new FormData();

    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('personViewData', JSON.stringify(memberData));
    return this.http.put(`${this.apiEndPoint}/members`, formData).map(res => res).catch(this.errorHandler)
  }
  archiveDocument(documentId) {
    return this.http.delete(`${this.apiEndPointMemberFile}/${documentId}`, this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
  getMemberStatus() {
    return this.http.get(`${this.baseApiEndPoint}membership/v1/member/memberstatus`).map((response) => response).catch(this.errorHandler)
  }
  deleteMember(id) {
    return this.http.delete(`${this.baseApiEndPoint}membership/v1/membergroups/${id}`,this.httpOptions).map((response) => response).catch(this.errorHandler)
  }
}
