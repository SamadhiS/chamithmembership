﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition
{
    public class MemberTypeTabFieldsRenderService: MemberBaseService, IMemberTypeTabFieldsRenderService
    {
        private readonly IUnitOfWokMembership uowMembership;
        public MemberTypeTabFieldsRenderService(IUnitOfWokMembership _uowMembership, ICoreInjector coreInjector) : base(coreInjector)
        {
            uowMembership = _uowMembership;
        }

        public async Task<MemberTypeTabFieldsRenderViewResult> RenderInfo(Request<Enums.MemberType> request)
        {

            List<TabsFieldsViewResult> checkIsSystemAttributes(List<TabsFieldsViewResult> result,List<MemberFieldRequired> requredList)
            {
                var guids = requredList.Where(p => p.MemberType == request.Item).Select(p=>p.Key).ToList();
                result.ForEach(x =>
                {
                    if (guids.Contains(x.Key))
                    {
                        x.IsSystemField = true;
                    }
                });
                return result;
            }
            try
            {
                var tabs = await (from memberTypeTab in uowMembership.MemberTypeTabRepository.TableAsNoTracking
                    join memberTabDefinition in uowMembership.MemberTabDefinitionRepository.TableAsNoTracking on
                        memberTypeTab.TabId equals memberTabDefinition.Id
                    where memberTypeTab.MemberType == request.Item && memberTypeTab.TenantId == request.TenantId &&
                          memberTabDefinition.IsVisible
                    select memberTabDefinition).ToListAsync();


                var systemFields = await uowMembership.MemberFieldRequiredRepository.TableAsNoTracking.ToListAsync();

                var tabids = tabs.Select(p => p.Id);

                var fields = await (from memberTabField in uowMembership.MemberTabFieldsRepository.TableAsNoTracking
                    join memberFieldDefinition in uowMembership.MemberFieldDefinitionRepository.TableAsNoTracking on
                        memberTabField.FieldId equals memberFieldDefinition.Id
                    where tabids.Contains(memberTabField.TabId)
                    select new
                    {
                        memberFieldDefinition,
                        tabid = memberTabField.TabId
                    }).ToListAsync();

                var result = new List<MemberTypeTabsViewReslut>();
                foreach (var item in tabs)
                {
                    var field = fields.Where(p => p.tabid == item.Id).Select(p => p.memberFieldDefinition)
                        .MapListObject<MemberFieldDefinition, TabsFieldsViewResult>().ToList();
                    field = checkIsSystemAttributes(field, systemFields);
                    result.Add(new MemberTypeTabsViewReslut
                    {
                        Key = item.Key,
                        DisplayName = item.DisplayName,
                        Order = item.Order,
                        TabsFields = field
                    });
                }

                return new MemberTypeTabFieldsRenderViewResult(request.Item, result);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
