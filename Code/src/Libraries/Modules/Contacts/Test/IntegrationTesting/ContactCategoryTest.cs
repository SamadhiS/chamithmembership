﻿using System;
using System.Threading.Tasks;
using Inx.Module.Contacts.Core.Bo.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Inx.Module.Contacts.Test.IntegrationTesting
{
    [TestClass]
    public class ContactCategoryTest : CoreTestBase, ICoreTestBase
    {
        private IContactCategoryService service;
        [TestInitialize]
        public void Initialize()
        {
            base.MockCache<PageList<ContactCategoryBo>>();
            service = new ContactCategoryService(new Comman().IUnitOfWokContacts, null);
        }
        [DataTestMethod]
        [DataRow("test category")]
        [DataRow("123123")]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Create(string one)
        {
            try
            {
                var mock = new Mock<IMemoryCacheManager>();
                mock.Setup(foo => foo.Remove(It.IsAny<string>())).Returns(Task.FromResult(true));
                var bo = new ContactCategoryBo
                {
                    Name = one
                };
                await base.Create(bo, service);
                True("ContactCategoryTest=>Create");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(100)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<ContactCategoryBo, IContactCategoryService>(Convert.ToInt32(id), service);
                True("ContactCategoryTest=>Delete");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Read(int skip,int take,string search)
        {
            try
            {
                await base.Read<ContactCategoryBo, IContactCategoryService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("ContactCategoryTest=>Read");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Update()
        {
            try
            {
                await base.Update(new ContactCategoryBo()
                {
                }, service);
                True("ContactCategoryTest=>Update");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Task Search(int skip, int take, string search)
        {
            throw new NotImplementedException();
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(1000)]
        public async Task ReadById(int id)
        {
            try
            {
                await service.Read(TestRequest(id));
                True("ContactCategoryTest=>ReadById");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [DataTestMethod]
        [DataRow(1)]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<ContactCategoryBo, IContactCategoryService>(service);
                True("ContactCategoryTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        

        #region NotImplementedException

        public Task Create()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
