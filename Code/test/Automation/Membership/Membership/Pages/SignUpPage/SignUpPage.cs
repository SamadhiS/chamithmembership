﻿using Protractor;
using System;
using Membership.PageObjects;
using Membership.PageObjects.SignUpPO;

namespace Membership.Pages.SignUpPage
{
    class SignUpPage
    {
        private NgWebDriver SignUpPageDriver;
        private SignUpPO signUpPo = new SignUpPO();

        public SignUpPage(NgWebDriver ngWebDriver)
        {
            this.SignUpPageDriver = ngWebDriver;
        }

        public void SignUpPageLoad(NgWebDriver d)
        {
            SignUpPageDriver.IgnoreSynchronization = true;
            this.SignUpPageDriver = d;
            SignUpPageDriver.Manage().Window.Maximize();
            SignUpPageDriver.Url = "http://membershipinexis.azurewebsites.net/#/signup";
        }

        //public void login(string un, string pw)
        //{
        //    loginPo.UName(LoginPageDriver).SendKeys(un);
        //    loginPo.Pwd(LoginPageDriver).SendKeys(pw);

        //}
        internal void LoginPageLoad(object ngWebDriver)
        {
            throw new NotImplementedException();
        }
    }
}
