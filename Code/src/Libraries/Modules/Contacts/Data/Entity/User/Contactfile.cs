﻿using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inx.Module.Contacts.Data.Entity.User
{
    [Table("Contactfiles", Schema = Constraints.Schema)]
    public class Contactfile : AuditableEntity
    {
        [NumberNotZero]
        public int ContactId { get; set; }
        [StringLength(100)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string FileSaveName { get; set; }
        public decimal Size { get; set; }
        [Obsolete, NotMapped]
        public new string Name { get; set; }
        #region relations
        [ForeignKey("ContactId")]
        public PersonInfo ContactInfo { get; set; }
        #endregion
    }
}
