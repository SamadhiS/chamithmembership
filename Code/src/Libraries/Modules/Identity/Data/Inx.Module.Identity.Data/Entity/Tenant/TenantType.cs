﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models;

namespace Inx.Module.Identity.Data.Entity.Tenant
{
    [Table("TenantTypes", Schema = Constraints.Schema)]
    public class TenantType: BaseEntity
    {
        [Obsolete,NotMapped]
        public new int TenantId { get; set; }
    }
}
