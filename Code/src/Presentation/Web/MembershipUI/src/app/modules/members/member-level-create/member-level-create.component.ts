import { LevelService } from './../../core/services/level.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { MembershipLevelStates } from '../../core/enums/membershipLevelState';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MemberLevelModel } from '../models/memberLevelModel';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LookupService } from '../../core/services/lookup.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MembersService } from '../../core/services/members.service';
import { FormBuilder, Validators, FormGroup, AbstractControl, FormControl } from '@angular/forms';
import { Component, OnInit, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-member-level-create',
  templateUrl: './member-level-create.component.html',
  styleUrls: ['./member-level-create.component.scss']
})
export class MemberLevelCreateComponent implements OnInit {

  isMemberLevelFormSubmitted = false;
  limitDisable: boolean;
  limitValueDesiable = 1;
  limitValueEnable = 2;
  membershipExpiredValue = 0;
  memberLevelForm: FormGroup;
  memberLevel = new MemberLevelModel();
  membershipLevelState = MembershipLevelStates;
  isBlocked = false;
  @Output() memberLevelSaving = new EventEmitter();

  constructor(private fb: FormBuilder, private levelService: LevelService,
    private lookUpService: LookupService,
    private router: Router,
    translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private blockUiService: BlockUiService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.limitDisable = true;
    this.getMemberLevelForm();
  }

  getMemberLevelForm() {
    this.isMemberLevelFormSubmitted = false;
    let priceregx = /^\d+(\.\d{1,2})?$/;
    let numbereregx = /^\d{10}$/;
    this.memberLevelForm = this.fb.group({
      name: ["", [Validators.required, Validators.maxLength(50)]],
      price: ["0", [Validators.required, patternValidator(priceregx)]],
      description: [" ", [Validators.maxLength(500)]],
      membershipExpired: new FormControl('1'),
      capacity: ["1", [Validators.required]],
      isLimited: [" "],
      Limited: new FormControl('2'),
      subscriptionTypeId: new FormControl('1'),
      subscriptionTo: [new Date(), ""],
      subscriptionFrom: [new Date(), ""],
    }, { validator: this.customValidate }, )
  }

  customValidate(c: AbstractControl) {
    if (c.get('subscriptionTypeId').value == 1) {
      return false;
    }
    else return true;
  }

  dateRange(event) {
    if (event.currentTarget.defaultValue == this.limitValueDesiable) {
      this.limitDisable = false;
    }

    if (event.currentTarget.defaultValue == this.limitValueEnable) {
      this.limitDisable = true;
    }
  }

  addDateRange() {
    if (this.memberLevelForm.value.membershipExpired == this.membershipLevelState.Annualy) {
      this.memberLevelForm.value.subscriptionTo = moment();
      this.memberLevelForm.value.subscriptionTo.add(1, 'years');
    }
    else if (this.memberLevelForm.value.membershipExpired == this.membershipLevelState.Monthly) {
      this.memberLevelForm.value.subscriptionTo = moment();
      this.memberLevelForm.value.subscriptionTo.add(1, 'months');
    }
    else if (this.memberLevelForm.value.membershipExpired == this.membershipLevelState.Weekly) {
      this.memberLevelForm.value.subscriptionTo = moment();
      this.memberLevelForm.value.subscriptionTo.add(7, 'days');
    }
    else if (this.memberLevelForm.value.membershipExpired == this.membershipLevelState.Daily) {
      this.memberLevelForm.value.subscriptionTo = moment();
      this.memberLevelForm.value.subscriptionTo.add(1, 'days');
    }
    else if (this.memberLevelForm.value.membershipExpired == this.membershipLevelState.LifeTime) {
      this.memberLevelForm.value.subscriptionTo = moment();
      this.memberLevelForm.value.subscriptionTo.add(100, 'years');
    }

  }
  save() {
    this.isMemberLevelFormSubmitted = true;
    if(!this.memberLevelForm.valid) return;
    var name = this.memberLevelForm.value.name.trim();
    if (name == "") {
      this.toastr.error('Membership Level Name Cannot be Empty', 'Oops');
    }
    else {
      if (!this.memberLevelForm.valid) return;
      this.isBlocked = true;
      if (this.limitDisable == false) {
        this.memberLevelForm.value.membershipExpired = this.membershipExpiredValue;
      }
      else {
        this.addDateRange();
      }
      this.memberLevelForm.value.isLimited = this.limitDisable;
      var memberLvl = Object.assign({}, this.memberLevel, this.memberLevelForm.value)
      if (this.memberLevelForm.value.subscriptionTypeId == this.limitValueEnable) {
        memberLvl.capacity = this.membershipExpiredValue;
        memberLvl.isLimited = false;

      }
      else {
        memberLvl.isLimited = true;
      }
      this.levelService.addMemberLevel(memberLvl)
        .subscribe(
          memberLvl => {
            this.toastr.success(ToasterMessages.save('Member Level'), toasterHeadings.Success);
            this.isBlocked = false;
            this.getMemberLevelForm();
            this.memberLevelSaving.emit();
          }, error => {
            this.toastr.error(ToasterMessages.alreadyExistError('Member Level'), toasterHeadings.Error);
            this.isBlocked = false;
            this.getMemberLevelForm();
          });       
    }
    
  }

  keyPress(event: any) {
    const pattern = /[0-9\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
