﻿using Inx.Module.Identity.Data.Entity.Application;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Payment.Data.Entity
{
    [Table("PaymentMaster", Schema = Constraints.Schema)]
    public class PaymentMaster : AuditableEntity
    {
        public int MemberSubscriptionId { get; set; }
        public int MemberId { get; set; }
        public decimal Amount { get; set; }
        public decimal Remaining { get; set; }
        public PaymentState PaymentState { get; set; }
        public TransActionType TransActionType { get; set; }
        public string Description { get; set; }

        public ICollection<PaymentInfo> PaymentInfoes { get; set; }

        public PaymentMaster()
        {
            PaymentInfoes = new List<PaymentInfo>();
        }
    }
}
