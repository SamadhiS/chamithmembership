﻿using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Service.Models.Utility
{
   public  class MemberActiveGraphViewModel
    {
        public List<DateTime> MemberSubscriptionDate { get; set; }
        public List<int> ActiveMembers { get; set; }
        public MemberActiveGraphViewModel()
        {
            MemberSubscriptionDate = new List<DateTime>();
            ActiveMembers = new List<int>();

        }
    }
}
