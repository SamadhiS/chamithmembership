﻿using Inx.Core.Base;
using Inx.Module.Payment.Core.Bo;
using Inx.Utility.Models;
using System.Threading.Tasks;

namespace Inx.Module.Payment.Core.Services.Interfaces
{
    public interface IPaymentService : IBaseService<PaymentBo>
    {
        Task<PaymentBo> ReadMemberPayment(Request<int> req);
    }
}
