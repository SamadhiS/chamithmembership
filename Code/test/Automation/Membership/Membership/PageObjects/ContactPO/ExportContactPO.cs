﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.ContactPO
{
    class ExportContactPO
    {
        public void ContactExport(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnExport"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public NgWebElement ExportPdf(NgWebDriver d)
        {
            return d.FindElement(By.CssSelector("body > membership-root > membership-layout > div > div > div > membership-contact-details > div > div > div > div > section > div.panel.panel-default.m-b-0.bottom-round > div > div > div.col-md-5 > div > div.btn-group.open > ul > li:nth-child(1) > a"));
        }

        
        public NgWebElement ExportCsv(NgWebDriver d)
        {
            return d.FindElement(By.CssSelector("body > membership-root > membership-layout > div > div > div > membership-contact-details > div > div > div > div > section > div.panel.panel-default.m-b-0.bottom-round > div > div > div.col-md-5 > div > div.btn-group.open > ul > li:nth-child(2) > a"));
        }

      
        public void GenerateReport(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnGenerateReport"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }

        public void ProfileImage(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[1]")).FindElement(By.Id("chkColumns"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        public void CreatedOn(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Name("created On"));
            ScrollToElem(elem, d);
            elem.Click();
            Thread.Sleep(2000);
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }

        public void CheckToggleStatusProfileImage(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[1]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                 toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
               // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
               // Thread.Sleep(2500);
            }

        }

        public void CheckToggleStatusCreatedOn(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[2]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }

        public void CheckToggleStatusIdentity(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[4]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }

        public void CheckToggleStatusDob(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[3]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }

        public void CheckToggleStatusAddress(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[8]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }
        public void CheckToggleStatusCity(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[9]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }
        public void CheckToggleStatusCompany(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[12]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }
        public void CheckToggleStatusDesignation(NgWebDriver d)
        {
            bool status = true;
            NgWebElement toggle = d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-contact-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[13]")).FindElement(By.Id("chkColumns"));

            if (status.Equals(toggle.Selected))
            {
                toggle.Click();
                Thread.Sleep(2500);
                //return;
            }
            else
            {
                // toggle.Click();
                //Thread.Sleep(2500);
                return;
                //toggle.Click();
                // Thread.Sleep(2500);
            }

        }
        public void selectColumn(NgWebDriver d, string u)
        {
            for (int n = 1; n <= 18; n++)
            {
                if (d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-member-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[" + n + "]/span")).Text == u)
                {
                    this.CheckToggleStatusProfileImage(d);
                    break;
                    // d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-member-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li["+n+"]")).Click();

                }
            }
            //d.FindElement(By.XPath("/html/body/membership-root/membership-layout/div/div/div/membership-member-details/div/div/membership-select-report-columns/div/div/div/div[2]/div/div/div/ul/li[1]")).Click();
        }
    }
}
