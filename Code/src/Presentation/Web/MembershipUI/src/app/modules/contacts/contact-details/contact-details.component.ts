import { TableTemplateService } from '../../core/services/table-template.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { SortBaseService } from '../../core/services/sort-base.service';
import { ToastsManager } from 'ng2-toastr';
import { ContactAllDataModel } from '../models/contactAllDataModel';
import { Categories } from '../../settings/models/categoryModel';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { LookupService } from '../../core/services/lookup.service';
import { Subject } from 'rxjs/Subject';
import { ContactSearchModel } from '../models/contactSearchModel';
import { PaginationModel } from '../../core/models/paginationModel';
import { TableTemplate } from '../../core/enums/tableTemplate';
import { ContactsService } from '../../core/services/contacts.service';


@Component({
  selector: 'membership-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {

  reportFormats = ['PDF', 'CSV'];
  category = "Category";
  searchTerm$ = new Subject<string>();
  contactSearch = new ContactSearchModel();
  paginationModel = new PaginationModel(20, '', false);
  contactDetails = [];
  categories = []
  contactCategoryIds = [];
  ContactCount = 0;
  Isedit = true;
  catogeryId: number;
  categoryIds = [];
  contactAllColumns = [];
  selectedContactColumns = [];
  isBlocked = false;
  tableTemplateType = TableTemplate.ContactTableTemplate;
  maxSize=10;

  constructor(private router: Router, private contactService: ContactsService,
    private sortBaseService: SortBaseService,
    private tableTemplateService: TableTemplateService,
    private blockUiService: BlockUiService,
    public toastr: ToastsManager, vcr: ViewContainerRef, private lookUpService: LookupService) {
    this.toastr.setRootViewContainerRef(vcr);

    this.searchTerm$.debounceTime(300)
      .distinctUntilChanged().subscribe(data => {
        if (data != "" && data) {
          this.contactSearch.contactCategoryTypes = this.contactCategoryIds;
          this.contactSearch.search = data;
        }
        else {
          this.contactSearch.search = '';
        }
        this.getContacts();
      })
  }

  ngOnInit() {
    this.tableTemplateService.init(TableTemplate.ContactTableTemplate)
    this.getContactCategories();

  }

  getContacts() {
    this.isBlocked = true;
    this.contactService.getContacts(this.paginationModel.skip, this.paginationModel.take, JSON.stringify(this.contactSearch), this.paginationModel.orderByTerm)
      .subscribe(allContacts => {
        this.getContactTemplate();
        this.contactDetails = allContacts.result.item.items;
        this.paginationModel.totalRecords = allContacts.result.item.totalRecodeCount;
        this.ContactCount = allContacts.result.item.totalRecodeCount;
      },
      error => {
        this.isBlocked = false;
        this.toastr.error('Error in Loading Contact', 'Oops');
      }, () => {
        this.isBlocked = false;
      });
  }

  goToNewContact() {
    this.router.navigate(["/contacts/createContact"])
  }

  getContactTemplate() {
    this.tableTemplateService.getSavedTemplateColumns(TableTemplate.ContactTableTemplate).subscribe(data => {
      this.selectedContactColumns = data.result.item;

    },
      error => { })
  }

  showColumn(name) {
    return this.selectedContactColumns.indexOf(name) > -1
  }
  showColumns(event){
    this.selectedContactColumns=event
  }

  editContact(id) {
    this.router.navigate(["/contacts/createContact", id])
  }

  getContactCategories() {
    this.lookUpService.getContactTypes().subscribe(res => {
      this.categories = res.result.item;
      this.contactCategoryIds = this.categories.map(item => item.value)
      this.contactSearch.contactCategoryTypes = this.contactCategoryIds;
      this.getContacts();
    },
      error => { })
  }

  filterByCategory(id,name) {
    this.category = name;
    this.contactSearch.contactCategoryTypes = [];
    this.contactSearch.contactCategoryTypes.push(id)
    this.getContacts();
  }

  resetFilter(){
    this.category = "Category"
    this.contactSearch.contactCategoryTypes = this.contactCategoryIds;
    this.getContacts();
  }

  public pageChanged(event: any): void {
    this.paginationModel.skip = (event.page - 1) * this.paginationModel.take;
    this.paginationModel.currentPage = event.page;
    this.getContacts();
  }

  changeSorting(columnName): void {

    this.sortBaseService.changeSorting(columnName, this.paginationModel)
    this.getContacts();
  }

  selectedClass(columnName): string {
    return this.sortBaseService.selectedClass(columnName, this.paginationModel)
  }

}

