﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models.Comman.User;
using Inx.Data.Base.Utility;
using Inx.Utility.Attributes;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Contacts.Data.Entity.User
{
    [Table("ContactInfo", Schema = Constraints.Schema)]
    public class ContactInfo : AuditableEntity, IContactInfo
    {
        [NumberNotZero] public int PersonId { get; set; }
        [NumberNotZero] public int Country { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string City { get; set; }
        [StringLength(DbConstraints.NameLength)]
        public string Zip { get; set; }
        [Required, StringLength(DbConstraints.AddressLength)]
        public string Address { get; set; }
        [Required, EmailAddress, StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [Required, StringLength(DbConstraints.PhoneLength)]
        public string Phone { get; set; }
        [StringLength(DbConstraints.PhoneLength)]
        public string Telephone { get; set; }
        #region relations 
        [ForeignKey("PersonId")] public PersonInfo PersonInfo { get; set; }
        #endregion
        [NotMapped, Obsolete] public new string Name { get; set; }
      
    }
}
