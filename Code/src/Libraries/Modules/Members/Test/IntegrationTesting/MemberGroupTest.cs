﻿
using Inx.Module.Membership.Core.Bo.Groups;
using Inx.Module.Membership.Core.Service.Members;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Test.Utility;
using Inx.Test.Base.Core;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Test.IntegrationTesting
{
    [TestClass]
    public class MemberGroupTest : CoreTestBase, ICoreTestBase
    {
        private IMemberGroupService service;

        public object It { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            base.MockCache<PageList<MemberGroupBo>>();
            service = new MemberGroupService(new Comman().IUnitOfWokMembership, null);
        }


        [DataTestMethod]
        [DataRow("1,2,4","1")]//pass
        [DataRow("1", "1")]//pass
        [DataRow("test", "1")]//fail
        [DataRow("1,2,4", "test")]//pass
        [DataRow("test", "test")]//pass
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Create(List<int> memberIds,int groupId)
        {
            try
            {
                var bo = new MemberGroupBo
                {
                   MemberIds= memberIds,
                   GroupId = groupId,
                 
                };
                await base.Create(bo, service);
                True("MemberGroupTest=>Create");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(100)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Delete(int id)
        {
            try
            {
                await base.Delete<MemberGroupBo, IMemberGroupService>(Convert.ToInt32(id), service);
                True("MemberGroupTest=>Delete");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(0, 25, "searchTerm")]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Read(int skip, int take, string search)
        {
            try
            {
                await base.Read<MemberGroupBo, IMemberGroupService>(
                    base.TestSearchRequest(skip: skip, take: take
                        , searchTerm: search, fk: 0, orderByTerm: string.Empty), service);
                True("MemberGroupTest=>Read");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task Update()
        {
            try
            {
                await base.Update(new MemberGroupBo()
                {
                }, service);
                True("MemberGroupTest=>Update");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        public async Task ReadKeyValue()
        {
            try
            {
                await base.ReadKeyValue<MemberGroupBo, IMemberGroupService>(service);
                True("MemberGroupTest=>ReadKeyValue");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Task ReadById(int id)
        {
            throw new NotImplementedException();
        }

        public new Task Create()
        {
            throw new NotImplementedException();
        }

        public Task Search(int skip, int take, string search)
        {
            throw new NotImplementedException();
        }
    }
}
