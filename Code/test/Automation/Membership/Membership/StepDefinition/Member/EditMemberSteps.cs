﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;
using Membership.PageObjects.MemberPO;
using System.Windows.Forms;
using System;
using System.Diagnostics;


namespace Membership.StepDefinition.Member
{
    [Binding]
    public sealed class EditMemberSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private LoginPO loginPO = new LoginPO();
        private NavPage navPage = new NavPage();
        private CategoryPO categoryPO = new CategoryPO();
        private MemberPO memberPO = new MemberPO();

        [Given(@"Membership Home page")]
        public void GivenMembershipHomePage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Then(@"User entered valid credential ""(.*)"" and ""(.*)""")]
        public void ThenUserEnteredValidCredentialAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Then(@"I Click on login")]
        public void ThenIClickOnLogin()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"I navigate to dashboard")]
        public void ThenINavigateToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Then(@"I direct to Members Section")]
        public void ThenIDirectToMembersSection()
        {
            navPage.Btn_Member_click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Then(@"Select a member and clicked on edit button")]
        public void ThenSelectAMemberAndClickedOnEditButton()
        {
            memberPO.Btn_Edit_Click(ngWebDriver);
            Thread.Sleep(3000);
        }
        [Then(@"Enter First name as (.*)")]
        public void ThenEnterFirstNameAsSanaya(string p0)
        {
            memberPO.Firstname(ngWebDriver).Clear();
            memberPO.Firstname(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enter Last name as (.*)")]
        public void ThenEnterLastNameAsRochell(string p0)
        {
            memberPO.Firstname(ngWebDriver).Clear();
            memberPO.Lastname(ngWebDriver).SendKeys(p0);
        }


        [Then(@"I select profile picture")]
        public void ThenISelectProfilePicture()
        {
            memberPO.Btn_ProfileImage_Click(ngWebDriver);
            Thread.Sleep(2000);
            SendKeys.SendWait(@"E:\New folder\D.jpg");
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(3000);
        }

        [Then(@"Entered DOB  as (.*)")]
        public void ThenEnteredDOBAs(string p0)
        {
            memberPO.DOB(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Select martial Status")]
        public void ThenSelectMartialStatus()
        {
            memberPO.Click_Status(ngWebDriver).Click();
            Thread.Sleep(3000);
            memberPO.MartialStatus(ngWebDriver).Click();
        }

        [Then(@"Enter NIC as (.*)")]
        public void ThenEnterNICAsV(string p0)
        {
            memberPO.NIC(ngWebDriver).SendKeys(p0);
        }

        [Then(@"I Clicks on the NEXT btn")]
        public void ThenIClicksOnTheNEXTBtn()
        {
            memberPO.Btn_NextPersonal_Click(ngWebDriver);
            Thread.Sleep(2000);
        }

        [Then(@"Select a Country")]
        public void ThenSelectACountry()
        {
            memberPO.Country(ngWebDriver).Click();
        }
        [Then(@"Enter City as (.*)")]
        public void ThenEnterCityAsAuckland(string p0)
        {
            memberPO.City(ngWebDriver).SendKeys(p0);
            Thread.Sleep(2000);
        }

       
        [Then(@"Enters Postal as (.*)")]
        public void ThenEntersPostalAs(string p0)
        {
            memberPO.Postal(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Address as (.*)")]
        public void ThenEntersAddressAsAucklandNewZealand(string p0)
        {
            memberPO.Address(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Mob as (.*)")]
        public void ThenEntersMobAs(string p0)
        {
            memberPO.Mobile(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Telephone as (.*)")]
        public void ThenEntersTelephoneAs(string p0)
        {
            memberPO.Telephone(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Email as (.*)")]
        public void ThenEntersEmailAsKrishniHotmail_Com(string p0)
        {
            memberPO.Email(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Clicked on NEXT button")]
        public void ThenClickedOnNEXTButton()
        {
            memberPO.Btn_ContactNextButton_Click(ngWebDriver);
        }

        [Then(@"Enters Designation as (.*)")]
        public void ThenEntersDesignationAsHRIntern(string p0)
        {
            memberPO.Designation(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Company as (.*)")]
        public void ThenEntersCompanyAsMoreOctaneFitness(string p0)
        {
            memberPO.Company(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Entered Company address as (.*)")]
        public void ThenEnteredCompanyAddressAsPostmanSuiteNewYork(string p0)
        {
            memberPO.CompanyAddress(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Entered Ofc Telephone as (.*)")]
        public void ThenEnteredOfcTelephoneAs(string p0)
        {
            memberPO.TeleWork(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Enters Ofc Mobile as (.*)")]
        public void ThenEntersOfcMobileAs(string p0)
        {
            memberPO.MobileWork(ngWebDriver).SendKeys(p0);
        }

        [Then(@"Click Next Button")]
        public void ThenClickNextButton()
        {
            memberPO.Btn_WorkNextButton_Click(ngWebDriver);
        }

        [Then(@"Select Membership Level")]
        public void ThenSelectMembershipLevel()
        {
            memberPO.MemberLevel(ngWebDriver);
        }

        [Then(@"Click on Save btn")]
        public void ThenClickOnSaveBtn()
        {
            memberPO.MemberLevel(ngWebDriver);
        }

        [Then(@"Click on logout")]
        public void ThenClickOnLogout()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
