import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RenderViewService extends BaseService {

  private renderViewEndPoint = `${this.baseApiEndPoint}membership/v1/settings/`;
  private 

  constructor(private http: HttpClient) {
    super();
  }

  getRenderFieldData(memberType) {
    return this.http.get(`${this.renderViewEndPoint}membertype/${memberType}/wizardrenderinfo`)
      .map((response) => response)
      .catch(this.errorHandler)
  }
}
