﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Identity.Core.Bo;
using Inx.Module.Identity.Core.Bo.Tenant;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Module.Identity.Data.DbContext;
using Inx.Utility.Cache.Runtime;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Identity.Core.Services.Tenant
{
    public class TenantTypeService : BaseService, ITenantTypeService
    {
        private readonly IUnitOfWorkIdentity uow;
        private readonly IMemoryCacheManager cache;
        public TenantTypeService(IUnitOfWorkIdentity _uow, IMemoryCacheManager _cache)
        {
            uow = _uow;
            cache = _cache;
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search req)
        {
            try
            {
                var result = await cache.GetAsync<List<KeyValueListItem<int>>>(Enums.CacheKey.TenantTypes.ToString());
                return result.IsNull()
                    ? (await (from item in uow.TenantTypeRepository.TableAsNoTracking
                            select new { item.Id, item.Name }).AsQueryable()
                        .Select(p => new KeyValueListItem<int>
                        {
                            Value = p.Id,
                            Text = p.Name
                        }).ToListAsync())
                    .CacheSetAndGet(cache, Enums.CacheKey.TenantTypes.ToString())
                    : result;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
        private async Task RemoveCache()
        {
            await cache.Remove(Enums.CacheKey.TenantTypes.ToString());
        }
        #region not implemented

        public Task<TenantTypeBo> Create(Request<TenantTypeBo> req)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Request<int> req)
        {
            throw new NotImplementedException();
        }

        public Task<TenantTypeBo> Read(Request<int> req)
        {
            throw new NotImplementedException();
        }

        public Task Update(Request<TenantTypeBo> req)
        {
            throw new NotImplementedException();
        }

        public Task<PageList<TenantTypeBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }
 
        #endregion

    }
}
