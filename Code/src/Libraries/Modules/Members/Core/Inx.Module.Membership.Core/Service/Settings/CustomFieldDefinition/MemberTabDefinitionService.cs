﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition
{
    public class MemberTabDefinitionService : MemberBaseService, IMemberTabDefinitionService
    {
        private readonly IUnitOfWokMembership uowMembership;
        public MemberTabDefinitionService(IUnitOfWokMembership _uowMembership, ICoreInjector coreInjector):base(coreInjector)
        {
            uowMembership = _uowMembership;
        }
        public async Task<MemberTabDefinitionBo> Create(Request<MemberTabDefinitionBo> request)
        {

            try
            {
               return (await uowMembership.MemberTabDefinitionRepository.CreateAndSave(request.MapRequestObject<MemberTabDefinitionBo, MemberTabDefinition>()))
                   .MapObject<MemberTabDefinition, MemberTabDefinitionBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Delete(Request<int> request)
        {
            try
            {
                await uowMembership.MemberTabDefinitionRepository.DeletePermanentAndSave(request);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<MemberTabDefinitionBo>> Read(Search search)
        {
            try
            {
                var result = (await uowMembership.MemberTabDefinitionRepository.TableAsNoTracking.Where(p =>
                        p.TenantId == search.TenantId).ToListAsync())
                    .MapListObject<MemberTabDefinition, MemberTabDefinitionBo>();
                return new PageList<MemberTabDefinitionBo>(result, 0, 0, result.Count());
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberTabDefinitionBo> Read(Request<int> request)
        {
            try
            {
                return (await uowMembership.MemberTabDefinitionRepository.Read(request))
                  .MapObject<MemberTabDefinition, MemberTabDefinitionBo>();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<MemberTabDefinitionBo>> ReadDefault()
        {
            try
            {
                return (await uowMembership.MemberTabDefinitionRepository.TableAsNoTracking
                        .Where(p => p.TenantId == 0).ToListAsync())
                    .MapListObject<MemberTabDefinition, MemberTabDefinitionBo>().ToList();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search request)
        {
            try
            {
                return await uowMembership.MemberTabDefinitionRepository.ReadKeyValue<MemberTabDefinition>(request);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task Update(Request<MemberTabDefinitionBo> request)
        {
            try
            {
                await uowMembership.MemberTabDefinitionRepository.UpdateAndSave(
                    request.MapRequestObject<MemberTabDefinitionBo, MemberTabDefinition>());
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

    }
}
