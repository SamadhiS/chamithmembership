﻿using System.Collections.Generic;
using System.ComponentModel;
using Inx.Utility.Models;

namespace Inx.Module.Identity.Core.Bo.Security
{
    public class AuthResponse:BaseBo
    {
        public string ProfileImage { get; set; }
        [Description("username or email address")]
        public string Email { get; set; }
        public Inx.Utility.Utility.Enums.UserRecordStatus UserState { get; set; }
        public List<string> Roles { get; set; }

        public AuthResponse()
        {
            Roles = new List<string>();
            Name = "Admin";
            ProfileImage = "no.jpg";
        }
    }
}
