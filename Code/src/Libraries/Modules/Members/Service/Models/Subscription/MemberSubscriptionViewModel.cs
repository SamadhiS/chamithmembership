﻿using Inx.Utility.Utility;
using System;

namespace Inx.Module.Membership.Service.Models.Subscription
{
    public class MemberSubscriptionViewModel
    {
        public int MemberId { get; set; }
        public int SubscriptionTypeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime RenewDate { get; set; } = DateTime.UtcNow;
        public Enums.PaymentState PaymentState { get; set; } = Enums.PaymentState.Pending;
    }
}
