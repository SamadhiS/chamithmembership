﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Utility.Extentions;
using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition
{
    public class MemberTabFieldsSetupService:MemberBaseService, IMemberTabFieldsSetupService
    {
        private readonly IUnitOfWokMembership unitOfWokMembership;
        public MemberTabFieldsSetupService(IUnitOfWokMembership _unitOfWokMembership)
        {
            unitOfWokMembership = _unitOfWokMembership;
        }

        public async Task Seed(Request<MemberTabFieldsSetupBo> request)
        {
            using (var transaction = await unitOfWokMembership.Context.Database.BeginTransactionAsync())
            {
                try
                {
                    var tabs = await SeedTabs(Request(request.Item.MemberTabDefinition, request.TenantId,
                        request.UserId));
                    var fields = await SeedFields(Request(request.Item.MemberFieldDefinition, request.TenantId,
                        request.UserId));

                    var tabfields = request.Item.MemberTabField;
                    foreach (var item in tabs)
                    {
                        tabfields.Where(p => p.TabId == item["sysid"]).ToList().ForEach(p => p.TabId = item["newid"]);
                    }
                    foreach (var item in fields)
                    {
                        tabfields.Where(p => p.FieldId == item["sysid"]).ToList().ForEach(p => p.FieldId = item["newid"]);
                    }
                    await SeedTabFields(Request(tabfields, request.TenantId, request.UserId));

                    var memberTypeFields = request.Item.MemberTypeTab;
                    foreach (var item in tabs)
                    {
                        memberTypeFields.Where(p => p.TabId == item["sysid"]).ToList().ForEach(p => p.TabId = item["newid"]);
                    }

                    await SeedMemberTypeTab(Request(memberTypeFields, request.TenantId, request.UserId));
                    await unitOfWokMembership.SaveAsync();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e.HandleException();
                }
            }
        }

        async Task<List<Dictionary<string,int>>> SeedTabs(Request<List<MemberTabDefinitionBo>> request)
        {
            try
            {
                var oldnewKeyValue = new List<Dictionary<string,int>>();
                foreach (var item in request.Item)
                {
                    oldnewKeyValue.Add(new Dictionary<string, int>()
                    {
                        {"sysid", item.Id},
                        {
                            "newid", (await unitOfWokMembership.MemberTabDefinitionRepository.CreateAndSave(
                                Request
                                (item.MapObject<MemberTabDefinitionBo, MemberTabDefinition>(), request.TenantId,
                                    request.UserId))).Id
                        }
                    });
                }
                return oldnewKeyValue;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        async Task<List<Dictionary<string, int>>> SeedFields(Request<List<MemberFieldDefinitionBo>> request)
        {
            try
            {
                var oldnewKeyValue = new List<Dictionary<string, int>>();
                foreach (var item in request.Item)
                {
                    oldnewKeyValue.Add(new Dictionary<string, int>()
                    {
                        {"sysid", item.Id},
                        {
                            "newid", (await unitOfWokMembership.MemberFieldDefinitionRepository.CreateAndSave(
                                Request
                                (item.MapObject<MemberFieldDefinitionBo, MemberFieldDefinition>(), request.TenantId,
                                    request.UserId))).Id
                        }
                    });
                }
                return oldnewKeyValue;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        async Task SeedTabFields(Request<List<MemberTabFieldBo>> request)
        {
            try
            {
                foreach (var item in request.Item)
                {
                  await unitOfWokMembership.MemberTabFieldsRepository.Create(
                        Request
                        (item.MapObject<MemberTabFieldBo, MemberTabField>(), request.TenantId,
                            request.UserId));
                }
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        async Task SeedMemberTypeTab(Request<List<MemberTypeTabBo>> request)
        {
            try
            {
                foreach (var item in request.Item)
                {
                    await unitOfWokMembership.MemberTypeTabRepository.Create(
                        Request
                        (item.MapObject<MemberTypeTabBo, MemberTypeTab>(), request.TenantId,
                            request.UserId));
                }
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
