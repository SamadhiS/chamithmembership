import { OrganizationCreateComponent } from './organization-create/organization-create.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: OrganizationCreateComponent },
  { path: 'createOrganization', component: OrganizationCreateComponent },
  { path: 'updateOrganization/:id', component: OrganizationCreateComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { } 
