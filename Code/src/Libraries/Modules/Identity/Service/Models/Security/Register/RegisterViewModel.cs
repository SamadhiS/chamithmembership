﻿using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;

namespace Inx.Module.Identity.Service.Models.Security.Register
{
    public class RegisterViewModel
    {
        [EmailAddress, Required,StringLength(DbConstraints.EmailLength)]
        public string Email { get; set; }
        [Required,StringLength(DbConstraints.NameLength)]
        public string Password { get; set; }
    }
}
