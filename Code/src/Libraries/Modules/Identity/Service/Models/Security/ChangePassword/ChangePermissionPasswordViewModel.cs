﻿using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Inx.Module.Identity.Service.Models.Security.ChangePassword
{
    public class ChangePermissionPasswordViewModel : BaseViewModel
    {
        [Required, StringLength(DbConstraints.NameLength)]
        public string Password { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
