﻿using System;

namespace Inx.Module.Payment.Core.Bo
{
    public class PaymentSearchBo
    {
        public string Search { get; set; }
        public DateTimeOffset FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
