﻿using Inx.Service.Base.Models;
using System.ComponentModel.DataAnnotations;

namespace Inx.Module.Membership.Service.Models.CustomFieldDefinition
{
    public class MemberTabFieldDefinitionViewModel : BaseViewModel
    {
        public int TabId { get; set; }
        [Required]
        public string Key { get; set; }
        public int DataType { get; set; }
        [Required]
        public string DisplayName { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
    }
}
