﻿using Inx.Utility.Attributes;

namespace Inx.Module.Membership.Core.Bo.Member
{
    public class MemberState
    {
        [NumberNotZero]
        public int MemberId { get; set; }
        [NumberNotZero]
        public Enums.RecordStatus State { get; set; }
    }
}
