﻿using System.Threading;
using System.Threading.Tasks;
using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Data.Entity.Tenant;
using Inx.Utility;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Identity.Data.DbContext
{
    public class IdentityDbContext : Microsoft.EntityFrameworkCore.DbContext, IIdentityDbSet
    {
        private readonly string connectionString;
        #region Security

        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Role> Roles { get; set; }

        #endregion

        #region Tenant
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<TenantType> TenantTypes { get; set; }
        public DbSet<TenantInfo> TenantInfo { get; set; }
        #endregion
        public IdentityDbContext()
        {
            connectionString = GlobleConfig.ConnectionString;
        }

        public IdentityDbContext(string _connectionstring)
        {
            connectionString = _connectionstring;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(c => new {c.Email}).IsUnique();
            modelBuilder.Entity<Tenant>()
                .HasIndex(c => new { c.Host }).IsUnique();
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
