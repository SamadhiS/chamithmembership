﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;
using Inx.Utility.Utility;
namespace Inx.Module.Membership.Service.Models.Subscription
{
    public class SubscriptionViewModel : BaseViewModel
    {
        public decimal Price { get; set; }
        public bool IsLimited { get; set; }
        public int Capacity { get; set; }
        public bool IsRenewable { get; set; }
        public bool IsSendEmail { get; set; }
        public int RenewalDays { get; set; }
        public int EmaillTemplateId { get; set; }
        public DateTime SubscriptionFrom { get; set; } = DateTime.UtcNow;
        public DateTime SubscriptionTo { get; set; } = DateTime.UtcNow.AddYears(1);
        [StringLength(500)] public string Description { get; set; }
        public int SubscriptionTypeId { get; set; }
        [StringLength(DbConstraints.NameLength)]
        public new string Name { get; set; }
        public Enums.MembershipExpired MembershipExpired { get; set; } = Enums.MembershipExpired.None;
    }
}
