﻿using Abp.AspNetCore;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Inx.Module.Messenger.Service.Utility.Config;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Inx.Module.Messenger.Service
{
    [DependsOn(typeof(AbpAspNetCoreModule))]
    public class MessengerServiceModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MessengerServiceModule(IHostingEnvironment env)
        {

        }

        public override void PreInitialize()
        {
            IocConfig.Register(IocManager);
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MessengerServiceModule).GetAssembly());
            Automapper.Register(Configuration);

        }
    }
}