import { Injectable } from '@angular/core';
import { CheckboxElement } from './elements/checkbox-element';
import { TextboxElement } from './elements/textbox-element';
import { DropdownElement } from './elements/dropdown-element';
import { TextAreaElement } from './elements/textarea-element';
import { CalendarElement } from './elements/calendar-element';
import { RadioElement } from './elements/radio-element';
import { FileElement } from './elements/file-element';

@Injectable()
export class ElementInitialisationService {
 
  constructor() { }

  textElement(fields) {
    return new TextboxElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value
    });
  }

  checkboxElement(fields) {
    return new CheckboxElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      type: 'checkbox',
      value: fields.value
    });
  }

  dropdownElement(fields) {
    return new DropdownElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      options: fields.value,
      value: fields.value
    });
  }

  textareaElement(fields) {
    return new TextAreaElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value
    });
  }

  calendarElement(fields) {
    return new CalendarElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value,
      type: 'Date'
    });
  }
  
  radioElement(fields){
    return new RadioElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      options: fields.value,
      value: fields.value
    });
  }

  fileElement(fields): any {
    return new FileElement({
      key: fields.key,
      label: fields.displayName,
      required: fields.isRequired,
      order: fields.order,
      value: fields.value,
      type: 'File'
    });
  }

}
