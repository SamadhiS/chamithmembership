﻿Feature: As an Admin, I should be able to edit member


@mytag
Scenario Outline: As an Admin, I should be able to edit member
	Given Membership Home page
	Then User entered valid credential "membership.inexis@gmail.com" and "Admin@123"
	And I Click on login
	And I navigate to dashboard 
	And I direct to Members Section
	And Select a member and clicked on edit button
	And Enter First name as <_FN>
	And Enter Last name as <_LN>
	And I select profile picture
	And Entered DOB  as <_DOB>
	And Select martial Status
	And Enter NIC as <_NIC>
	And I Clicks on the NEXT btn
	And Select a Country
	And Enter City as <_City>
	And Enters Postal as <_Postal>
	And Enters Address as <_Address>
	And Enters Mob as <Mob>
	And Enters Telephone as <_Tele>
	And Enters Email as <_Email>
	And Clicked on NEXT button 
	And Enters Designation as <_desig>
	And Enters Company as <_Cmpy>
	And Entered Company address as <C_add>
	And Entered Ofc Telephone as <_TeleAdd>
	And Enters Ofc Mobile as <_TeleMob>
	And Click Next Button
	And Select Membership Level
	And Click on Save btn
	And Click on logout
Examples: 
| _FN   | _LN		| _DOB      | _NIC			| _Country	| _City			| _Postal	| _Address        | Mob			| _Tele     | _Email            | _desig         | _Cmpy				| C_add                      | _TeleAdd  | _TeleMob   |
| Sanaya | Rochell	| 1994-2-4	| 944333329V	| New Zeland     | Auckland	| 2582   | 25, Auckland, New Zealand| 	2525444444	| 3233333322 | krishni@hotmail.com | HR Intern | More Octane Fitness |25 Postman Suite, New York | 0827338125 | 0266735292	|

#Scenario Outline: As an Admin, I should not be able to add a new member
#	Given The membership portal
#	Then Entered the "membership.inexis@gmail.com" and "Admin@123"
#	And I Click on Submit button
#	And I navigated to Homepage page
#	And I directed to Member Section
#	And I Click on Add New Member
#	And I entered First name as <_FN>
#	And I entered Last name as <_LN>
#	And I selected profile Image
#	And I entered DOB  as <_DOB>
#	And I selected martial Status
#	And I have enter NIC as <_NIC>
#	And I Clicked on the NEXT button and display the error message
#	And I clicked on logout button
#	
#Examples: 
#| _FN     | _LN  | _DOB     | _NIC |
#| Rocheal | Stew | 1992-2-4 |      |
#
#Scenario Outline: Enter Invalid Contact Information
#	Given The Application Page load
#	Then User entered un & pw "membership.inexis@gmail.com" and "Admin@123"
#	And Clicks on Submit btn
#	And is navigated to Homepage page
#	And User navigates to Members Section
#	And I Click on Add New Member button
#	And I entered Firstname as <_FN>
#	And I entered Lastname as <_LN>
#	And User selected profile Image
#	And User entered DOB  as <_DOB>
#	And User selected martial Status
#	And User have enter NIC as <_NIC>
#	And User Clicked on the NEXT button
#	And I selected a Country
#	And User enter City as <_City>
#	And User enter Postal as <_Postal>
#	And User enter Address as <_Address>
#	And User enters Mob as <Mob>
#	And User enter Telephone as <_Tele>
#	And User enter Email as <_Email>
#	And I Clicked on NEXT button and display error msg
#	And User clicked on logout button
#
#	Examples: 
#	| _FN      | _LN      | _DOB      | _NIC       | _Country | _City   | _Postal | _Address          | Mob     | _Tele     | _Email                |
#	| Liallian | Salvator | 1998-4-13 | 926723229V | Kuwait   | Jabriya | 9090    | 22, Bayan Streeet | 99631223 | 257243213 | salvatory@gmal. | 