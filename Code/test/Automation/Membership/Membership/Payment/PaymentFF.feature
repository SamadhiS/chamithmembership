﻿Feature: CreatePayment
As an Admin. I should be able to create new payments

@mytag
Scenario: Make a payment in Payment section for a pending status 
Given System application page is opened
And Enter input for Login "membership.inexis@gmail.com" and "Admin@123"
And click on login 
And Naviagate to Homepage 
And click on Payments 
And search the payment status "pending"   
And click on Settle
And Select Payment Type as Cash 
And Click on Pay Now 
And Success message appears
And Navigate back to Payment section
And Log out 


Scenario: Make payments in Payment section for a paid status
Given System application page is opened
And Enter input for Login "membership.inexis@gmail.com" and "Admin@123"
And click on login
And Naviagate to Homepage 
And click on Payments 
And search the payment status "paid"   
And there is no Settle Button 
And Navigate back to Payment section
And Log out 

Scenario: Confirm payments in Member section,Payment Tab 
Given System application page is opened
And Enter input for Login "membership.inexis@gmail.com" and "Admin@123"
And click on login 
And Naviagate to Homepage 
And Click on Members
And Filter by payment status pending 
And Click on the member View
And Go to Payment Tab 
And Click on view INvoice
And click on Settle
#And Select Payment Type as Cash 
And Click on Pay Now 
And Success message appears
And Navigate back to Payment section
And Log out
 And Log out 

Scenario: Confirm payments in Member section,Timeline Tab 
Given System application page is opened
And Enter input for Login "membership.inexis@gmail.com" and "Admin@123"
And click on login 
And Naviagate to Homepage 
And Click on Members
And Filter by payment status pending 
And Click on the member View
And Click on view INvoice
And click on Settle
#And Select Payment Type as Cash 
And Click on Pay Now 
And Success message appears
And Navigate back to Payment section
And Log out
 And Log out 