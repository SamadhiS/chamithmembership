import { EmailTemplateService } from '../../core/services/email-template.service';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'membership-email-template-type',
  templateUrl: './email-template-type.component.html',
  styleUrls: ['./email-template-type.component.scss']
})
export class EmailTemplateTypeComponent implements OnInit {

  @ViewChild('emailTemplateModal') public emailTemplateModal: ModalDirective;
  templateTypes = [];
  templateType: string;
  isBlocked = false;  
  constructor(private emailTemplateService: EmailTemplateService,private router:Router) 
  { }

  ngOnInit() {
    this.getTemplateTypes();
  }

  public showTemplateModal(): void {
    this.emailTemplateModal.show();

  }

  hideTemplateModal() {
    this.emailTemplateModal.hide();
  }

  selectTemplate() {
    this.router.navigate(["/communication/createTemplate",this.templateType])

  }

  getTemplateTypes() {
   this.isBlocked = true;
    this.emailTemplateService.getTemplateTypes().subscribe(data => {
      this.templateTypes = data.result.item;
      this.isBlocked = false;
    },
      error => {

      })
  }


}
