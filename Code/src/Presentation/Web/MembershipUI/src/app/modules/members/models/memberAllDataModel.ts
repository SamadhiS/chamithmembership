import { WorkModel } from './workModel';
import { MemberModel } from './memberModel';
import { ContactModel } from './contactModel';

export class MemberAllDataModel {
    contactInfo: ContactModel;
    memberInfo: MemberModel;
    workingInfo: WorkModel;
    subscription: number;
    groups : any;
    id: number;
}