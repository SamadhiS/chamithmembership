﻿using Inx.Module.Identity.Data.Entity.Security;
using Inx.Module.Identity.Data.Entity.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Identity.Data.DbContext
{
    public interface IIdentityDbSet
    {
        DbSet<User> Users { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<Tenant> Tenants { get; set; }
        DbSet<TenantType> TenantTypes { get; set; }
        DbSet<TenantInfo> TenantInfo { get; set; }
    }
}
