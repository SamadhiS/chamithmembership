﻿using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Models.CustomFieldDefinition;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
namespace Inx.Module.Membership.Service.Controllers.Api.Settings.CustomFieldDefinition
{
    [Route(Constraints.ApiPrefix)]
    public class MemberFieldDefinitionApiController : MembershipBaseApiController, IBaseApi<MemberTabFieldDefinitionViewModel, int>
    {
        private readonly IMemberFieldDefinitionService service;
        public MemberFieldDefinitionApiController(IMemberFieldDefinitionService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("fielddefinition"), ModelValidation]
        public async Task<IActionResult> Create(MemberTabFieldDefinitionViewModel item)
        {
            return await Create<MemberTabFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(item, service);
        }
        [HttpPut, Route("fielddefinition"), ModelValidation]
        public async Task<IActionResult> Update(MemberTabFieldDefinitionViewModel item)
        {
            return await Update<MemberTabFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(item, this.service);
        }
        [HttpGet, Route("fielddefinition/{id:int}"), ModelValidation]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<MemberTabFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(id, service);
        }
        [HttpDelete, Route("fielddefinition/{id:int}"), ModelValidation]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<MemberFieldDefinitionBo, IMemberFieldDefinitionService>(id, service);
        }
        [HttpGet, Route("fielddefinition"), ModelValidation]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<MemberTabFieldDefinitionViewModel, MemberFieldDefinitionBo, IMemberFieldDefinitionService>(SearchRequest(skip,
               take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpGet, Route("fielddefinition/keyvalue"), ModelValidation]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<MemberFieldDefinitionBo, IMemberFieldDefinitionService>(service);
        }

    }
}
