import { Component, OnInit, Input } from '@angular/core';
import { ElementBase } from '../../core/custom-field-base/element-base';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'membership-element',
  templateUrl: './dynamic-form-element.component.html',
  styleUrls: ['./dynamic-form-element.component.scss']
})
export class DynamicFormElementComponent {
  @Input() element: ElementBase<any>;
  @Input() form: FormGroup;
  @Input() isFormSubmitted: boolean;

  get isValid() {
    return (this.form.controls[this.element.key].valid);
  }
}
