﻿using Inx.Service.Base.Controllers;

namespace Inx.Module.Payment.Service.Controllers
{
    public class PaymentBaseApiController:BaseApiController
    {
        public PaymentBaseApiController(IBaseInjector baseinject) : base(baseinject)
        {
        }
    }
}
