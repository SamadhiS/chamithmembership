﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Module.Identity.Core.Services.Tenant.Interface;
using Inx.Service.Base.Controllers;
using Inx.Service.Base.Models;
using Inx.Utility.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Identity.Service.Controllers.Api.Tenant
{
    [Route("api/identity/v1/tenant/lookups")]
    public class TenantLookupsApiController: BaseApiController
    {
        private ITenantTypeService tenantTypeService;
        public TenantLookupsApiController(ITenantTypeService _tenantTypeService, IBaseInjector baseinject) : base(baseinject)
        {
            tenantTypeService = _tenantTypeService;
        }
        [HttpGet,Route("organizationtypes")]
        public async Task<IActionResult> OrganizationType()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = await tenantTypeService.ReadKeyValue(new Search());
                stopwatch.Stop();
                return Ok(new Response<List<KeyValueListItem<int>>>
                {
                    Item = result,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("Localizationlookup")]
        public async Task<IActionResult> Localizationlookup(string key,string lang="en")
        {
            try
            {
                HttpContext.Request.Headers["Accept-Language"] = lang;
                return Ok(await Lang(key, HttpContext));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
    }
}
