﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.MemberPO
{
    class MemberPO
    {
        private NgWebElement Btn_NewMember(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnAddnewMember"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_NewMember_Click(NgWebDriver d)
        {
            this.Btn_NewMember(d).Click();
        }

        public NgWebElement Firstname(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtfname"));
        }
        public NgWebElement Lastname(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtLastName"));
        }
        private NgWebElement Btn_ProfileImage(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("#btnPersonal > div > div > div > form > fieldset > div:nth-child(1) > div:nth-child(3) > label.fileContainer"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ProfileImage_Click(NgWebDriver d)
        {
            this.Btn_ProfileImage(d).Click();
        }
        private NgWebElement Btn_Edit(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnEditMember"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_Edit_Click(NgWebDriver d)
        {
            this.Btn_Edit(d).Click();
        }
    
        public NgWebElement DOB(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDob"));
        }
        public NgWebElement MartialStatus(NgWebDriver d)
        {
         
            return d.FindElement(By.Id("ddMartialStatus")).FindElement(By.CssSelector("#ddMartialStatus > option:nth-child(3)"));
        }
        public NgWebElement Click_Status(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddMartialStatus"));
        }
        public NgWebElement NIC(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtIdentity"));
        }
        public NgWebElement Category(NgWebDriver d)
        {
            return d.FindElement(By.Id("contactType")).FindElement(By.CssSelector("#contactType > option:nth-child(5)"));
        }
        public NgWebElement Click_Cat(NgWebDriver d)
        {
            return d.FindElement(By.CssSelector("#contactType"));
        }
        private NgWebElement Btn_NextPersonal(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnPersonalNext"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_NextPersonal_Click(NgWebDriver d)
        {
            this.Btn_NextPersonal(d).Click();
        }
       
        public NgWebElement Country(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddCountry")).FindElement(By.CssSelector("#ddCountry > option:nth-child(76)"));
         
        }
        public NgWebElement Click_Country(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddCountry"));
        }
        public NgWebElement City(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCity"));
        }
        public NgWebElement Postal(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtZip"));
        }
        public NgWebElement Address(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtAddress"));
        }
        public NgWebElement Mobile(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtPhone"));
        }
        public NgWebElement Telephone(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtTelephone"));
        }
        public NgWebElement Email(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtEmail"));
        }
        private NgWebElement Btn_UpdateContact(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("contactWork"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_UpdateContact_Click(NgWebDriver d)
        {
            this.Btn_UpdateContact(d).Click();
        }
        private NgWebElement Btn_ContactNextButton(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("contactNext"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ContactNextButton_Click(NgWebDriver d)
        {
            this.Btn_ContactNextButton(d).Click();
        }
        public NgWebElement Designation(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDesignation"));
        }
        public NgWebElement Company(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCompany"));
        }
        public NgWebElement CompanyAddress(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComAddress"));
        }
        public NgWebElement TeleWork(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComtelephone"));
        }
        public NgWebElement MobileWork(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtComphone"));
        }
        private NgWebElement Btn_WorkNextButton(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnWork"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_WorkNextButton_Click(NgWebDriver d)
        {
            this.Btn_WorkNextButton(d).Click();
        }
        public NgWebElement MemberLevel(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddMembershiplevel")).FindElement(By.CssSelector("#ddMembershiplevel > option:nth-child(6)"));

        }
        private NgWebElement Btn_MemberLevelNext(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("memberLevel"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_MemberLevelNext_Click(NgWebDriver d)
        {
            this.Btn_MemberLevelNext(d).Click();
        }
        public NgWebElement TransactionType(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtTransActionType")).FindElement(By.XPath("//*[@id='txtTransActionType']"));

        }
        public NgWebElement PaymentType(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddPaymentType")).FindElement(By.XPath("//*[@id='ddPaymentType']/option[2]"));

        }
        public NgWebElement Descript(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtDescription"));
        }
        private NgWebElement Btn_PayNow(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnPayNow"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_PayNow_Click(NgWebDriver d)
        {
            this.Btn_PayNow(d).Click();
        }
        private NgWebElement Btn_PayLater(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnPayLater"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_PayLater_Click(NgWebDriver d)
        {
            this.Btn_PayLater(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
    }
