import { BlockUiService } from './../../core/services/block-ui.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TableTemplateService } from './../../core/services/table-template.service';
import { TableTemplate } from './../../core/enums/tableTemplate';
import { Component, OnInit, ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { TableTemplateModel } from '../models/tableTemplateModel';

@Component({
  selector: 'membership-select-columns',
  templateUrl: './select-columns.component.html',
  styleUrls: ['./select-columns.component.scss']
})
export class SelectColumnsComponent implements OnInit {

  allColumns = []; 
  displayAllColumns = [] ;
  isBlocked = false;

  @ViewChild('templateModal') templateModal: ModalDirective;
  @Output() show: EventEmitter<any> = new EventEmitter();
  @Input() tableTemplate: number;
  tableTemplateModel = new TableTemplateModel();

  public header: string;

  constructor(private tableTemplateService: TableTemplateService, 
    private spinnerService: Ng4LoadingSpinnerService,    
    private blockUiService: BlockUiService ) { }


  ngOnInit() {
    this.getColumns();
    this.setHeader();
  }

  public setHeader() {
    switch (this.tableTemplate) {
      case TableTemplate.ContactTableTemplate: {
        this.header = 'Contact Columns';
        break;
      }
      case TableTemplate.MemberTableTemplate: {
        this.header = 'Member Columns';
        break;
      }
    }
  }
  public showTemplateModal(): void {
    this.templateModal.show();

  }

  hideTemplateModal() {
    this.templateModal.hide();
  }

  saveColumns() {
  this.isBlocked = true;
    let selectedColumns = this.allColumns.filter(data => data.isSelected).map(data => data.name)
    this.tableTemplateModel.templateType = this.tableTemplate;
    this.tableTemplateModel.keys = selectedColumns;

    this.spinnerService.show();
    this.tableTemplateService.saveTemplateColumns(this.tableTemplateModel).subscribe(data => {
      this.show.emit(selectedColumns);
      this.templateModal.hide();
      this.isBlocked = false;
    }, error => {
      this.isBlocked = false;
    }, () => {
      this.isBlocked = false;
    })
  }

  showColumns() {
    let selectedColumns = this.allColumns.filter(data => data.isSelected).map(data => data.name)

    this.show.emit(selectedColumns);
    this.templateModal.hide();

  }

  getColumns() {
    this.tableTemplateService.getTemplateColumns(this.tableTemplate).subscribe(data => {
      let columns = []; 
      columns = data.result.item;
      this.allColumns = columns.map(item => { return { name: item, isSelected: false } })

      this.displayAllColumns = this.allColumns;
      this.displayAllColumns.forEach(element => {
        element.name = element.name.replace(/([A-Z])/g, ' $1').trim()
      });

      this.getSavedTemplateColumns();
    })

  }

  getSavedTemplateColumns() {
    this.tableTemplateService.getSavedTemplateColumns(this.tableTemplate).subscribe(data => {
      let savedColumns = [];
      savedColumns = data.result.item;
      savedColumns.forEach(col => {

        let column = this.allColumns.find(data => data.name == col)
        if (column)
          column.isSelected = true;
      })

    })
  }


}
