﻿using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Core.Bo.Dashboard
{
    public class MemberActiveGraphBo
    {
        public List<DateTime> MemberSubscriptionDate { get; set; }
        public List<int> ActiveMembers { get; set; }
        public MemberActiveGraphBo()
        {
            MemberSubscriptionDate = new List<DateTime>();
            ActiveMembers = new List<int>();
           
        }
    }
}
