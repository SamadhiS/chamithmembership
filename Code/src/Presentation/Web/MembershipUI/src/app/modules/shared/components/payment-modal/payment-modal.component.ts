import { PaymentModel } from './../../../payments/models/paymentModel';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { PaymentService } from '../../../core/services/payment.service';
import { PaymentViewModel } from '../../../payments/models/paymentViewModel';
import { TransActionType } from '../../../core/enums/transActionType';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { PaymentStates } from '../../../core/enums/paymentState';

@Component({
  selector: 'membership-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: ['./payment-modal.component.scss']
})
export class PaymentModalComponent implements OnInit {

  paymentForm: FormGroup;
  paymentViewModel = new PaymentViewModel();
  paymentModel = new PaymentModel();
  transActionTypes = [];
  paymentTypes = [];
  @ViewChild('paymentModal') paymentModal: ModalDirective;
  @Output() pay: EventEmitter<PaymentModel> = new EventEmitter();

  constructor(private fb: FormBuilder, private paymentService: PaymentService,
    private toaster: ToastsManager) {

  }

  ngOnInit() {
    this.paymentForm = this.getPaymentForm();
    this.getPaymentLookupData();

  }

  public showPaymentModal(paymentViewModel): void {
    this.paymentViewModel = paymentViewModel;
    this.paymentForm.patchValue({
      amount: this.paymentViewModel.amount
    })
    this.paymentModal.show();
  }

  public hidePaymentModal(): void {
    this.paymentModal.hide();
  }

  payNow() {

    this.paymentModel.amount = this.paymentViewModel.amount - this.paymentViewModel.remaining;
    this.paymentModel.paymentMasterId = this.paymentViewModel.paymentMasterId;
    this.paymentModel.paymentType = 1;
    this.paymentModel.paymentState=PaymentStates.Paid;

    var payment = Object.assign({}, this.paymentModel, this.paymentForm.value)

    this.paymentModal.hide();
    this.pay.emit(payment)

  }


  private getPaymentForm() {
    return this.fb.group({
      transActionType: ["1", [Validators.required]],
      paymentType: ["1", [Validators.required]],
      amount: [{ value: this.paymentViewModel.amount, disabled: true }, [Validators.required]],
      description: [""],

    })
  }

  private getPaymentLookupData() {
 
    Observable.forkJoin(
      this.paymentService.getTransActionTypes(),
      this.paymentService.getPaymentTypes(),
    ).subscribe(data => {
      this.transActionTypes = data[0].result.item;
      this.paymentTypes = data[1].result.item;
    },
      error => {
      },
      () => {
      })
  }

}
