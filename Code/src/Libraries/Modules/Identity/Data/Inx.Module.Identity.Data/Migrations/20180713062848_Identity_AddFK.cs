﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Identity.Data.Migrations
{
    public partial class Identity_AddFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users_CreatedById",
                schema: "Inexis.Security",
                table: "Users",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Users_EditedById",
                schema: "Inexis.Security",
                table: "Users",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TenantId",
                schema: "Inexis.Security",
                table: "Users",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_CreatedById",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_EditedById",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_TenantId",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantInfo_CreatedById",
                schema: "Inexis.Security",
                table: "TenantInfo",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_TenantInfo_EditedById",
                schema: "Inexis.Security",
                table: "TenantInfo",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_TenantInfo_TenantId",
                schema: "Inexis.Security",
                table: "TenantInfo",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_CreatedById",
                schema: "Inexis.Security",
                table: "Roles",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_EditedById",
                schema: "Inexis.Security",
                table: "Roles",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_TenantId",
                schema: "Inexis.Security",
                table: "Roles",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Roles_Users_CreatedById",
                schema: "Inexis.Security",
                table: "Roles",
                column: "CreatedById",
                principalSchema: "Inexis.Security",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Roles_Users_EditedById",
                schema: "Inexis.Security",
                table: "Roles",
                column: "EditedById",
                principalSchema: "Inexis.Security",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Roles_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "Roles",
                column: "TenantId",
                principalSchema: "Inexis.Security",
                principalTable: "Tenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantInfo_Users_CreatedById",
                schema: "Inexis.Security",
                table: "TenantInfo",
                column: "CreatedById",
                principalSchema: "Inexis.Security",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantInfo_Users_EditedById",
                schema: "Inexis.Security",
                table: "TenantInfo",
                column: "EditedById",
                principalSchema: "Inexis.Security",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantInfo_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "TenantInfo",
                column: "TenantId",
                principalSchema: "Inexis.Security",
                principalTable: "Tenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_Users_CreatedById",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "CreatedById",
                principalSchema: "Inexis.Security",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_Users_EditedById",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "EditedById",
                principalSchema: "Inexis.Security",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "UserRoles",
                column: "TenantId",
                principalSchema: "Inexis.Security",
                principalTable: "Tenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Users_Users_CreatedById",
            //    schema: "Inexis.Security",
            //    table: "Users",
            //    column: "CreatedById",
            //    principalSchema: "Inexis.Security",
            //    principalTable: "Users",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Users_Users_EditedById",
            //    schema: "Inexis.Security",
            //    table: "Users",
            //    column: "EditedById",
            //    principalSchema: "Inexis.Security",
            //    principalTable: "Users",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "Users",
                column: "TenantId",
                principalSchema: "Inexis.Security",
                principalTable: "Tenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Roles_Users_CreatedById",
                schema: "Inexis.Security",
                table: "Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Roles_Users_EditedById",
                schema: "Inexis.Security",
                table: "Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_Roles_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_TenantInfo_Users_CreatedById",
                schema: "Inexis.Security",
                table: "TenantInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_TenantInfo_Users_EditedById",
                schema: "Inexis.Security",
                table: "TenantInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_TenantInfo_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "TenantInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_Users_CreatedById",
                schema: "Inexis.Security",
                table: "UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_Users_EditedById",
                schema: "Inexis.Security",
                table: "UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_CreatedById",
                schema: "Inexis.Security",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_EditedById",
                schema: "Inexis.Security",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Tenants_TenantId",
                schema: "Inexis.Security",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_CreatedById",
                schema: "Inexis.Security",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_EditedById",
                schema: "Inexis.Security",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_TenantId",
                schema: "Inexis.Security",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_UserRoles_CreatedById",
                schema: "Inexis.Security",
                table: "UserRoles");

            migrationBuilder.DropIndex(
                name: "IX_UserRoles_EditedById",
                schema: "Inexis.Security",
                table: "UserRoles");

            migrationBuilder.DropIndex(
                name: "IX_UserRoles_TenantId",
                schema: "Inexis.Security",
                table: "UserRoles");

            migrationBuilder.DropIndex(
                name: "IX_TenantInfo_CreatedById",
                schema: "Inexis.Security",
                table: "TenantInfo");

            migrationBuilder.DropIndex(
                name: "IX_TenantInfo_EditedById",
                schema: "Inexis.Security",
                table: "TenantInfo");

            migrationBuilder.DropIndex(
                name: "IX_TenantInfo_TenantId",
                schema: "Inexis.Security",
                table: "TenantInfo");

            migrationBuilder.DropIndex(
                name: "IX_Roles_CreatedById",
                schema: "Inexis.Security",
                table: "Roles");

            migrationBuilder.DropIndex(
                name: "IX_Roles_EditedById",
                schema: "Inexis.Security",
                table: "Roles");

            migrationBuilder.DropIndex(
                name: "IX_Roles_TenantId",
                schema: "Inexis.Security",
                table: "Roles");
        }
    }
}
