﻿using Inx.Utility.Models;

namespace Inx.Module.Identity.Core.Bo.Security.User
{
   public  class UserBo : BaseBo
    {
        public string Email { get; set; }
    }
}
