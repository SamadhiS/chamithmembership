﻿using System;
using System.Collections.Generic;
using Inx.Utility.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Core.Bo.Member.Master
{
    public class MemberInfoBo<T>:BaseBo
    {
        public T SystemFields { get; set; }
        public int MemberId { get; set; }
        public Enums.MemberType MemberType { get; set; }
        public string ProfileImage { get; set; } = Constants.ProfileNoImage;
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OrganizationName { get; set; }
        public int? SubscriptionId { get; set; } = 0;
        public List<MemberFieldKeyValueBo> MemberFieldKeyValue { get; set; }
    }
}
