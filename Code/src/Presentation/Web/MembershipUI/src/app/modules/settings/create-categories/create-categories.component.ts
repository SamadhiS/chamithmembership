import { Subject } from 'rxjs/Subject';
import { error } from 'protractor';
import { Router } from '@angular/router';
import { ContactCategoryService } from './../services/ContactCategory.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Categories } from './../models/categoryModel';
import { SettingService } from './../services/setting.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { ChangePassword } from '../models/passwordModel';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { BlockUiService } from '../../core/services/block-ui.service';

@Component({
  selector: 'membership-create-categories',
  templateUrl: './create-categories.component.html',
  styleUrls: ['./create-categories.component.scss']
})
export class CreateCategoriesComponent implements OnInit {

  categorieForm: FormGroup;
  categorie = new Categories();
  categoryId = 0;
  searchTerm$ = new Subject<string>();
  categorieList = new Categories();
  patternModel = new PatternModel();
  password = new ChangePassword();
  iscategorieFormSubmitted = false;
  Isedit = true;
  catogeryId: number;
  categoryIds = [];
  skip: number;
  take: number;
  isBlocked = false;

  constructor(private fb: FormBuilder, vcr: ViewContainerRef, public toastr: ToastsManager,
    private router: Router,
    private contactCategoryService: ContactCategoryService,
    private blockUiService: BlockUiService) {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.skip = 0;
    this.take = 0;
    this.Isedit = true;
    this.catogeryId = 0;
    this.getCategory();
    this.getCategorieForm();
    this.Isedit = true;
  }
  updateCategory() {
    this.iscategorieFormSubmitted = true;
    if (!this.categorieForm.valid) return;
    {
      var name = this.categorieForm.value.Name.trim();
      if (name == "") {
        this.toastr.error('Category Name Cannot be Empty', 'Oops');
      }
      else {
        this.isBlocked = true;
        var nameObj = { Name: name, Id: this.catogeryId }
        var categorie = Object.assign({}, this.categorie, this.categorieForm.value)
        this.contactCategoryService.updateCategory(nameObj)
          .subscribe(
          payment => {
            this.toastr.success(ToasterMessages.update('Category'), toasterHeadings.Success);
            this.getCategory();
            this.getCategorieForm();
            this.isBlocked = false;
            this.Isedit = true;
          }, error => {
            this.toastr.error(ToasterMessages.updateError('Category'), toasterHeadings.Error);
            this.isBlocked = false;
          });
          this.isBlocked = false;
      }
    }
  }
  getCategorieForm() {
    this.categorieForm = this.fb.group({
      Name: ["", [Validators.required, Validators.maxLength(25)]]
    })
  }
  getCategorie() {
    this.categorieForm = this.fb.group({
      Name: [""]
    })
  }
  addcategory() {
    this.iscategorieFormSubmitted = true;
    if (!this.categorieForm.valid) return;
    {
      var name = this.categorieForm.value.Name.trim();
      if (name == "") {
        this.toastr.error('Category Name Cannot be Empty', 'Oops');
      }
      else {
        this.isBlocked = true;
        var nameObj = { Name: name }
        var categorie = Object.assign({}, this.categorie, this.categorieForm.value)
        this.contactCategoryService.addCategory(nameObj)
          .subscribe(
          payment => {
            this.toastr.success(ToasterMessages.save('Category'), toasterHeadings.Success);
            this.getCategory();
            this.getCategorieForm();
            this.isBlocked = false;
          }, error => {
            if(error == "already exists"){
              this.toastr.error(ToasterMessages.alreadyExistError('Category'), toasterHeadings.Error);
            }else{
              this.toastr.error(ToasterMessages.deleteError('Category'), toasterHeadings.Error);          
            }
            this.isBlocked = false;
          });
          this.isBlocked = false;
      }
    }
  }
  getCategory() {
    this.isBlocked = true;
    this.contactCategoryService.getAllCategories(this.skip, this.take)
      .subscribe(allCategories => {
        this.categorieList = allCategories.result.item.items;
        if (allCategories.result.item.totalRecodeCount == 0) {
        }
        this.isBlocked = false;
      },
      error => {
        this.toastr.error(ToasterMessages.loadError('Category'), toasterHeadings.Error);
        this.isBlocked = false;
      });
  }
  deletecategoryId(categoryId) {
    this.categoryId = categoryId;

  }
  deletecategory() {
  
    this.isBlocked = true;
    this.contactCategoryService.deleteCategory(this.categoryId)
      .subscribe(allCategories => {
          this.toastr.success(ToasterMessages.delete('Category'), toasterHeadings.Success);   
          this.CancelCategory();      
        this.getCategory();
      },
      error => {
        if(error == "FK exist"){
          this.toastr.error("Category has been allocated");
        }else{
          this.toastr.success(ToasterMessages.deleteError('Category'), toasterHeadings.Error);          
        }
        this.isBlocked = false;
      });
  }
  editcategory(id, Name) {
    this.catogeryId = id;
    this.getCategorie(); {
      this.categorieForm = this.fb.group({
        Name: [Name]
      })
    }
    this.Isedit = false;
  }
  deleteId(Id) {
    this.categoryId = Id;
  }

  CancelCategory() {
    this.getCategorieForm();
    this.Isedit = true;
  }
}
