﻿Feature: As an Admin, I must be able to reset existing password

@mytag
#Scenario Outline: As an Admin, I must be able to reset existing password
#Given Load Membership Login pg
#And Enter credentials "membership.inexis@gmail.com" and "aDmin@123" 
#And Click on Sign in
#Then I Navigated to Dashboard
#And Click on Settings button
#And Click on Account Settings
#And I have entered <CurrentPassword> as Current Password
#And I have entered <NewPassword> as new password 
#And I have entered <ConfirmPassword> as Confirm password 
#And I click on Reset Password button
#And I click on Logout

#Examples: 
#| CurrentPassword | NewPassword | ConfirmPassword |
#| Admin@123      | ADmin@123   | ADmin@123       |
## Because of testing purposes i have commented this

Scenario Outline: As an Admin, I should not be able to reset existing password with invalid data
Given Load Membership Login
And Enter valid credentials "membership.inexis@gmail.com" and "Admin@123" 
And Click on Signin
Then I Navigated to the Dashboard
And Click on the Settings button
And Click on the Account Settings
And I entered <CurrentPassword> as Current Password
And I entered <NewPassword> as new password 
And I entered <ConfirmPassword> as Confirm password 
And I click on Reset Password 
And I click on Logout button

Examples: 
| CurrentPassword | NewPassword | ConfirmPassword |
| ADmin@123       | aDmin@123   | aDmin@123       |
| ADmin@123       | aDmin@123   |                 |
| ADmin@123       |             | aDmin@123       |
|                 | aDmin@123   | aDmin@123       |
| ADmin@123       | admin@123   | admin@123       |
| Admin@123       | admin@123   | admin@123       |