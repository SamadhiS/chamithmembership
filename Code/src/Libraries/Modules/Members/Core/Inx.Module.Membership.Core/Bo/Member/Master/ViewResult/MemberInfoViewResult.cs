﻿using System.Collections.Generic;

namespace Inx.Module.Membership.Core.Bo.Member.Master.ViewResult
{
    public class MemberInfoViewResult
    {
        public int MemberId { get; set; }
        public string ProfileImage { get; set; }
        public Enums.MemberType MemberType { get; set; }
        public List<MemberFieldKeyValueBo> MemberFieldKeyValue { get; set; }
        public MemberInfoViewResult(int memberId, string profileImage, Enums.MemberType memberType, List<MemberFieldKeyValueBo> memberFieldKeyValue)
        {
            MemberId = memberId;
            ProfileImage = profileImage;
            MemberFieldKeyValue = memberFieldKeyValue;
            MemberType = memberType;
        }
        
    }
}
