import { QuillModule } from 'ngx-quill';
import { SafeHtmlPipe } from './modules/shared/pipes/safe-html.pipe';

import { TokenInterceptor } from './token-interceptor';
import { SettingsModule } from './modules/settings/settings.module';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { ContactsModule } from './modules/contacts/contacts.module';
import { LayoutModule } from './modules/layout/layout.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './modules/login/login.module';
import { CoreModule } from './modules/core/core.module';
import { SharedModule } from './modules/shared/shared.module';
import { Location,LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AuthGuardService } from './modules/login/auth.guard.service';
import { CommunicationsModule } from './modules/communications/communications.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {TranslateModule, TranslateLoader, TranslatePipe} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SettingService } from './modules/settings/services/setting.service';
import { PermissionPasswordComponent } from './modules/settings/permission-password/permission-password.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/languages/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularMultiSelectModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    SettingsModule,
    QuillModule,
    LayoutModule,
    LoginModule,
    CoreModule.forRoot(),  
  ],
  providers: [
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy }],

  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
