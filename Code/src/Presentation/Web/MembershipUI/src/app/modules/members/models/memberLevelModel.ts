export class MemberLevelModel {
    id: number;
    name: string;
    price: number;
    description: string;
    membershipExpired: string;
    capacity: number;
    isLimited: string;
    subscriptionTypeId: number;
    isRenewable: boolean;
    isSendEmail: boolean;
    renewalDays: number
    emaillTemplateId: number;
    subscriptionTo: Date;
    subscriptionFrom: Date;
}