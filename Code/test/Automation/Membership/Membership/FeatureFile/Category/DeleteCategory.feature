﻿Feature:As an Admin, I should be able to delete categories

@mytag

Scenario: As an Admin, I should be able to delete categories
Given Load Membership page
And I entered username & password "membership.inexis@gmail.com" and "Admin@123" 
And I Clicked on Sign in button 
Then Navigate to Dashboard
And I Clicked on Settings
And I select a row and click on delete button
And I confirmed the delete
And Clicked on logout button
 

 Scenario: As an Admin, I should be able to cancel the delete categories
Given Load Membership pg
And I entered "membership.inexis@gmail.com" and "Admin@123" 
And I Clicked on Sign in btn
Then Navigate to Dashboard page
And I Clicked on Settings button
And I select a row and click on delete btn
And I cancel the delete
And Clicked on logout btn


 Scenario: As an Admin, I should be able to Close the delete categories
Given Load the membership
And I entered credentials "membership.inexis@gmail.com" and "Admin@123" 
And I Clicked on SignIn
Then Navigate to Dashboard pg
And I Clicked on Settings btn
And I select a row and click on delete bttn
And I close the delete
And Clicked on logout bttn