/*
****General guidelines****
--->Separate your imports and vendor imports
--->Separate private and public methods
--->Please keep your code base short and sweet
*/

import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { PersonalModel } from '../models/personalModel';
import { ContactAllDataModel } from '../models/contactAllDataModel';
import { ToastsManager } from 'ng2-toastr';
import { WorkModel } from '../models/workModel';
import { LookupService } from '../../core/services/lookup.service';
import { ContactModel } from '../models/contactModel';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { ContactsService } from '../../core/services/contacts.service';
import { BlockUiService } from '../../core/services/block-ui.service';

@Component({
  selector: 'membership-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.scss']
})
export class ContactCreateComponent implements OnInit {

  personalForm: FormGroup;
  contactForm: FormGroup;
  workForm: FormGroup;
  isPersonalFormSubmitted = false;
  isContactFormSubmitted = false;
  isWorkFormSubmitted = false;
  showimage = false;

  url: any;
  countryCode: any;
  workCountryCode : any;
  contactId = 0;
  isBlocked = false;
  countries = [];
  maritialStatus = [];
  contactTypes = [];
  
  language="";
  createword :any;
  editword:any;
  _subheading:any;
  edithubheading :any;
  profileheading:any;
  personal = new PersonalModel();
  contact = new ContactModel();
  work = new WorkModel();
  patternModel = new PatternModel();

  contactAllData = new ContactAllDataModel();
  fileList: FileList;
  maxDate = new Date();
  contactHeading = 'New Contact';
  subheading ="Wizard  will guide you in creating a new contact";
  profileImageName='Choose your Profile Picture';



  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(private fb: FormBuilder, private contactService: ContactsService,
    private lookUpService: LookupService,
    private router: Router,
    private blockUiService: BlockUiService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
   this.getTranslator();
    this.personalForm = this.getPersonalForm();
    this.contactForm = this.getContactForm();
    this.workForm = this.getWorkForm();
    this.getContactLookupData();
    this.profileImageName = this.profileheading;

    this.activatedRoute.params.subscribe((params: Params) => {
      this.contactId = params['id'];

      if (this.contactId && this.contactId != 0) {
        this.contactHeading = this.editword; //"Edit Contact"
        this.subheading = this.edithubheading;
        this.getContact();
      }
      else {
        this.contactId = 0;
        this.contactHeading = this.createword;
        this.subheading = this._subheading;
        this.url = 'assets/img/user.svg';
      }

    });
  }

  getTranslator(){
    this.language = localStorage.getItem('Language');
    if (this.language == "en") {
      this.createword ='New Contact';
      this.editword = "Edit Contact";
      this.profileheading ='Choose your Profile Picture';
      this._subheading ="Wizard  will guide you in creating a new contact";
      this.edithubheading ="Wizard  will guide you in creating a edit contact";
     
    }
    if (this.language == "sh") {
     
    this.createword = 'නව සම්බන්ධතා';
    this.editword = "සම්බන්ධතා සංස්කරණය කරන්න";
    this.profileheading = 'ඔබගේ පැතිකඩ පින්තූරය තෝරන්න';
    this._subheading = "නව සම්බන්ධතාවයක් නිර්මාණය කිරීමෙහිලා ඔබව මග පෙන්වනු ඇත";   
    this.edithubheading = "සංස්කරණ සබඳතා නිර්මාණය කිරීමේදී ඔබව මග පෙන්වනු ඇත";
     
    }
    if (this.language == "tm") {
      
    this.createword = 'புதிய தொடர்பு';
    this.editword = "திருத்து தொடர்பு";
    this.profileheading = 'உங்கள் சுயவிவர படத்தை தேர்வு செய்க';
    this._subheading = "வழிகாட்டி ஒரு புதிய தொடர்பு உருவாக்க நீங்கள் வழிகாட்டும்";
    this.edithubheading = "ஒரு திருப்பம் உருவாக்கும் வகையில் வழிகாட்டி உங்களை வழிகாட்டுவார்";
    }
  }
  save() {
    this.isPersonalFormSubmitted = true;
    this.isContactFormSubmitted = true;
    this.isWorkFormSubmitted = true;

    if (!this.personalForm.valid || !this.contactForm.valid || !this.workForm.valid) return;

    var personal = Object.assign({}, this.personal, this.personalForm.value)
    var contact = Object.assign({}, this.contact, this.contactForm.value)
    var work = Object.assign({}, this.work, this.workForm.value)

    this.contactAllData.personInfo = personal;
    this.contactAllData.contactInfo = contact;
    this.contactAllData.workingInfo = work;
    this.contactAllData.id = this.contactId;
    this.isBlocked = true;
    this.contactService.saveContact(this.fileList, this.contactAllData).subscribe(res => {
      this.toastr.success(ToasterMessages.save('Contact'), toasterHeadings.Success);
      this.isBlocked = false;
      this.router.navigate(["/contacts"])
    }, error => {
      this.isBlocked = false;
      this.toastr.error(ToasterMessages.saveError('Contact'), toasterHeadings.Error);
    }, () => {

    })
  }

  getContact() {
    this.isBlocked = true;
    this.contactService.getContactById(this.contactId).subscribe(res => {
      this.patchPersonalForm(res.result.item.personInfo);
      this.patchContactForm(res.result.item.contactInfo);
      this.patchWorkForm(res.result.item.workingInfo);
     
    },
      error => {
        this.isBlocked = false;
        this.toastr.error('Contact loading error!', 'Error!');
      }, () => {
        this.isBlocked = false;
      })
  }


  private patchPersonalForm(person: PersonalModel) {
    this.personal = person;
    this.personalForm.patchValue({
      firstName: person.firstName,
      lastName: person.lastName,
      dob: person.dob,
      martialStatus: person.martialStatus,
      identity: person.identity,
      contactCategoryId: person.contactCategoryId,
    })
  }

  private patchContactForm(contact: ContactModel) {
    this.contact = contact;
    this.countries.forEach(element => {
      if (element.value == contact.country) {
        this.countryCode = element.phoneCode
      }
    });
    this.contactForm.patchValue({
      country: contact.country,
      city: contact.city,
      zip: contact.zip,
      address: contact.address,
      phone: contact.phone,
      telephone: contact.telephone,
      email: contact.email
    })
  }
  private patchWorkForm(work: WorkModel) {
    this.work = work;
    this.countries.forEach(element => {
      if (element.value == work.country) {
        this.workCountryCode = element.phoneCode
      }
    });
    this.workForm.patchValue({
      designation: work.designation,
      company: work.company,
      address: work.address,
      telephone: work.telephone,
      country: work.country,
      phone: work.phone,
    })
  }

  setTabActive(tab_id: number) {
    switch (tab_id) {
      case 1: {
        this.isPersonalFormSubmitted = true;
        if (!this.personalForm.valid) return;
        this.staticTabs.tabs[tab_id].active = true;
        break;
      }
      case 2: {
        this.isContactFormSubmitted = true;
        if (!this.contactForm.valid) return;
        this.staticTabs.tabs[tab_id].active = true;
        break;
      }
    }
  }

  updateWork() {
    if (!this.workForm.valid) return;
    this.isBlocked = true;
    var work = Object.assign({}, this.work, this.workForm.value)
    this.contactService.updateWork(work).subscribe(res => {
      this.toastr.success(ToasterMessages.update('Work Info'), toasterHeadings.Success);
      
    },
      error => {
        this.isBlocked =false;
        this.toastr.error(ToasterMessages.updateError('Work Info'), toasterHeadings.Error);
      }, () => {
        this.isBlocked = false;
      })
  }

  updateContact() {
    if (!this.contactForm.valid) return;
    this.isBlocked = true;
    var contact = Object.assign({}, this.contact, this.contactForm.value)
    this.contactService.updateContact(contact).subscribe(res => {
      this.toastr.success(ToasterMessages.update('Contact Info'), toasterHeadings.Success);
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Contact Info'), toasterHeadings.Error);
      }, () => {
        this.isBlocked = false;
      })
  }

  personUpdate() {
    if (!this.personalForm.valid) return;
    if (this.personal.profileImage)
      this.personal.profileImage = this.getImageName(this.personal.profileImage);

      this.isBlocked = true;
    var personal = Object.assign({}, this.personal, this.personalForm.value)

    this.contactService.updatePerson(this.fileList, personal).subscribe(res => {
      this.toastr.success(ToasterMessages.update('Person Info'), toasterHeadings.Success);
    },
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.updateError('Person Info'), toasterHeadings.Error);
      }, () => {
        this.isBlocked = false;
      })
  }

  fileUpload(event) {
    this.fileList = event.target.files;
    this.profileImageName= this.fileList[0].name
    if (event.target.files && event.target.files[0]) {
      this.showimage = true
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.url = event.target.result;
      }

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  archiveContact() {
    this.isBlocked = true;
    this.contactService.archivecontact(this.contactId).subscribe(res => {
      this.toastr.success(ToasterMessages.delete('Contact'), toasterHeadings.Success);
      this.isBlocked = false;
      setTimeout(() => {
        this.router.navigate(['/contacts']);
      },1000);  
    }, 
      error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.deleteError('Contact'), toasterHeadings.Error);
      }, () => {
        this.isBlocked = false;
      })
  }

  private getPersonalForm() {
    return this.fb.group({
      firstName: ["", [Validators.required, Validators.maxLength(50), patternValidator(this.patternModel.whiteSpace)]],
      lastName: ["", [Validators.required, Validators.maxLength(50), patternValidator(this.patternModel.whiteSpace)]],
      dob: [new Date(), ""],
      martialStatus: [null,[Validators.required]],
      identity: ["", [Validators.required, Validators.maxLength(50), patternValidator(this.patternModel.whiteSpace)]],
      contactCategoryId: [null,[Validators.required]],

    })
  }
  private getContactForm() {
    return this.fb.group({
      country: [null,[Validators.required]],
      city: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      zip: [""],
      address: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      phone: ["", [Validators.required, patternValidator(this.patternModel.phoneregx)]],
      telephone: ["", [patternValidator(this.patternModel.phoneregx)]],
      email: ["", [Validators.required, patternValidator(this.patternModel.emailRegex)]]

    })
  }

  private getWorkForm() {
    return this.fb.group({
      designation: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      company: ["", [Validators.required, patternValidator(this.patternModel.whiteSpace)]],
      address: [""],
      country: [],
      phone: ["", [patternValidator(this.patternModel.phoneregx)]],
      telephone: ["", [patternValidator(this.patternModel.phoneregx)]],
    })

  }

  private getContactLookupData() {

    Observable.forkJoin(
      this.lookUpService.getContactTypes(),
      this.lookUpService.getCountries(),
      this.lookUpService.getMartialStatus()
    ).subscribe(data => {
      this.countryCode = "";
      this.contactTypes = data[0].result.item;
      this.countries = data[1].result.item;
      this.maritialStatus = data[2].result;
    },
      error => {

      },
      () => {

      })
  }

  countryCodechange(event) {
    this.countries.forEach(element => {
      if (element.text.trim() == event.label) {
        this.countryCode = element.phoneCode
      }
    });
  }

  workCountryCodechange(event){
    this.countries.forEach(element => {
      if (element.text.trim() == event.label) {
        this.workCountryCode = element.phoneCode
      }
    });
  }

  private getImageName(profileImage) {
    let splitFilePath = profileImage.split("/");
    return splitFilePath[splitFilePath.length - 1]
  }


}
