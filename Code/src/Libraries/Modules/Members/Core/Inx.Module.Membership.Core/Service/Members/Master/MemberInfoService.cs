﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Module.Membership.Core.Bo.Member.Master;
using Inx.Module.Membership.Core.Service.Members.interfaces;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;
using Microsoft.EntityFrameworkCore;
using Inx.Module.Membership.Core.Bo.Member.Master.ViewResult;
using Inx.Module.Membership.Core.Bo.Member.SearchViewResult.Search;
using Inx.Module.Membership.Core.Bo.Member.SearchViewResult.ViewResult;
using Inx.Data.Base.DbaContext;
using Dapper;

namespace Inx.Module.Membership.Core.Service.Members.Master
{
    public class MemberInfoService : MemberBaseService, IMemberInfoService
    {
        private readonly IUnitOfWokMembership unitOfWokMembership;
        private Enums.MemberType MemberType;
        private IMemberInfoTypeService memberInfoTypeService;
        public MemberInfoService(IUnitOfWokMembership _unitOfWokMembership, ICoreInjector coreInjector) : base(coreInjector)
        {
            unitOfWokMembership = _unitOfWokMembership;
        }

        public async Task<int> Create<T>(Request<MemberInfoBo<T>> request) where T : BaseBo
        {
            using (var transaction = await unitOfWokMembership.Context.Database.BeginTransactionAsync())
            {
                Member member;
                AuditableEntity membertypEntity;
                try
                {
                    if (request.Item.MemberType == Enums.MemberType.Person)
                    {
                        memberInfoTypeService = new MemberPersonService(unitOfWokMembership);
                        member = new Member(request.Item.Email, request.Item.Phone, request.Item.FirstName, request.Item.LastName);
                    }
                    else
                    {
                        memberInfoTypeService = new MemberOrganizationService(unitOfWokMembership);
                        member = new Member(request.Item.Email, request.Item.Phone, request.Item.FirstName, request.Item.LastName, request.Item.OrganizationName);
                    }
                    var savedMember = await unitOfWokMembership.MemberRepository.CreateAndSave(
                        Request(member, request.TenantId, request.UserId));

                    if (request.Item.MemberType == Enums.MemberType.Person)
                    {
                        membertypEntity = new PersonalInfo
                        {
                            Id = savedMember.Id,
                        };
                    }
                    else
                    {
                        membertypEntity = new OrganizationInfo()
                        {
                            Id = savedMember.Id,
                            OrganizationName = request.Item.OrganizationName
                        };
                    }
                    await memberInfoTypeService.Create(Request(membertypEntity, request.TenantId,
                        request.UserId));

                    //save keyValuepare
                    foreach (var customField in request.Item.MemberFieldKeyValue)
                    {
                        await unitOfWokMembership.MemberFieldKeyValueRepository.Create(Request(new MemberFieldKeyValue
                        {
                            Key = customField.Key,
                            MemberId = savedMember.Id,
                            Value = customField.Value
                        }, request.TenantId, request.UserId));
                    }
                    await unitOfWokMembership.SaveAsync();
                    transaction.Commit();
                    return savedMember.Id;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e.HandleException();
                }
            }
        }

        public async Task Delete(Request<int> request)
        {
            try
            {
                await unitOfWokMembership.MemberRepository.DeleteAndSave(request);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<PageList<MembersViewResult>> Search(Search request)
        {
            try
            {
                List<MembersViewResult> queryResult = new List<MembersViewResult>();
                var search = request.SearchTerm.ToJsonObject<MemberSearch>();
                if (search.IsNull())
                {
                    search = new MemberSearch();
                }
                var term = request.OrderByTerm != "" ? "member." + request.OrderByTerm.Split(':')[0].TrimAndToLower() : "member.Id";
                var order = request.OrderByTerm != "" ? request.OrderByTerm.Split(':')[1] : "ASC";
                var searchresult = search.Search;
                var skip = request.Skip;
                var take = request.Take;

                var query = $@"select member.Id,member.Name, member.ProfileImage,member.MemberType, member.Email, member.Phone, subscripttype.Name as SubscriptionType, subscript.MembershipExpired,
                            membersubscription.JoinDate, membersubscription.RenewDate, membersubscription.SubscriptinId, membersubscription.MembershipStatus, member.CreatedOn
                            from [Inexis.Membership].[Member] member 
                            left join [Inexis.Membership].[MemberSubscription]  membersubscription on member.id = membersubscription.MemberId
                            left join [Inexis.Membership].[SubscriptionType] subscripttype on subscripttype.id = membersubscription.SubscriptinId
                            left join [Inexis.Membership].[Subscription] subscript on subscripttype.Id = subscript.Id
                            where member.FirstName like '{searchresult}%' or
                            member.Email like '{searchresult}%' or
                            member.Phone like '{searchresult}%'
                            and member.TenantId = @tenantId
                            and member.RecordState = @recordState
                            order by {term} {order}
                            OFFSET {skip} ROWS 
                            FETCH NEXT {take} ROWS ONLY";


                //and membersubscription.MembershipStatus = @memberStatus

                using (var conn = new DatabaseInfo().Connection())
                {
                    queryResult = conn.Query<MembersViewResult>(query, new
                    {
                        tenantId = request.TenantId,
                        recordState = Enums.RecordStatus.Active,
                        memberStatus = search.Status == 0 ? Enums.MembershipStatus.All : search.Status
                    }).ToList();
                }

                var itemcount = unitOfWokMembership.MemberRepository.TableAsNoTracking.Count();


                //if (search.Status.Equals("inactive"))
                //{
                //    query = queryResult.Where(s => s.MembershipStatus == Enums.MembershipStatus.Inactive);
                //}
                //else if (search.Status.Equals("active"))
                //{
                //    query = queryResult.Where(s => s.MembershipStatus == Enums.MembershipStatus.Active);
                //}
                //else if (search.Status.Equals("unterminated"))
                //{
                //    query = queryResult.Where(s => s.MembershipStatus != Enums.MembershipStatus.Terminated);
                //}
                //else if (search.Status.Equals("pending"))
                //{
                //    query = query.Where(s => s.MembershipStatus == Enums.MembershipStatus.Pending);
                //}



                var pageresult = new PageList<MembersViewResult>
                {
                    Skip = request.Skip,
                    Take = request.Take,
                    Items = queryResult,
                    TotalRecodeCount = itemcount
                };
                return pageresult;
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<MemberInfoViewResult> Read(Request<int> request)
        {
            try
            {
                var item = await unitOfWokMembership.MemberRepository.Read(request);
                var keyValue = (await unitOfWokMembership.MemberFieldKeyValueRepository.TableAsNoTracking
                        .Where(p => p.MemberId == item.Id).ToListAsync())
                    .MapListObject<MemberFieldKeyValue, MemberFieldKeyValueBo>().ToList();
                return new MemberInfoViewResult(request.Item, item.ProfileImage, item.MemberType, keyValue);
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }

        public async Task<List<KeyValueListItem<int>>> ReadKeyValue(Search request)
        {
            try
            {
                return (await (unitOfWokMembership.MemberRepository.TableAsNoTracking.Where(p =>
                            p.TenantId == request.TenantId && p.RecordState == Enums.RecordStatus.Active)
                        .Select(p => new { p.Id, p.Name, p.MemberType }).ToListAsync()))
                    .Select(p => new KeyValueListItem<int>(p.Id, $"{p.Name}({p.MemberType.GetDescription()})"))
                    .ToList();
            }
            catch (Exception e)
            {
                throw e.HandleException();
            }
        }
    }
}
