﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberTypeTabFieldsRenderViewResult
    {
        public Utility.Utility.Enums.MemberType MemberType { get; set; }
        public List<MemberTypeTabsViewReslut> MemberTypeTabs { get; set; }

        public MemberTypeTabFieldsRenderViewResult(Utility.Utility.Enums.MemberType memberType, List<MemberTypeTabsViewReslut> memberTypeTabs)
        {
            MemberType = memberType;
            MemberTypeTabs = memberTypeTabs;
        }
    }

    public class MemberTypeTabsViewReslut
    {
        public Guid Key { get; set; }
        public int Order { get; set; }
        public string DisplayName { get; set; }
        public List<TabsFieldsViewResult> TabsFields { get; set; }
    }

    public class TabsFieldsViewResult
    {
        public Guid Key { get; set; }
        public int DataType { get; set; }
        public int Order { get; set; }
        public string DisplayName { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public bool IsSystemField { get; set; }
    }
}
