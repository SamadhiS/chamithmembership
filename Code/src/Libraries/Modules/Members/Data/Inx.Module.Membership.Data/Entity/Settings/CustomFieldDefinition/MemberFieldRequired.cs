﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition
{
    [Table("MemberFieldRequired", Schema = Constraints.Schema)]
    public class MemberFieldRequired:BaseEntity
    {
        public Guid Key { get; set; }
        public Enums.MemberType MemberType { get; set; }
    }
}
