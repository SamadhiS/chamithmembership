import { AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export function patternValidator(regexp: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const value = control.value;
    if (value === '') {
      return null;
    }
    //  return Observable.of(!regexp.test(value) ? { 'patternInvalid': { regexp } } : null);

    if (!regexp.test(value)) return { "patternInvalid": true }
    return null;
  };
}