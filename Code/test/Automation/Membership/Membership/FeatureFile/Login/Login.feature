﻿Feature: As an Admin, I should be able to log in to the system based on the given credentials and logout
	
@mytag
Scenario Outline: Login with valid credentials
	Given Navigate to Membership Login Page
	When I enter Username as <Email>
	And I Enter Password as <Password>
	And I Click on Sign In
	Then I should be directed to Dashboard
	And I Click on Logout Button
	
Examples:
	| Email                         | Password  |
	| membership.inexis@gmail.com	| Admin@123 |  
	

Scenario Outline: Login with invalid credentials
	Given Navigate to Membership Login
	When Enter Username as <Email>
	And Enter Password as <Password>
	And Click on Sign In & user shown an error message 
Examples:
	| Email                         | Password  |
	| membership@gmail.com          | Admin@123 |
	| membership_inexis@gmail.com	| admin@123 |