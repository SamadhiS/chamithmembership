import { LookupService } from '../../core/services/lookup.service';
import { EmailToken } from '../model/emailToken';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { User } from '../model/user';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { RegisteruserService } from '../service/registeruser.service';
import { ToastsManager } from 'ng2-toastr';
import { patternValidator } from '../../shared/patternvalidator/patternvalidator'
import { PatternModel } from '../../shared/patternvalidator/pattern';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { error } from 'protractor';
import { TenantInfoService } from '../../core/services/tenant-info.service';

@Component({
  selector: 'membership-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  imageLogoSource = 'assets/img/upload.png';

  user = new User();
  emailToken = new EmailToken();
  patternModel = new PatternModel();
  registerForm: FormGroup;
  isRequested = false;
  isTokenValid = true;
  isHostVerified = true;
  orgTypes = [];
  organizationLogo: FileList;
  hostName: string;

  constructor(private fb: FormBuilder, private lookupservice: LookupService, private router: Router, private route: ActivatedRoute, private tenantInfoService: TenantInfoService,
    private registerUser: RegisteruserService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  createForm() {
    this.registerForm = this.fb.group({
      Name: ['', Validators.required],
      TenantTypeId: [null,[Validators.required]],
      Host: ['', [Validators.required, patternValidator(this.patternModel.hostNameRegex)]],
      IsAgreement: ['', Validators.required]
    }, { validator: this.customValidate },
    );
  }
  customValidate(c: AbstractControl) {
    if (c.get('IsAgreement').value == false) {
      return false;
    } else return true;
  }
  hostVerified() {
    console.log(this.hostName)
    this.registerUser.verifyHostName(this.hostName)
      .subscribe(res => {
        if (res.result.item === false) {
          return this.isHostVerified = true;
        }
        else if (res.result.item === true) {
          this.toastr.error('Host name already exists', 'Oops');
          return this.isHostVerified = false;
        }
      }, error => {
        this.toastr.error('Host name is not validated', 'Oops');
      });
  }
  createAccount() {
    if (!this.registerForm.valid) return;
    this.isRequested = true;
    var hostName = this.registerForm.value.Host;
    this.user = Object.assign({}, this.user, this.registerForm.value)
    if (this.isHostVerified && this.isTokenValid) {
      this.registerUser.register(this.organizationLogo, this.user)
        .subscribe(res => {
          this.toastr.success('Account has been successfully created', 'Success');
          localStorage.setItem('Token_id', res.result.item.token);
          this.tenantInfoService.getTenantInfo()
            .subscribe(res => {
              localStorage.setItem('tenantObj', JSON.stringify(res.result));
              this.tenantInfoService.getUserInfo()
                .subscribe(user => {
                  this.isRequested = false;
                  localStorage.setItem('userObj', JSON.stringify(user.result));
                  this.router.navigate(['/dashboard',true]);
                });
            });
            this.lookupservice.getTenantInfo()
            .subscribe(res => {
              localStorage.setItem('tenantfullObj', JSON.stringify(res.result));
            });
        }, error => {
          this.isRequested = false;
          this.toastr.error('Errors in creating the account!', 'Oops');
        })
    } else {
      this.isRequested = false;
      this.toastr.error('Given host name is already exists or Token has been expired', 'Oops');
    }
  }
  loadOrgTypes() {
    this.registerUser.getOrganizationtypes()
      .subscribe(orgtyp => {
        this.orgTypes = orgtyp.result.item;
      }, error => {
        this.toastr.error('Organization types not loaded', 'Oops');
      })
  }
  validateEmail(emailToken) {
    if (emailToken.email === "") return;
    this.registerUser.validateMail(emailToken)
      .subscribe(res => {
        localStorage.setItem('Token_id', res.result.item.token);
        this.isTokenValid = true;
      }, error => {
        this.isTokenValid = false;
      })
  }
  gotoLogin() {
    this.router.navigate(['/login']);
  }
  resendMail() {
    var resendMailObj = { Value: this.emailToken.email }
    this.registerUser.resendMail(resendMailObj)
      .subscribe(res => {
        this.toastr.success('Email has been resend to your email account', 'Success');
      }, error => {
      })
  }
  fileUpload(event) {
    this.organizationLogo = event.target.files;
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageLogoSource = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }
  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.emailToken.email = params['email'];
        this.emailToken.token = params['token'];
      });

    this.validateEmail(this.emailToken);

    if (this.emailToken.token != undefined) {
      this.createForm();
      this.loadOrgTypes();
      let enteredHost = this.registerForm.get('Host');
      enteredHost.valueChanges
        .debounceTime(1000)
        .distinctUntilChanged().subscribe(host => {
          this.hostName = host;
          this.hostVerified();
        })
    }
  }

}
