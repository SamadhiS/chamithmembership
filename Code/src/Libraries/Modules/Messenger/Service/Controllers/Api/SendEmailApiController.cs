﻿using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Module.Messenger.Service.Utility;
using Inx.Module.Messenger.Service.Models;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Inx.Utility.Utility;

namespace Inx.Module.Messenger.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class SendEmailApiController:MessengerBaseApiController
    {
        private readonly IEmailSendService service;
        public SendEmailApiController(IEmailSendService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("email/send")]
        public async Task<IActionResult> Send([FromBody]EmailSendingInfoViewModel model)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                await service.SendEmails(Request<EmailSendingInfoViewModel, EmailSendingInfoModel>(model));
                stopwatch.Stop();
               
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }


    }
}
