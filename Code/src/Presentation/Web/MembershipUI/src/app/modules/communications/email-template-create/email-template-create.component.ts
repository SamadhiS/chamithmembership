import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { EmailTemplateService } from '../../core/services/email-template.service';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { EmailTemplateModel } from '../models/emailTemplateModel';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { EmailTemplateHeaders } from '../models/emailTemplateHeaders';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'membership-email-template-create',
  templateUrl: './email-template-create.component.html',
  styleUrls: ['./email-template-create.component.scss']
})
export class EmailTemplateCreateComponent implements OnInit {

  headers = new EmailTemplateHeaders();
  templateId = 0;
  templateType = 0;
  emailTemplateForm: FormGroup;
  emailTemplateModel = new EmailTemplateModel();
  emailTemplateText = '';
  dragText = "";

  savetext:any;
  edittext:any;
  language="";
  saveBtnText = "Save & Close"
  isEmailTemplateFormSubmitted = false;
  isBlocked = false;

  constructor(private emailTemplateService: EmailTemplateService,
    private toastr: ToastsManager,
    private fb: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);


  }

  ngOnInit() {
    this.getTranslator();
    this.emailTemplateForm = this.getEmailTemplateForm();

    this.activatedRoute.params.subscribe((params: Params) => {
      this.templateId = params['id'];
      this.templateType = params['type'];
      this.emailTemplateModel.emailTemplateTypes = this.templateType
      if (this.templateId && this.templateId != 0) {
        this.saveBtnText = this.edittext;
        this.getTemplate();
      }
      else{
        this.templateId = 0;
        this.saveBtnText = this.savetext;
      }
      if (this.templateType && this.templateType != 0) {
        this.getHeaders();
      }

    });

    this.emailTemplateForm.get("emailBody").valueChanges.subscribe(data => {
      this.emailTemplateText = data;
    })
  }

  getTranslator(){
    this.language = localStorage.getItem('Language');
    if (this.language == "en") {
      this.savetext ="Save & Close";
      this.edittext ="Update Template";
    }
    if (this.language == "sh") {  
    this.savetext = "සුරකින්න සහ වසන්න";
    this.edittext = "යාවත්කාලීන ආකෘතිය";
    }
    if (this.language == "tm") {
    this.savetext = "சேமி மற்றும் மூடு";
    this.edittext = "புதுப்பிப்பு டெம்ப்ளேட்";
    }
  }


  ngOnDestroy() {
    console.log('imgoing')
  }
  getHeaders() {
    this.isBlocked = true;
    this.emailTemplateService.getHeaders(this.templateType).subscribe(data => {

      this.headers = data.result.item;
    }, error => {
      this.isBlocked = false;

    }, () => {
      this.isBlocked = false;
    })
  }

  saveTemplate() {

    this.isEmailTemplateFormSubmitted = true;
    if (!this.emailTemplateForm.valid) return;
    var eamilModel = Object.assign({}, this.emailTemplateModel, this.emailTemplateForm.value)

    this.isBlocked = true;

    if (this.emailTemplateModel.id == 0 || !this.emailTemplateModel.id) {
      this.emailTemplateService.saveTemplate(eamilModel).subscribe(data => {

        this.router.navigate(['/communication'])

      }, error => {
        this.toastr.error(ToasterMessages.saveError('Email template'), toasterHeadings.Error);
        this.isBlocked = false;
      }, () => {
        this.isBlocked = false;
      })
    }

    else {
      this.updateTemplate(eamilModel)
    }


  }

  updateTemplate(eamilModel) {
    this.isBlocked = true;
    this.emailTemplateService.updateTemplate(eamilModel).subscribe(data => {

      this.router.navigate(['/communication'])

    }, error => {
      this.toastr.error(ToasterMessages.updateError('Email template'), toasterHeadings.Error);
      this.isBlocked = false;
    }, () => {
      this.isBlocked = false;
    })
  }

  getTemplate() {
    this.isBlocked = true;
    this.emailTemplateService.getTemplate(this.templateId).subscribe(data => {
      this.patchEmailTemplateForm(data.result.item)
      this.isBlocked = false;
      // this.emailTemplateText = data.result.item.emailBody;

    }, error => {
      this.toastr.error(ToasterMessages.loadError('Email template'), toasterHeadings.Error);
    })
  }


  drag(ev) {
    this.dragText = ev.target.innerText;
  }

  drop(ev) {
    ev.preventDefault()
    this.insertTextAtCursor(this.dragText)
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  private getEmailTemplateForm() {

    return this.fb.group({
      name: ["", [Validators.required]],
      emailBody: ["", [Validators.required]],

    })
  }

  private patchEmailTemplateForm(emailModel: EmailTemplateModel) {

    this.emailTemplateModel = emailModel;
    this.emailTemplateForm.patchValue({
      emailBody: emailModel.emailBody,
      name: emailModel.name

    })
  }


  private insertTextAtCursor(text) {

    var sel, range, html;

    if (window.getSelection) {
      sel = window.getSelection();
      let isEditorFocused = sel.focusNode.parentElement.offsetParent.className.indexOf("ql-container")
      if (isEditorFocused == -1) return;

      if (sel.getRangeAt && sel.rangeCount) {
        range = sel.getRangeAt(0);
        range.deleteContents();
        range.insertNode(document.createTextNode(text));
      }
    } else if (document['selection'] && document['selection'].createRange) {
      document['selection'].createRange().text = text;
    }


  }


}
