﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Inx.Module.Contacts.Core.Bo.User;
using Inx.Module.Contacts.Service.Models.Contacts;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Inx.Module.Contacts.Core.Service.Contacts.interfaces;
using Inx.Module.Contacts.Service.Utility;
using Inx.Service.Base.Attributes;
using Newtonsoft.Json;
using Inx.Utility.Utility;

namespace Inx.Module.Contacts.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class ContactsApiController : ContactsBaseApiController, IBaseApi<ContactViewModel, int>
    {
        private readonly IContactService service;
        public ContactsApiController(IContactService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("contacts"), ModelValidation]
        public async Task<IActionResult> Create()
        {
            var imagename = string.Empty;
            try
            {
                var httpRequest = HttpContext.Request;
                var contactViewData = httpRequest.Form["contactViewData"];
                var item = JsonConvert.DeserializeObject<ContactViewModel>(contactViewData);
                var result = await UploadBlob(Enums.FileType.ProfileImageDefault);
                if (result.FileIsThere)
                {
                    imagename = result.FileNames[0];
                    await UploadBlob(Enums.FileType.ProfileImageThumbnail, imagename);
                    item.SetProfileImage(result.FileNames[0]);
                }
                return await Create<ContactViewModel, ContactBo, IContactService>(item, service);
            }
            catch (Exception e)
            {
                //if any happen delete images if exist.
                if (!string.IsNullOrEmpty(imagename))
                {
                    Hangfire.BackgroundJob.Enqueue(() => DeleteBlob(Enums.FileType.ProfileImageDefault, imagename));
                    Hangfire.BackgroundJob.Enqueue(() => DeleteBlob(Enums.FileType.ProfileImageThumbnail, imagename));
                }

                return await HandleException(e);
            }
        }

        [HttpDelete, Route("contacts/{id:int}"), Description("member will be archive")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<ContactBo, IContactService>(id, service);
        }
        [HttpPut, Route("contacts/{id:int}/restore"), Description("member will be archive")]
        public async Task<IActionResult> Restore(int id)
        {
            await service.Restore(Request(id));
            return Ok();
        }
        [HttpGet, Route("contacts/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<ContactViewModel, ContactBo, IContactService>(id, service);
        }
        [HttpGet, Route("contacts"), ModelValidation]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Search<ContactsViewResult, IContactService>(SearchRequest(skip, take, searchTerm: search, orderByTerm: orderby), service);
        }
        [HttpGet, Route("contactsArchive"), ModelValidation]
        public async Task<IActionResult> ReadArchive(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            try
            {
                return Ok(await service.SearchArchive(SearchRequest(skip, take, searchTerm: search, orderByTerm: orderby)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }

        }
        [HttpPut, Route("contacts/contacts"), ModelValidation]
        public async Task<IActionResult> Update([FromBody] ContactInfoViewModel item)
        {
            try
            {
                await service.Update(Request<ContactInfoViewModel, ContactInfoBo>(item));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpPut, Route("contacts/working"), ModelValidation]
        public async Task<IActionResult> Update([FromBody] WorkingInfoViewModel item)
        {
            try
            {
                await service.Update(Request<WorkingInfoViewModel, WorkingInfoBo>(item));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        [HttpPut, Route("contacts/person"), ModelValidation]
        public async Task<IActionResult> Update()
        {
            try
            {
                var httpRequest = HttpContext.Request;
                var personViewData = httpRequest.Form["personViewData"];
                var item = JsonConvert.DeserializeObject<PersonInfoViewModel>(personViewData);
                var result = await UploadBlob(Enums.FileType.ProfileImageDefault);
                if (result.FileIsThere)
                {
                    await UploadBlob(Enums.FileType.ProfileImageThumbnail, result.FileNames[0]);
                    item.ProfileImage = result.FileNames[0];
                }
                await service.Update(Request<PersonInfoViewModel, PersonInfoBo>(item));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }
        #region NotImplemented
        [ApiExplorerSettings(IgnoreApi = true)]
        public new Task<IActionResult> Update(ContactViewModel item)
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<IActionResult> ReadKeyValue()
        {
            throw new NotImplementedException();
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public new Task<IActionResult> Create(ContactViewModel item)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
