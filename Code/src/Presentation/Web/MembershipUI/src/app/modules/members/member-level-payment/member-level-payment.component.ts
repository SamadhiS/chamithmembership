import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { PaymentModel } from '../../payments/models/paymentModel';
import { MembersService } from '../../core/services/members.service';
import { MemberSubscriptionService } from '../services/member-subscription.service';
import { PaymentService } from '../../core/services/payment.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { ActivatedRoute, Router, Params } from '../../../../../node_modules/@angular/router';
import { LevelService } from '../../core/services/level.service';
import { ToastsManager } from '../../../../../node_modules/ng2-toastr';
import { MemberType } from '../../core/enums/memberType';
import { PaymentStates } from '../../core/enums/paymentState';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { TimeLine } from '../models/memberTimelineModel';
import { LevelInfoModel } from '../models/levelInfoModel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'membership-member-level-payment',
  templateUrl: './member-level-payment.component.html',
  styleUrls: ['./member-level-payment.component.scss']
})
export class MemberLevelPaymentComponent implements OnInit {

  paymentForm: FormGroup;

  payment = new PaymentModel();
  timeLine = new TimeLine();
  levelInfo = new LevelInfoModel();

  transActionTypes = [];
  paymentTypes = [];

  memberType: MemberType;
  subscriptionChanged: string = null;

  isPackageUtilized = false;
  isBlocked = false;
  isPayLater = false;
  isEdit = false;

  @Input() memberId: number = 0;
  @Input() levelPrice: string;

  constructor(private fb: FormBuilder, private memberService: MembersService, private memberSubscriptionService: MemberSubscriptionService,
    private router: Router, private activatedRoute: ActivatedRoute, private blockUiService: BlockUiService,
    private paymentService: PaymentService, private levelService: LevelService, private toastr: ToastsManager, vcr: ViewContainerRef) {
  }

  ngOnInit() {
    this.paymentForm = this.getPaymentForm();
    this.getMemberLookupData();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberType = params['type'];

      // this.paymentForm.patchValue({
      //   amount: this.levelPrice
      // })

      if (params['id'] != 0 && params['id'] != undefined) {
        this.memberId = params['id'];
        this.subscriptionChanged = params['state'];
        this.isEdit = true;
      }
    });
  }

  private getPaymentForm() {
    return this.fb.group({
      transActionType: [1, [Validators.required]],
      paymentType: [1, [Validators.required]],
      amount: [{ value: this.levelPrice, disabled: true }, [Validators.required]],
      description: [""],
    })
  }

  private getMemberLookupData() {
    Observable.forkJoin(
      this.paymentService.getTransActionTypes(),
      this.paymentService.getPaymentTypes(),
    ).subscribe(data => {
      this.transActionTypes = data[0].result.item;
      this.paymentTypes = data[1].result.item;
    });
  }

  save() {
    this.isBlocked = true;
    debugger;
    this.payment.paymentState = this.isPayLater ? PaymentStates.Pending : PaymentStates.Paid;
    var payment = Object.assign({}, this.payment, this.paymentForm.value)

    this.paymentService.savePaymnets(payment)
      .mergeMap(payment => {
        return this.levelService.changeMemberStatus(payment.result);
      }).
      // .mergeMap(res => {
      //   return this.memberService.saveTimeLine(this.timeLine)
      // }).
      subscribe(response => {
        this.toastr.success(ToasterMessages.save('Member'), toasterHeadings.Success);
        this.isBlocked = false;
        setTimeout(() => {
          this.router.navigate(["/members"])
        }, 1000);

      }, error => {
        this.isBlocked = false;
        this.toastr.error(ToasterMessages.saveError('Member'), toasterHeadings.Error);
      });
  }
}
