﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Inx.Data.Base.Utility;
using Inx.Module.Identity.Data.Entity.Application;
using Inx.Utility.Utility;

namespace Inx.Module.Membership.Data.Entity.Members.Master
{
    [Table("Member", Schema = Constraints.Schema)]
    public class Member:AuditableEntity
    {
        public Enums.MemberType MemberType { get; set; }
        [Required, StringLength(100)]
        public string ProfileImage { get; set; } = Constants.ProfileNoImage;
        [EmailAddress,StringLength(DbConstraints.EmailLength),Required]
        public string Email { get; set; }
        [StringLength(DbConstraints.PhoneLength),Required]
        public string Phone { get; set; }
        [StringLength(DbConstraints.NameLength), Required]
        public string FirstName { get; set; }
        [StringLength(DbConstraints.NameLength)]
        public string LastName { get; set; }
        public int? SubscriptionId { get; set; } = 0;

        public Member(string email,string phone,string fname,string lname)
        {
            MemberType = Enums.MemberType.Person;
            Email = email;
            Phone = phone;
            FirstName = fname;
            LastName = lname;
            Name = $"{fname} {lname}";
        }

        public Member(string email, string phone, string fname, string lname, string orgname)
        {
            MemberType = Enums.MemberType.Person;
            Email = email;
            Phone = phone;
            FirstName = fname;
            LastName = lname;
            Name = orgname;
        }

        public Member()
        {
            
        }
    }
}
