﻿Feature: As an Admin, I should be able to retrieve the password "Forget Password" option.

@mytag
Scenario Outline: As an Admin, I should be able to retrieve the password "Forget Password" option.
Given Load Membership login
And I click on Reset Here
And I enter email as <Email>
And I click on Reset Password

Examples: 
| Email                       |
| membership.inexis@gmail.com |


Scenario Outline: As an Admin, I should be able to retrieve the password "Forget Password" option & go back to ForgotPassword
Given Load Membership login page
And I click on Reset here
And I entered email as <Email>
And I clicked on Reset Password
And click on Back button
Examples: 
| Email                       |
| membership.inexis@gmail.com |

Scenario Outline: As an Admin, I should be able to retrieve the password "Forget Password" option & go back to login
Given Load Membership login page
And I click on Reset here
And I entered email as <Email>
And I clicked on Reset Password
#And click on Back button
And click on back to login button
And Entered valid crefentials "membership.inexis@gmail.com" and "Admin@123"
And click on Sign In button
And navigated to dashboard
And click on signout button

Examples: 
| Email                       |
| membership.inexis@gmail.com |