﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inx.Core.Base;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Data.DbContext;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition
{
    public class MemberTabFieldService : MemberBaseService, IMemberTabFieldService
    {
        private readonly IUnitOfWokMembership uowMembership;
        public MemberTabFieldService(IUnitOfWokMembership _uowMembership,ICoreInjector icoreInjector):base(icoreInjector)
        {
            uowMembership = _uowMembership;
        }
        public Task<MemberTabFieldBo> Create(Request<MemberTabFieldBo> request)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Request<int> request)
        {
            throw new NotImplementedException();
        }

        public Task<PageList<MemberTabFieldBo>> Read(Search search)
        {
            throw new NotImplementedException();
        }

        public Task<MemberTabFieldBo> Read(Request<int> request)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MemberTabFieldBo>> ReadDefault()
        {
            try
            {
                return (await uowMembership.MemberTabFieldsRepository.TableAsNoTracking
                        .Where(p => p.TenantId == 0).ToListAsync())
                    .MapListObject<MemberTabField, MemberTabFieldBo>().ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public Task<List<KeyValueListItem<int>>> ReadKeyValue(Search request)
        {
            throw new NotImplementedException();
        }

        public Task Update(Request<MemberTabFieldBo> request)
        {
            throw new NotImplementedException();
        }
    }
}
