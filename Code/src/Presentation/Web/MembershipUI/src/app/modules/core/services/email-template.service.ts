import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';


@Injectable()
export class EmailTemplateService extends BaseService {

  private apiEndPoint = `${this.baseApiEndPoint}messenger/v1/emailtemplate`;
  constructor(private http: HttpClient) {
    super()
  }


  getHeaders(type) {
    return this.http.get(`${this.apiEndPoint}/headers/${type}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getTemplates(skip, take, search, orderby) {
    return this.http.get(`${this.apiEndPoint}?skip=${skip}&take=${take}&search=${search}&orderby=${orderby}`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getAllTemplates() {
    return this.http.get(`${this.apiEndPoint}/keyvalue`)
      .map((response) => response)
      .catch(this.errorHandler)
  }

  getTemplate(id) {
    return this.http.get(`${this.apiEndPoint}/${id}`).map(res => res).catch(this.errorHandler)
  }

  saveTemplate(template) {
    return this.http.post(this.apiEndPoint, template).map(res => res).catch(this.errorHandler)
  }


  getTemplateTypes() {
    return this.http.get(`${this.apiEndPoint}/types`).map(res => res).catch(this.errorHandler)
  }

  updateTemplate(template) {
    return this.http.put(this.apiEndPoint, template).map(res => res).catch(this.errorHandler)
  }

  sendEmail(emailModel) {
    return this.http.post(`${this.baseApiEndPoint}membership/v1/members/emailsend`, emailModel).map(res => res).catch(this.errorHandler)
  }
  deleteTemplate() {

  }

}

