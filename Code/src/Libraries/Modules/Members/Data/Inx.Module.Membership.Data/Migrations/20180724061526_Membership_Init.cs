﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Membership.Data.Migrations
{
    public partial class Membership_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.EnsureSchema(
                name: "Inexis.Membership");

            migrationBuilder.CreateTable(
                name: "MemberFieldRequired",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Key = table.Column<Guid>(nullable: false),
                    MemberType = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberFieldRequired", x => x.Id);
                });

           
            migrationBuilder.CreateTable(
                name: "Group",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Group_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Group_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Group_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Member",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    MemberType = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Phone = table.Column<string>(maxLength: 20, nullable: false),
                    ProfileImage = table.Column<string>(maxLength: 100, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    SubscriptionId = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Member", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Member_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Member_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Member_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberFieldDefinition",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DataType = table.Column<int>(nullable: false),
                    DisplayName = table.Column<string>(maxLength: 50, nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    IsRequired = table.Column<bool>(nullable: false),
                    Key = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(maxLength: 7000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberFieldDefinition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberFieldDefinition_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFieldDefinition_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFieldDefinition_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MembershipTimeline",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<double>(nullable: true),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    MemberId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    PaymentId = table.Column<int>(nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    SubscriptionId = table.Column<int>(nullable: true),
                    SubscriptionName = table.Column<string>(nullable: true),
                    TemplateType = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MembershipTimeline", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MembershipTimeline_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MembershipTimeline_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MembershipTimeline_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberTabDefinition",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DisplayName = table.Column<string>(maxLength: 50, nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    IsVisible = table.Column<bool>(nullable: false),
                    Key = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberTabDefinition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberTabDefinition_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTabDefinition_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTabDefinition_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubscriptionType",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscriptionType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubscriptionType_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubscriptionType_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubscriptionType_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberFieldKeyValue",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Key = table.Column<Guid>(nullable: false),
                    MemberId = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(maxLength: 7000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberFieldKeyValue", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberFieldKeyValue_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFieldKeyValue_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFieldKeyValue_Member_MemberId",
                        column: x => x.MemberId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFieldKeyValue_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberFiles",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    FileName = table.Column<string>(maxLength: 100, nullable: true),
                    FileSaveName = table.Column<string>(maxLength: 100, nullable: true),
                    MemberId = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    Size = table.Column<decimal>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberFiles_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFiles_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFiles_Member_MemberId",
                        column: x => x.MemberId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberFiles_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberGroups",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    GroupId = table.Column<int>(nullable: false),
                    MemberId = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberGroups_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberGroups_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberGroups_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberGroups_Member_MemberId",
                        column: x => x.MemberId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberGroups_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrganizationInfo",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    OrganizationName = table.Column<string>(maxLength: 50, nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrganizationInfo_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrganizationInfo_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrganizationInfo_Member_Id",
                        column: x => x.Id,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrganizationInfo_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PersonalInfo",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonalInfo_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalInfo_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalInfo_Member_Id",
                        column: x => x.Id,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonalInfo_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberTabField",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    FieldId = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TabId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberTabField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberTabField_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTabField_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTabField_MemberFieldDefinition_FieldId",
                        column: x => x.FieldId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "MemberFieldDefinition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTabField_MemberTabDefinition_TabId",
                        column: x => x.TabId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "MemberTabDefinition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTabField_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberTypeTab",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    MemberType = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TabId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberTypeTab", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberTypeTab_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTypeTab_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTypeTab_MemberTabDefinition_TabId",
                        column: x => x.TabId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "MemberTabDefinition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberTypeTab_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Capacity = table.Column<int>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    EmaillTemplateId = table.Column<int>(nullable: false),
                    HasApproval = table.Column<bool>(nullable: false),
                    IsLimited = table.Column<bool>(nullable: false),
                    IsRenewable = table.Column<bool>(nullable: false),
                    IsSendEmail = table.Column<bool>(nullable: false),
                    MembershipExpired = table.Column<byte>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    RenewalDays = table.Column<int>(nullable: false),
                    SubscriptionFrom = table.Column<DateTime>(nullable: false),
                    SubscriptionTo = table.Column<DateTime>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscription_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscription_SubscriptionType_Id",
                        column: x => x.Id,
                        principalSchema: "Inexis.Membership",
                        principalTable: "SubscriptionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscription_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberSubscription",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    IsRenewed = table.Column<bool>(nullable: false),
                    JoinDate = table.Column<DateTime>(nullable: false),
                    MemberId = table.Column<int>(nullable: false),
                    MembershipStatus = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    RenewDate = table.Column<DateTime>(nullable: false),
                    SubscriptinId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberSubscription", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberSubscription_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberSubscription_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberSubscription_Member_MemberId",
                        column: x => x.MemberId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Member",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberSubscription_Subscription_SubscriptinId",
                        column: x => x.SubscriptinId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "Subscription",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MemberSubscription_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TerminationNote",
                schema: "Inexis.Membership",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    MemberSubscriptionId = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TerminationNote", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TerminationNote_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TerminationNote_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TerminationNote_MemberSubscription_MemberSubscriptionId",
                        column: x => x.MemberSubscriptionId,
                        principalSchema: "Inexis.Membership",
                        principalTable: "MemberSubscription",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TerminationNote_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Group_CreatedById",
                schema: "Inexis.Membership",
                table: "Group",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Group_EditedById",
                schema: "Inexis.Membership",
                table: "Group",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_Group_TenantId",
                schema: "Inexis.Membership",
                table: "Group",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Group_Name_TenantId",
                schema: "Inexis.Membership",
                table: "Group",
                columns: new[] { "Name", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Member_CreatedById",
                schema: "Inexis.Membership",
                table: "Member",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Member_EditedById",
                schema: "Inexis.Membership",
                table: "Member",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_Member_TenantId",
                schema: "Inexis.Membership",
                table: "Member",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldDefinition_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldDefinition_EditedById",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldDefinition_TenantId",
                schema: "Inexis.Membership",
                table: "MemberFieldDefinition",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldKeyValue_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberFieldKeyValue",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldKeyValue_EditedById",
                schema: "Inexis.Membership",
                table: "MemberFieldKeyValue",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldKeyValue_MemberId",
                schema: "Inexis.Membership",
                table: "MemberFieldKeyValue",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldKeyValue_TenantId",
                schema: "Inexis.Membership",
                table: "MemberFieldKeyValue",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldKeyValue_Key_TenantId_MemberId",
                schema: "Inexis.Membership",
                table: "MemberFieldKeyValue",
                columns: new[] { "Key", "TenantId", "MemberId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MemberFieldRequired_Key_TenantId_MemberType",
                schema: "Inexis.Membership",
                table: "MemberFieldRequired",
                columns: new[] { "Key", "TenantId", "MemberType" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MemberFiles_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberFiles",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFiles_EditedById",
                schema: "Inexis.Membership",
                table: "MemberFiles",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFiles_MemberId",
                schema: "Inexis.Membership",
                table: "MemberFiles",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberFiles_TenantId",
                schema: "Inexis.Membership",
                table: "MemberFiles",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberGroups_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberGroups",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberGroups_EditedById",
                schema: "Inexis.Membership",
                table: "MemberGroups",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberGroups_GroupId",
                schema: "Inexis.Membership",
                table: "MemberGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberGroups_MemberId",
                schema: "Inexis.Membership",
                table: "MemberGroups",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberGroups_TenantId",
                schema: "Inexis.Membership",
                table: "MemberGroups",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MembershipTimeline_CreatedById",
                schema: "Inexis.Membership",
                table: "MembershipTimeline",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MembershipTimeline_EditedById",
                schema: "Inexis.Membership",
                table: "MembershipTimeline",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MembershipTimeline_TenantId",
                schema: "Inexis.Membership",
                table: "MembershipTimeline",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberSubscription_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberSubscription",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberSubscription_EditedById",
                schema: "Inexis.Membership",
                table: "MemberSubscription",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberSubscription_MemberId",
                schema: "Inexis.Membership",
                table: "MemberSubscription",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberSubscription_SubscriptinId",
                schema: "Inexis.Membership",
                table: "MemberSubscription",
                column: "SubscriptinId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberSubscription_TenantId",
                schema: "Inexis.Membership",
                table: "MemberSubscription",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabDefinition_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabDefinition_EditedById",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabDefinition_TenantId",
                schema: "Inexis.Membership",
                table: "MemberTabDefinition",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabField_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberTabField",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabField_EditedById",
                schema: "Inexis.Membership",
                table: "MemberTabField",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabField_FieldId",
                schema: "Inexis.Membership",
                table: "MemberTabField",
                column: "FieldId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabField_TabId",
                schema: "Inexis.Membership",
                table: "MemberTabField",
                column: "TabId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTabField_TenantId",
                schema: "Inexis.Membership",
                table: "MemberTabField",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTypeTab_CreatedById",
                schema: "Inexis.Membership",
                table: "MemberTypeTab",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTypeTab_EditedById",
                schema: "Inexis.Membership",
                table: "MemberTypeTab",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTypeTab_TabId",
                schema: "Inexis.Membership",
                table: "MemberTypeTab",
                column: "TabId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTypeTab_TenantId",
                schema: "Inexis.Membership",
                table: "MemberTypeTab",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationInfo_CreatedById",
                schema: "Inexis.Membership",
                table: "OrganizationInfo",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationInfo_EditedById",
                schema: "Inexis.Membership",
                table: "OrganizationInfo",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationInfo_TenantId",
                schema: "Inexis.Membership",
                table: "OrganizationInfo",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalInfo_CreatedById",
                schema: "Inexis.Membership",
                table: "PersonalInfo",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalInfo_EditedById",
                schema: "Inexis.Membership",
                table: "PersonalInfo",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalInfo_TenantId",
                schema: "Inexis.Membership",
                table: "PersonalInfo",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_CreatedById",
                schema: "Inexis.Membership",
                table: "Subscription",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_EditedById",
                schema: "Inexis.Membership",
                table: "Subscription",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_TenantId",
                schema: "Inexis.Membership",
                table: "Subscription",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionType_CreatedById",
                schema: "Inexis.Membership",
                table: "SubscriptionType",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionType_EditedById",
                schema: "Inexis.Membership",
                table: "SubscriptionType",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionType_TenantId",
                schema: "Inexis.Membership",
                table: "SubscriptionType",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionType_Name_TenantId",
                schema: "Inexis.Membership",
                table: "SubscriptionType",
                columns: new[] { "Name", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TerminationNote_CreatedById",
                schema: "Inexis.Membership",
                table: "TerminationNote",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_TerminationNote_EditedById",
                schema: "Inexis.Membership",
                table: "TerminationNote",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_TerminationNote_MemberSubscriptionId",
                schema: "Inexis.Membership",
                table: "TerminationNote",
                column: "MemberSubscriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_TerminationNote_TenantId",
                schema: "Inexis.Membership",
                table: "TerminationNote",
                column: "TenantId");
 
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MemberFieldKeyValue",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberFieldRequired",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberFiles",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberGroups",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MembershipTimeline",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberTabField",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberTypeTab",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "OrganizationInfo",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "PersonalInfo",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "TerminationNote",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "Group",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberFieldDefinition",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberTabDefinition",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "MemberSubscription",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "Member",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "Subscription",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "SubscriptionType",
                schema: "Inexis.Membership");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Tenants",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "TenantTypes",
                schema: "Inexis.Security");
        }
    }
}
