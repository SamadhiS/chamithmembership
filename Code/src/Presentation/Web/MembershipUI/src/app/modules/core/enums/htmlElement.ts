export enum HTMLElements {
    text = 1,
    textarea = 2,
    checkbox = 3,
    checkboxGroup = 4,
    radio = 5,
    radioButtonGroup = 6,
    dropdown = 7,
    calendar = 8,
    file = 9,
}