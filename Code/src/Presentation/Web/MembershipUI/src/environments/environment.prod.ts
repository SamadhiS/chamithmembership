export const environment = {
  production: true,
  message: 'production',
   
  apiEndPoint:'https://membershipinexisapitest.azurewebsites.net/api/',
  apiBaseEndPoint: 'https://membershipinexisapitest.azurewebsites.net/',
};
