﻿using Inx.Module.Messenger.Data.Entity;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Messenger.Data.DbContext
{
    public interface IMessengerDbSet
    {
        DbSet<EmailTemplate> EmailTemplate { get; set; }
    }
}
