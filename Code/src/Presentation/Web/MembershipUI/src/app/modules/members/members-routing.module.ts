import { MemberGroupDetailsComponent } from './member-group-details/member-group-details.component';
import { MemberLevelsComponent } from './member-levels/member-levels.component';
import { MemberRestoreComponent } from './member-restore/member-restore.component';
import { MemberLevelInformationComponent } from './member-level-information/member-level-information.component';
import { MemberLevelCreateComponent } from './member-level-create/member-level-create.component';
import { MemberGroupComponent } from './member-group/member-group.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { MemberCreateComponent } from './member-create/member-create.component';
import { MemberProfileComponent } from './member-profile/member-profile.component';
import { MemberCreateCustomComponent } from './member-create-custom/member-create-custom.component';
const routes: Routes = [
  { path: '', component: MemberDetailsComponent },
  // { path: 'createMember', component: MemberCreateComponent },
  { path: 'createGroup', component: MemberGroupComponent },
  { path: 'createGroup/:id', component: MemberGroupComponent },
  // { path: 'updateMember/:id', component: MemberCreateComponent },
  // { path: 'updateMember/:id/:state', component: MemberCreateComponent },
  { path: 'createMemberlevel', component: MemberLevelCreateComponent },
  { path: 'memberprofile/:id', component: MemberProfileComponent },
  { path: 'createMemberlevel', component: MemberLevelCreateComponent },
  { path: 'memberlevelInformation/:id', component: MemberLevelInformationComponent },
  { path: 'memberRestore', component: MemberRestoreComponent },
  { path: 'memberLevels', component: MemberLevelsComponent },
  { path: 'memberGroup', component: MemberGroupDetailsComponent },

  { path: 'createMember/:type', component: MemberCreateCustomComponent },
  { path: 'updateMember/:type/:id', component: MemberCreateCustomComponent },
  { path: 'updateMember/:type/:id/:state', component: MemberCreateCustomComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule { }
