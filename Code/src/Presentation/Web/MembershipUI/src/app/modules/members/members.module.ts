import { MemberLevelInformationComponent } from './member-level-information/member-level-information.component';
import { MemberGroupComponent } from './member-group/member-group.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MembersRoutingModule } from './members-routing.module';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { MemberCreateComponent } from './member-create/member-create.component';
import { MemberGroupDetailsComponent } from './member-group-details/member-group-details.component';
import { MemberLevelsComponent } from './member-levels/member-levels.component';
import { MemberLevelCreateComponent } from './member-level-create/member-level-create.component';
import { MemberDocumentComponent } from './member-document/member-document.component';
import { MemberSubscriptionService } from './services/member-subscription.service';
import { MemberProfileComponent } from './member-profile/member-profile.component';
import { MemberRestoreComponent } from './member-restore/member-restore.component';
import { MemberGroupMemberComponent } from './member-group-member/member-group-member.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/multiselect.component';
import { TranslateModule } from '@ngx-translate/core';
import { MemberCardViewComponent } from './member-card-view/member-card-view.component';
import { MemberCreateCustomComponent } from './member-create-custom/member-create-custom.component';
import { RenderViewService } from './services/render-view.service';
import { CutomMemberService } from './services/member.service';
import { MemberLevelSubscribeComponent } from './member-level-subscribe/member-level-subscribe.component';
import { MemberLevelPaymentComponent } from './member-level-payment/member-level-payment.component';
@NgModule({
  imports: [
    CommonModule,
    MembersRoutingModule,
    SharedModule,
    TranslateModule,
    AngularMultiSelectModule,
    BsDatepickerModule.forRoot(),
  ],
  declarations: [MemberDetailsComponent, MemberCreateComponent, MemberGroupComponent, MemberGroupDetailsComponent, MemberLevelsComponent, MemberLevelCreateComponent, MemberDocumentComponent, MemberProfileComponent, MemberLevelInformationComponent, MemberRestoreComponent, MemberGroupMemberComponent, MemberCardViewComponent, MemberCreateCustomComponent, MemberLevelSubscribeComponent, MemberLevelPaymentComponent],
  providers: [MemberSubscriptionService, RenderViewService, CutomMemberService]
})
export class MembersModule { }
