﻿using System.Threading.Tasks;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Inx.Service.Base.Attributes;
using Inx.Module.Messenger.Service.Models;
using Inx.Module.Messenger.Core.Bo.Emails;
using Inx.Module.Messenger.Core.Service.Interfaces;
using Inx.Module.Messenger.Service.Utility;
using static Inx.Utility.Utility.Enums;
using Inx.Service.Base.Models;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Inx.Utility.Extentions;
using Inx.Utility.Models;
using Inx.Utility.Utility;

namespace Inx.Module.Messenger.Service.Controllers.Api
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class EmailTemplateApiController : MessengerBaseApiController, IBaseApi<EmailTemplateViewModel, int>
    {

        private readonly IEmailTemplateService service;
        public EmailTemplateApiController(IEmailTemplateService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }
        [HttpPost, Route("emailtemplate"), ModelValidation]
        public async Task<IActionResult> Create([FromBody]  EmailTemplateViewModel item)
        {
            return await Create<EmailTemplateViewModel, EmailTemplateBo, IEmailTemplateService>(item, service);
        }
        [HttpDelete, Route("emailtemplate/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<EmailTemplateBo, IEmailTemplateService>(id, this.service);
        }
        [HttpGet, Route("emailtemplate/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<EmailTemplateViewModel, EmailTemplateBo, IEmailTemplateService>(id, this.service);
        }
        [HttpGet, Route("emailtemplate")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            return await Read<EmailTemplateViewModel, EmailTemplateBo, IEmailTemplateService>(SearchRequest(skip,
                take, searchTerm: search, orderByTerm: orderby), service);
        }


        [HttpGet, Route("emailtemplate/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            return await ReadKeyValue<EmailTemplateBo, IEmailTemplateService>(service);
        }
        [HttpPut, Route("emailtemplate"), ModelValidation]
        public async Task<IActionResult> Update([FromBody]  EmailTemplateViewModel item)
        {
            return await Update<EmailTemplateViewModel, EmailTemplateBo, IEmailTemplateService>(item, this.service);
        }

        [HttpGet, Route("emailtemplate/headers/{type:int}")]
        public async Task<IActionResult> Read(EmailTemplateTypes type)
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var result = service.GetColomHeaders(type);
                stopwatch.Stop();
                return Ok(new Response<EmailHeaderBo>
                {
                    Item = result,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

       
        [HttpGet, Route("emailtemplate/types")]
        public async Task<IActionResult> Read()
        {
            try
            {
                var stopwatch = Stopwatch.StartNew();
                var emailTypes = EnumsExtention.GetList<EmailTemplateTypes>();
                stopwatch.Stop();
                return Ok(new Response<List<EnumMember>>
                {
                    Item = emailTypes,
                    TimeTaken = stopwatch.ElapsedMilliseconds
                });
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

    }
}
