import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberLevelPaymentComponent } from './member-level-payment.component';

describe('MemberLevelPaymentComponent', () => {
  let component: MemberLevelPaymentComponent;
  let fixture: ComponentFixture<MemberLevelPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberLevelPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberLevelPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
