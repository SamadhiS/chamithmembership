﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Threading;

namespace Membership.PageObjects.DashboardPO
{
    class DashboardPO
    {
        private NgWebElement Btn_SevenDays(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnWeek"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_SevenDays_Click(NgWebDriver d)
        {
            this.Btn_SevenDays(d).Click();
        }
        private NgWebElement Btn_ThreeMonths(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnThreeMonths"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ThreeMonths_Click(NgWebDriver d)
        {
            this.Btn_ThreeMonths(d).Click();
        }
        private NgWebElement Btn_SixMonths(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnSixMonths"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_SixMonths_Click(NgWebDriver d)
        {
            this.Btn_SixMonths(d).Click();
        }
        private NgWebElement Btn_OneYear(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnOneYear"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_OneYear_Click(NgWebDriver d)
        {
            this.Btn_OneYear(d).Click();
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
