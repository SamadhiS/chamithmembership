import { LevelService } from './../../core/services/level.service';
import { LookupService } from '../../core/services/lookup.service';
import { BlockUiService } from '../../core/services/block-ui.service'; import { MemberSubscriptionService } from '../services/member-subscription.service';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ViewContainerRef, Output, EventEmitter } from '@angular/core';

import { patternValidator } from '../../shared/patternvalidator/patternvalidator';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { MembershipLevelStates } from '../../core/enums/membershipLevelState';
import { MembersService } from '../../core/services/members.service';
import { MemberLevelRenewalModel } from '../models/memberLevelRenewalModel';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MemberLevelModel } from '../models/memberLevelModel';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-member-level-information',
  templateUrl: './member-level-information.component.html',
  styleUrls: ['./member-level-information.component.scss']
})
export class MemberLevelInformationComponent implements OnInit {

  @Output() memberLevelCounts: EventEmitter<any> = new EventEmitter();

  customValidate: any;
  isRequested: boolean;
  updateClick: boolean = false;
  showEmailtype: boolean = false;
  showEmail: boolean = false;
  showLable: boolean = false
  isRenewable: boolean;
  Renewable = 0;
  limitDisable: boolean;
  dateCreated: Date;
  memberLevelName = "";
  memberLevelDescription = "No Description Availble";
  deactive = "Deactive";
  active = "Active";
  isBlocked = false;
  memberLevelCount = 0;
  memberlevelId = 0;
  memberId = 0;
  subscriptionId = 0;
  allocatedActiveCount = 0;
  allocatedPendingCount = 0;
  allocatedInactiveCount = 0;
  allocatedTerminatedCount = 0;
  limit = "0";
  dateRangeRadio = "0";
  packageV = "2"
  dateV = "1"
  membershipExpired = "0";
  limited = "1";
  unlimited = "2";
  membershipLevelState = MembershipLevelStates;
  memberlevel = [];
  templates = []
  minDate = new Date();
  memberLevelForm: FormGroup;
  memberLevelInformationForm: FormGroup;
  memberLevelInrenewalForm: FormGroup;
  memberLevel = new MemberLevelModel();
  memberLevelRenewa = new MemberLevelRenewalModel();
  currency='';

  constructor(private fb: FormBuilder, private memberSubscriptionService: MemberSubscriptionService,
    private levelService: LevelService,
    private lookUpService: LookupService,
    private router: Router,
    translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private blockUiService: BlockUiService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.memberlevelId = params['id'];
      this.memberId = params['id'];
    });
    this.getMemberLevelForm();
    this.getMemberLevelInformation();
    this.getMemberLevelInfomationForm();
    this.memberLevelInRenewalformationForm();
    this.displayLevelInfo();
    this.getTemplates();
  }
  getTemplates() {
    this.lookUpService.getTemplates().subscribe(res => {
      this.templates = res.result.item;
    },
      error => { })
  }
  archiveMembershipLevel() {
    this.levelService.archiveMemberLevel(this.memberId).subscribe(res => {
      this.toastr.success(ToasterMessages.delete('Member Level'), toasterHeadings.Success);
      setTimeout(() => {
        this.router.navigate(['/memberLevels']);
      },1000);  
    },
      error => { this.toastr.error('Member Level already assigned', 'Oops'); })
  }
  getMemberLevelInformation() {
    this.currency  = localStorage.getItem('Currency');
    this.isBlocked = true;
    this.levelService.getMemberLevelById(this.memberlevelId)
      .subscribe(allmemberLevel => {
        this.subscriptionId = allmemberLevel.result.item.subscriptionTypeId;
        this.patchMemberForm(allmemberLevel.result.item);
        this.patchMemberRenewalForm(allmemberLevel.result.item);
        this.memberLevelCounts = allmemberLevel.result.totalRecodeCount;
        if (allmemberLevel.result.totalRecodeCount == 0) {
        }
        this.isBlocked = false;
      },
      error => {
        this.toastr.error('Error in Loading Member Level', 'Oops');
        this.isBlocked = false;
      });
  }
  getMemberLevelInUpdateformation() {
    this.isBlocked = true;
    this.levelService.getMemberLevelById(this.memberId)
      .subscribe(allmemberLevel => {
        this.subscriptionId = allmemberLevel.result.item.subscriptionTypeId;
        this.patchMemberForm(allmemberLevel.result.item);
        if (allmemberLevel.result.totalRecodeCount == 0) {
        }
        this.isBlocked = false;
      },
      error => {
        this.toastr.error('Error in Loading Member Level', 'Oops');
        this.isBlocked = false;
      });
  }
  cancelClicked() {
    this.updateClick = true;
    this.patchMemberForm(this.memberLevel);
  }
  patchMemberForm(memeberLevel: MemberLevelModel) {
    this.memberLevel = memeberLevel;
    let Limited = memeberLevel.isLimited;
    this.memberlevelId = memeberLevel.subscriptionTypeId;
    this.membershipExpired = memeberLevel.membershipExpired;
    this.dateCreated = memeberLevel.subscriptionFrom;
    if (Limited) {
      this.limit = this.limited
    }
    else {
      this.limit = this.unlimited
    }
    this.memberLevelName = memeberLevel.name
    this.memberLevelDescription = memeberLevel.description
    if (this.memberLevelDescription == "") {
      this.memberLevelDescription = this.memberLevelDescription;
    }
    else {
      this.memberLevelDescription = memeberLevel.description;
    }
    this.memberLevelCount = memeberLevel.capacity
    this.getmembershipExpired(this.membershipExpired);
    this.memberLevelForm.patchValue({
      price: memeberLevel.price,
      name: memeberLevel.name,
      description: this.memberLevelDescription,
      capacity: memeberLevel.capacity,
      subscriptionType: this.limit,
      membershipExpired: this.membershipExpired,
      Limited: this.dateRangeRadio,
      subscriptionTo: memeberLevel.subscriptionTo
    })
  }
  patchMemberRenewalForm(memeberLevel: MemberLevelModel) {
    this.memberLevel = memeberLevel;
    if (memeberLevel.isRenewable) {
      this.Renewable = 1;
      this.showEmail = true; 
      this.showEmailtype = true;
    }
    else {
      this.Renewable = 2;
      this.showEmail = false;      
      this.showEmailtype = false;
      
    }
    // if (memeberLevel.isSendEmail) {
    //   this.showEmail = true;
    // }
    // else {
    //   this.showEmail = false;
    // }
    this.memberLevelInrenewalForm.patchValue({
      IsRenewable: this.Renewable,
      RenewalDays: memeberLevel.renewalDays,
      template: memeberLevel.emaillTemplateId,
      IsSendEmail: memeberLevel.isSendEmail,
    })
  }
  getmembershipExpired(membershipExpired) {
    if (membershipExpired == this.membershipLevelState.Annualy) {
      this.membershipExpired = this.membershipLevelState.Annualy;
      this.dateRangeRadio = this.packageV;
      this.limitDisable = true;
    }
    else if (membershipExpired == this.membershipLevelState.Monthly) {
      this.membershipExpired = this.membershipLevelState.Monthly;
      this.dateRangeRadio = this.packageV;
      this.limitDisable = true;
    }
    else if (membershipExpired == this.membershipLevelState.Weekly) {
      this.dateRangeRadio = this.packageV;
      this.membershipExpired = this.membershipLevelState.Weekly;
      this.limitDisable = true;
    }
    else if (membershipExpired == this.membershipLevelState.Daily) {
      this.dateRangeRadio = this.packageV;
      this.membershipExpired = this.membershipLevelState.Daily;
      this.limitDisable = true;
    }
    else if (membershipExpired == this.membershipLevelState.LifeTime) {
      this.dateRangeRadio = this.packageV;
      this.membershipExpired = this.membershipLevelState.LifeTime;
      this.limitDisable = true;
    }
    else if (membershipExpired == this.membershipExpired) {
      this.dateRangeRadio = this.dateV;
      this.limitDisable = false;
    }
  }
  displayLevelInfo() {
    this.isBlocked = true;
    this.memberSubscriptionService.getAllMemberSubscription(this.memberId)
      .subscribe(membersubscription => {
        this.allocatedActiveCount = membersubscription.result.activeCount;
        this.allocatedPendingCount = membersubscription.result.pendingCount;
        this.allocatedInactiveCount = membersubscription.result.inActiveCount;
        this.allocatedTerminatedCount = membersubscription.result.terminatedCount;
        this.isBlocked = false;
      });
  }
  update() {
    if (this.memberLevelCount > this.memberLevelForm.value.capacity) {
      let updateCapacity = true;
      this.toastr.error('Limited Count Should be higher than current Limit ' + this.memberLevelCount + ' Count', 'Oops');
    }
    else {
      var name = this.memberLevelForm.value.name.trim();
      if (name == "") {
        this.toastr.error('Membership Level Name Cannot be Empty', 'Oops');
      }
      else {
        if (!this.memberLevelForm.valid) return;
        this.isBlocked = true;
        this.memberLevelForm.value.isLimited = this.limitDisable;
        var memberlvl = { Name: name, Description: this.memberLevelForm.value.description, id: this.memberlevelId }
        var memberCapacity = Object.assign({}, this.memberLevel, this.memberLevelForm.value)
        this.levelService.updateMemberLevelCapacity(memberCapacity)
          .subscribe();
        this.levelService.updateMemberLevel(memberlvl)
          .subscribe(
          memberLvl => {
            this.toastr.success(ToasterMessages.update('Member Level'), toasterHeadings.Success);
            this.isBlocked = false;
            this.getMemberLevelInUpdateformation();
            this.updateClick = true;
          }, error => {
            this.toastr.error(ToasterMessages.updateError('Member Level'), toasterHeadings.Error);
            this.isBlocked = false;
            this.getMemberLevelForm();
          });
      }
    }
  }
  getMemberLevelForm() {
    this.updateClick = true;
    let priceregx = /\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})?/;
    let numbereregx = /^\d{10}$/;
    this.memberLevelForm = this.fb.group({
      name: [" ", [Validators.required, Validators.maxLength(50)]],
      price: ["0", [Validators.required, patternValidator(priceregx)]],
      description: [" ", [Validators.maxLength(500)]],
      membershipExpired: new FormControl('1'),
      capacity: ["0", [Validators.required]],
      isLimited: [" "],
      Limited: new FormControl('2'),
      subscriptionType: new FormControl('1'),
      subscriptionTo: [new Date(), ""],
      subscriptionFrom: [new Date(), ""],
    }, { validator: this.customValidate }, )
  }
  getMemberLevelInfomationForm() {
    this.isRequested = true;
    this.memberLevelInformationForm = this.fb.group({
      levelStatus: [this.active],
    }, )
  }
  memberLevelInRenewalformationForm() {
    this.memberLevelInrenewalForm = this.fb.group({
      IsRenewable: ['1'],
      RenewalDays: ["1", [Validators.required]],
      template: [null,[Validators.required]],
      IsSendEmail: false
    }, )
  }
  updateSubscriptionRenewal() {
    if (this.memberLevelInrenewalForm.value.IsRenewable == 1) {
      this.isRenewable = true;
    }
    else {
      this.isRenewable = false;
    }
    if (this.membershipExpired == this.membershipLevelState.Annualy) {
      if (this.memberLevelInrenewalForm.value.RenewalDays > 365) {
        return this.toastr.error("Email Remider cannot be more than 365 days");
      }
    }
    else if (this.membershipExpired == this.membershipLevelState.Monthly) {
      if (this.memberLevelInrenewalForm.value.RenewalDays > 31) {
        return this.toastr.error("Email Remider cannot be more than 31 days");
      }
    }
    else if (this.membershipExpired == this.membershipLevelState.Weekly) {
      if (this.memberLevelInrenewalForm.value.RenewalDays > 7) {
        return this.toastr.error("Email Remider cannot be more than 7 days");
      }
    }
    else if (this.membershipExpired == this.membershipLevelState.Daily) {
      if (this.memberLevelInrenewalForm.value.RenewalDays > 1) {
        return this.toastr.error("Email Remider cannot be more than 1 day");
      }
    }
    var memberlvlRenewal = { Id: this.memberId, IsRenewable: this.isRenewable, RenewalDays: this.memberLevelInrenewalForm.value.RenewalDays, isSendEmail: this.showEmail, EmaillTemplateId: this.memberLevelInrenewalForm.value.template }
    if(this.memberLevelInrenewalForm.value.template == null){
      this.toastr.error('Select an Email Template');      
    }else{
    this.levelService.updateMemberLevelRenewal(memberlvlRenewal)
      .subscribe(
      memberLvl => {
        this.toastr.success(ToasterMessages.update('Member Level Renewal'), toasterHeadings.Success);
        this.spinnerService.hide();
        this.getMemberLevelInUpdateformation();
        this.updateClick = true;
      }, error => {
        this.toastr.error(ToasterMessages.updateError('Member Level Renewal'), toasterHeadings.Error);
        this.spinnerService.hide();
        this.getMemberLevelForm();
      });
    }
  }
  sendEmailChange(event) {
    if (event.target.checked) {
      this.showEmail = true;
    }
    else {
      this.showEmail = false;
    }
  }
  sendEmail(event) {
    if (event.target.value == 1) {
      this.showEmailtype = true;
      this.showEmail = true;
    }
    else {
      this.showEmailtype = false;
      this.showEmail = false;
    }
  }
  updateClicked() {
    this.updateClick = false;
  }
  statusChange(event) {
    if (event.target.value == this.active) {
      this.isRequested = true;
      this.showLable = false;
    }

    if (event.target.value == this.deactive) {
      if (this.allocatedActiveCount != 0 || this.allocatedInactiveCount != 0 || this.allocatedTerminatedCount != 0) {
        this.showLable = true;
        this.isRequested = true;
      }
      else {
        this.isRequested = false;
      }
    }
  }
  dateRange(event) {
    if (event.currentTarget.defaultValue == 1) {
      this.limitDisable = false;
    }

    if (event.currentTarget.defaultValue == 2) {
      this.limitDisable = true;
    }
  }

  Createnewtemplate(){
    this.router.navigate(["/communication"]);
  }

}
