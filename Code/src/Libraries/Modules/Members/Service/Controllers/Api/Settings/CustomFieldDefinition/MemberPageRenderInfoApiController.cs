﻿using System;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Controllers;
using Inx.Utility.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Inx.Module.Membership.Service.Controllers.Api.Settings.CustomFieldDefinition
{
    [Route(Constraints.ApiPrefix)]
    public class MemberPageRenderInfoApiController: MembershipBaseApiController
    {
        private readonly IMemberTypeTabFieldsRenderService memberTypeTabFieldsRenderService;
        public MemberPageRenderInfoApiController(IMemberTypeTabFieldsRenderService _memberTypeTabFieldsRenderService,
            IBaseInjector baseinject ) : base(baseinject)
        {
            memberTypeTabFieldsRenderService = _memberTypeTabFieldsRenderService; 
        }

        [HttpGet("settings/membertype/{membertype:int}/wizardrenderinfo")]
        public async Task<IActionResult> MemberWizardRenderInfo(Enums.MemberType membertype)
        {
            try
            {
                return Ok(await memberTypeTabFieldsRenderService.RenderInfo(Request(membertype)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
     
        }
    }
}
