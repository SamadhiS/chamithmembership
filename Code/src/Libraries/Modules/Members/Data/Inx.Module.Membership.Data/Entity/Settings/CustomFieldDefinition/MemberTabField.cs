﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Inx.Module.Identity.Data.Entity.Application;

namespace Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition
{
    [Table("MemberTabField", Schema = Constraints.Schema)]
    public class MemberTabField : AuditableEntity
    {
        public int TabId { get; set; }
        public int FieldId { get; set; }

        #region relations 
        [ForeignKey("FieldId")]
        public MemberFieldDefinition MemberFieldDefinition { get; set; }
        [ForeignKey("TabId")]
        public MemberTabDefinition MemberTabDefinition { get; set; }
        #endregion

        #region notmapped
        [NotMapped, Obsolete]
        public new string Name { get; set; }
        #endregion
    }
}
