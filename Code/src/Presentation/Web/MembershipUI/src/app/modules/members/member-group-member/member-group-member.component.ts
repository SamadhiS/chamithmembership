import { MembershipState } from './../../core/enums/membershipState';
import { MemberType } from './../../core/enums/memberType';
import { GroupService } from './../../core/services/group.service';
import { BlockUiService } from '../../core/services/block-ui.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MembersService } from '../../core/services/members.service';
import { ToasterMessages } from '../../core/utility/toasterMessages';
import { toasterHeadings } from '../../core/utility/toasterHeadings';
import { MemberSearchModel } from '../models/memberSearchModel';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'membership-member-group-member',
  templateUrl: './member-group-member.component.html',
  styleUrls: ['./member-group-member.component.scss']
})
export class MemberGroupMemberComponent implements OnInit {
  groupId: number = 0;
  memberIds: any;
  skip: number = 0;
  take: number = 0;
  orderby: string;
  selectmemberId = 0;
  memberSearch = new MemberSearchModel();
  memberdropdownList = [];
  dataLoaded = false;
  isBlocked = false;
  isRequested = false;
  selected = false;
  selectedItems = [];
  dropdownSettings = {};
  memberCategoryIds = [];
  memberAllDetails = [];
  memberSelectedDetails = [];
  filteredMembers = [];


  select =[];
  selectAll =[];


  constructor(private fb: FormBuilder, public toastr: ToastsManager,
    vcr: ViewContainerRef,
    private blockUiService: BlockUiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    translate: TranslateService,
    private membersService: MembersService,
    private groupService: GroupService) { }

  ngOnInit() {
    this.skip = 0;
    this.take = 0;
    this.orderby = '';
    this.getAllMembers();

    this.activatedRoute.params.subscribe((params: Params) => {
      this.groupId = params['id'];


    });
    this.dropdownSettings = {
      singleSelection: false,
      placeholder: "Search Memebrs to Add...",
      text: "Select Members",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
  }

  onItemSelect(items: any) {
    this.select = items;
    if(this.select !=null)
    {
      this.selected = true;
    }
   }
  OnItemDeSelect(item: any) { 
    this.select = item;
    if(this.select ==null)
    {
      this.selected = false;
    }
  }
  onSelectAll(items: any) { 
    this.selectAll = items;
    if(this.selectAll !=null)
    {
      this.selected = true;
    }
  }
  onDeSelectAll(items: any) {
    this.selected = false;
   }

  getAllMembers() {
    this.memberSearch.status = MembershipState.Terminated;
    this.membersService.getMembers(this.skip, this.take, JSON.stringify(this.memberSearch), this.orderby)
      .subscribe(allmembers => {
        this.memberAllDetails = allmembers.result.item.items;
        if (this.groupId) {
          this.getSelectedMembers();
        }
      },
      error => {
        this.toastr.error('Error in Loading member list', 'Oops');
      }, () => {
      });
  }
  addMembers() {

   this.isRequested =this.selected;
   if( this.isRequested == false)
   {
    this.toastr.error('Select Members!', 'Oops');
    this.isRequested = false; 
    this.selected = false;
    this.isBlocked = false;  
   }
   else{
    this.isBlocked = true;
    let selectedMembers = this.selectedItems.map(item => { return item.id });
    this.memberIds = selectedMembers;
    var membersObj = { memberIds: this.memberIds, groupId: this.groupId }
    if (this.memberIds == "") {
      this.toastr.error('Select Members!', 'Oops');
    this.isBlocked = false;  
    this.isRequested = false;  
    this.selected = false;
    }
    else if (this.groupId == 0) {
      this.toastr.error('No Group Selected!', 'Oops');
    this.isBlocked = false;  
    this.isRequested = false;    
    }
    else {
      this.groupService.AddMembers(membersObj).subscribe(
        data => {
          this.toastr.success(ToasterMessages.save('Members assigned to Group'), toasterHeadings.Success);
          this.isRequested = false;
          this.getSelectedMembers();
          this.selectedItems = [];
          this.memberIds = "";
          this.selected = false;
          this.isBlocked = false;
        },
        error => {
          this.toastr.error(ToasterMessages.saveError('Members assigned to Group'), toasterHeadings.Error);
          this.isBlocked = false;
          this.isRequested = true; 
        })
    }
    this.isBlocked = false;   
    this.isRequested = true;    
   }
 
    
  }

  getSelectedMembers() {
    this.isBlocked = true;
    this.groupService.getMemberDetails(this.groupId)
      .subscribe(memberlist => {
        this.memberSelectedDetails = memberlist.result;
        this.filterExistingMembers();
        if (memberlist.result.item.totalRecodeCount == 0) {
        }
        this.isBlocked = false;
      },
      error => {
        this.toastr.error(ToasterMessages.loadError('Members'), toasterHeadings.Error);
        this.isBlocked = false;
      });
  }
  deleteMember(member) {
    this.isBlocked = true;
    var deleteIndex = this.memberSelectedDetails.indexOf(member);
    this.memberdropdownList.push({ "id": member.id, "itemName": member.name });
    this.selectmemberId = member.id;
    this.membersService.deleteMember(member.id)
      .subscribe(res => {
        this.isBlocked = false;
        this.memberSelectedDetails.splice(deleteIndex, 1);
        this.toastr.success("Member removed Successfully");
        this.getAllMembers();
      },
      error => {
        this.toastr.success(ToasterMessages.deleteError('Member'), toasterHeadings.Error);
        this.isBlocked = false;
      });
  }
  filterExistingMembers() {
    var existingMemberId;
    var existingMemberName;
    this.filteredMembers = [];
    this.memberAllDetails.forEach(mem => {
      existingMemberId = mem.id;
      existingMemberName = mem.name;
      var existingMember = this.memberSelectedDetails.find(data => data.memberId == existingMemberId);
      if (!existingMember) {
        this.filteredMembers.push(mem)
      
      }
      this.isBlocked = false;
    });
    this.memberdropdownList = this.filteredMembers
      .map(item => { return { "id": item.id, "itemName":item.id +": "+ item.name } });
    this.dataLoaded = true;
  

  }


}
