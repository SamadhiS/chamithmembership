﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inx.Module.Payment.Data.Migrations
{
    public partial class Payment_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Inexis.Payments");

            migrationBuilder.CreateTable(
                name: "PaymentMaster",
                schema: "Inexis.Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    MemberId = table.Column<int>(nullable: false),
                    MemberSubscriptionId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    PaymentState = table.Column<byte>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    Remaining = table.Column<decimal>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    TransActionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMaster", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentMaster_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentMaster_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentMaster_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PaymentInfoes",
                schema: "Inexis.Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EditedById = table.Column<int>(nullable: true),
                    EditedOn = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    PaymentDate = table.Column<DateTimeOffset>(nullable: false),
                    PaymentMasterId = table.Column<int>(nullable: false),
                    PaymentType = table.Column<int>(nullable: false),
                    RecordState = table.Column<byte>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentInfoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentInfoes_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentInfoes_Users_EditedById",
                        column: x => x.EditedById,
                        principalSchema: "Inexis.Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentInfoes_PaymentMaster_PaymentMasterId",
                        column: x => x.PaymentMasterId,
                        principalSchema: "Inexis.Payments",
                        principalTable: "PaymentMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentInfoes_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "Inexis.Security",
                        principalTable: "Tenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentInfoes_CreatedById",
                schema: "Inexis.Payments",
                table: "PaymentInfoes",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentInfoes_EditedById",
                schema: "Inexis.Payments",
                table: "PaymentInfoes",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentInfoes_PaymentMasterId",
                schema: "Inexis.Payments",
                table: "PaymentInfoes",
                column: "PaymentMasterId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentInfoes_TenantId",
                schema: "Inexis.Payments",
                table: "PaymentInfoes",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMaster_CreatedById",
                schema: "Inexis.Payments",
                table: "PaymentMaster",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMaster_EditedById",
                schema: "Inexis.Payments",
                table: "PaymentMaster",
                column: "EditedById");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMaster_TenantId",
                schema: "Inexis.Payments",
                table: "PaymentMaster",
                column: "TenantId");
 
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentInfoes",
                schema: "Inexis.Payments");

            migrationBuilder.DropTable(
                name: "PaymentMaster",
                schema: "Inexis.Payments");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "Tenants",
                schema: "Inexis.Security");

            migrationBuilder.DropTable(
                name: "TenantTypes",
                schema: "Inexis.Security");
        }
    }
}
