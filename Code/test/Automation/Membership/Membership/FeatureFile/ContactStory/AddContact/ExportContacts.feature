﻿Feature: As an Admin, I should be able to Export Contact Information

@mytag
Scenario: As an Admin, I should be able to Export Contact Information PDF format
	Given Load membership Home Page
	Then I have entered "membership.inexis@gmail.com" and "Admin@123"
	And I Clicked on Submit
	And I Directed to Dashboard 
	And I click on cotact page
	And I click on export button
	And I click on Pdf
	And I unselect Profile Image
	And I unselect Created On
	And I unselect Identity
	And I unselect DOB
	And I unselect Address
	And I unselect City
	And I unselect Company
	And I unselect Designation
	And I click Generate Report
	And I clicked on logout


	Scenario: As an Admin, I should be able to Export Contact Information CSV format
	Given Load membership Home Page
	Then I have entered "membership.inexis@gmail.com" and "Admin@123"
	And I Clicked on Submit
	And I Directed to Dashboard 
	And I click on cotact page
	And I click on export button
	And I click on Csv
	And I unselect Profile Image
	And I unselect Created On
	And I unselect Identity
	And I unselect DOB
	And I unselect Address
	And I unselect City
	And I unselect Company
	And I unselect Designation
	And I click Generate Report
	And I clicked on logout