﻿namespace Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition
{
    public class MemberTabFieldBo
    {
        public int TabId { get; set; }
        public int FieldId { get; set; }
    }
}
