﻿using Inx.Utility.Models;
using System.Collections.Generic;
using static Inx.Utility.Utility.Enums;

namespace Inx.Module.Messenger.Core.Bo.Emails
{
    public class EmailSendingInfoModel: BaseBo
    {
        
        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public EmailSendTypes EmailSendType { get; set; }
        public MembershipStatus? MemberStatusId { get; set; }
        public int? MemberLevelId { get; set; }
        public int? MemberId { get; set; }
        public int? TemplateId { get; set; }
        public string ToAddress { get; set; }
        public List<int> MemberIds { get; set; }
    }
}
