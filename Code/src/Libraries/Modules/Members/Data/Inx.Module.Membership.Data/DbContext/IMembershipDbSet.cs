﻿using Inx.Module.Membership.Data.Entity.MemberGroup;
using Inx.Module.Membership.Data.Entity.Members;
using Inx.Module.Membership.Data.Entity.Members.Master;
using Inx.Module.Membership.Data.Entity.Settings;
using Inx.Module.Membership.Data.Entity.Settings.CustomFieldDefinition;
using Inx.Module.Membership.Data.Entity.Subscription;
using Microsoft.EntityFrameworkCore;

namespace Inx.Module.Membership.Data.DbContext
{
    public interface IMembershipDbSet
    {
        DbSet<Member> Member { get; set; }
        DbSet<OrganizationInfo> OrganizationInfo { get; set; }
        DbSet<PersonalInfo> PersonalInfo { get; set; }
        DbSet<MemberFieldKeyValue> MemberFieldKeyValue { get; set; }

        DbSet<SubscriptionType> SubscriptionType { get; set; }
        DbSet<Subscription> Subscription { get; set; }
        DbSet<MemberFile> MemberFile { get; set; }
        DbSet<MemberSubscription> MemberSubscription { get; set; }
        DbSet<Group> MembershipGroup { get; set; } 
        DbSet<MemberGroup> MemberGroup { get; set; }
        DbSet<MembershipTimeline> MembershipTimeline { get; set; }

        //definitions
        DbSet<MemberTabDefinition> MemberTabDefinition { get; set; }
        DbSet<MemberFieldDefinition> MemberFieldDefinition { get; set; }
        DbSet<MemberTypeTab> MemberTypeTab { get; set; }
        DbSet<MemberTabField> MemberTabField { get; set; }
        DbSet<MemberFieldRequired> MemberFieldRequired { get; set; }
    }
}
    
