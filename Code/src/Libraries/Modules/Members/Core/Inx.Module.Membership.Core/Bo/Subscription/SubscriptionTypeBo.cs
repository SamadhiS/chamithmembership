﻿using Inx.Utility.Models;

namespace Inx.Module.Membership.Core.Bo.Subscription
{
    public class SubscriptionTypeBo : BaseBo
    {
        public string Description { get; set; }
    }
}
