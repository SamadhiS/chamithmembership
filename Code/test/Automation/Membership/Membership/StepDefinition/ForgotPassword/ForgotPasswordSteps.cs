﻿using Membership.PageObjects;
using Membership.PageObjects.CategoryPO;
using Membership.PageObjects.ContactPO;
using Membership.PageObjects.ForgotPasswordPO;
using Membership.PageObjects.NavPage;
using Membership.Pages.LoginPage;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Protractor;
using System.Threading;
using TechTalk.SpecFlow;


namespace Membership.StepDefinition.ForgotPassword
{
    [Binding]
    public sealed class ForgotPasswordSteps
    {
        private static NgWebDriver ngWebDriver = new NgWebDriver(new ChromeDriver());
        private LoginPage loginPage = new LoginPage(ngWebDriver);
        private ForgotPasswordPO forgotPasswordPO = new ForgotPasswordPO();
        private NavPage navPage = new NavPage();
        private LoginPO loginPO = new LoginPO();

        [Given(@"Load Membership login")]
        public void GivenLoadMembershipLogin()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I click on Reset Here")]
        public void GivenIClickOnResetHere()
        {
            forgotPasswordPO.Btn_ResetHere_Click(ngWebDriver);
        }

        [Given(@"I enter email as (.*)")]
        public void GivenIEnterEmailAsMembership_InexisGmail_Com(string p0)
        {
            forgotPasswordPO.Email(ngWebDriver).SendKeys(p0);
        }

        [Given(@"I click on Reset Password")]
        public void GivenIClickOnResetPassword()
        {
            forgotPasswordPO.Btn_ResetPassword_Click(ngWebDriver);
        }

        //Back to login
        [Given(@"Load Membership login page")]
        public void GivenLoadMembershipLoginPage()
        {
            loginPage.LoginPageLoad(ngWebDriver);
        }

        [Given(@"I click on Reset here")]
        public void GivenIClickedOnResetHere()
        {
            forgotPasswordPO.Btn_ResetHere_Click(ngWebDriver);
        }

        [Given(@"I entered email as (.*)")]
        public void GivenIEnteredEmailAsMembership_InexisGmail_Com(string p0)
        {
            forgotPasswordPO.Email(ngWebDriver).SendKeys(p0);
            Thread.Sleep(3000);
        }

        [Given(@"I clicked on Reset Password")]
        public void GivenIClickedOnResetPassword()
        {
            forgotPasswordPO.Btn_ResetPassword_Click(ngWebDriver);
            Thread.Sleep(5000);
        }

        [Given(@"click on Back button")]
        public void GivenClickOnBackButton()
        {
            forgotPasswordPO.Btn_Back_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Given(@"click on back to login button")]
        public void GivenClickOnBackToLoginButton()
        {
            forgotPasswordPO.Btn_BackToLogin_Click(ngWebDriver);
            Thread.Sleep(4000);
        }

        [Given(@"Entered valid crefentials ""(.*)"" and ""(.*)""")]
        public void GivenEnteredValidCrefentialsAnd(string p0, string p1)
        {
            loginPage.login(p0, p1);
        }

        [Given(@"click on Sign In button")]
        public void GivenClickOnSignInButton()
        {
            loginPO.Btn_SignIn_Click(ngWebDriver);
            Thread.Sleep(2000);
        }
        [Given(@"navigated to dashboard")]
        public void GivenNavigatedToDashboard()
        {
            navPage.Load_Dashboard();
            Thread.Sleep(4000);
        }

        [Given(@"click on signout button")]
        public void GivenClickOnSignoutButton()
        {
            loginPO.Btn_Logout_Click(ngWebDriver);
        }

    }
}
