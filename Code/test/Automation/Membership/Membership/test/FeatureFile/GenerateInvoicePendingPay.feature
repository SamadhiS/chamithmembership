﻿Feature: GenerateInvoicePendingPay
	As an Admin, I should be able generate invoice for pending payments

@mytag
Scenario: As an Admin, I should be able generate invoice for pending payments
	Given System Application opens 
	And Enter <UN> and <PWD>
	And click on Login 
	And Navigate to Payments 
	And Select Pending Payment 
	And Click on the View 
	And Click ON Print INvoice
	And Print Window Opens   
	And Click on Print 
	And Log Out. 
 Examples:
| UN							| PWD		| 
| membership.inexis@gmail.com	| Admin@123 |

Scenario: As an Admin, I should NOT be able to generate invoice for paid payments
	Given System Application opens 
	And Enter <UN> and <PWD>
	And click on Login 
	And Navigate to Payments 
	And Select Paid Payment 
	And Click on the View 
	And Print Invoice is not visible 
	And Move to Payments section again 
	And Log Out. 
 Examples:
| UN							| PWD		| 
| membership.inexis@gmail.com	| Admin@123 |