﻿using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Settings.CustomFieldDefinition;
using Inx.Utility.Models;
using Inx.Utility.Utility;
namespace Inx.Module.Membership.Core.Service.Settings.CustomFieldDefinition.Interface
{
    public interface IMemberTypeTabFieldsRenderService
    {
        Task<MemberTypeTabFieldsRenderViewResult> RenderInfo(Request<Enums.MemberType> request);
    }
}
