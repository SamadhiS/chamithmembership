﻿using System;
using System.Threading.Tasks;
using Inx.Module.Membership.Core.Bo.Subscription;
using Inx.Module.Membership.Core.Service.Subscription.Interfaces;
using Inx.Module.Membership.Service.Models.Subscription;
using Inx.Module.Membership.Service.Utility;
using Inx.Service.Base.Attributes;
using Inx.Service.Base.Controllers;
using Microsoft.AspNetCore.Mvc;
using Enums = Inx.Utility.Utility.Enums;

namespace Inx.Module.Membership.Service.Controllers.Api.Subscription
{
    [Route(Constraints.ApiPrefix), AuthorizeRoles(Enums.Roles.SuperAdmin, Enums.Roles.TenantAdmin)]
    public class SubscriptionApiController : MembershipBaseApiController, IBaseApi<SubscriptionViewModel, int>
    {
        private readonly ISubscriptionService service;

        public SubscriptionApiController(ISubscriptionService _service, IBaseInjector baseinject) : base(baseinject)
        {
            service = _service;
        }

        [HttpPost, Route("subscription"), ModelValidation]
        public async Task<IActionResult> Create([FromBody] SubscriptionViewModel item)
        {
            return await Create<SubscriptionViewModel, SubscriptionBo, ISubscriptionService>(item, service);
        }

        [HttpDelete, Route("subscription/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                return await Delete<SubscriptionBo, ISubscriptionService>(id, service);
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("subscription/{id:int}")]
        public async Task<IActionResult> Read(int id)
        {
            return await Read<SubscriptionViewModel, SubscriptionBo, ISubscriptionService>(id, service);
        }

        [HttpGet, Route("subscription")]
        public async Task<IActionResult> Read(int skip = 0, int take = 0, string search = null, string orderby = null)
        {
            try
            {
                return Ok(await service.Read(SearchRequest()));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("subscription/keyvalue")]
        public async Task<IActionResult> ReadKeyValue()
        {
            try
            {
                return Ok(await service.ReadKeyValue(SearchRequest()));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpGet, Route("subscriptions/{id:int}/members")]
        public async Task<IActionResult> GetSubscribedMembers(int id)
        {
            try
            {
                return Ok(await service.GetSubscriptions(Request(id)));
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }

        }

        [HttpPut, Route("subscriptionRenewal"), ModelValidation]
        public async Task<IActionResult> UpdateRenewal([FromBody] SubscriptionRenewalViewModel item)
        {
            try
            {
                await service.UpdateRenewal(Request<SubscriptionRenewalViewModel, SubscriptionRenewalBo>(item));
                return Ok();
            }
            catch (Exception e)
            {
                return await HandleException(e);
            }
        }

        [HttpPut, Route("subscription"), ModelValidation, ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Update([FromBody] SubscriptionViewModel item)
        {
            return await Update<SubscriptionViewModel, SubscriptionBo, ISubscriptionService>(item, service);
        }

    }
}
