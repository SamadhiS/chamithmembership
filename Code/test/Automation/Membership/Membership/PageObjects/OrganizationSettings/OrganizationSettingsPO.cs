﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Protractor;

namespace Membership.PageObjects.OrganizationSettings
{
    class OrganizationSettingsPO
    {
        private NgWebElement Btn_ImageBrowser(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.CssSelector("body > membership-root > membership-layout > div > div > div > membership-account-settings > div > div > div > div > section > div > div > div.col-md-9 > div > form > div > div.col-md-3 > div > label"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ImageBrowser_Click(NgWebDriver d)
        {
            this.Btn_ImageBrowser(d).Click();
        }
        private NgWebElement Btn_ImageUpload(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("BtnImageUpload"));
            ScrollToElem(elem, d);
            return elem;
        }
        public void Btn_ImageUpload_Click(NgWebDriver d)
        {
            this.Btn_ImageUpload(d).Click();
        }
        public NgWebElement Email(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtEmail"));
        }
        public NgWebElement Phone(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtPhone"));
        }
        public NgWebElement Click_Country(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddCountry")).FindElement(By.CssSelector("#ddCountry > option:nth-child(56)"));

        }
        public NgWebElement Click_Currency(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddcurrency")).FindElement(By.CssSelector("#ddcurrency > option:nth-child(2)"));

        }
        
        public NgWebElement Fax(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtFax"));
        }
        public NgWebElement Country(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddCountry"));
        }
        public NgWebElement Prefix(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtprefix"));
        }
        public NgWebElement Address(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtAddress"));
        }
        public NgWebElement State(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtState"));
        }
        public NgWebElement City(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtCity"));
        }

        public NgWebElement Zip(NgWebDriver d)
        {
            return d.FindElement(By.Id("txtZip"));
        }
        public NgWebElement Language(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddlanguage")).FindElement(By.CssSelector("#ddlanguage > option:nth-child(2)"));

        }
        public NgWebElement Click_Language(NgWebDriver d)
        {
            return d.FindElement(By.Id("ddlanguage"));
        }
        public void Btn_Update_Click(NgWebDriver d)
        {
            this.Btn_Update(d).Click();
        }
        private NgWebElement Btn_Update(NgWebDriver d)
        {
            NgWebElement elem = d.FindElement(By.Id("btnUpdateTenantInfo"));
            ScrollToElem(elem, d);
            return elem;
        }
        private void ScrollToElem(NgWebElement elem, NgWebDriver d)
        {
            Actions actions = new Actions(d);
            actions.MoveToElement(elem);
            actions.Perform();
        }
    }
}
