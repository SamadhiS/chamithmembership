﻿using Inx.Module.Membership.Core.Bo.Dashboard;
using Inx.Utility.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Inx.Module.Membership.Core.Service.Dashboard.Interface
{
    public interface IDashboardService
    {
        Task<DashboardGraphBo> DashboardGraph(Request<Bo.Enums.DateRangeFilter> request);
    }
}
