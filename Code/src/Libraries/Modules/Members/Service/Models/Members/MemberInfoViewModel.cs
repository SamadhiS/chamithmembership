﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Data.Base.Utility;
using Inx.Service.Base.Models;
using Inx.Utility.Utility;
using Inx.Module.Membership.Data;
namespace Inx.Module.Membership.Service.Models.Members
{
    public class MemberInfoViewModel : BaseViewModel
    {
        [Required, StringLength(DbConstraints.NameLength)]
        public string FirstName { get; set; }
        [Required, StringLength(DbConstraints.NameLength)]
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Website { get; set; }
        public bool IsaMember { get; set; }
        public Enums.MartialStatus MartialStatus { get; set; }
        [Required, StringLength(Constraints.IdentityLength)]
        public string Identity { get; set; }
        public bool IsProfileImageChanged { get; set; } = false;
        public string ProfileImage { get; set; } = Constants.ProfileNoImage;
        [Obsolete("first name and last name included")]
        public new string Name { get; set; }
    }
}
